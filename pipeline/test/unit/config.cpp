#define BOOST_TEST_MODULE pipeline.Config
#define BOOST_TEST_DYN_LINK
#include <sstream>
#include <fstream>
#include <boost/test/unit_test.hpp>
#include "platform_config.cpp"

const char rpi_config [] = R"__(
[test]
video_sink = shmvideosink
audio_sink = fakesink
[default]
video_sink = es2eglvideosink
video_caps = video/x-h264,stream-format=avc
)__";
const char config_file [] = "/tmp/test.pipeline.config";

BOOST_AUTO_TEST_CASE(fallbak) {
    auto platform = vw::PlatformConfig::create("");
    BOOST_CHECK(platform);
    const auto & config = platform->get_config("test");
    BOOST_CHECK_EQUAL(config.video.sink, "fakesink");
    BOOST_CHECK_EQUAL(config.video.caps, "");
    BOOST_CHECK_EQUAL(config.audio.sink, "fakesink");
    BOOST_CHECK_EQUAL(config.audio.caps, "");
}

BOOST_AUTO_TEST_CASE(stream) {
    std::stringstream stream(rpi_config);
    auto platform = vw::PlatformConfig::create(stream);
    BOOST_CHECK(platform);
    const auto & tc = platform->get_config("test");
    BOOST_CHECK_EQUAL(tc.video.sink, "shmvideosink");
    BOOST_CHECK_EQUAL(tc.video.caps, "");
    BOOST_CHECK_EQUAL(tc.audio.sink, "fakesink");
    BOOST_CHECK_EQUAL(tc.audio.caps, "");
    const auto & fc = platform->get_config("file");
    BOOST_CHECK_EQUAL(fc.video.sink, "es2eglvideosink");
    BOOST_CHECK_EQUAL(fc.video.caps, "video/x-h264,stream-format=avc");
    BOOST_CHECK_EQUAL(fc.audio.sink, "fakesink");
    BOOST_CHECK_EQUAL(fc.audio.caps, "");
}

BOOST_AUTO_TEST_CASE(file) {
    unlink(config_file);
    std::ofstream stream(config_file, std::ofstream::out);
    stream << rpi_config;
    stream.close();
    auto platform = vw::PlatformConfig::create(config_file);
    BOOST_CHECK(platform);
    const auto & tc = platform->get_config("test");
    BOOST_CHECK_EQUAL(tc.video.sink, "shmvideosink");
    BOOST_CHECK_EQUAL(tc.video.caps, "");
    BOOST_CHECK_EQUAL(tc.audio.sink, "fakesink");
    BOOST_CHECK_EQUAL(tc.audio.caps, "");
    const auto & fc = platform->get_config("file");
    BOOST_CHECK_EQUAL(fc.video.sink, "es2eglvideosink");
    BOOST_CHECK_EQUAL(fc.video.caps, "video/x-h264,stream-format=avc");
    BOOST_CHECK_EQUAL(fc.audio.sink, "fakesink");
    BOOST_CHECK_EQUAL(fc.audio.caps, "");
}
