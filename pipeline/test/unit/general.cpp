#define BOOST_TEST_MODULE pipeline.General
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/program_options.hpp>
#include <unistd.h>
// TODO: make static lib instead
#include "platform_config.cpp"
#include "pipeline.cpp"

struct Config {
    static std::string uri;
    static std::string dot_dump_dir;
    static std::string config_file;
    static size_t num_iterations;
    Config() {
        auto & mts = boost::unit_test::framework::master_test_suite();
        parse_command_line(mts.argc, mts.argv);
        if (!dot_dump_dir.empty())
            setenv("GST_DEBUG_DUMP_DOT_DIR", dot_dump_dir.c_str(), 1);
        setenv("GST_DEBUG", "*:3", 1);
        vw::Pipeline::set_platform_config(config_file);
        gst_init(&mts.argc, &mts.argv);
    }
    void parse_command_line(int argc, char ** argv) {
        namespace po = boost::program_options;
        po::options_description opt("Usage");
        opt.add_options()
                ("info", "print this help message and exit")
                ("uri", po::value<std::string>(&uri)->
                 default_value("test://"), "set media uri")
                ("dot-dir", po::value<std::string>(&dot_dump_dir)->
                 default_value(""), "set gst dump dot dir")
                ("config", po::value<std::string>(&config_file)->
                 default_value("/dev/null"), "set pipeline config file")
                ("iterations", po::value<size_t>(&num_iterations)->
                 default_value(1), "set number of iterations");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, opt), vm);
        if (vm.count("info")) {
            std::cout << opt << std::endl;
            exit(0);
        }
        try {
            po::notify(vm);
        } catch( const std::exception & e) {
            std::cerr << e.what() << std::endl;
            std::cout << opt << std::endl;
            exit(-1);
        }
    }
};

std::string Config::uri;
std::string Config::dot_dump_dir;
std::string Config::config_file;
size_t Config::num_iterations;

BOOST_GLOBAL_FIXTURE(Config);

struct PipelineFixture {
    PipelineFixture() : pipeline(""), position(0.), duration(0.) {
        pipeline.set_duration_handler([this](double d){ duration = d; });
        pipeline.set_position_handler([this](double p){ position = p; });
        pipeline.set_video_info_handler([this](const vw::VideoInfo & vi){
            video_info = vi;
        });
    }
    vw::Pipeline pipeline;
    vw::VideoInfo video_info;
    double duration;
    double position;
};

void report(size_t iteration) {
    TRACE << "running " << boost::unit_test::framework::current_test_case().p_name
          << " # " << iteration << " of " << Config::num_iterations << "...";
}

BOOST_AUTO_TEST_CASE(load) {
    size_t iteration = 0;
    while(Config::num_iterations > iteration++) {
        report(iteration);
        vw::Pipeline pipeline("");
        pipeline.set_duration_handler([](double duration){
            TRACE << "got duration : " << duration;
        });
        pipeline.set_position_handler([](double position){
            TRACE << "got position : " << position;
        });
        pipeline.set_video_info_handler([](const vw::VideoInfo & vi){
            TRACE << "got video-info: " << vi.width << "x" << vi.height << " "
                  << vi.framerate.numerator << "/" << vi.framerate.denominator << " fps";
        });
        pipeline.set_eos_handler([](){
            TRACE << "got eof event";
        });
        BOOST_CHECK(pipeline.load(Config::uri));
    }
    BOOST_CHECK(!g_main_context_pending(g_main_context_default()));
}

BOOST_FIXTURE_TEST_CASE(position_reporting, PipelineFixture) {
    size_t iteration = 0;
    while(Config::num_iterations > iteration++) {
        report(iteration);
        BOOST_CHECK(pipeline.load(Config::uri));
        BOOST_CHECK(pipeline.play());
        // wait for 1000 ms
        while (position < 1000.) {
            usleep(10000);
        }
        BOOST_CHECK_GE(position, 200.);
        BOOST_CHECK_GE(video_info.width, 0);
        BOOST_CHECK_GE(video_info.height, 0);
        BOOST_CHECK_LE(video_info.width, 2000);
        BOOST_CHECK_LE(video_info.height, 2000);
    }
    BOOST_CHECK(!g_main_context_pending(g_main_context_default()));
}

BOOST_FIXTURE_TEST_CASE(seeking, PipelineFixture) {
    size_t iteration = 0;
    while(Config::num_iterations > iteration++) {
        report(iteration);
        BOOST_CHECK(pipeline.load(Config::uri));
        BOOST_CHECK(pipeline.seek(100));
        BOOST_CHECK_EQUAL(position, 100.);
    }
    BOOST_CHECK(!g_main_context_pending(g_main_context_default()));
}
