#include <iostream>
#include <cassert>
#include <map>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <log/log.h>
#include "platform_config.h"
#include "pipeline.h"

#define BAD_STREAM_POSITION -1.
#define UPDATE_INTERVAL 200

namespace vw {

std::unique_ptr<PlatformConfig> platform;
void Pipeline::set_platform_config(const std::string &config) {
    platform = PlatformConfig::create(config);
}

static GstCaps * get_video_caps(GstElement * pipeline) {
    GstIterator * it = gst_bin_iterate_sinks(GST_BIN(pipeline));
    GValue ival = G_VALUE_INIT;
    GstCaps * vcaps = nullptr;
    while(GST_ITERATOR_OK == gst_iterator_next(it, &ival)) {
        GstElement * esink = GST_ELEMENT(g_value_get_object(&ival));
        GstIterator * sit = gst_element_iterate_sink_pads(esink);
        GValue sival = G_VALUE_INIT;
        gst_iterator_next(sit, &sival);
        gst_iterator_free(sit);
        GstPad * psink = GST_PAD(g_value_get_object(&sival));
        vcaps = gst_pad_get_current_caps(psink);
        gst_object_unref(psink);
        gst_object_unref(esink);
        GstStructure * s = gst_caps_get_structure(vcaps, 0);
        std::string name = gst_structure_get_name(s);
        if (0 == name.find("video"))
            break;
    }
    gst_iterator_free(it);
    return vcaps;
}

void Pipeline::on_eos(GstBus *, GstMessage * message, gpointer context) {
    Pipeline * pipeline = static_cast<Pipeline *>(context);
    if (message->src == GST_OBJECT(pipeline->pipeline)) {
        TRACE << "eos reached";
        if (pipeline->eos_signal)
            pipeline->eos_signal();
    }
}

void Pipeline::on_state_chaged(GstBus *, GstMessage * message, gpointer context) {
    Pipeline * pipeline = static_cast<Pipeline *>(context);
    std::lock_guard<std::mutex> _(pipeline->pipeline_mutex);
    GstState old_state, new_state;
    gst_message_parse_state_changed(message, &old_state, &new_state, nullptr);
    if (message->src == GST_OBJECT(pipeline->pipeline)) {
        if (new_state == GST_STATE_PAUSED && old_state == GST_STATE_READY) {
            TRACE << "load completed";
            GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS
                    (GST_BIN(pipeline->pipeline), GST_DEBUG_GRAPH_SHOW_ALL, "pipeline");

            GstCaps * video_caps = get_video_caps(pipeline->pipeline);
            GstStructure * s = gst_caps_get_structure(video_caps, 0);
            VideoInfo video_info;
            gst_structure_get_int(s, "width", &video_info.width);
            gst_structure_get_int(s, "height", &video_info.height);
            gst_structure_get_fraction(s, "framerate", &video_info.framerate.numerator,
                                       &video_info.framerate.denominator);
            gst_caps_unref(video_caps);
            TRACE << "video-info : " << video_info.width << "x" << video_info.height
                  << " " << video_info.framerate.numerator
                  << "/" << video_info.framerate.denominator << " fps";
            if (pipeline->video_info_signal)
                pipeline->video_info_signal(video_info);
            gint64 duration;
            gst_element_query_duration(pipeline->pipeline, GST_FORMAT_TIME, &duration);
            TRACE << "stream-duration : " << duration * 1.e-6;
            if (pipeline->duration_signal)
                pipeline->duration_signal(duration * 1.e-6);
            auto update_position = [] (gpointer context)->gboolean {
                Pipeline * pipeline = static_cast<Pipeline *>(context);
                return pipeline->update_position();
            };
            update_position(pipeline);
            pipeline->timer_id = g_timeout_add(UPDATE_INTERVAL, update_position, pipeline);
        } else if (new_state == GST_STATE_NULL) {
            TRACE << "unload completed";
        }
    }
}

void Pipeline::on_tag(GstBus *, GstMessage * message, gpointer) {
    GstTagList * tags = nullptr;
    gst_message_parse_tag(message, &tags);
    TRACE << "got tag signal from " << GST_OBJECT_NAME(message->src);
    auto log_one_tag = [](const GstTagList * list, const gchar * tag, gpointer){
        int num = gst_tag_list_get_tag_size(list, tag);
        for (int t = 0; t < num; ++t) {
            const GValue * value = gst_tag_list_get_value_index (list, tag, t);
            if (G_VALUE_HOLDS_STRING(value))
                TRACE << tag << " : " << g_value_get_string(value);
            else if (G_VALUE_HOLDS_UINT(value))
                TRACE << tag << " : " << g_value_get_uint(value);
            else
                TRACE << tag << " : tag of type : " << G_VALUE_TYPE_NAME(value);
        }
    };
    gst_tag_list_foreach(tags, log_one_tag, nullptr);
    gst_tag_list_unref(tags);
}

Pipeline::Pipeline(const std::string & _id)
    : id(_id), pipeline(nullptr), timer_id(0)
    , last_reported_position(BAD_STREAM_POSITION) {
    if (id.empty()) {
        boost::uuids::uuid uuid = boost::uuids::random_generator()();
        id = boost::uuids::to_string(uuid);
    }
    main_loop = g_main_loop_new(g_main_context_default(), false);
    bus_thread = std::thread([this](){
        TRACE << "starting bus thread...";
        g_main_loop_run(main_loop);
        TRACE << "stopping bus thread...";
    });
}

Pipeline::~Pipeline() {
    unload();
    while(!g_main_loop_is_running(main_loop))
        std::this_thread::yield();
    g_main_loop_quit(main_loop);
    bus_thread.join();
    g_main_loop_unref(main_loop);
}
// TODO: remove asserts
bool Pipeline::load(const std::string & uri) {
    unload();
    const std::string test_protocol = "test";
    gchar * protocol = gst_uri_get_protocol(uri.c_str());
    // Fallback to global defaults
    if(!platform)
        platform = PlatformConfig::create("/dev/null");
    const auto & config = platform->get_config(protocol);

    auto make_test_pipeline = [this, &config] () {
        std::string launch = "videotestsrc ! " + config.video.sink + " uuid=" + id;
        INFO << "pipeline: " << launch;
        pipeline = gst_parse_launch(launch.c_str(), nullptr);
        assert(pipeline);
    };
    auto make_uri_pipeline = [this, &config] (const std::string & uri) {
        std::string launch = "uridecodebin uri=" + uri + " ";
        if (!config.video.caps.empty())
            launch += "caps=" + config.video.caps + " ";
        launch += "buffer=40960 ! " + config.video.sink + " uuid=" + id;
        INFO << "pipeline: " << launch;
        pipeline = gst_parse_launch(launch.c_str(), nullptr);
        assert(pipeline);
    };

    if (test_protocol == protocol)
        make_test_pipeline();
    else
        make_uri_pipeline(uri);

    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
    assert(bus);
    gst_bus_add_signal_watch(bus);
    auto connection = g_signal_connect(bus, "message::eos", G_CALLBACK(on_eos), this);
    assert(connection > 0);
    connection = g_signal_connect(bus, "message::tag", G_CALLBACK(on_tag), this);
    assert(connection > 0);
    connection = g_signal_connect(bus, "message::state-changed", G_CALLBACK(on_state_chaged), this);
    assert(connection > 0);
    return pause();
}

bool Pipeline::unload() {
    std::lock_guard<std::mutex> _(pipeline_mutex);
    auto result = GST_STATE_CHANGE_FAILURE;
    if(pipeline) {
        last_reported_position = BAD_STREAM_POSITION;
        if (timer_id) {
            g_source_remove(timer_id);
            timer_id = 0;
        }
        result = gst_element_set_state(pipeline, GST_STATE_NULL);
        gst_object_unref(pipeline);
        // TODO: should we wait on transition completion?
        pipeline = nullptr;
        gst_bus_remove_signal_watch(bus);
        gst_object_unref(bus);
    }
    return result == GST_STATE_CHANGE_SUCCESS;
}

bool Pipeline::play() {
    GstState current, pending;
    gst_element_set_state(pipeline, GST_STATE_PLAYING);
    return GST_STATE_CHANGE_SUCCESS ==
            gst_element_get_state(pipeline, &current, &pending, GST_CLOCK_TIME_NONE);
}

bool Pipeline::pause() {
    GstState current, pending;
    gst_element_set_state(pipeline, GST_STATE_PAUSED);
    return GST_STATE_CHANGE_SUCCESS ==
            gst_element_get_state(pipeline, &current, &pending, GST_CLOCK_TIME_NONE);
}

bool Pipeline::seek(double pos_ms) {
    auto result = gst_element_seek_simple(pipeline, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH,
                                          gint64(pos_ms) * GST_MSECOND);
    GstState current, pending;
    gst_element_get_state(pipeline, &current, &pending, GST_CLOCK_TIME_NONE);
    update_position();
    return result;
}

void Pipeline::set_video_info_handler(video_info_handler_t && handler) {
    video_info_signal = handler;
}

void Pipeline::set_duration_handler(position_handler_t && handler) {
    duration_signal = handler;
}

void Pipeline::set_position_handler(position_handler_t && handler) {
    position_signal = handler;
}

void Pipeline::set_eos_handler(eos_handler_t && handler) {
    eos_signal = handler;
}

bool Pipeline::update_position() {
    gint64 position;
    gst_element_query_position(pipeline, GST_FORMAT_TIME, &position);
    if (position_signal && last_reported_position != position) {
        last_reported_position = position;
        position_signal(position * 1.e-6);
    }
    return true;
}

}
