#include <memory>

#include <boost/program_options.hpp>

#include <io/server.h>
#include <rpc/dispatcher.h>
#include <rpc/protocol.h>

#include <io/stream/server.h>
#include <rpc/json-rpc/protocol.h>

#include "pipeline.h"

using namespace vw::io;
using namespace vw::rpc;

typedef boost::asio::io_service io_service_t;
typedef Server<io_service_t, stream::LocalServer> server_t;
typedef Proto<json::Proto> proto_t;
typedef Dispatcher<proto_t, server_t::socket_t> dispatcher_t;

const std::string proto_name = "local://";
const std::string socket_base = "/tmp/vw.pipeline.";

int main(int argc, char *argv[]) {
	namespace po = boost::program_options;

    std::string id;
	po::options_description opt("Usage");
	opt.add_options()
			("help,h", "print this help message")
            ("id,i", po::value<std::string>(&id)->required(), "set pipeline id")
            ("config,c", po::value<std::string>()->default_value("/etc/vw/pipeline.conf"), "config file");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, opt), vm);
	if (vm.count("help")) {
		std::cout << opt << std::endl;
		return 0;
	}

    try {
        po::notify(vm);
    } catch( const std::exception & e) {
        std::cerr << e.what() << std::endl;
        std::cout << opt << std::endl;
        return -1;
    }

    vw::Pipeline::set_platform_config(vm["config"].as<std::string>());

	gst_init(&argc, &argv);

	io_service_t io_service;
	boost::asio::signal_set signals(io_service, SIGINT, SIGTERM);
	signals.async_wait(std::bind(&boost::asio::io_service::stop, &io_service));
	server_t server(io_service, proto_name + socket_base + id);

    vw::Pipeline pipeline(id);

	std::shared_ptr<dispatcher_t> dispatcher;
	std::shared_ptr<server_t::socket_t> socket;

	server.set_connection_handler([&](server_t::socket_t && s){
		socket.reset(new server_t::socket_t(std::move(s)));
		dispatcher.reset(new dispatcher_t(*socket));

		dispatcher->register_handler( "load",   &pipeline, &vw::Pipeline::load         );
		dispatcher->register_handler( "unload", &pipeline, &vw::Pipeline::unload       );
		dispatcher->register_handler( "play",   &pipeline, &vw::Pipeline::play         );
		dispatcher->register_handler( "pause",  &pipeline, &vw::Pipeline::pause        );
		dispatcher->register_handler( "seek",   &pipeline, &vw::Pipeline::seek         );
		dispatcher->register_handler( "exit",   [&](){ io_service.stop(); return true; } );

		pipeline.set_video_info_handler([&] (const vw::VideoInfo & video_info) {
			io_service.dispatch([=](){
				dispatcher->notify("video-info", video_info.width, video_info.height,
								   video_info.framerate.numerator,
								   video_info.framerate.denominator);
			});
		});
		pipeline.set_duration_handler([&] (double duration) {
			io_service.dispatch([=](){
				dispatcher->notify("duration", duration);
			});
		});
		pipeline.set_position_handler([&] (double position) {
			io_service.dispatch([=](){
				dispatcher->notify("position", position);
			});
		});
		pipeline.set_eos_handler([&] () {
			io_service.dispatch([=](){
				dispatcher->notify("eos");
			});
		});
	});

	io_service.run();

	return 0;
}
