#ifndef __WV_PIPELINE_H__
#define __WV_PIPELINE_H__

#include <string>
#include <functional>
#include <thread>
#include <memory>
#include <future>
#include <mutex>
#include <map>
#include <gst/gst.h>

namespace vw {

struct Fraction {
	int numerator;
	int denominator;
};

struct VideoInfo {
	int width;
	int height;
	Fraction framerate;
};

class Pipeline {
	static void on_eos(GstBus *, GstMessage *, gpointer);
	static void on_tag(GstBus *, GstMessage *, gpointer);
	static void on_state_chaged(GstBus *, GstMessage *, gpointer);

public:
	typedef std::function<void(const VideoInfo &)> video_info_handler_t;
	typedef std::function<void(double)> position_handler_t;
	typedef std::function<void()> eos_handler_t;

    static void set_platform_config(const std::string & config);

    Pipeline(const std::string & _id);
	~Pipeline();

	bool load(const std::string & uri);
	bool unload();
	bool play();
	bool pause();
	bool seek(double pos_ms);

	void set_video_info_handler(video_info_handler_t && handler);
	void set_duration_handler(position_handler_t && handler);
	void set_position_handler(position_handler_t && handler);
	void set_eos_handler(eos_handler_t && handler);

private:
	std::string id;

	// signals:
	video_info_handler_t video_info_signal;
	position_handler_t duration_signal;
	position_handler_t position_signal;
	eos_handler_t eos_signal;

	// utils
	bool update_position();

	gint64 last_reported_position;
	guint timer_id;

	GstElement * pipeline;
	std::mutex pipeline_mutex;
	std::thread bus_thread;
	GMainLoop * main_loop;
	GstBus * bus;
};

}

#endif //__WV_PIPELINE_H__
