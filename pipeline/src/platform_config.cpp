#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <log/log.h>

#include "platform_config.h"

namespace vw {

namespace pt = boost::property_tree;
typedef vw::PipelineConfig config_t;
typedef std::map<std::string, config_t> config_map_t;

void parse_config(const pt::ptree & props, config_map_t & platform_config, config_t & default_config) {
    default_config = {
        { props.get<std::string>("default.video_sink", "fakesink"), props.get<std::string>("default.video_caps", "") },
        { props.get<std::string>("default.audio_sink", "fakesink"), props.get<std::string>("default.audio_caps", "") }
    };
    for (const auto & proto : props) {
        const auto & key = proto.first;
        if (key != "default") {
            platform_config.emplace(key, config_t {
                { props.get<std::string>(key + ".video_sink", ""), props.get<std::string>(key + ".video_caps", "") },
                { props.get<std::string>(key + ".audio_sink", ""), props.get<std::string>(key + ".audio_caps", "") }
            });
        }
    }
}

std::unique_ptr<PlatformConfig> PlatformConfig::create(const std::string & config_file) {
    pt::ptree props;
    try {
        /**
         * # format example:
         * [default]
         * video_sink = es2eglvideosink
         * video_caps = video/x-h264,stream-format=avc
         * [test]
         * video_sink = shmvideosink
         * audio_sink = fakesink
         */
        pt::read_ini(config_file, props);
    } catch(const std::exception & e) {
        ERROR << "Failed to parse config (" << config_file << "): " << e.what();
    }

    config_map_t platform_config;
    config_t default_config;
    parse_config(props, platform_config, default_config);

    return std::unique_ptr<PlatformConfig>(new PlatformConfig(platform_config, default_config));
}

std::unique_ptr<PlatformConfig> PlatformConfig::create(std::istream & stream) {
    pt::ptree props;
    try {
        pt::read_ini(stream, props);
    } catch(const std::exception & e) {
        ERROR << "Failed to parse config (...): " << e.what();
    }

    config_map_t platform_config;
    config_t default_config;
    parse_config(props, platform_config, default_config);

    return std::unique_ptr<PlatformConfig>(new PlatformConfig(platform_config, default_config));
}

const PipelineConfig & PlatformConfig::get_config(const std::string & protocol) const {
    auto it = config_map.find(protocol);
    return it == config_map.end() ? default_config : it->second;
}

}
