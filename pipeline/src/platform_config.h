#ifndef __WV_PLATFORM_CONFIG_H__
#define __WV_PLATFORM_CONFIG_H__

#include <string>
#include <map>
#include <memory>
#include <iostream>

namespace vw {

struct PipelineConfig {
    struct SinkConfig {
        std::string sink;
        std::string caps;
    };
    SinkConfig video;
    SinkConfig audio;
};

struct PlatformConfig {
    static std::unique_ptr<PlatformConfig> create(const std::string & config_file);
    static std::unique_ptr<PlatformConfig> create(std::istream & stream);
    const PipelineConfig & get_config(const std::string & protocol) const;
private:
    PlatformConfig(const std::map<std::string, PipelineConfig> & cmap, const PipelineConfig & defc)
        : config_map(cmap), default_config(defc) {}
    const std::map<std::string, PipelineConfig> config_map;
    const PipelineConfig default_config;
};

}

#endif // __WV_PLATFORM_CONFIG_H__
