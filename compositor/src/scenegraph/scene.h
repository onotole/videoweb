#ifndef __COMPOSITOR_SCENE_H__
#define __COMPOSITOR_SCENE_H__

#include <map>
#include <memory>
#include <mutex>
#include <vector>
#include <gfx/render_context/irender_context.h>
#include <gfx/gl/shader_manager.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/texture.h>
#include <gfx/gl/canvas.h>

#include "config.h"
#include "actor.h"

namespace vw { namespace comp {

class Scene {
public:
    Scene(gfx::rc::IRenderContext * ctx);
    void render();
    void resize(int w, int h);

    bool add_source(const std::string & id);
    bool rem_source(const std::string & id);
    bool update_source(const std::string & id);
    bool map_fd(const std::string & id, int remote, int local);

    bool set_z_order(const std::string & id, size_t z);
    bool get_z_order(const std::string & id, size_t & z) const;
    bool set_crop_rect(const std::string & id, const base::rect_t & rect);
    bool get_crop_rect(const std::string & id, base::rect_t & rect) const;
    bool set_crop_rect_n(const std::string & id, const base::n_rect_t & rect);
    bool get_crop_rect_n(const std::string & id, base::n_rect_t & rect) const;
    bool set_out_rect(const std::string & id, const base::rect_t & rect);
    bool get_out_rect(const std::string & id, base::rect_t & rect) const;
    bool set_out_rect_n(const std::string & id, const base::n_rect_t & rect);
    bool get_out_rect_n(const std::string & id, base::n_rect_t & rect) const;

private:
    gfx::rc::IRenderContext * _render_context;
    base::rect_t _viewport;
    gfx::gl::Canvas _canvas;
    std::unique_ptr<gfx::gl::ShaderManager> _shader;
    mutable std::mutex _mutex;
    std::map<std::string, Actor> _sources;
    std::vector<std::pair<std::unique_ptr<gfx::gl::Mesh>,
                          std::unique_ptr<gfx::gl::Texture>>> _scrap;
};

}}


#endif // __COMPOSITOR_SCENE_H__
