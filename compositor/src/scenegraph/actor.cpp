#include "actor.h"

namespace vw { namespace comp {

Actor::Actor(gfx::rc::IRenderContext * ctx, const gfx::gl::ShaderManager * shader, const std::string & id)
    : _render_context(ctx), _shader(shader), source(id), texture(new gfx::gl::Texture) {}

void Actor::set_z_order(size_t z) {
    z_order = z;
}

size_t Actor::get_z_order() const {
    return z_order;
}

void Actor::set_crop_rect(const base::rect_t & rect) {
    float dw = 1.f / texture->width();
    float dh = 1.f / texture->height();
    mesh->set_uv_bounds({dw * rect.x, dh * rect.y, dw * rect.w, dh * rect.h});
}

base::rect_t Actor::get_crop_rect() const {
    auto w = texture->width();
    auto h = texture->height();
    const auto & nr = mesh->get_uv_bounds();
    return {int(w * nr.x), int(h * nr.y), int(w * nr.w), int(h * nr.h)};
}

void Actor::set_crop_rect_n(const base::n_rect_t & rect) {
    mesh->set_uv_bounds(rect);
}

const base::n_rect_t & Actor::get_crop_rect_n() const {
    return mesh->get_uv_bounds();
}

void Actor::set_out_rect(const base::rect_t & rect) {
    frame.set_viewport(rect);
}

const base::rect_t & Actor::get_out_rect() const {
    return frame.get_viewport();
}

void Actor::update() {
    update_pending = true;
}

void Actor::render() {
    frame.set();
    if (update_pending) {
        const auto * image = source.image();
        _shader->use(image->header.memory);
        auto egl_image = _render_context->load_image(*image);
        if (egl_image) texture->fill(*egl_image);
        _render_context->release_image(egl_image);
        update_pending = false;
    }
    if (!mesh)
        mesh.reset(new gfx::gl::Mesh());
    mesh->render();
}

void Actor::map_fd(int remote, int local) {
    source.map_fd(remote, local);
}

std::pair<std::unique_ptr<gfx::gl::Mesh>, std::unique_ptr<gfx::gl::Texture>> Actor::recycle() {
    return std::make_pair(std::move(mesh), std::move(texture));
}

}}
