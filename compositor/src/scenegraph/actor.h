#ifndef __COMPOSITOR_ACTOR_H__
#define __COMPOSITOR_ACTOR_H__

#include <gfx/render_context/irender_context.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/texture.h>
#include <gfx/gl/shader_manager.h>
#include "source.h"

namespace vw { namespace comp {

class Actor {
public:
    Actor(gfx::rc::IRenderContext * ctx, const gfx::gl::ShaderManager * shader, const std::string & id);

    void set_z_order(size_t z);
    size_t get_z_order() const;
    void set_crop_rect(const base::rect_t & rect);
    base::rect_t get_crop_rect() const;
    void set_crop_rect_n(const base::n_rect_t & rect);
    const base::n_rect_t & get_crop_rect_n() const;
    void set_out_rect(const base::rect_t & rect);
    const base::rect_t & get_out_rect() const;

    void update();
    void render();
    void map_fd(int remote, int local);
    std::pair<std::unique_ptr<gfx::gl::Mesh>, std::unique_ptr<gfx::gl::Texture>> recycle();
private:
    size_t z_order = 0;
    Source source;
    bool update_pending = false;

    gfx::rc::IRenderContext * _render_context;
    const gfx::gl::ShaderManager * _shader;
    gfx::gl::Frame frame;
    std::unique_ptr<gfx::gl::Mesh> mesh;
    std::unique_ptr<gfx::gl::Texture> texture;
};


}}

#endif //__COMPOSITOR_ACTOR_H__
