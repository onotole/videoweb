#include "scene.h"

namespace vw { namespace comp {

Scene::Scene(gfx::rc::IRenderContext * ctx) : _render_context(ctx) {}

void Scene::render() {
    std::lock_guard<std::mutex> _(_mutex);
    _scrap.clear();
    _canvas.clear();
    for (auto & source : _sources) {
        source.second.render();
    }
}

void Scene::resize(int w, int h) {
    _canvas.set_background(1.f, .5f, .5f, .5f);
    _shader.reset(new gfx::gl::ShaderManager);
    _viewport = {0, 0, w, h};
}

bool Scene::add_source(const std::string & id) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.emplace(std::make_pair(id, Actor(_render_context, _shader.get(), id)));
    it.first->second.set_out_rect(_viewport);
    it.first->second.update();
    return true;
}

bool Scene::rem_source(const std::string & id) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()){
        _scrap.push_back(it->second.recycle());
        _sources.erase(it);
        return true;
    }
    return false;
}

bool Scene::update_source(const std::string & id) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        it->second.update();
        return true;
    }
    return false;
}

bool Scene::map_fd(const std::string & id, int remote, int local) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        it->second.map_fd(remote, local);
        return true;
    }
    return false;
}

bool Scene::set_z_order(const std::string & id, size_t z) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        it->second.set_z_order(z);
        return true;
    }
    return false;
}

bool Scene::get_z_order(const std::string & id, size_t & z) const {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        z = it->second.get_z_order();
        return true;
    }
    return false;
}

bool Scene::set_crop_rect(const std::string & id, const base::rect_t & rect) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        it->second.set_crop_rect(rect);
        return true;
    }
    return false;
}

bool Scene::get_crop_rect(const std::string & id, base::rect_t & rect) const {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        rect = it->second.get_crop_rect();
        return true;
    }
    return false;
}

bool Scene::set_crop_rect_n(const std::string & id, const base::n_rect_t & rect) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        it->second.set_crop_rect_n(rect);
        return true;
    }
    return false;
}

bool Scene::get_crop_rect_n(const std::string & id, base::n_rect_t & rect) const {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        rect = it->second.get_crop_rect_n();
        return true;
    }
    return false;
}

bool Scene::set_out_rect(const std::string & id, const base::rect_t & rect) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        it->second.set_out_rect({rect.x, _viewport.h - rect.h - rect.y, rect.w, rect.h});
        return true;
    }
    return false;
}

bool Scene::get_out_rect(const std::string & id, base::rect_t & rect) const {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        rect = it->second.get_out_rect();
        return true;
    }
    return false;
}

bool Scene::set_out_rect_n(const std::string & id, const base::n_rect_t & rect) {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        auto w = _viewport.w;
        auto h = _viewport.h;
        it->second.set_out_rect({int(w * rect.x), int(h * rect.y), int(w * rect.w), int(h * rect.h)});
        return true;
    }
    return false;
}

bool Scene::get_out_rect_n(const std::string & id, base::n_rect_t & rect) const {
    std::lock_guard<std::mutex> _(_mutex);
    auto it = _sources.find(id);
    if (it != _sources.end()) {
        const auto & r = it->second.get_out_rect();
        auto dw = 1.f / _viewport.w;
        auto dh = 1.f / _viewport.h;
        rect = {dw * r.x, dh * r.y, dw * r.w, dh * r.h};
        return true;
    }
    return false;
}


}}
