#ifndef __COMPOSITOR_SOURCE_H__
#define __COMPOSITOR_SOURCE_H__

#include <functional>
#include <unordered_set>
#include <thread>
#include <memory>
#include <gfx/ipc/import.h>

namespace vw { namespace comp {

class Source {
public:
    Source(const std::string & id);
    const std::string & id() const;
    const gfx::Image * image() const;
    void map_fd(int remote, int local);

private:
    gfx::ipc::Import _import;
};

}} // namespace vw::comp

#endif // __COMPOSITOR_SOURCE_H__
