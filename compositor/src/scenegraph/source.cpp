#include <memory>
#include <sstream>

#include "source.h"

namespace vw { namespace comp {

Source::Source(const std::string & id) : _import(id) {}

const std::string & Source::id() const {
    return _import.uuid();
}

const gfx::Image * Source::image() const {
    return _import.load();
}

void Source::map_fd(int remote, int local) {
    _import.map_fd(remote, local);
}

}} // namespace vw::comp
