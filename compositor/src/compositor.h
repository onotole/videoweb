#ifndef __COMPOSITOR_COMPOSITOR_H__
#define __COMPOSITOR_COMPOSITOR_H__

#include <list>
#include <memory>
#include <algorithm>

#include <io/server.h>
#include <io/stream/server.h>
#include <rpc/dispatcher.h>
#include <rpc/protocol.h>
#include <rpc/json-rpc/protocol.h>

#include "scenegraph/scene.h"

namespace vw { namespace comp {

template<typename io_service_t, typename transport_t>
class Compositor {
    typedef io::Server<io_service_t, transport_t> server_t;
    typedef rpc::Proto<rpc::json::Proto> proto_t;
    typedef rpc::Dispatcher<proto_t, typename server_t::socket_t> dispatcher_t;
    typedef std::unique_ptr<dispatcher_t> dispatcher_ptr_t;
    typedef std::unique_ptr<typename server_t::socket_t> socket_ptr_t;
    typedef struct Connection {
        Connection(typename server_t::socket_t && s)
            : socket(new typename server_t::socket_t(std::move(s)))
            , dispatcher(new dispatcher_t(*socket)) {}
        std::string uuid;
        socket_ptr_t socket;
        dispatcher_ptr_t dispatcher;
    } connection_t;
public:
    Compositor(Scene & scene, io_service_t & io_service, const std::string & address)
        : _scene(scene), _server(io_service, address) {
        _server.set_connection_handler([this](typename server_t::socket_t && s) {
            _connections.push_back({std::move(s)});
            auto & conn = _connections.back();
            // source management
            conn.dispatcher->register_handler("add_source", [&](const std::string & uuid) {
                conn.uuid = uuid;
                _scene.add_source(uuid);
                return true;
            });
            conn.dispatcher->register_handler("rem_source", [&](const std::string & uuid) {
                _scene.rem_source(uuid);
                return true;
            });
            conn.dispatcher->register_handler("update", [&](const std::string & uuid) {
                _scene.update_source(uuid);
                return true;
            });
            conn.dispatcher->register_handler("fd", [&](int remote, int local) {
                _scene.map_fd(conn.uuid, remote, local);
                return true;
            });
            // source layout
            conn.dispatcher->register_handler("set_z_order", [&](const std::string & uuid, size_t z) {
                _scene.set_z_order(uuid, z);
                return true;
            });
            conn.dispatcher->register_handler("set_crop_rect", [&](const std::string & uuid,
                                              const base::rect_t & rect) {
                _scene.set_crop_rect(uuid, rect);
                base::n_rect_t nrect;
                _scene.get_crop_rect_n(uuid, nrect);
                conn.dispatcher->notify("crop_rect_n", nrect);
                return true;
            });
            conn.dispatcher->register_handler("set_crop_rect_n", [&](const std::string & uuid,
                                              const base::n_rect_t & nrect) {
                _scene.set_crop_rect_n(uuid, nrect);
                base::rect_t rect;
                _scene.get_crop_rect(uuid, rect);
                conn.dispatcher->notify("crop_rect", rect);
                return true;
            });
            conn.dispatcher->register_handler("set_out_rect", [&](const std::string & uuid,
                                              const base::rect_t & rect) {
                _scene.set_out_rect(uuid, rect);
                base::n_rect_t nrect;
                _scene.get_out_rect_n(uuid, nrect);
                conn.dispatcher->notify("out_rect_n", nrect);
                return true;
            });
            conn.dispatcher->register_handler("set_out_rect_n", [&](const std::string & uuid,
                                              const base::n_rect_t & nrect) {
                _scene.set_out_rect_n(uuid, nrect);
                base::rect_t rect;
                _scene.get_out_rect(uuid, rect);
                conn.dispatcher->notify("out_rect", rect);
                return true;
            });
            conn.socket->set_disconnect_handler([&]() {
                auto it = std::find_if(_connections.begin(), _connections.end(),
                                       [&conn](const connection_t & c) {
                    return &conn.socket == &c.socket;
                });
                if (it != _connections.end()) {
                    _scene.rem_source(it->uuid);
                    _connections.erase(it);
                }
            });
        });
    }

    void notify_rendered() {
        for (auto & conn : _connections)
            conn.dispatcher->notify("rendered");
    }

private:
    server_t _server;
    std::list<connection_t> _connections;
    Scene & _scene;
};

}} // namespace vw::comp


#endif // __COMPOSITOR_COMPOSITOR_H__
