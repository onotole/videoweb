#include "pulse.h"

namespace vw { namespace comp {

const boost::int64_t render_interval = 1000000 / 30;

Pulse::Pulse(boost::asio::io_service &io_service) : timer(io_service) {
    timer.expires_from_now(boost::posix_time::microseconds(render_interval));
    timer.async_wait(std::bind(&Pulse::timeout, this));
}

void Pulse::set_callback(IPulse::callback_t && callback) {
    pulse_callback = callback;
}

void Pulse::timeout() {
    if (pulse_callback)
        pulse_callback();
    timer.expires_at(timer.expires_at() + boost::posix_time::microseconds(render_interval));
    timer.async_wait(std::bind(&Pulse::timeout, this));
}

}}
