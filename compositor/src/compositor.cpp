#include <iostream>
#include <fstream>
#include <thread>
#include <boost/program_options.hpp>

#include "config.h"

#include <gfx/render_context/render_context.h>
#include "pulse.h"
#include "compositor.h"

typedef boost::asio::io_service io_service_t;
typedef vw::io::stream::LocalServer local_server_t;
typedef vw::io::stream::TcpServer tcp_server_t;
typedef vw::comp::Compositor<io_service_t, local_server_t> local_compositor_t;
typedef vw::comp::Compositor<io_service_t, tcp_server_t> tcp_compositor_t;

namespace po = boost::program_options;

int parse_config(int argc, char *argv[], po::variables_map & vm) {
    po::options_description generic("Generic Options");
    generic.add_options()
            ("help,h", "print out help message");

    po::options_description config("Configuration");
    config.add_options()
            ("local,l", po::value<std::string>()->default_value("local:///tmp/vw.compositor"),
             "local socket address")
            ("tcp,t", po::value<std::string>()->default_value("tcp://:7211"),
             "tcp socket address");

    po::options_description cmd_line_options;
    cmd_line_options.add(generic).add(config);

    po::options_description config_file_options;
    config_file_options.add(config);

    try {
        po::store(po::parse_command_line(argc, argv, cmd_line_options), vm);
    } catch( const std::exception & e) {
        std::cerr << e.what() << std::endl;
        std::cout << cmd_line_options << std::endl;
        return -1;
    }

    if (vm.count("help")) {
        std::cout << cmd_line_options << std::endl;
        exit(0);
    }

    std::ifstream cfg_file(COMPOSITOR_CONFIG_FILE);
    if (cfg_file.good()) {
        try {
            po::store(po::parse_config_file(cfg_file, config_file_options), vm);
        } catch( const std::exception & e) {
            std::cerr << e.what() << std::endl;
            std::cout << cmd_line_options << std::endl;
            return -1;
        }
    }

    try {
        po::notify(vm);
    } catch( const std::exception & e) {
        std::cerr << e.what() << std::endl;
        std::cout << cmd_line_options << std::endl;
        return -1;
    }

    return 0;
}

using namespace boost;

int main(int argc, char *argv[]) {
    po::variables_map vm;
    int error = parse_config(argc, argv, vm);
    if (error)
        return error;

    io_service_t io_service;
    auto context = vw::gfx::rc::create_render_context();
    vw::comp::Scene scene(context.get());

    boost::asio::signal_set term_signals(io_service, SIGINT, SIGTERM);
    term_signals.async_wait(std::bind(&vw::gfx::rc::IRenderContext::stop, context.get()));

    std::unique_ptr<local_compositor_t> local_compositor
            (new local_compositor_t(scene, io_service, vm["local"].as<std::string>()));
    std::unique_ptr<tcp_compositor_t> tcp_compositor
            (new tcp_compositor_t(scene, io_service, vm["tcp"].as<std::string>()));

    std::thread ctrl_thread([&io_service](){ io_service.run(); });

    std::unique_ptr<vw::comp::Pulse> up_pulse;
    auto pulse = context->get_pulse();
    if (!pulse) {
        up_pulse.reset(new vw::comp::Pulse(io_service));
        pulse = up_pulse.get();
    }
    pulse->set_callback([&context]() {
        context->update();
    });
    context->set_resize_callback([&scene](int w, int h) {
        scene.resize(w, h);
    });
    context->set_update_callback([&scene, &context, &local_compositor, &io_service] {
        scene.render();
        context->swap_buffers();
        io_service.post([&local_compositor] { local_compositor->notify_rendered(); });
    });

    context->exec();

    io_service.stop();
    ctrl_thread.join();

    return 0;
}
