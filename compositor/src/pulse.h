#ifndef __COMPOSITOR_PULSE_H__
#define __COMPOSITOR_PULSE_H__

#include <boost/asio.hpp>
#include <functional>
#include <gfx/render_context/ipulse.h>

namespace vw { namespace comp {

class Pulse : public gfx::rc::IPulse {
public:
	Pulse(boost::asio::io_service & io_service);
	virtual void set_callback(IPulse::callback_t &&) override;
private:
	void timeout();
	boost::asio::deadline_timer timer;
	std::function<void()> pulse_callback;
};

}}

#endif
