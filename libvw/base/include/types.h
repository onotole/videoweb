#ifndef __VW_BASE_TYPES_H__
#define __VW_BASE_TYPES_H__

#include <boost/fusion/adapted.hpp>

namespace vw { namespace base {

template <typename T> struct Rect { T x, y, w, h; };
typedef Rect<int> rect_t;
typedef Rect<float> n_rect_t;

namespace keys {
struct x; struct y;	struct w; struct h;
}

}}

BOOST_FUSION_ADAPT_ASSOC_STRUCT(vw::base::rect_t,
        (int, x, vw::base::keys::x)
        (int, y, vw::base::keys::y)
        (int, w, vw::base::keys::w)
        (int, h, vw::base::keys::h))
BOOST_FUSION_ADAPT_ASSOC_STRUCT(vw::base::n_rect_t,
        (float, x, vw::base::keys::x)
        (float, y, vw::base::keys::y)
        (float, w, vw::base::keys::w)
        (float, h, vw::base::keys::h))

#endif //__VW_BASE_TYPES_H__
