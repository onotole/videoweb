#ifndef __BASE_REF_COUNTER_H__
#define __BASE_REF_COUNTER_H__

#include <atomic>

namespace vw { namespace base {

struct ref_counter {
	virtual ~ref_counter() {}
	mutable std::atomic<size_t> _ref_count;
};

inline void intrusive_ptr_add_ref(ref_counter * p) {
	std::atomic_fetch_add_explicit(&p->_ref_count, size_t(1), std::memory_order_relaxed);
}

inline void intrusive_ptr_release(ref_counter * p) {
	if (std::atomic_fetch_sub_explicit (&p->_ref_count, size_t(1), std::memory_order_release) == 1) {
		std::atomic_thread_fence(std::memory_order_acquire);
		delete p;
	}
}

}} // namespace vw::base

#endif // __BASE_REF_COUNTER_H__
