#ifndef __BASE_UTIL_DEBUG_HELPERS_H__
#define __BASE_UTIL_DEBUG_HELPERS_H__

#include <iostream>
#include <cxxabi.h>

namespace vw { namespace base { namespace debug {

// compile time metafunctions

// print (via compile error) deduced template param type
template<typename T> struct deduced_type;
template<typename T> void print_type(T&&) { deduced_type<T>::show; }

// runtime utilities

// print variable type
template<typename T>
typename std::enable_if< ! std::is_void<T>::value >::type
print_value(std::ostream & s, const T & val) {
    s << "<" << abi::__cxa_demangle(typeid(val).name(), nullptr, nullptr, nullptr)
             << std::boolalpha << ">(" << val << ")";
}
void print_value(std::ostream & s) {
    s << "<void>()";
}

struct ObjectSpy {
    ObjectSpy() : tag(this), gen(0) {
        print(std::cout, "default ctor");
    }
    ~ObjectSpy() {
        print(std::cout, "dtor");
    }
    ObjectSpy(const ObjectSpy & o) : tag(o.tag), gen(o.gen + 1) {
        print(std::cout, "copy ctor");
    }
    ObjectSpy(ObjectSpy && o) : tag(o.tag), gen(o.gen + 1) {
        print(std::cout, "move ctor");
    }
    ObjectSpy & operator = (const ObjectSpy &) {
        print(std::cout, "copy assignment");
		return *this;
    }
    ObjectSpy & operator = (ObjectSpy &&) {
        print(std::cout, "move assignment");
		return *this;
    }
private:
    void print(std::ostream & s, const char * stage) {
        s << tag << "(" << gen << "):" << stage << std::endl;
    }
    void * tag;
    size_t gen;
};

}}} // namespace vw::base::debug

#endif // __BASE_UTIL_DEBUG_HELPERS_H__
