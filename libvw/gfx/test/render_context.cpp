#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <thread>
#include <cmath>

#include <render_context.h>
#include <gl.h>
#include <canvas.h>

class Painter {
public:
    Painter() : _counter(0) {}
    void init(int w, int h) {
        std::cout << "=============================================================================" << std::endl;
        std::cout << "SURFACE_SIZE\t\t\t| "            << w << "x" << h                              << std::endl;
        std::cout << "GL_VENDOR\t\t\t| "               << vw::gfx::gl::vendor()                      << std::endl;
        std::cout << "GL_RENDERER\t\t\t| "             << vw::gfx::gl::renderer()                    << std::endl;
        std::cout << "GL_VERSION\t\t\t| "              << vw::gfx::gl::version()                     << std::endl;
        std::cout << "GL_SHADING_LANGUAGE_VERSION\t| " << vw::gfx::gl::sl_version()                  << std::endl;
        std::cout << "GL_EXTENSIONS\t\t\t| "           << vw::gfx::gl::extensions()                  << std::endl;
        std::cout << "=============================================================================" << std::endl;
    }
    void paint() {
        float r(.010f * _counter), g(.012f * _counter), b(.015f * _counter++), i;
        _canvas.set_background(1.f, std::modf(r, &i), std::modf(g, &i), std::modf(b, &i));
        _canvas.clear();
    }
private:
    vw::gfx::gl::Canvas _canvas;
    size_t _counter;
};

class Pulse : public vw::gfx::rc::IPulse {
public:
    Pulse(const bool & run) :
        _run(run), _pulse_thread([this]() {
            usleep(20000);
            while (_run) {
                if (_callback)
                    _callback();
                usleep(1000000/60);
            }
        })
    {}
    virtual ~Pulse() {
        if (_pulse_thread.joinable())
            _pulse_thread.join();
    }
    virtual void set_callback(callback_t && cb) override {
        _callback = cb;
    }
private:
    const bool & _run;
    std::thread  _pulse_thread;
    callback_t   _callback;
};

namespace ph = std::placeholders;
int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " <path to drm device>" << std::endl;
        return -1;
    }
    int drm_fd = open(argv[1], O_RDWR);
    auto context = vw::gfx::rc::create_render_context(drm_fd);
    Painter painter;
    context->set_resize_callback(std::bind(&Painter::init, painter, ph::_1, ph::_2));
    context->set_update_callback([&](){ painter.paint(); context->swap_buffers(); });
    bool run = true;
    auto pulse = context->get_pulse();
    if (!pulse) {
        pulse = new Pulse(run);
    }
    pulse->set_callback([&context](){
        context->update();
    });

    context->exec();
    run = false;

    close(drm_fd);

    return 0;
}
