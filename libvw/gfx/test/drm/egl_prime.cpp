#include <iostream>
#undef NDEBUG
#include <cassert>
#include <cstring>
#include <atomic>
#include <string>
#include <set>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include <xf86drm.h>
#include <xf86drmMode.h>
#include <drm_fourcc.h>

#include <gbm.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

const int DPY_W = 800;
const int DPY_H = 600;

int drm_fd = -1;
drmModeModeInfoPtr drm_mode = nullptr;
uint drm_crtc = uint(-1);
uint drm_conn = uint(-1);
uint drm_fb = 0;

int prime_fd = -1;
uint prime_stride = 0;
uint prime_size = 0;
uint prime_hdl = 0;

gbm_device * gbm_dev = nullptr;
gbm_surface * gbm_surf = nullptr;
gbm_bo * surf_bo = nullptr;

EGLDisplay egl_dpy = nullptr;
EGLContext egl_ctx = nullptr;
EGLSurface egl_surf = nullptr;

GLuint gl_program = -1;
GLuint gl_vertices = -1;
GLuint gl_indices = -1;
GLuint gl_texture = -1;

std::string drm_device_path;

void send_fd(int sock, int fd) {
    struct msghdr msg;
    struct iovec iov;
    union {
        struct cmsghdr  cmsghdr;
        char control[CMSG_SPACE(sizeof(int))];
    } cmsgu;
    struct cmsghdr  *cmsg;

    char c = '*';
    iov.iov_base = &c;
    iov.iov_len = sizeof(c);

    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    msg.msg_control = cmsgu.control;
    msg.msg_controllen = sizeof(cmsgu.control);

    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_len = CMSG_LEN(sizeof (int));
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;

    printf ("[send_fd]: passing fd %d\n", fd);
    *((int *) CMSG_DATA(cmsg)) = fd;

    auto size = sendmsg(sock, &msg, 0);
    printf ("[send_fd]: sendmsg = %d\n", size);
}

int recv_fd(int sock) {
    struct msghdr msg;
    struct iovec iov;
    union {
        struct cmsghdr cmsghdr;
        char control[CMSG_SPACE(sizeof (int))];
    } cmsgu;
    struct cmsghdr  *cmsg;

    char c;
    iov.iov_base = &c;
    iov.iov_len = sizeof(c);

    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = cmsgu.control;
    msg.msg_controllen = sizeof(cmsgu.control);
    auto size = recvmsg (sock, &msg, 0);
    printf("[recv_fd]: recvmsg = %d\n", size);
    if (size < 0)
        return -1;

    cmsg = CMSG_FIRSTHDR(&msg);
    if (cmsg && cmsg->cmsg_len == CMSG_LEN(sizeof(int))) {
        int fd = *((int *) CMSG_DATA(cmsg));
        printf ("[recv_fd]: received fd %d\n", fd);
        return fd;
    }
    return -1;
}

void open_drm() {
    std::cout << "enter open_drm()" << std::endl;
    drm_fd = open(drm_device_path.c_str(), O_RDWR);
    if (drm_fd == -1) {
        std::cerr << "failed to open drm device: " << strerror(errno) << std::endl;
        assert(0);
    }
    std::cout << "exit open_drm()" << std::endl;
}

void print_resources(drmModeRes * resources) {
    for (int i = 0; i < resources->count_connectors; ++i) {
        drmModeConnector * connector = drmModeGetConnector(drm_fd, resources->connectors[i]);
        std::cerr << "connector = " << connector->connector_id
                  << ", encoder = " << connector->encoder_id
                  << ", connection = " << connector->connection
                  << ", type = " << connector->connector_type
                  << ", encoders = [ ";
        for (int e = 0; e < connector->count_encoders; ++e)
            std::cerr << connector->encoders[e] << " ";
        std::cerr << "], modes = [ ";
        std::set<std::string> modes;
        for (int m = 0; m < connector->count_modes; ++m) {
            std::string mode = connector->modes[m].name;
            if (modes.count(mode)) continue;
            modes.insert(mode);
            std::cerr << mode << " ";
        }
        std::cerr << "]" << std::endl;
        drmModeFreeConnector(connector);
    }
    for (int i = 0; i < resources->count_encoders; ++i) {
        drmModeEncoder * encoder = drmModeGetEncoder(drm_fd, resources->encoders[i]);
        std::cerr << "encoder = " << encoder->encoder_id
                  << ", crtc = " << encoder->crtc_id
                  << ", crtcs = " << std::hex << encoder->possible_crtcs << std::dec
                  << ", type = " << encoder->encoder_type << std::endl;
        drmModeFreeEncoder(encoder);
    }
    for (int i = 0; i < resources->count_crtcs; ++i) {
        drmModeCrtc * crtc = drmModeGetCrtc(drm_fd, resources->crtcs[i]);
        std::cerr << "crtc = " << crtc->crtc_id
                  << ", mode = " << crtc->mode.name << std::endl;
        drmModeFreeCrtc(crtc);
    }
}

void init_drm() {
    std::cout << "enter init_drm()" << std::endl;
    if (drm_fd == -1)
        open_drm();
    drmSetMaster(drm_fd);
    drmModeRes * resources = drmModeGetResources(drm_fd);
    if (!resources) {
        std::cerr << "failed to get resources: " << strerror(errno) << std::endl;
        assert(0);
    }
    print_resources(resources);
    drmModeConnector * connector = nullptr;
    for (int i = 0; i < resources->count_connectors; ++i) {
        connector = drmModeGetConnector(drm_fd, resources->connectors[i]);
        if (connector->connection == DRM_MODE_CONNECTED &&
           (connector->connector_type == DRM_MODE_CONNECTOR_HDMIA ||
            connector->connector_type == DRM_MODE_CONNECTOR_HDMIB ))
            break;
        drmModeFreeConnector(connector);
        connector = nullptr;
    }
    if (!connector) {
        std::cerr << "failed to find connected connector" << std::endl;
        assert(0);
    }
    for (int i = 0; i < connector->count_modes; ++i) {
        drmModeModeInfo * mode = &connector->modes[i];
        if (mode->hdisplay == DPY_W && mode->vdisplay == DPY_H) {
            drm_mode = mode;
            break;
        }
    }
    if (!drm_mode) {
        std::cerr << "failed to find requested display mode" << std::endl;
        assert(0);
    }
    for (int i = 0; i < connector->count_encoders; ++i) {
        drmModeEncoder * encoder = drmModeGetEncoder(drm_fd, connector->encoders[i]);
        for (int j = 0; j < resources->count_crtcs; ++j) {
            uint crtc_mask = 1 << j;
            if (encoder->possible_crtcs & crtc_mask) {
                drm_crtc = resources->crtcs[j];
                break;
            }
        }
        drmModeFreeEncoder(encoder);
        if (drm_crtc != uint(-1))
            break;
    }
    if (drm_crtc == uint(-1)) {
        std::cerr << "failed to match crtc" << std::endl;
        assert(0);
    }
    drmModeFreeResources(resources);
    drm_conn = connector->connector_id;
    std::cout << "exit init_drm()" << std::endl;
}

void fill_buffer() {
    std::cout << "enter fill_buffer()" << std::endl;
    struct drm_mode_create_dumb dmcb;
    memset(&dmcb, 0, sizeof(dmcb));
    dmcb.bpp = 32;
    dmcb.width = (DPY_W + 15) & ~0x0f;
    dmcb.height = (DPY_H + 15) & ~0x0f;
    int ret = ioctl(drm_fd, DRM_IOCTL_MODE_CREATE_DUMB, &dmcb);
    if (ret == -1) {
        std::cerr << "failed to create dumb buffer: " << strerror(errno) << std::endl;
        assert(0);
    }

    prime_stride = dmcb.pitch;
    prime_size = dmcb.size;
    prime_hdl = dmcb.handle;

    struct drm_mode_map_dumb dmmd;
    memset(&dmmd, 0, sizeof(dmmd));
    dmmd.handle = dmcb.handle;
    ret = ioctl(drm_fd, DRM_IOCTL_MODE_MAP_DUMB, &dmmd);
    if (ret == -1) {
        std::cerr << "failed to map dumb buffer: " << strerror(errno) << std::endl;
        assert(0);
    }
    void * buffer = mmap(nullptr, DPY_W * DPY_H * 4, PROT_READ | PROT_WRITE,
                         MAP_SHARED, drm_fd, dmmd.offset);
    if (!buffer) {
        std::cerr << "failed to mmap dumb buffer: " << strerror(errno) << std::endl;
        assert(0);
    }
    // fill buffer with grey pixels
    memset(buffer, 255/2, DPY_W * DPY_H * 4);
    munmap(buffer, DPY_W * DPY_H * 4);
    std::cout << "exit fill_buffer()" << std::endl;
}

void export_buffer() {
    std::cout << "enter export_buffer()" << std::endl;
    struct drm_prime_handle dph;
    memset(&dph, 0, sizeof(dph));
    dph.handle = prime_hdl;
    dph.fd = -1;
    dph.flags = 0;
    int ret = ioctl(drm_fd, DRM_IOCTL_PRIME_HANDLE_TO_FD, &dph);
    if (ret == -1) {
        std::cerr << "failed to get prime fd: " << strerror(errno) << std::endl;
        assert(0);
    }
    prime_fd = dph.fd;
    std::cout << "exit export_buffer()" << std::endl;
}

void import_buffer() {
    std::cout << "enter import_buffer()" << std::endl;
    struct drm_prime_handle dph;
    memset(&dph, 0, sizeof(dph));
    dph.handle = -1;
    dph.fd = prime_fd;
    dph.flags = 0;
    int ret = ioctl(drm_fd, DRM_IOCTL_PRIME_FD_TO_HANDLE, &dph);
    if (ret == -1) {
        std::cerr << "failed to get prime fd: " << strerror(errno) << std::endl;
        assert(0);
    }
    prime_hdl = dph.handle;
    std::cout << "exit import_buffer()" << std::endl;
}

void set_fb(uint handle) {
    std::cout << "enter set_fb()" << std::endl;
    uint32_t handles[4] = {handle, 0, 0, 0};
    uint32_t strides[4] = {DPY_W * 4, 0, 0, 0};
    uint32_t offsets[4] = {0, 0, 0, 0};
    int ret = drmModeAddFB2(drm_fd, DPY_W, DPY_H, GBM_FORMAT_XRGB8888,
                            handles, strides, offsets, &drm_fb, 0);
    if (ret) {
        std::cerr << "failed to add framebuffer: " << strerror(errno) << std::endl;
        assert(0);
    }
    std::cerr << "set crts: " << drm_crtc << ", mode = " << drm_mode->name
              << ", connector = " << drm_conn << std::endl;
    ret = drmModeSetCrtc(drm_fd, drm_crtc, drm_fb, 0, 0, &drm_conn, 1, drm_mode);
    if (ret) {
        std::cerr << "failed to set crtc: " << strerror(errno) << std::endl;
        assert(0);
    }
    std::cout << "exit set_fb()" << std::endl;
}

void init_gbm() {
    std::cout << "enter init_gbm()" << std::endl;
    gbm_dev = gbm_create_device(drm_fd);
    if (!gbm_dev) {
        std::cerr << "failed to create gbm device: " << strerror(errno) << std::endl;
        assert(0);
    }
    gbm_surf = gbm_surface_create(gbm_dev, DPY_W, DPY_H, GBM_FORMAT_XRGB8888,
                                  GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
    if (!gbm_surf) {
        std::cerr << "failed to create gbm surface: " << strerror(errno) << std::endl;
        assert(0);
    }
    std::cout << "exit init_gbm()" << std::endl;
}

void lock_fb() {
    std::cout << "enter lock_fb()" << std::endl;
    surf_bo = gbm_surface_lock_front_buffer(gbm_surf);
    if (!surf_bo) {
        std::cerr << "failed to lock surface front buffer: " << strerror(errno) << std::endl;
        assert(0);
    }
    std::cout << "exit lock_fb()" << std::endl;
}

void init_egl() {
    std::cout << "enter init_egl()" << std::endl;
    egl_dpy = eglGetDisplay(gbm_dev);
    if (!egl_dpy) {
        std::cerr << "failed to get egl display: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    EGLint major, minor;
    auto ret = eglInitialize(egl_dpy, &major, &minor);
    if (!ret) {
        std::cerr << "failed to initialize egl: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    ret = eglBindAPI(EGL_OPENGL_ES_API);
    if (!ret) {
        std::cerr << "failed to build gles api: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    const EGLint config_attribs[] = {
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_RED_SIZE, 1,
        EGL_GREEN_SIZE, 1,
        EGL_BLUE_SIZE, 1,
        EGL_ALPHA_SIZE, 0,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_NONE
    };

    EGLConfig config;
    EGLint num_config;

    ret = eglChooseConfig(egl_dpy, config_attribs, &config, 1, &num_config);
    if (!ret) {
        std::cerr << "failed to choose egl config: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    const EGLint context_attribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };

    egl_ctx = eglCreateContext(egl_dpy, config, EGL_NO_CONTEXT, context_attribs);
    if (egl_ctx == EGL_NO_CONTEXT) {
        std::cerr << "failed to create egl context: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    egl_surf = eglCreateWindowSurface(egl_dpy, config, gbm_surf, nullptr);
    if (egl_surf == EGL_NO_SURFACE) {
        std::cerr << "failed to create egl window surface: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    ret = eglMakeCurrent(egl_dpy, egl_surf, egl_surf, egl_ctx);
    if (!ret) {
        std::cerr << "failed to make egl context current: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    std::string extensions = eglQueryString(egl_dpy, EGL_EXTENSIONS);
    if (extensions.find("EGL_EXT_image_dma_buf_import") == std::string::npos) {
        std::cerr << "dma buffer import not suported: " << extensions << std::endl;
        assert(0);
    }

    std::cout << "exit init_egl()" << std::endl;
}

#define GL_CHECK_ERROR() do { \
    GLint error = glGetError(); \
    if (error != GL_NO_ERROR) { \
        std::cerr << "EGL ERROR: 0x" << std::hex << error << std::dec \
                  << " @ " << __FILE__ << ":" << __LINE__ << std::endl; \
        assert(0); \
    } \
} while(false)
#define SHADER_POSITION_ATTR_SLOT 0
#define SHADER_TEX_COOR_ATTR_SLOT 1

struct Vertex {
    float position[3];
    float tex_coor[2];
};

namespace {
EGLImageKHR eglCreateImageKHR(EGLDisplay dpy, EGLContext ctx, EGLenum target,
                              EGLClientBuffer buffer, const EGLint *attrib_list) {
    static const PFNEGLCREATEIMAGEKHRPROC createImageProc =
            (PFNEGLCREATEIMAGEKHRPROC) eglGetProcAddress("eglCreateImageKHR");
    return createImageProc(dpy, ctx, target, buffer, attrib_list);
}
EGLBoolean eglDestroyImageKHR(EGLDisplay dpy, EGLImageKHR image) {
    static const PFNEGLDESTROYIMAGEKHRPROC destroyImageProc =
            (PFNEGLDESTROYIMAGEKHRPROC) eglGetProcAddress("eglDestroyImageKHR");
    return destroyImageProc(dpy, image);
}
static PFNGLEGLIMAGETARGETTEXTURE2DOESPROC imageTargetTexture2DProc = NULL;

void glEGLImageTargetTexture2DOES(EGLenum target, EGLImageKHR image) {
    static const PFNGLEGLIMAGETARGETTEXTURE2DOESPROC imageTargetTexture2DProc =
            (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC) eglGetProcAddress("glEGLImageTargetTexture2DOES");
    imageTargetTexture2DProc(target, image);
}
}

void init_gles() {
    std::cout << "enter init_gles()" << std::endl;

    const char external_oes_vshader[] = R"__(
    #ifdef GL_ES
    precision mediump int;
    precision mediump float;
    #endif

    attribute vec4 a_position;
    attribute vec2 a_texcoord;

    varying vec2 v_texcoord;

    void main()
    {
        gl_Position = a_position;
        v_texcoord = a_texcoord;
    }

    )__";

    const char external_oes_fshader[] = R"__(
    #extension GL_OES_EGL_image_external : enable
    #ifdef GL_ES
    precision mediump int;
    precision mediump float;
    #endif

    uniform samplerExternalOES texture;
    varying vec2 v_texcoord;

    void main()
    {
        gl_FragColor = texture2D(texture, v_texcoord);
        gl_FragColor.r = 0.5;
    }

    )__";

    const auto load_shader = [] (const char * source, GLenum type)->GLuint {
        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, &source, nullptr);
        GL_CHECK_ERROR();
        glCompileShader(shader);
        GL_CHECK_ERROR();
        return shader;
    };

    // setup shaders
    gl_program = glCreateProgram();
    GL_CHECK_ERROR();

    auto vs = load_shader(external_oes_vshader, GL_VERTEX_SHADER);
    glAttachShader(gl_program, vs);
    GL_CHECK_ERROR();
    auto fs = load_shader(external_oes_fshader, GL_FRAGMENT_SHADER);
    glAttachShader(gl_program, fs);
    GL_CHECK_ERROR();

    glBindAttribLocation(gl_program, SHADER_POSITION_ATTR_SLOT, "a_position");
    GL_CHECK_ERROR();
    glBindAttribLocation(gl_program, SHADER_TEX_COOR_ATTR_SLOT, "a_texcoord");
    GL_CHECK_ERROR();

    glLinkProgram(gl_program);
    GL_CHECK_ERROR();

    glDetachShader(gl_program, vs);
    GL_CHECK_ERROR();
    glDetachShader(gl_program, fs);
    GL_CHECK_ERROR();

    glUseProgram(gl_program);
    GL_CHECK_ERROR();

    glEnableVertexAttribArray(SHADER_POSITION_ATTR_SLOT);
    GL_CHECK_ERROR();
    glEnableVertexAttribArray(SHADER_TEX_COOR_ATTR_SLOT);
    GL_CHECK_ERROR();

    GLint texture = glGetUniformLocation(gl_program, "texture");
    GL_CHECK_ERROR();
    glUniform1i(texture, 0);
    GL_CHECK_ERROR();

    glActiveTexture(GL_TEXTURE0);
    GL_CHECK_ERROR();

    // setup buffers
    Vertex vertices[] =
    { { {-1., -1., 0.f}, {0.f, 1.f} },
      { { 1., -1., 0.f}, {1.f, 1.f} },
      { { 1.,  1., 0.f}, {1.f, 0.f} },
      { {-1.,  1., 0.f}, {0.f, 0.f} } };
    glGenBuffers(1, &gl_vertices);
    GL_CHECK_ERROR();
    glBindBuffer(GL_ARRAY_BUFFER, gl_vertices);
    GL_CHECK_ERROR();
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    GL_CHECK_ERROR();

    GLushort indices[] = {0, 1, 2, 0, 2, 3};
    glGenBuffers(1, &gl_indices);
    GL_CHECK_ERROR();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_indices);
    GL_CHECK_ERROR();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    GL_CHECK_ERROR();

    // setup texture
   EGLint image_attributes[] = {
        EGL_WIDTH,                      DPY_W,
        EGL_HEIGHT,                     DPY_H,
        EGL_LINUX_DRM_FOURCC_EXT,       DRM_FORMAT_XRGB8888,
        EGL_DMA_BUF_PLANE0_FD_EXT,      prime_fd,
        EGL_DMA_BUF_PLANE0_OFFSET_EXT,  0,
        EGL_DMA_BUF_PLANE0_PITCH_EXT,   DPY_W * 4,
        EGL_NONE
    };

    auto egl_image =  eglCreateImageKHR(egl_dpy, EGL_NO_CONTEXT, EGL_LINUX_DMA_BUF_EXT, 0, image_attributes);
    if (egl_image == EGL_NO_IMAGE_KHR) {
        std::cerr << "failed to create egl image: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }
    std::cout << "egl image handle = " << egl_image << std::endl;

    glGenTextures(1, &gl_texture);
    GL_CHECK_ERROR();
    glBindTexture(GL_TEXTURE_EXTERNAL_OES, gl_texture);
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GL_CHECK_ERROR();
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GL_CHECK_ERROR();
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    GL_CHECK_ERROR();
    glTexParameteri(GL_TEXTURE_EXTERNAL_OES, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    GL_CHECK_ERROR();

    glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, egl_image);
    GL_CHECK_ERROR();

    auto ret = eglDestroyImageKHR(egl_dpy, egl_image);
    if (!ret) {
        std::cerr << "failed to destroy egl image: 0x" << std::hex << eglGetError() << std::dec << std::endl;
    }
    std::cout << "exit init_gles()" << std::endl;
}

void draw_gles() {
    std::cout << "enter draw_gles()" << std::endl;

    glViewport(20, 20, DPY_W - 40, DPY_H - 40);
    GL_CHECK_ERROR();

    glClearColor(0.f, 0.f, 1.f, 1.f);
    GL_CHECK_ERROR();
    glClear(GL_COLOR_BUFFER_BIT);
    GL_CHECK_ERROR();

    glBindBuffer(GL_ARRAY_BUFFER, gl_vertices);
    GL_CHECK_ERROR();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_indices);
    GL_CHECK_ERROR();

    glVertexAttribPointer(SHADER_POSITION_ATTR_SLOT, 3, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex), (const void *)(0));
    GL_CHECK_ERROR();
    glVertexAttribPointer(SHADER_TEX_COOR_ATTR_SLOT, 2, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex), (const void *)(sizeof(Vertex::position)));
    GL_CHECK_ERROR();

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
    GL_CHECK_ERROR();

    auto ret = eglSwapBuffers(egl_dpy, egl_surf);
    if (!ret) {
        std::cerr << "failed to swap buffers: 0x" << std::hex << eglGetError() << std::dec << std::endl;
        assert(0);
    }

    std::cout << "exit draw_gles()" << std::endl;
}

void render_egl(int socket) {
    if (socket != -1)
        prime_fd = recv_fd(socket);
    init_drm();
    init_gbm();
    init_egl();
    init_gles();
    draw_gles();
    lock_fb();
    set_fb(gbm_bo_get_handle(surf_bo).u32);
}

void render_drm(int socket) {
    if (socket != -1)
        prime_fd = recv_fd(socket);
    init_drm();
    import_buffer();
    set_fb(prime_hdl);
}

void pipeline(int socket) {
    open_drm();
    fill_buffer();
    export_buffer();
    close(drm_fd);
    if (socket != -1)
        send_fd(socket, prime_fd);
}

int main(int argc, char* argv[]) {
    int sp[2] = {-1, -1};

    if (socketpair(AF_LOCAL, SOCK_STREAM, 0, sp) < 0) {
        std::cerr << "failed to create sockets: " << strerror(errno) << std::endl;
        return -1;
    }

    const std::string opts[] = {"drm", "egl"};
    if (argc < 3) {
        std::cout << "usage: " << argv[0] << "<path to drm device> drm | egl" << std::endl;
        return -1;
    }
    drm_device_path = argv[1];

    pid_t pid =  fork();
    if (pid) {
        close(sp[1]);
        if (argv[2] == opts[0]) {
            render_drm(sp[0]);
        } else if (argv[2] == opts[1]) {
            render_egl(sp[0]);
        } else {
            std::cout << "usage: " << argv[0] << "<path to drm device> drm | egl" << std::endl;
            return -1;
        }
        sleep(5);
    } else {
        close(sp[0]);
        pipeline(sp[1]);
    }

    return 0;
}
