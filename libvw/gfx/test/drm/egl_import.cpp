#include <iostream>
#include <memory>
#include <sstream>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <cstdio>
#include <thread>
#include <gbm.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <drm_fourcc.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <cmath>
#include <cassert>
#include <vector>

#include <sys/ioctl.h>
#include <sys/mman.h>

#include <drm/vw_drm.h>

#include <render_context/render_context.h>

constexpr uint32_t W = 1920;
constexpr uint32_t H = 1080;
constexpr uint32_t dma_formats[] = {
    DRM_FORMAT_R8,
    DRM_FORMAT_RG88,
    DRM_FORMAT_GR88,
    DRM_FORMAT_RGB888,
    DRM_FORMAT_BGR888,
    DRM_FORMAT_XRGB8888,
    DRM_FORMAT_XBGR8888,
    DRM_FORMAT_RGBX8888,
    DRM_FORMAT_BGRX8888,
    DRM_FORMAT_ARGB8888,
    DRM_FORMAT_ABGR8888,
    DRM_FORMAT_RGBA8888,
    DRM_FORMAT_BGRA8888,
    DRM_FORMAT_YUYV,
    DRM_FORMAT_YVYU,
    DRM_FORMAT_UYVY,
    DRM_FORMAT_VYUY,
    DRM_FORMAT_AYUV,
    DRM_FORMAT_XRGB8888_A8,
    DRM_FORMAT_XBGR8888_A8,
    DRM_FORMAT_RGBX8888_A8,
    DRM_FORMAT_BGRX8888_A8,
    DRM_FORMAT_RGB888_A8,
    DRM_FORMAT_BGR888_A8,
    DRM_FORMAT_NV12,
    DRM_FORMAT_NV21,
    DRM_FORMAT_YUV420,
    DRM_FORMAT_YUV422,
    DRM_FORMAT_YUV444
};

struct TestImage {
    TestImage(int fd, vw::gfx::Image * i) : drm_fd(fd), img(i) {}
    TestImage(const TestImage&) = delete;
    TestImage(TestImage && rhs) {
        img = rhs.img;
        drm_fd = rhs.drm_fd;
        rhs.img = nullptr;
    }
    ~TestImage() {
        if (img) {
            vw::gfx::DMAImage * dma = static_cast<vw::gfx::DMAImage *>(img->data.ptr);
            for (uint32_t p = 0; p < dma->num_planes; ++p) {
                drm_prime_handle dph {};
                dph.fd = dma->planes[p].fd;
                auto ret = ioctl(drm_fd, DRM_IOCTL_PRIME_FD_TO_HANDLE, &dph);
                assert(ret != -1);
                drm_mode_destroy_dumb dmdd {};
                dmdd.handle = dph.handle;
                ret = ioctl(drm_fd, DRM_IOCTL_MODE_DESTROY_DUMB, &dmdd);
                assert(ret != -1);
            }
            delete dma;
            delete img;
        }
    }
    int drm_fd = -1;
    vw::gfx::Image * img = nullptr;
};

std::tuple<int, uint32_t, uint64_t> create_plane(int drm_fd, uint32_t bpp, uint32_t w, uint32_t h) {
    drm_mode_create_dumb dmcb {};
    dmcb.bpp = bpp;
    dmcb.width = w;
    dmcb.height = h;
    auto ret = ioctl(drm_fd, DRM_IOCTL_MODE_CREATE_DUMB, &dmcb);
    assert(ret != -1);

    drm_prime_handle dph {};
    dph.handle = dmcb.handle;
    ret = ioctl(drm_fd, DRM_IOCTL_PRIME_HANDLE_TO_FD, &dph);
    assert(ret != -1);

    return std::make_tuple(dph.fd, dmcb.pitch, dmcb.size);
}

// bpp, width, height
using plane_info_t = std::array<uint32_t, 3>;

std::tuple<uint32_t, std::array<plane_info_t, 3>> planes_info(uint32_t format) {
    std::tuple<uint32_t, std::array<plane_info_t, 3>> nfo =
            std::make_tuple(0U, std::array<plane_info_t, 3> {});
    const auto align16 = [](uint32_t dim)->uint32_t {
        return (dim + 15) & ~0x000f;
    };
    switch (format) {
        case DRM_FORMAT_R8:
            std::get<0>(nfo) = 1;
            std::get<1>(nfo)[0] = {8, align16(W), align16(H)};
            break;
        case DRM_FORMAT_RG88:
        case DRM_FORMAT_GR88:
            std::get<0>(nfo) = 1;
            std::get<1>(nfo)[0] = {16, align16(W), align16(H)};
            break;
        case DRM_FORMAT_RGB888:
        case DRM_FORMAT_BGR888:
            std::get<0>(nfo) = 1;
            std::get<1>(nfo)[0] = {24, align16(W), align16(H)};
            break;
        case DRM_FORMAT_XRGB8888:
        case DRM_FORMAT_XBGR8888:
        case DRM_FORMAT_RGBX8888:
        case DRM_FORMAT_BGRX8888:
        case DRM_FORMAT_ARGB8888:
        case DRM_FORMAT_ABGR8888:
        case DRM_FORMAT_RGBA8888:
        case DRM_FORMAT_BGRA8888:
        case DRM_FORMAT_YUYV:
        case DRM_FORMAT_YVYU:
        case DRM_FORMAT_UYVY:
        case DRM_FORMAT_VYUY:
        case DRM_FORMAT_AYUV:
            std::get<0>(nfo) = 1;
            std::get<1>(nfo)[0] = {32, align16(W), align16(H)};
            break;
        case DRM_FORMAT_XRGB8888_A8:
        case DRM_FORMAT_XBGR8888_A8:
        case DRM_FORMAT_RGBX8888_A8:
        case DRM_FORMAT_BGRX8888_A8:
            std::get<0>(nfo) = 2;
            std::get<1>(nfo)[0] = {32, align16(W), align16(H)};
            std::get<1>(nfo)[1] = {8, align16(W), align16(H)};
            break;
        case DRM_FORMAT_RGB888_A8:
        case DRM_FORMAT_BGR888_A8:
            std::get<0>(nfo) = 2;
            std::get<1>(nfo)[0] = {24, align16(W), align16(H)};
            std::get<1>(nfo)[1] = {8, align16(W), align16(H)};
            break;
        case DRM_FORMAT_NV12:
        case DRM_FORMAT_NV21:
            std::get<0>(nfo) = 2;
            std::get<1>(nfo)[0] = {8, W, H};
            std::get<1>(nfo)[1] = {8, W, H / 2};
            break;
        case DRM_FORMAT_YUV420:
            std::get<0>(nfo) = 3;
            std::get<1>(nfo)[0] = {8, W, H};
            std::get<1>(nfo)[1] = {8, W / 2, H / 2};
            std::get<1>(nfo)[2] = {8, W / 2, H / 2};
            break;
        case DRM_FORMAT_YUV422:
            std::get<0>(nfo) = 3;
            std::get<1>(nfo)[0] = {8, W, H};
            std::get<1>(nfo)[1] = {8, W / 2, H};
            std::get<1>(nfo)[2] = {8, W / 2, H};
            break;
        case DRM_FORMAT_YUV444:
            std::get<0>(nfo) = 3;
            std::get<1>(nfo)[0] = {8, W, H};
            std::get<1>(nfo)[1] = {8, W, H};
            std::get<1>(nfo)[2] = {8, W, H};
            break;
    }
    return nfo;
}

vw::gfx::DMAImage * create_image(int drm_fd, uint32_t format) {
    vw::gfx::DMAImage * dma = new vw::gfx::DMAImage;
    auto nfo = planes_info(format);
    dma->num_planes = std::get<0>(nfo);
    for (uint32_t p = 0; p < dma->num_planes; ++p) {
        const auto & pi = std::get<1>(nfo)[p];
        auto plane = create_plane(drm_fd, pi[0], pi[1], pi[2]);
        dma->planes[p].offset = 0;
        dma->planes[p].pitch = std::get<1>(plane);
        dma->planes[p].fd = std::get<0>(plane);
    }
    return dma;
}

TestImage create(int drm_fd, uint32_t format) {
    vw::gfx::Image * img = new vw::gfx::Image;
    img->header.memory = vw::gfx::Image::Memory::DMA;
    img->header.format = format;
    img->header.width = W;
    img->header.height = H;
    img->data.size = sizeof(vw::gfx::DMAImage);
    img->data.ptr = create_image(drm_fd, format);
    return TestImage(drm_fd, img);
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " <path to drm device>" << std::endl;
        return -1;
    }
    int drm_fd = open(argv[1], O_RDWR);
    auto rc = vw::gfx::rc::create_render_context(drm_fd);
    rc->init();
    for (const auto & format : dma_formats) {
        // auto format = DRM_FORMAT_NV12;
        TestImage img = create(drm_fd, format);
        auto egl_image = rc->load_image(*img.img);
        std::cout << (std::string) img.img->header.format << ":\t[" <<
                     (egl_image->data.ptr ? "OK" : "..") << "]" << std::endl;
        rc->release_image(egl_image);
    }
    close(drm_fd);
    return 0;
}
