#include <iostream>
#include <memory>
#include <sstream>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <cstdio>
#include <thread>
#include <gbm.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <cmath>
#include <cassert>
#include <vector>

#include <sys/ioctl.h>
#include <sys/mman.h>

#include <drm/vw_drm.h>

#include <gl/canvas.h>
#include <gl/shader_manager.h>
#include <gl/frame.h>
#include <gl/texture.h>
#include <gl/mesh.h>
#include <gl/gl.h>

#include <render_context/render_context.h>

using namespace vw::gfx::drm;

constexpr uint32_t W = 1024;
constexpr uint32_t H = 768;

std::string drm_device_path;

void send_fd(int sock, int fd) {
    struct msghdr msg;
    struct iovec iov;
    union {
        struct cmsghdr  cmsghdr;
        char control[CMSG_SPACE(sizeof(int))];
    } cmsgu;
    struct cmsghdr  *cmsg;

    char c = '*';
    iov.iov_base = &c;
    iov.iov_len = sizeof(c);

    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    msg.msg_control = cmsgu.control;
    msg.msg_controllen = sizeof(cmsgu.control);

    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_len = CMSG_LEN(sizeof (int));
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;

    printf ("[send_fd]: passing fd %d\n", fd);
    *((int *) CMSG_DATA(cmsg)) = fd;

    auto size = sendmsg(sock, &msg, 0);
    printf ("[send_fd]: sendmsg = %d\n", size);
}

int recv_fd(int sock) {
    struct msghdr msg;
    struct iovec iov;
    union {
        struct cmsghdr cmsghdr;
        char control[CMSG_SPACE(sizeof (int))];
    } cmsgu;
    struct cmsghdr  *cmsg;

    char c;
    iov.iov_base = &c;
    iov.iov_len = sizeof(c);

    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = cmsgu.control;
    msg.msg_controllen = sizeof(cmsgu.control);
    auto size = recvmsg (sock, &msg, 0);
    printf("[recv_fd]: recvmsg = %d\n", size);
    if (size < 0)
        return -1;

    cmsg = CMSG_FIRSTHDR(&msg);
    if (cmsg && cmsg->cmsg_len == CMSG_LEN(sizeof(int))) {
        int fd = *((int *) CMSG_DATA(cmsg));
        printf ("[recv_fd]: received fd %d\n", fd);
        return fd;
    }
    return -1;
}

class GlRender {
public:
    GlRender(int w, int h);
    void render(vw::gfx::Image * image);
private:
    size_t _counter = 100;
    vw::gfx::gl::Canvas         _canvas;
    vw::gfx::gl::ShaderManager  _shader;
    vw::gfx::gl::Frame          _frame;
    vw::gfx::gl::Mesh           _mesh;
    vw::gfx::gl::Texture        _texture;
};

GlRender::GlRender(int w, int h) {
    std::cout << "=============================================================================" << std::endl;
    std::cout << "SURFACE_SIZE\t\t\t| "            << w << "x" << h                              << std::endl;
    std::cout << "GL_VENDOR\t\t\t| "               << vw::gfx::gl::vendor()                      << std::endl;
    std::cout << "GL_RENDERER\t\t\t| "             << vw::gfx::gl::renderer()                    << std::endl;
    std::cout << "GL_VERSION\t\t\t| "              << vw::gfx::gl::version()                     << std::endl;
    std::cout << "GL_SHADING_LANGUAGE_VERSION\t| " << vw::gfx::gl::sl_version()                  << std::endl;
    std::cout << "GL_EXTENSIONS\t\t\t| "           << vw::gfx::gl::extensions()                  << std::endl;
    std::cout << "=============================================================================" << std::endl;

    _frame.set_viewport({10, 10, w-20, h-20});
}

void GlRender::render(vw::gfx::Image * image) {
    _shader.use(vw::gfx::Image::Memory::DMA);
    float r(.010f * _counter), g(.012f * _counter), b(.015f * _counter++), i;
    _canvas.set_background(1.f, std::modf(r, &i), std::modf(g, &i), std::modf(b, &i));
    _canvas.clear();
    _frame.set();
    _texture.fill(*image);
    _mesh.render();
}

int compositor(int socket) {
    auto prime = recv_fd(socket);

    auto drm_fd = open(drm_device_path.c_str(), O_RDWR);
    auto rc = vw::gfx::rc::create_render_context(drm_fd);
    rc->init();

    GlRender render(1600, 900);

    vw::gfx::DMAImage dma_image;
    dma_image.num_planes = 1;
    dma_image.planes[0].fd = prime;
    dma_image.planes[0].offset = 0;
    dma_image.planes[0].pitch = W * 4;

    vw::gfx::Image image;
    image.header.memory = vw::gfx::Image::Memory::DMA;
    image.header.format = GBM_FORMAT_XRGB8888;
    image.header.width = W;
    image.header.height = H;
    image.data.size = sizeof(vw::gfx::DMAImage);
    image.data.ptr = &dma_image;

    auto egl_image = rc->load_image(image);
    render.render(egl_image);
    rc->release_image(egl_image);

    rc->swap_buffers();

     sleep(2);
     close(drm_fd);

     return 0;
}

int reader(int socket) {
    auto prime = recv_fd(socket);

    int drm_fd = open(drm_device_path.c_str(), O_RDWR);
    std::unique_ptr<Drm> _drm(new Drm(drm_fd));

    struct drm_prime_handle dph;
    memset(&dph, 0, sizeof(struct drm_prime_handle));
    dph.handle = -1;
    dph.fd = prime;
    dph.flags = 0;
    int ret = ioctl(_drm->fd(), DRM_IOCTL_PRIME_FD_TO_HANDLE, &dph);
    assert(ret != -1);

    struct drm_mode_map_dumb dmmd;
    memset(&dmmd, 0, sizeof(dmmd));
    dmmd.handle = dph.handle;
    ret = ioctl(_drm->fd(), DRM_IOCTL_MODE_MAP_DUMB, &dmmd);
    assert(ret != -1);
    void * buffer = mmap(nullptr, W * H * 4, PROT_READ | PROT_WRITE, MAP_SHARED, _drm->fd(), dmmd.offset);
    assert(buffer);

    auto print_hex = [](char * buffer)->std::string {
        std::stringstream ss;
        for (auto c = 0; c < 16; ++c)
            ss << "0x" << std::hex << int(buffer[c]) << " ";
        return ss.str();
    };

    std::cout << "mapped buffer content: " << print_hex((char*)buffer) << std::endl;

    close(drm_fd);
}

int pipeline(int socket) {
    auto drm_fd = open(drm_device_path.c_str(), O_RDWR);

    struct drm_mode_create_dumb dmcb;
    memset(&dmcb, 0, sizeof(struct drm_mode_create_dumb));
    dmcb.bpp = 32;
    dmcb.width = W;
    dmcb.height = H;
    int ret = ioctl(drm_fd, DRM_IOCTL_MODE_CREATE_DUMB, &dmcb);
    assert(ret != -1);

    struct drm_prime_handle dph;
    memset(&dph, 0, sizeof(struct drm_prime_handle));
    dph.handle = dmcb.handle;
    dph.fd = -1;
    dph.flags = 0;
    ret = ioctl(drm_fd, DRM_IOCTL_PRIME_HANDLE_TO_FD, &dph);
    assert(ret != -1);

    struct drm_mode_map_dumb dmmd;
    memset(&dmmd, 0, sizeof(dmmd));
    dmmd.handle = dph.handle;
    ret = ioctl(drm_fd, DRM_IOCTL_MODE_MAP_DUMB, &dmmd);
    assert(ret != -1);
    void * buffer = mmap(nullptr, W * H * 4, PROT_READ | PROT_WRITE, MAP_SHARED, drm_fd, dmmd.offset);
    assert(buffer);
    memset(buffer, 255/2, W * H * 4);
    munmap(buffer, W * H *4);

    close(drm_fd);

    send_fd(socket, dph.fd);
    return 0;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "usage: " << argv[0] << " <path to drm device>" << std::endl;
        return -1;
    }
    drm_device_path = argv[1];

    int sp[2];

    if (socketpair(AF_LOCAL, SOCK_STREAM, 0, sp) < 0) {
        std::cout << "[main]: failed to create sockets" << std::endl;
        return -1;
    }

    pid_t pid =  fork();
    if (pid) {
        close(sp[1]);
        return compositor(sp[0]);
    } else {
        close(sp[0]);
        return pipeline(sp[1]);
    }
}
