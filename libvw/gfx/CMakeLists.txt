cmake_minimum_required(VERSION 2.8)
project(vw.lib.gfx)

set(VW_FRAMEWORK_ROOT ../..)
include(${VW_FRAMEWORK_ROOT}/cmake.config)
project(vw.lib.gfx)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

find_package(PkgConfig REQUIRED)
find_package(Threads REQUIRED)

file(GLOB_RECURSE HEADERS "*.h")
add_custom_target(show_headers SOURCES ${HEADERS})

include_directories(include)

if("${VW_PLATFORM}" STREQUAL "native")
    set(USE_DRM TRUE)
endif()

add_subdirectory(include)
add_subdirectory(src)
add_subdirectory(test)
