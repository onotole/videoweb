#include <list>
#include <unordered_set>
#include <log/log.h>
#include <ipc/export.h>
#include "buffer.h"
#include "connector.h"

namespace vw { namespace gfx { namespace ipc {

class ExportPrivate {
public:
    ExportPrivate(const std::string & addr, const std::string & uuid);

    const std::string & uuid() const { return _uuid; }
    void create(const Image::Header & descr);
    void update(std::unique_ptr<Export::Packet> && packet);
    void commit();

private:
    std::string _uuid;
    std::string _addr;
    std::unique_ptr<Connector> _connector;
    std::unique_ptr<Buffer> _buffer;
    std::unordered_set<int> _exported_fd;
    std::list<std::unique_ptr<Export::Packet>> _limbo;
    constexpr static uint32_t _keep_packets = 1;
};

ExportPrivate::ExportPrivate(const std::string & addr, const std::string & uuid)
    : _addr(addr), _uuid(uuid) {}

void ExportPrivate::create(const Image::Header & descr) {
    if(!_buffer)
        _buffer.reset(new Buffer(_uuid, descr));
    if (!_connector)
        _connector.reset(new Connector(_addr, _uuid, [this] {
            if (_limbo.size() > _keep_packets) _limbo.pop_front();
        }));
}

void ExportPrivate::update(std::unique_ptr<Export::Packet> && packet) {
    if (_buffer->descr().memory == Image::Memory::DMA) {
        const DMAImage * dma = static_cast<const DMAImage *>(packet->handle());
        for (auto p = 0; dma && p < dma->num_planes; ++p) {
            if (_exported_fd.find(dma->planes[p].fd) == _exported_fd.end()) {
                INFO << "export fd " << dma->planes[p].fd;
                _connector->export_fd(dma->planes[p].fd);
                _exported_fd.insert(dma->planes[p].fd);
            }
        }
    }
    _buffer->fill(packet->handle());
    _limbo.push_back(std::move(packet));
}

void ExportPrivate::commit() {
    _connector->update();
}

Export::Export(const std::string & addr, const std::string & uuid)
    : _priv(new ExportPrivate(addr, uuid)) {}

Export::~Export() {}

void Export::create(const Image::Header & descr) {
    return _priv->create(descr);
}

void Export::update(std::unique_ptr<Packet> && packet) {
    return _priv->update(std::move(packet));
}

void Export::commit() {
    return _priv->commit();
}

}}} // namespace vw::gfx::ipc
