#include "import_buffer.h"
#include "buffer.h"

namespace vw { namespace gfx { namespace ipc {

ImportBuffer::ImportBuffer(const std::string & uuid) : _impl(new Buffer(uuid)) {}

ImportBuffer::~ImportBuffer() {}

const Image::Header & ImportBuffer::descr() const {
    return _impl->descr();
}

const Image * ImportBuffer::load() const {
    return _impl->load();
}

void ImportBuffer::map_fd(int remote, int local) {
    return _impl->map_fd(remote, local);
}

}}} // namespace vw::gfx::ipc
