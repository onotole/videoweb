#ifndef __VW_GFX_IPC_EXPORT_BUFFER_H__
#define __VW_GFX_IPC_EXPORT_BUFFER_H__

#include <image.h>
#include <memory>

namespace vw { namespace gfx { namespace ipc {

class Buffer;

class ExportBuffer {
public:
    ExportBuffer(const std::string & uuid, const Image::Header & descr);
    ~ExportBuffer();

    const Image::Header & descr() const;
    void fill(const ImageHandle image);

private:
    std::unique_ptr<Buffer> _impl;
};

}}} // namespace vw::gfx::ipc

#endif // __VW_MEDIA_IPC_EXPORT_BUFFER_H__
