#include <log/log.h>
#include <ipc/import.h>
#include "import_buffer.h"

namespace vw { namespace gfx { namespace ipc {

Import::Import(const std::string & uuid)
    : _uuid(uuid), _buffer(new ImportBuffer(_uuid)) {}

Import::Import(Import && other) {
    if (&other != this) {
        _uuid.swap(other._uuid);
        _buffer.swap(other._buffer);
    }
}

Import::~Import() {}

const std::string & Import::uuid() const {
    return _uuid;
}

const Image * Import::load() const {
    return _buffer->load();
}

void Import::map_fd(int remote, int local) {
    return _buffer->map_fd(remote, local);
}

}}} // namespace vw::gfx::ipc
