#include <unistd.h>
#include <log/log.h>
#include "buffer.h"
#include "util.h"

namespace bipc = boost::interprocess;

namespace vw { namespace gfx { namespace ipc {

Buffer::Buffer(const std::string & uuid, const Image::Header & descr)
    : _shm_name(uuid), _ipc_cleanup(_shm_name) {
    alloc(descr);
}

Buffer::Buffer(const std::string & uuid) : _shm_name(uuid), _ipc_cleanup(_shm_name) {
    mmap();
}

Buffer::~Buffer() {
    for (const auto & p : _fd_map)
        ::close(p.second);
}

void Buffer::alloc(const Image::Header & descr) {
    size_t data_size = calculate_data_size(descr);
    bipc::shared_memory_object shm(bipc::create_only, _shm_name.c_str(), bipc::read_write);
    shm.truncate(sizeof(std::atomic_uint32_t) + 2 * (sizeof(Image) + data_size));
    _mmap = bipc::mapped_region(shm, bipc::read_write);
    void * dest = _mmap.get_address();
    _next = new(dest) std::atomic_uint32_t; dest = (char*)dest + sizeof(std::atomic_uint32_t);
    for (uint32_t i = 0; i < 2; ++i) {
        BufferInfo & nfo = _buffers[i];
        nfo.image = new (dest) Image; dest = (char*)dest + sizeof(Image);
        nfo.image->header.memory = descr.memory;
        nfo.image->header.format = descr.format;
        nfo.image->header.width = descr.width;
        nfo.image->header.height = descr.height;
        nfo.image->data.size = data_size;
        nfo.data = dest; dest = (char*)dest + data_size;
    }
}

void Buffer::mmap() {
    bipc::shared_memory_object shm(bipc::open_only, _shm_name.c_str(), bipc::read_write);
    _mmap = bipc::mapped_region(shm, bipc::read_write);
    void * dest = _mmap.get_address();
    _next = new(dest) std::atomic_uint32_t; dest = (char*)dest + sizeof(std::atomic_uint32_t);
    for (uint32_t i = 0; i < 2; ++i) {
        BufferInfo & nfo = _buffers[i];
        nfo.image = static_cast<Image*>(dest); dest = (char*)dest + sizeof(Image);
        nfo.data = dest; dest = (char*)dest + calculate_data_size(nfo.image->header);
        nfo.image->data.ptr = nfo.data;
    }
}

const Image::Header & Buffer::descr() const {
    return _buffers[0].image->header;
}

void Buffer::fill(const ImageHandle image) {
    if (image) {
        uint32_t next = _next->load();
        BufferInfo & nfo = _buffers[next];
        memcpy(nfo.data, image, nfo.image->data.size);
        _next->store(!next);
    }
}

const Image * Buffer::load() const {
    uint32_t curr = !_next->load();
    const BufferInfo & nfo = _buffers[curr];
    if (nfo.image->header.memory == Image::Memory::DMA) {
        DMAImage * dma = static_cast<DMAImage *>(nfo.image->data.ptr);
        for (auto p = 0; dma && p < dma->num_planes; ++p) {
            auto it = _fd_map.find(dma->planes[p].fd);
            if (it != _fd_map.end())
                dma->planes[p].fd = it->second;
        }
    }
    return nfo.image;
}

void Buffer::map_fd(int remote, int local) {
    _fd_map[remote] = local;
    INFO << "mapping fd: " << remote << " => " << local;
}

}}} // namespace vw::gfx::ipc
