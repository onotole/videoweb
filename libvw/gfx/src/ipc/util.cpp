#include "util.h"

namespace vw { namespace gfx { namespace ipc {

size_t calculate_data_size(const Image::Header & descr) {
    switch (descr.memory) {
        // TODO: handle fourcc
        case Image::Memory::SHM: return descr.width * descr.height * 4;
        case Image::Memory::EGL:   return sizeof(BRCMGlobalImage);
        case Image::Memory::DMA:   return sizeof(DMAImage);
    }
    return 0;
}

}}}
