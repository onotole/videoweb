#ifndef __VW_GFX_IPC_BUFFER_H__
#define __VW_GFX_IPC_BUFFER_H__

#include <atomic>
#include <unordered_map>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <image.h>

namespace vw { namespace gfx { namespace ipc {

class Buffer {
public:
    Buffer(const std::string & uuid, const gfx::Image::Header & descr);
    Buffer(const std::string & uuid);
    ~Buffer();

    const Image::Header & descr() const;

    // export api
    void fill(const ImageHandle image);
    // import api
    const Image * load() const;
    void map_fd(int remote, int local);

private:
    void alloc(const Image::Header & descr);
    void mmap();

    const std::string & _shm_name;
    boost::interprocess::mapped_region _mmap;
    struct ipc_cleanup {
        ipc_cleanup(const std::string & shm) : _shm(shm) {}
        ~ipc_cleanup() { boost::interprocess::shared_memory_object::remove(_shm.c_str()); }
        const std::string & _shm;
    } _ipc_cleanup;
    std::unordered_map<int, int> _fd_map;
    std::atomic_uint32_t * _next = nullptr;
    struct BufferInfo {
        mutable Image * image;
        void * data;
    } _buffers[2];
};

}}} // namespace vw::gfx::ipc

#endif // __VW_MEDIA_IPC_BUFFER_H__
