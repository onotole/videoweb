#ifndef __VW_GFX_IPC_IMPORT_BUFFER_H__
#define __VW_GFX_IPC_IMPORT_BUFFER_H__

#include <image.h>
#include <memory>

namespace vw { namespace gfx { namespace ipc {

class Buffer;

class ImportBuffer {
public:
    ImportBuffer(const std::string & uuid);
    ~ImportBuffer();

    const Image::Header & descr() const;
    const Image * load() const;
    void map_fd(int remote, int local);

private:
    std::unique_ptr<Buffer> _impl;
};

}}} // namespace vw::gfx::ipc

#endif // __VW_MEDIA_IPC_IMPORT_BUFFER_H__
