#ifndef __VW_GFX_IPC_CONNECTOR_H__
#define __VW_GFX_IPC_CONNECTOR_H__

#include <iostream>
#include <thread>
#include <functional>

#include <io/client.h>
#include <io/stream/client.h>

#include <rpc/call_manager.h>
#include <rpc/json-rpc/protocol.h>

namespace vw { namespace gfx { namespace ipc {

class Connector {
    typedef boost::asio::io_service io_service_t;
    typedef vw::io::Client<io_service_t, io::stream::LocalClient> client_t;
    typedef vw::rpc::Proto<rpc::json::Proto> proto_t;
    typedef vw::rpc::CallManager<proto_t, client_t> call_manager_t;
    typedef std::function<void(void)> rendered_callback_t;

public:
    Connector(const std::string & addr, const std::string & id, rendered_callback_t cb);
    ~Connector();

    void update();
    void export_fd(int fd);

private:
    const std::string & uuid;
    rendered_callback_t rendered_handler;
    io_service_t io_service;
    std::thread comm_thread;
    client_t client;
    call_manager_t call_manager;
};

}}} // namespace vw::gfx::ipc

#endif
