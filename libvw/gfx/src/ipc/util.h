#ifndef __VW_GFX_IPC_UTIL_H__
#define __VW_GFX_IPC_UTIL_H__

#include <string>
#include <image.h>

namespace vw { namespace gfx { namespace ipc {

size_t calculate_data_size(const Image::Header & descr);

}}}

#endif // __VW_GFX_IPC_UTIL_H__
