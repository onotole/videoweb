#include "export_buffer.h"
#include "buffer.h"

namespace vw { namespace gfx { namespace ipc {

ExportBuffer::ExportBuffer(const std::string & uuid, const Image::Header & descr)
    : _impl(new Buffer(uuid, descr)) {}

ExportBuffer::~ExportBuffer() {}

const Image::Header & ExportBuffer::descr() const {
    return _impl->descr();
}

void ExportBuffer::fill(const ImageHandle image) {
    return _impl->fill(image);
}

}}} // namespace vw::gfx::ipc
