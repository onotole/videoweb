#include <iostream>
#include "connector.h"
#include <rpc/protocol.h>

namespace vw { namespace gfx { namespace ipc {

void io_runner(boost::asio::io_service & io_service) {
    boost::asio::io_service::work _(io_service);
    io_service.run();
}

Connector::Connector(const std::string & addr, const std::string & id, rendered_callback_t cb)
    : rendered_handler(cb)
    , comm_thread([this](){ io_runner(io_service); })
    , client(io_service, addr), call_manager(client), uuid(id) {
    call_manager.register_event_handler("rendered", rendered_handler);
    call_manager.call<bool>("add_source", uuid);
}

Connector::~Connector() {
    io_service.stop();
    comm_thread.join();
}

void Connector::update() {
    call_manager.call<bool>("update", uuid);
}

void Connector::export_fd(int fd) {
    call_manager.send_fd(fd);
}

}}} // namespace vw::gfx::ipc
