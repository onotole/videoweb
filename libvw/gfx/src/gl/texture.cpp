#include <GLES2/gl2.h>
#include "gl_error.h"
#include "texture.h"
#include "platform_texture.h"

namespace vw { namespace gfx { namespace gl {

bool operator == (const Image::Header & left, const Image::Header & right) {
    return memcmp(&left, &right, sizeof(Image::Header)) == 0;
}

Texture::Texture() {}

Texture::Texture(Texture && rhs) {
    _id = rhs._id;
    _descr = rhs._descr;
    rhs._id = -1;
}

Texture::~Texture() {
    release();
}

void Texture::fill(const Image & image) {
    if (image.header == _descr)
        return update(image.header, image.data.ptr);
    return create(image.header, image.data.ptr);
}

void Texture::bind() {
    PlatformTexture::bind(_descr, _id);
}

void Texture::release() {
    if (glIsTexture(_id)) {
        glDeleteTextures(1, &_id);
        GL_CHECK_ERROR();
    }
}

void Texture::create(const Image::Header & header, const ImageHandle & handle) {
    release();
    _descr = header;
    _id = PlatformTexture::create(header, handle);
}

void Texture::update(const Image::Header & header, const ImageHandle & handle) {
    PlatformTexture::bind(header, _id);
    PlatformTexture::update(header, handle);
}

}}} // namespace vw::gfx::gl
