#include <GLES2/gl2.h>
#include "gl_error.h"
#include "frame.h"

namespace vw { namespace gfx { namespace gl {

void Frame::set() {
    glViewport(_viewport.x, _viewport.y, _viewport.w, _viewport.h);
    GL_CHECK_ERROR();
}

}}} // namespce vw::gfx::gl
