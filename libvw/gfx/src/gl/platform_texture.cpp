#include <log/log.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <iostream>
#include <image_util.h>
#include "gl_error.h"
#include "platform_texture.h"
// TODO: Factor out platform specific code

namespace vw { namespace gfx { namespace gl {

namespace {
void glEGLImageTargetTexture2DOES(EGLenum target, EGLImageKHR image) {
    static const PFNGLEGLIMAGETARGETTEXTURE2DOESPROC imageTargetTexture2DProc =
            (PFNGLEGLIMAGETARGETTEXTURE2DOESPROC) eglGetProcAddress("glEGLImageTargetTexture2DOES");
    imageTargetTexture2DProc(target, image);
}
inline GLenum texture_target(Image::Memory format) {
#ifdef __BRCM__
    return GL_TEXTURE_2D;
#endif
    return format == Image::Memory::EGL ? GL_TEXTURE_EXTERNAL_OES : GL_TEXTURE_2D;
}
}

uint32_t PlatformTexture::create(const Image::Header & header, const ImageHandle & handle) {
    uint32_t id;
    glGenTextures(1, &id);
    GL_CHECK_ERROR();
    bind(header, id);
    GLenum target = texture_target(header.memory);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    GL_CHECK_ERROR();
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    GL_CHECK_ERROR();
    glTexParameteri(target, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    GL_CHECK_ERROR();
    glTexParameteri(target, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    GL_CHECK_ERROR();
    switch (header.memory) {
    // TODO: handle fourcc
    case Image::Memory::SHM:
        glTexImage2D(target, 0, GL_RGBA, header.width, header.height, 0,
                     GL_RGBA, GL_UNSIGNED_BYTE, handle);
        GL_CHECK_ERROR();
        break;
    case Image::Memory::EGL:
    case Image::Memory::DMA:
        glEGLImageTargetTexture2DOES(target, handle);
        GL_CHECK_ERROR();
    }
    auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    INFO << "tex create: fb status = 0x" << std::hex << int(status)
         << ", target = 0x" << target << " header = " << header
         << ", handle = " << handle << ", id = " << id;
    return id;
}

void PlatformTexture::update(const Image::Header & header, const ImageHandle & handle) {
    GLenum target = texture_target(header.memory);
    switch (header.memory) {
    // TODO: handle fourcc
    case Image::Memory::SHM:
        glTexSubImage2D(target, 0, 0, 0, header.width, header.height,
                        GL_RGBA, GL_UNSIGNED_BYTE, handle);
        GL_CHECK_ERROR();
        break;
    case Image::Memory::EGL:
    case Image::Memory::DMA:
        glEGLImageTargetTexture2DOES(target, handle);
        GL_CHECK_ERROR();
    }
}

void PlatformTexture::bind(const Image::Header & header, uint32_t id) {
    GLenum target = texture_target(header.memory);
    glBindTexture(target, id);
    GL_CHECK_ERROR();
}

}}} // namespace vw::gfx::gl
