#include <string>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <log/log.h>
#include "gl.h"


namespace vw { namespace gfx { namespace gl {

const char * vendor() {
    return reinterpret_cast<const char *>(glGetString(GL_VENDOR));
}

const char * renderer() {
    return reinterpret_cast<const char *>(glGetString(GL_RENDERER));
}

const char * version() {
    return reinterpret_cast<const char *>(glGetString(GL_VERSION));
}

const char * sl_version() {
    return reinterpret_cast<const char *>(glGetString(GL_SHADING_LANGUAGE_VERSION));
}

const char * extensions() {
    return reinterpret_cast<const char *>(glGetString(GL_EXTENSIONS));
}

bool supported(const char * extension) {
    static const std::string extensions_cache = extensions();
    return extensions_cache.find(extension) != std::string::npos;
}

}}} // namespce vw::gfx::gl
