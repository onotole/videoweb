#include <unordered_map>
#include <string>
#include "shader_manager.h"
#include "shaders.h"

namespace vw { namespace gfx { namespace gl {
typedef std::pair<const char *, const char *> shader_source_t;
typedef std::unordered_map<std::string, shader_source_t> shader_name_map_t;
typedef std::unordered_map<Image::Memory, shader_source_t> shader_type_map_t;

static const shader_name_map_t shader_name_map = {
    {"default", {default_vshader, default_fshader}},
    {"external_oes", {external_oes_vshader, external_oes_fshader}}
};

// TODO: take in account fourcc
static const shader_type_map_t shader_type_map = {
    {Image::Memory::SHM, {default_vshader, default_fshader}},
    {Image::Memory::EGL, {default_vshader, default_fshader}},
    {Image::Memory::DMA, {external_oes_vshader, external_oes_fshader}}
};

shader_ptr_t create_shader(Image::Memory type) {
    auto it = shader_type_map.find(type);
    if (it != shader_type_map.end())
        return shader_ptr_t(new Shader(it->second.first, it->second.second));
    return nullptr;
}

void ShaderManager::use(Image::Memory type) const {
    auto it = _shaders.find(type);
    if (it == _shaders.end())
        it = _shaders.emplace(type, create_shader(type)).first;
    return it->second->use();
}

}}}
