#include <GLES2/gl2.h>
#include "gl.h"
#include <gl_error.h>
#include "mesh.h"

namespace vw { namespace gfx { namespace gl {

Mesh::Mesh() {
    create_index_buffer();
    create_vertex_buffer();
}

Mesh::~Mesh() {
    delete_index_buffer();
    delete_vertex_buffer();
}

void Mesh::set_uv_bounds(const base::n_rect_t & r) {
    _uv_bounds = r;
    _update_pending = true;
}

const base::n_rect_t & Mesh::get_uv_bounds() const {
    return _uv_bounds;
}

void Mesh::render() {
    if (_update_pending) {
        update_vertex_buffer();
        _update_pending = false;
    }
    glBindBuffer(GL_ARRAY_BUFFER, _vertex_buffer);
    GL_CHECK_ERROR();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _index_buffer);
    GL_CHECK_ERROR();

    glVertexAttribPointer(SHADER_POSITION_ATTR_SLOT, 3, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex), (const void *)(0));
    GL_CHECK_ERROR();
    glVertexAttribPointer(SHADER_TEX_COOR_ATTR_SLOT, 2, GL_FLOAT, GL_FALSE,
                          sizeof(Vertex), (const void *)(sizeof(Vertex::position)));
    GL_CHECK_ERROR();

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
    GL_CHECK_ERROR();
}

void Mesh::create_vertex_buffer() {
    delete_vertex_buffer();
    glGenBuffers(1, &_vertex_buffer);
    GL_CHECK_ERROR();

    update_vertex_buffer();
}

void Mesh::update_vertex_buffer() {
    Vertex vertices[] =
    { { {-1., -1., 0.f}, {_uv_bounds.x               , _uv_bounds.y + _uv_bounds.h} },
      { { 1., -1., 0.f}, {_uv_bounds.x + _uv_bounds.w, _uv_bounds.y + _uv_bounds.h} },
      { { 1.,  1., 0.f}, {_uv_bounds.x + _uv_bounds.w, _uv_bounds.y} },
      { {-1.,  1., 0.f}, {_uv_bounds.x               , _uv_bounds.y} } };

    glBindBuffer(GL_ARRAY_BUFFER, _vertex_buffer);
    GL_CHECK_ERROR();
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    GL_CHECK_ERROR();
}

void Mesh::delete_vertex_buffer() {
    if (_vertex_buffer) {
        glDeleteBuffers(1, &_vertex_buffer);
        GL_CHECK_ERROR();
    }
    _vertex_buffer = 0;
}

void Mesh::create_index_buffer() {
    delete_index_buffer();
    glGenBuffers(1, &_index_buffer);
    GL_CHECK_ERROR();

    GLushort indices[] = {0, 1, 2, 0, 2, 3};
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _index_buffer);
    GL_CHECK_ERROR();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    GL_CHECK_ERROR();
}

void Mesh::delete_index_buffer() {
    if (_index_buffer) {
        glDeleteBuffers(1, &_index_buffer);
        GL_CHECK_ERROR();
    }
    _index_buffer = 0;
}

}}}
