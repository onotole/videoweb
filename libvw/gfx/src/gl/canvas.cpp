#include <GLES2/gl2.h>
#include "gl_error.h"
#include "canvas.h"

namespace vw { namespace gfx { namespace gl {

#define SCALE 0x000000ff
#define A_MASK 0xff000000
#define A_SHIFT 24
#define R_MASK 0x00ff0000
#define R_SHIFT 16
#define G_MASK 0x0000ff00
#define G_SHIFT 8
#define B_MASK 0x000000ff
#define B_SHIFT 0
#define CC(C, rgba) float(((rgba) & C##_MASK) >> C##_SHIFT) / SCALE
#define A(rgba) CC(A, rgba)
#define R(rgba) CC(R, rgba)
#define G(rgba) CC(G, rgba)
#define B(rgba) CC(B, rgba)

void Canvas::set_background(uint32_t rgba) {
    set_background(A(rgba), R(rgba), G(rgba), B(rgba));
}

void Canvas::set_background(float a, float r, float g, float b) {
    _background_color = {a, r, g, b};
}

void Canvas::clear() {
    glClearColor(_background_color.r, _background_color.g, _background_color.b, _background_color.a);
    GL_CHECK_ERROR();
    glClear(GL_COLOR_BUFFER_BIT);
    GL_CHECK_ERROR();
}

}}}
