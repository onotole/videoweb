#ifndef __GFX_GL_PLATFORM_TEXTURE_H__
#define __GFX_GL_PLATFORM_TEXTURE_H__

#include <image.h>

namespace vw { namespace gfx { namespace gl {

struct PlatformTexture {
    static uint32_t create(const Image::Header & header, const ImageHandle & handle);
    static void     update(const Image::Header & header, const ImageHandle & handle);
    static void     bind(const Image::Header & descr, uint32_t id);
};

}}} // namespace vw::gfx::gl

#endif // __GFX_GL_PLATFORM_TEXTURE_H__
