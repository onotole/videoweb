#ifndef __GFX_GL_ERROR_H__
#define __GFX_GL_ERROR_H__

#include <GLES2/gl2.h>
#include <log/log.h>

#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif
#define GL_CHECK_ERROR() do { \
    GLint error = glGetError(); \
    if (error != GL_NO_ERROR) \
        ERROR << "EGL ERROR: 0x" << std::hex << error << std::dec << " @ " << __FILENAME__ << ":" << __LINE__; \
} while(false)

#endif // __GFX_GL_ERROR_H__
