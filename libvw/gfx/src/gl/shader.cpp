#include <vector>
#include <GLES2/gl2.h>
#include <gl.h>
#include "gl_error.h"
#include "shader.h"
#include "shaders.h"

namespace vw { namespace gfx { namespace gl {

Shader::Shader(const char * vs, const char * fs) {
    static auto load_shader = [](const char * source, GLenum type)->GLuint {
        // TODO: error handling
        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, &source, nullptr);
        GL_CHECK_ERROR();
        glCompileShader(shader);
        GL_CHECK_ERROR();
        return shader;
    };

    if (!_program) {
        _program = glCreateProgram();
        GL_CHECK_ERROR();

        auto vshader = load_shader(vs, GL_VERTEX_SHADER);
        glAttachShader(_program, vshader);
        GL_CHECK_ERROR();
        auto fshader = load_shader(fs, GL_FRAGMENT_SHADER);
        glAttachShader(_program, fshader);
        GL_CHECK_ERROR();

        glBindAttribLocation(_program, SHADER_POSITION_ATTR_SLOT, "a_position");
        GL_CHECK_ERROR();
        glBindAttribLocation(_program, SHADER_TEX_COOR_ATTR_SLOT, "a_texcoord");
        GL_CHECK_ERROR();

        glLinkProgram(_program);
        GL_CHECK_ERROR();

        glDetachShader(_program, vshader);
        GL_CHECK_ERROR();
        glDetachShader(_program, fshader);
        GL_CHECK_ERROR();

        glUseProgram(_program);
        GL_CHECK_ERROR();

        glEnableVertexAttribArray(SHADER_POSITION_ATTR_SLOT);
        GL_CHECK_ERROR();
        glEnableVertexAttribArray(SHADER_TEX_COOR_ATTR_SLOT);
        GL_CHECK_ERROR();

        GLint texture = glGetUniformLocation(_program, "texture");
        GL_CHECK_ERROR();
        glUniform1i(texture, 0);
        GL_CHECK_ERROR();

        glActiveTexture(GL_TEXTURE0);
        GL_CHECK_ERROR();
    }
}

void Shader::use() const {
    glUseProgram(_program);
    GL_CHECK_ERROR();
}

Shader::~Shader() {
    glDeleteProgram(_program);
    GL_CHECK_ERROR();
}

}}}
