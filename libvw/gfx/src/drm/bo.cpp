#include <string.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <log/log.h>
#include <gbm.h>
#include "bo.h"

namespace vw { namespace gfx { namespace drm {


Bo * Bo::create(gbm_device * gbm, int fd, uint32_t w, uint32_t h, uint32_t format) {
    gbm_bo * bo = gbm_bo_create(gbm, w, h, format, GBM_BO_USE_RENDERING);
    if (!bo) {
        ERROR << "Failed to create buffer object: " << strerror(errno);
        return nullptr;
    }
    return new Bo(bo);
}

void Bo::destroy(gbm_bo *, void * data) {
    Bo * bo = static_cast<Bo *>(data);
    delete bo;
}

int Bo::get_prime() {
    if (_prime == -1)
        _prime = gbm_bo_get_fd(_bo);
    return _prime;
}

Bo::Bo(gbm_bo * bo) : _bo(bo) {
    gbm_bo_set_user_data(_bo, this, Bo::destroy);
}

Bo::~Bo() {
    close(_prime);
    gbm_bo_set_user_data(_bo, nullptr, nullptr);
    gbm_bo_destroy(_bo);
}

Fb::~Fb() {
    drmModeRmFB(_drm_fd, _fb_id);
}

Fb * Fb::get(gbm_bo * bo, int fd) {
    Fb * fb = static_cast<Fb *>(gbm_bo_get_user_data(bo));
    if (fb)
        return fb;

    fb = new Fb(bo, fd);

    uint32_t width = gbm_bo_get_width(bo);
    uint32_t height = gbm_bo_get_height(bo);
    uint32_t format = gbm_bo_get_format(bo);
    uint32_t handles[4] = {gbm_bo_get_handle(bo).u32, 0, 0, 0};
    uint32_t strides[4] = {gbm_bo_get_stride(bo), 0, 0, 0};
    uint32_t offsets[4] = {0, 0, 0, 0};

    if (drmModeAddFB2(fd, width, height, format, handles, strides, offsets, &fb->_fb_id, 0)) {
        ERROR << "Failed to add framebuffer: [" << errno << "]: " << strerror(errno);
        delete fb;
        return nullptr;
    }
    return fb;
}

Fb::Fb(gbm_bo * bo, int fd) : Bo(bo), _drm_fd(fd) {}

}}}
