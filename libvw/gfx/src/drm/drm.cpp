#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <log/log.h>
#include "vw_drm.h"

namespace vw { namespace gfx { namespace drm {

namespace {
drmEventContext evt_ctx { 2, nullptr, [](int, uint32_t, uint32_t, uint32_t, void * ctx) {
                                            std::atomic<bool> * wait_flip = static_cast<std::atomic<bool> *>(ctx);
                                            wait_flip->store(false);
                                      }};
}

Drm::Drm(int fd) : _fd(fd) {
    _crtc_set.store(false);
    _wait_flip.store(false);
    _run.store(false);
    _update_pending.store(false);

    drmModeRes * resources = drmModeGetResources(_fd);
    if (!resources) {
        ERROR << "Failed to get drm resources: [" << errno << "]: " << strerror(errno);
        throw(std::runtime_error(strerror(errno)));
    }

    // find connector
    drmModeConnector * connector = nullptr;
    for (int i = 0; i < resources->count_connectors; ++i) {
        connector = drmModeGetConnector(_fd, resources->connectors[i]);
        if (connector->connection == DRM_MODE_CONNECTED &&
           (connector->connector_type == DRM_MODE_CONNECTOR_HDMIA ||
            connector->connector_type == DRM_MODE_CONNECTOR_HDMIB ))
            break;
        drmModeFreeConnector(connector);
        connector = nullptr;
    }

    if (!connector) {
        ERROR << "Failed to find connected connector";
        throw(std::runtime_error("could not find connected connector"));
    }

    // find preferred mode
    int area = 0;
    for (int i = 0; i < connector->count_modes; ++i) {
        drmModeModeInfo * current_mode = &connector->modes[i];
        int current_area = current_mode->hdisplay * current_mode->vdisplay;
        if (current_area > area) {
            _mode = static_cast<mode_info_t>(current_mode);
            area = current_area;
        }
    }

    if (!_mode) {
        ERROR << "Could not find drm mode";
        throw(std::runtime_error("could not find mode"));
    }

    // find crtc
    auto find_crtc_for_encoder = [&](const drmModeEncoder * encoder)->uint32_t {
        for (int i = 0; i < resources->count_crtcs; i++) {
            const uint32_t crtc_mask = 1 << i;
            if (encoder->possible_crtcs & crtc_mask) {
                TRACE << "found crtc[" << i << "], id = " << resources->crtcs[i]
                         << ", encoder = " << encoder->encoder_id
                         << ", connector = " << connector->connector_id
                         << ", possible_crtcs = 0x" << std::hex << encoder->possible_crtcs;
                return resources->crtcs[i];
            }
        }
        return -1;
    };

    for (int i = 0; i < connector->count_encoders; i++) {
        drmModeEncoder * encoder = drmModeGetEncoder(_fd, connector->encoders[i]);
        if (encoder) {
            const uint32_t crtc_id = find_crtc_for_encoder(encoder);
            drmModeFreeEncoder(encoder);
            if (crtc_id != -1) {
                _crtc = crtc_id;
                break;
            }
        }
    }

    if (_crtc == -1) {
        ERROR << "Could not match crtc";
        throw(std::runtime_error("could not match crtc"));
    }

    drmModeFreeResources(resources);

    _connector = connector->connector_id;

    INFO << "DRM: fd = " << _fd << ", mode = " << static_cast<drmModeModeInfo*>(_mode)->name
         << ", crtc = " << _crtc << ", connector = " << _connector;
}

Drm::~Drm() {
    stop();
    if (_curr) Bo::destroy(_curr->bo(), _curr);
    if (_next) Bo::destroy(_next->bo(), _next);
}

uint32_t Drm::width() const {
    return static_cast<drmModeModeInfoPtr>(_mode)->hdisplay;
}

uint32_t Drm::height() const {
    return static_cast<drmModeModeInfoPtr>(_mode)->vdisplay;
}

int Drm::fd() const {
    return _fd;
}

void Drm::exec(Gbm * gbm) {
    _curr = gbm->lock_fb();

    drmModeModeInfoPtr mode = static_cast<drmModeModeInfoPtr>(_mode);
    drmModeSetCrtc(_fd, _crtc, _curr->fb_id(), 0, 0, &_connector, 1, mode);
    if (_resize_cb)
        _resize_cb(width(), height());

    _run.store(true);
    while(_run.load()) {
        if (_vblank_cb)
            _vblank_cb();
        if (_update_pending.load()) {
            _update_pending.store(false);
            if (_update_cb)
                _update_cb();
        }
        while(_wait_flip.load()) {
            fd_set fds;
            FD_ZERO(&fds);
            FD_SET(0, &fds);
            FD_SET(_fd, &fds);
            select(_fd + 1, &fds, nullptr, nullptr, nullptr);
            drmHandleEvent(_fd, &evt_ctx);
        }
        gbm->release(_curr);
        std::swap(_curr, _next);
    }
    INFO << "DRM: exit loop done.";
}

void Drm::stop() {
    _run.store(false);
}

void Drm::update() {
    _update_pending.store(true);
}

void Drm::page_flip(Gbm * gbm) {
    _wait_flip.store(true);
    _next = gbm->lock_fb();
    drmModePageFlip(_fd, _crtc, _next->fb_id(), DRM_MODE_PAGE_FLIP_EVENT, &_wait_flip);
}

void Drm::display(Fb * fb) {
    INFO << "display fb = " << fb;
    if (fb) {
        if (!_crtc_set.load()) {
            drmModeModeInfoPtr mode = static_cast<drmModeModeInfoPtr>(_mode);
            if(drmModeSetCrtc(_fd, _crtc, fb->fb_id(), 0, 0, &_connector, 1, mode)) {
                ERROR << "Failed to set crtc: [" << errno << "]: " << strerror(errno);
                return;
            }
            _crtc_set.store(true);
            if (_resize_cb)
                _resize_cb(width(), height());
        } else {
            while(_wait_flip.load()) {
                fd_set fds;
                FD_ZERO(&fds);
                FD_SET(0, &fds);
                FD_SET(_fd, &fds);
                select(_fd + 1, &fds, nullptr, nullptr, nullptr);
                drmHandleEvent(_fd, &evt_ctx);
            }
            _wait_flip.store(true);
            if(drmModePageFlip(_fd, _crtc, fb->fb_id(), DRM_MODE_PAGE_FLIP_EVENT, &_wait_flip)) {
                _wait_flip.store(false);
                ERROR << "Failed to flip page: [" << errno << "]: " << strerror(errno);
            }
        }
    }
}

void Drm::set_resize_callback(resize_callback_t && cb) {
    _resize_cb = cb;
}

void Drm::set_update_callback(update_callback_t && cb) {
    _update_cb = cb;
}

void Drm::set_vblank_callback(rc::IPulse::callback_t && cb) {
    _vblank_cb = cb;
}

}}}
