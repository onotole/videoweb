#include <log/log.h>
#include <gbm.h>
#include "surface.h"

namespace vw { namespace gfx { namespace drm {

class SurfaceImpl {
public:
    SurfaceImpl(gbm_device * dev, uint32_t w, uint32_t h);
    virtual ~SurfaceImpl();

    uint32_t width() const  { return _width; }
    uint32_t height() const { return _height; }
    uint32_t format() const { return _format; }

    virtual gbm_surface * surface() { return nullptr; }

    virtual Fb * lock(int fd) { return nullptr; }
    virtual void release() { std::swap(_curr, _next); }

protected:
    gbm_device * _device = nullptr;

    uint32_t _width  = 0;
    uint32_t _height = 0;
    uint32_t _format = 0;

    Fb *       _curr = nullptr;
    Fb *       _next = nullptr;
};

SurfaceImpl::SurfaceImpl(gbm_device * dev, uint32_t w, uint32_t h)
    : _device(dev), _width(w), _height(h), _format(GBM_FORMAT_XRGB8888) {}

SurfaceImpl::~SurfaceImpl() {}

class WindowSurfaceImpl : public SurfaceImpl
{
public:
    WindowSurfaceImpl(gbm_device * dev, uint32_t w, uint32_t h);
    virtual ~WindowSurfaceImpl();

    virtual gbm_surface * surface() override { return _surface; }
    virtual void release() override;
    virtual Fb * lock(int fd) override;

private:
    gbm_surface * _surface = nullptr;
};

WindowSurfaceImpl::WindowSurfaceImpl(gbm_device * dev, uint32_t w, uint32_t h)
    : SurfaceImpl(dev, w, h) {
    gbm_surface_create(_device, _width, _height, _format,
                       GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
}

WindowSurfaceImpl::~WindowSurfaceImpl() {}

void WindowSurfaceImpl::release() {
    if (_curr)
        gbm_surface_release_buffer(_surface, _curr->bo());
    SurfaceImpl::release();
}

Fb * WindowSurfaceImpl::lock(int fd) {
    gbm_bo * bo = gbm_surface_lock_front_buffer(_surface);
    if (!_curr) {
        _curr = Fb::get(bo, fd);
        return _curr;
    }
    _next = Fb::get(bo, fd);
    return _next;
}

class DumbSurfaceImpl : public SurfaceImpl {
public:
    DumbSurfaceImpl(gbm_device * dev, uint32_t w, uint32_t h)
        : SurfaceImpl(dev, w, h) {}
    virtual ~DumbSurfaceImpl() {}

    virtual Fb * lock(int fd) override;
};

Fb * DumbSurfaceImpl::lock(int fd) {
    if (!_curr) {
        gbm_bo * bo = gbm_bo_create(_device, _width, _height, _format,
                                    GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
        _curr = Fb::get(bo, fd);
        return _curr;
    } else if (!_next) {
        gbm_bo * bo = gbm_bo_create(_device, _width, _height, _format,
                                    GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
        _next = Fb::get(bo, fd);
    }
    return _next;
}

Surface::Surface(gbm_device * dev, uint32_t w, uint32_t h, bool dumb) {
    if (dumb)
        _impl.reset(new DumbSurfaceImpl(dev, w, h));
    else
        _impl.reset(new WindowSurfaceImpl(dev, w, h));
}

Surface::~Surface() {}

uint32_t Surface::width() const { return _impl->width(); }

uint32_t Surface::height() const { return _impl->height(); }

uint32_t Surface::format() const { return _impl->format(); }

gbm_surface * Surface::surface() { return _impl->surface(); }

Fb * Surface::lock(int fd) { return _impl->lock(fd); }

void Surface::release() { return _impl->release(); }

}}}
