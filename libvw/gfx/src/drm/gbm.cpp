#include <string.h>
#include <gbm.h>
#include <log/log.h>
#include "vw_gbm.h"


namespace vw { namespace gfx { namespace drm {

Gbm::Gbm(int fd, uint32_t w, uint32_t h) : _fd(fd) {
    _dev = gbm_create_device(_fd);
    if (!_dev) {
        ERROR << "Failed to create gbm device: [" << errno << "]: " << strerror(errno);
        throw(std::runtime_error("failed to create gbm device"));
    }
    _format = GBM_FORMAT_XRGB8888;
    _width = w;
    _height = h;
    _surface = gbm_surface_create(_dev, _width, _height, _format, GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);
    if (!_surface) {
        ERROR << "Failed to create gbm surface: [" << errno << "]: " << strerror(errno);
        throw(std::runtime_error("failed to create gbm surface"));
    }
    INFO << "GBM: format = 0x" << std::hex << _format << std::dec << ", size = " << _width << "x" << _height;
}

Gbm::~Gbm() {
    gbm_device_destroy(_dev);
}

uint32_t Gbm::width() const {
    return _width;
}

uint32_t Gbm::height() const {
    return _height;
}

uint32_t Gbm::format() const {
    return _format;
}

int Gbm::fd() const {
    return _fd;
}

bool Gbm::supported(uint32_t format, uint32_t usage) const {
    return gbm_device_is_format_supported(_dev, format, usage);
}

Fb * Gbm::lock_fb() {
    gbm_bo * bo = gbm_surface_lock_front_buffer(_surface);
    return Fb::get(bo, _fd);
}

void Gbm::release(Fb * fb) {
    gbm_surface_release_buffer(_surface, fb->bo());
}

Bo * Gbm::create(uint32_t w, uint32_t h, uint32_t format) {
    return Bo::create(_dev, _fd, w, h, format);
}

void Gbm::destroy(Bo * bo) {
    return Bo::destroy(bo->bo(), bo);
}

}}}
