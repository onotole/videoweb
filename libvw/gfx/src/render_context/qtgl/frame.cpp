#include <QThread>
#include "frame.h"

namespace vw { namespace gfx { namespace rc {

Frame::Frame() : _gl_initialized(false) {}

void Frame::updateGL() {
    glDraw();
}

void Frame::paintEvent(QPaintEvent *) {
    glDraw();
}

void Frame::resizeEvent(QResizeEvent *e) {
    QWidget::resizeEvent(e);
    _gl_initialized = true;
    context()->makeCurrent();
    emit gl_resized(width(), height());
}

void Frame::glDraw() {
    context()->makeCurrent();
    emit gl_render();
}

}}} // namespace vw::gfx::rc
