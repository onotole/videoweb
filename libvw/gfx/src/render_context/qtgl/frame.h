#ifndef __GFX_FRAME_H__
#define __GFX_FRAME_H__

#include <map>
#include <memory>

#include <QGLWidget>

namespace vw { namespace gfx { namespace rc {

class Frame : public QGLWidget {

    Q_OBJECT

public:
    Frame();

    void updateGL();

signals:
    void gl_resized(int w, int h);
    void gl_render();

protected:
    void resizeEvent(QResizeEvent *e);
    void paintEvent(QPaintEvent *);

    void glDraw();

private:
    bool _gl_initialized;
};

}}} // namespace vw::gfx::rc

#endif // __GFX_FRAME_H__
