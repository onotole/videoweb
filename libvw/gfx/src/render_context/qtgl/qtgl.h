#ifndef __QTGL_RENDER_CONTEXT_H__
#define __QTGL_RENDER_CONTEXT_H__

#include <irender_context.h>

class QApplication;

namespace vw { namespace gfx { namespace rc {

class Frame;

class QTGLRenderContext : public IRenderContext {
public:
    virtual ~QTGLRenderContext();

    void set_resize_callback(resize_callback_t && callback) override;
    void set_update_callback(update_callback_t && callback) override;

    void init() override;
    void exec() override;
    void stop() override;
    void update() override;

    void swap_buffers() override;
    IPulse * get_pulse() override { return nullptr; }
private:
    QApplication * app = nullptr;
    Frame * frame = nullptr;
    resize_callback_t resize_callback;
    update_callback_t update_callback;
};


}}} // namespce vw::gfx::rc

#endif // __QTGL_RENDER_CONTEXT_H__
