#include <QApplication>
#include "frame.h"
#include "qtgl.h"


namespace vw { namespace gfx { namespace rc {

QTGLRenderContext::~QTGLRenderContext() {
    delete app;
    delete frame;
}

void QTGLRenderContext::set_resize_callback(resize_callback_t && callback) {
    resize_callback = callback;
}

void QTGLRenderContext::set_update_callback(update_callback_t && callback) {
    update_callback = callback;
}

void QTGLRenderContext::init() {
    if (!app) {
        int argc = 0;
        app = new QApplication(argc, nullptr);
        frame = new Frame();
        QObject::connect(frame, &Frame::gl_resized, [this](int w, int h) {
            if (resize_callback) resize_callback(w, h);
        });
        QObject::connect(frame, &Frame::gl_render, [this]() {
            if (update_callback) update_callback();
        });
        frame->show();
    }
}

void QTGLRenderContext::exec() {
    init();
    app->exec();
}

void QTGLRenderContext::stop() {
    app->quit();
}

void QTGLRenderContext::update() {
    if (frame)
        QMetaObject::invokeMethod(frame, "updateGL");
}

void QTGLRenderContext::swap_buffers() {
    init();
    frame->context()->swapBuffers();
}

}}} // namespce vw::gfx::rc
