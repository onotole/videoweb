#ifndef __EGL_NATIVE_CONTEXT_X11_H__
#define __EGL_NATIVE_CONTEXT_X11_H__

#include "native_context.h"

namespace vw { namespace gfx { namespace rc {

class EGLNativeContextX11 : public EGLNativeContextBase {
public:
    explicit EGLNativeContextX11(int);
    virtual ~EGLNativeContextX11();

    virtual EGLNativeDisplayType display() const override;
    virtual EGLNativeWindowType window() const override;
    virtual EGLNativePixmapType pixmap() const override;

    virtual bool ready() const override;

    virtual void init(int w, int h, bool /*TODO: offscreen mode*/) override;
    virtual void exec() override;
    virtual void stop() override;
    virtual void update() override;

private:
    EGLNativeDisplayType dpy = 0;
    EGLNativeWindowType win = 0;

};


}}} // namespace vw::gfx::rc


#endif // __EGL_NATIVE_CONTEXT_X11_H__
