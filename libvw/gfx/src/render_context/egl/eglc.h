#ifndef __EGL_RENDER_CONTEXT_H__
#define __EGL_RENDER_CONTEXT_H__

#include "native_context.h"

namespace vw { namespace gfx { namespace rc {

class EGLRenderContext : public IRenderContext {
public:
    EGLRenderContext(int fd);
    virtual ~EGLRenderContext();

    virtual void set_resize_callback(resize_callback_t && callback) override;
    virtual void set_update_callback(update_callback_t && callback) override;

    virtual void init(int w = IRenderContext::DEFAULT,
                      int h = IRenderContext::DEFAULT,
                      bool offscreen = false) override;
    virtual void exec() override;
    virtual void stop() override;
    virtual void update() override;

    virtual void swap_buffers() override;
    virtual IPulse * get_pulse() override;

    virtual const Image & snapshot() override;

    virtual Image * load_image(const Image &) override;
    virtual void release_image(Image *) override;

private:
    EGLNativeContextBase * native_context;

    struct _EGLContext {
        EGLContext  ctx;
        EGLDisplay  dpy;
        EGLSurface surf;
        bool  offscreen;
    } egl_context = {};
    Image _snapshot;
};


}}} // namespce vw::gfx::rc

#endif // __EGL_RENDER_CONTEXT_H__
