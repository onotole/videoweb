#include "native_context_x11.h"

namespace vw { namespace gfx { namespace rc {
EGLNativeContextX11::EGLNativeContextX11(int) {}

EGLNativeContextX11::~EGLNativeContextX11() {
    if (dpy) {
        XDestroyWindow(dpy, win);
        XCloseDisplay(dpy);
    }
}

EGLNativeDisplayType EGLNativeContextX11::display() const {
    return dpy;
}

EGLNativeWindowType EGLNativeContextX11::window() const {
    return win;
}

EGLNativePixmapType EGLNativeContextX11::pixmap() const {
    return 0;
}

bool EGLNativeContextX11::ready() const {
    return dpy;
}

void EGLNativeContextX11::init(int w, int h, bool) {
    if (w == IRenderContext::DEFAULT || h == IRenderContext::DEFAULT) {
        w = 800;
        h = 600;
    }

    XInitThreads();
    dpy = XOpenDisplay(nullptr);
    win = XCreateWindow(dpy, DefaultRootWindow(dpy), 0, 0, w, h, 0,
                        CopyFromParent, InputOutput, CopyFromParent, 0, nullptr);
    XSelectInput(dpy, win, StructureNotifyMask | ExposureMask);
    XMapWindow(dpy, win);
    Atom WM_DELETE_WINDOW = XInternAtom(dpy, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(dpy, win, &WM_DELETE_WINDOW, 1);
}

void EGLNativeContextX11::exec() {
    while(true) {
        XEvent e;
        XNextEvent(dpy, &e);
        if (e.type == ConfigureNotify) {
            XConfigureEvent xce = e.xconfigure;
            if (width != xce.width || height != xce.height) {
                width = xce.width;
                height = xce.height;
                if (resize_callback) {
                    resize_callback(width, height);
                }
            }
        } else if (e.type == Expose) {
            XExposeEvent xe = e.xexpose;
            if (!xe.count && update_callback) {
                update_callback();
            }
        } else if (e.type == ClientMessage) {
            break;
        }
    }
}

void EGLNativeContextX11::stop() {
    XEvent cm;
    cm.type = ClientMessage;
    cm.xclient.format = 16;

    XSendEvent(dpy, win, False, 0, &cm);
}

void EGLNativeContextX11::update() {
    XEvent xp = {};
    xp.type = Expose;
    xp.xexpose.window = win;
    xp.xexpose.count = 0;

    XSendEvent(dpy, win, false, ExposureMask, &xp);
    XFlush(dpy);
}

}}} // namespace vw::gfx::rc
