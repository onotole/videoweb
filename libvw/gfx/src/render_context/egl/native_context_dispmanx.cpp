#include <thread>
#include <log/log.h>
#include "native_context_dispmanx.h"

namespace vw { namespace gfx { namespace rc {

EGLNativeContextDispmanx::EGLNativeContextDispmanx(int) {}

EGLNativeContextDispmanx::~EGLNativeContextDispmanx() {
    stop();
    if (pix) {
        EGLint * global_pixmap = static_cast<EGLint *>(pix);
        eglDestroyGlobalImageBRCM(global_pixmap);
        EGL_CHECK_ERROR();
    }
    if (dpy) {
        DISPMANX_UPDATE_HANDLE_T dispman_update = vc_dispmanx_update_start(0);
        vc_dispmanx_element_remove(dispman_update, win.element);
        vc_dispmanx_update_submit_sync(dispman_update);
        vc_dispmanx_display_close(dpy);
    }
}

EGLNativeDisplayType EGLNativeContextDispmanx::display() const {
    return EGL_DEFAULT_DISPLAY;
}

EGLNativeWindowType EGLNativeContextDispmanx::window() const {
    return const_cast<EGL_DISPMANX_WINDOW_T*>(&win);
}

EGLNativePixmapType EGLNativeContextDispmanx::pixmap() const  {
    return const_cast<EGLint *>(pix);
}

bool EGLNativeContextDispmanx::ready() const {
    return dpy;
}

void EGLNativeContextDispmanx::init(int w, int h, bool offscr) {
    INFO << "Dispmanx context init...";
    bcm_host_init();

    offscreen = offscr;

    if (w == IRenderContext::DEFAULT || h == IRenderContext::DEFAULT)
        graphics_get_display_size(0, (uint32_t*)(&width), (uint32_t*)(&height));
    else {
        width = w;
        height = h;
    }
    pix[2] = width;
    pix[3] = height;

    dpy = vc_dispmanx_display_open(0);

    if (offscreen) {
        eglCreateGlobalImageBRCM(width, height, pix[4], 0, pix[3]*4, pix);
        EGL_CHECK_ERROR();
    } else {
        DISPMANX_UPDATE_HANDLE_T dispman_update = vc_dispmanx_update_start(0);

        VC_RECT_T dst_rect {0, 0, width, height};
        VC_RECT_T src_rect {0, 0, width << 16, height << 16};
        win.element = vc_dispmanx_element_add(dispman_update, dpy, 0, &dst_rect, 0, &src_rect,
                                              DISPMANX_PROTECTION_NONE, 0, 0, DISPMANX_NO_ROTATE);
        win.width = width;
        win.height = height;

        vc_dispmanx_update_submit_sync(dispman_update);
    }

    io_service.post([this](){ if (resize_callback) resize_callback(width, height); });
}

void EGLNativeContextDispmanx::exec() {
    boost::asio::io_service::work work(io_service);
    io_service.run();
}

void EGLNativeContextDispmanx::stop() {
    io_service.stop();
    while (!io_service.stopped()) {
        std::this_thread::yield();
    }
}

void EGLNativeContextDispmanx::update() {
    io_service.post([this](){ if (update_callback) update_callback(); });
}

void EGLNativeContextDispmanx::swap_buffers() {
    if (offscreen) {
        eglFlushBRCM();
        EGL_CHECK_ERROR();
    }
}

void EGLNativeContextDispmanx::take_snapshot(Image & snapshot) {
    if (pix[0]) {
        snapshot.header.memory = Image::Memory::EGL;
        snapshot.header.width = width;
        snapshot.header.height = height;
        snapshot.data.size = sizeof(BRCMGlobalImage);
        snapshot.data.ptr = pix;
    }
}

EGLImageKHR EGLNativeContextDispmanx::create_egl_image(EGLDisplay dpy, const Image & img) {
    EGLint * global_image = (EGLint *)img.data.ptr;
    TRACE << "global image = " << global_image[0] << ", " << global_image[1] << ", "
          << global_image[2] << ", " << global_image[3] << ", " << global_image[4];
    auto result = eglQueryGlobalImageBRCM(global_image, global_image + 2);
    EGL_CHECK_ERROR();
    if(!result)
        return nullptr;

    EGLImageKHR egl_image = eglCreateImageKHR(dpy, EGL_NO_CONTEXT, EGL_NATIVE_PIXMAP_KHR,
                                              (EGLClientBuffer)global_image, nullptr);
    EGL_CHECK_ERROR();
    return egl_image;
}

void EGLNativeContextDispmanx::destroy_egl_image(EGLDisplay dpy, EGLImageKHR img) {
    eglDestroyImageKHR(dpy, img);
    EGL_CHECK_ERROR();
}

}}} // namespace vw::gfx::rc
