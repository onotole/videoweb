#ifndef __EGL_NATIVE_CONTEXT_DRM_H__
#define __EGL_NATIVE_CONTEXT_DRM_H__

#include <memory>
#include <drm/vw_drm.h>
#include "native_context.h"

namespace vw { namespace gfx { namespace rc {

class EGLNativeContextDrm : public EGLNativeContextBase, public IPulse {
public:
    EGLNativeContextDrm(int drm_fd);
    virtual ~EGLNativeContextDrm();

    EGLNativeDisplayType display() const override;
    EGLNativeWindowType window() const override;
    EGLNativePixmapType pixmap() const override;

    virtual bool ready() const override;

    virtual void init(int w, int h, bool /*TODO: offscreen*/) override;
    virtual void exec() override;
    virtual void stop() override;
    virtual void update() override;

    virtual void swap_buffers() override;

    virtual IPulse * get_pulse() override;
    virtual void set_callback(callback_t &&) override;

private:
    int fd = -1;
    std::unique_ptr<drm::Drm> drm;
    std::unique_ptr<drm::Gbm> gbm;
    callback_t pulse_callback;
};


}}} // namespace vw::gfx::rc


#endif // __EGL_NATIVE_CONTEXT_DRM_H__
