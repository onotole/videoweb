#include <iostream>
#include <vector>
#include <drm/bo.h>
#include <drm/drm_fourcc.h>
#include <image_util.h>
#include "native_context_drm.h"

namespace vw { namespace gfx { namespace rc {

EGLNativeContextDrm::EGLNativeContextDrm(int drm_fd) : fd(drm_fd) {}

EGLNativeContextDrm::~EGLNativeContextDrm() {}

EGLNativeDisplayType EGLNativeContextDrm::display() const {
    return gbm ? gbm->device() : nullptr;
}

EGLNativeWindowType EGLNativeContextDrm::window() const {
    return gbm ? (EGLNativeWindowType)gbm->surface() : (EGLNativeWindowType)nullptr;
}

EGLNativePixmapType EGLNativeContextDrm::pixmap() const {
    return (EGLNativePixmapType)nullptr;
}

void EGLNativeContextDrm::init(int, int, bool) {
    if (drm) return;
    drm.reset(new drm::Drm(fd));
    gbm.reset(new drm::Gbm(drm->fd(), drm->width(), drm->height()));
}

bool EGLNativeContextDrm::ready() const {
    return drm.get();
}

void EGLNativeContextDrm::exec() {
    init(IRenderContext::DEFAULT, IRenderContext::DEFAULT, false);
    if (pulse_callback)
        drm->set_vblank_callback(std::move(pulse_callback));
    if (resize_callback)
        drm->set_resize_callback(std::move(resize_callback));
    if (update_callback)
        drm->set_update_callback(std::move(update_callback));
    return drm->exec(gbm.get());
}

void EGLNativeContextDrm::stop() {
    if (drm)
        return drm->stop();
}

void EGLNativeContextDrm::update() {
    if (drm)
        return drm->update();
}

void EGLNativeContextDrm::swap_buffers() {
    if (drm)
        return drm->page_flip(gbm.get());
}

IPulse * EGLNativeContextDrm::get_pulse() {
    return this;
}

void EGLNativeContextDrm::set_callback(callback_t && cb) {
    if (drm)
        drm->set_vblank_callback(std::move(cb));
    else
        pulse_callback = cb;
}

}}} // namespace vw::gfx::rc
