#ifndef __EGL_NATIVE_CONTEXT_H__
#define __EGL_NATIVE_CONTEXT_H__

#include <log/log.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <render_context/irender_context.h>

#ifndef __FILENAME__
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif
#define EGL_CHECK_ERROR() do { \
    EGLint error = eglGetError(); \
    if (error != EGL_SUCCESS) \
        ERROR << "EGL ERROR: 0x" << std::hex << error << std::dec << " @ " << __FILENAME__ << ":" << __LINE__; \
} while(false)

namespace vw { namespace gfx { namespace rc {

class EGLNativeContextBase {
public:
    virtual ~EGLNativeContextBase() {}
    void set_resize_callback(IRenderContext::resize_callback_t && callback) {
        resize_callback = callback;
    }
    void set_update_callback(IRenderContext::update_callback_t && callback) {
        update_callback = callback;
    }

    virtual void swap_buffers() {}
    virtual IPulse * get_pulse() { return nullptr; }

    virtual void take_snapshot(Image & snapshot) {}

    virtual EGLImageKHR create_egl_image(EGLDisplay, const Image &);
    virtual void destroy_egl_image(EGLDisplay, EGLImageKHR);

    virtual EGLNativeDisplayType display() const = 0;
    virtual EGLNativeWindowType window() const = 0;
    virtual EGLNativePixmapType pixmap() const = 0;

    virtual bool ready() const = 0;

    virtual void init(int w, int h, bool offscr) = 0;
    virtual void exec() = 0;
    virtual void stop() = 0;
    virtual void update() = 0;

protected:
    IRenderContext::resize_callback_t resize_callback;
    IRenderContext::update_callback_t update_callback;

    int width = 0;
    int height = 0;
    bool offscreen = false;
};

}}} // namespace vw::gfx::rc


#endif // __EGL_NATIVE_CONTEXT_H__
