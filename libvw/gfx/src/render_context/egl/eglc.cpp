#include <cassert>
#include <iostream>
#include <log/log.h>
#include <image_util.h>
#include "eglc.h"

// FIXME: clean this mess
#ifdef USE_DISPMANX
    #include "native_context_dispmanx.h"
    typedef vw::gfx::rc::EGLNativeContextDispmanx EGLNativeContext;
#else
    #ifdef USE_DRM
        #include "native_context_drm.h"
        typedef vw::gfx::rc::EGLNativeContextDrm EGLNativeContext;
    #else
        #ifdef USE_X11
            #include "native_context_x11.h"
            typedef vw::gfx::rc::EGLNativeContextX11 EGLNativeContext;
        #endif
    #endif
#endif

namespace vw { namespace gfx { namespace rc {

namespace {
EGLImageKHR eglCreateImageKHR(EGLDisplay dpy, EGLContext ctx, EGLenum target,
                              EGLClientBuffer buffer, const EGLint *attrib_list) {
    static const PFNEGLCREATEIMAGEKHRPROC createImageProc =
            (PFNEGLCREATEIMAGEKHRPROC) eglGetProcAddress("eglCreateImageKHR");
    return createImageProc(dpy, ctx, target, buffer, attrib_list);
}
EGLBoolean eglDestroyImageKHR(EGLDisplay dpy, EGLImageKHR image) {
    static const PFNEGLDESTROYIMAGEKHRPROC destroyImageProc =
            (PFNEGLDESTROYIMAGEKHRPROC) eglGetProcAddress("eglDestroyImageKHR");
    return destroyImageProc(dpy, image);
}
}

EGLRenderContext::EGLRenderContext(int fd) : native_context(new EGLNativeContext(fd)) {}

EGLRenderContext::~EGLRenderContext() {
    delete native_context;

    if (egl_context.dpy) {
        eglMakeCurrent(egl_context.dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(egl_context.dpy, egl_context.surf);
        eglDestroyContext(egl_context.dpy, egl_context.ctx);
        eglTerminate(egl_context.dpy);
    }
}

void EGLRenderContext::set_resize_callback(resize_callback_t && callback) {
    native_context->set_resize_callback(std::move(callback));
}

void EGLRenderContext::set_update_callback(update_callback_t && callback) {
    native_context->set_update_callback(std::move(callback));
}

void EGLRenderContext::init(int w, int h, bool offscreen) {
    if (!native_context->ready()) {
        native_context->init(w, h, offscreen);

        egl_context.offscreen = offscreen;
        egl_context.dpy = eglGetDisplay(native_context->display());
        assert(egl_context.dpy != EGL_NO_DISPLAY);

        int32_t result = eglInitialize(egl_context.dpy, nullptr, nullptr);
        assert(EGL_FALSE != result);

        static const EGLint attr[] = {
            EGL_RED_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_BLUE_SIZE, 8,
            EGL_ALPHA_SIZE, 8,
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_NONE
        };
        static const EGLint ctx_attr[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE
        };
        EGLConfig config;
        EGLint num_config;

        result = eglChooseConfig(egl_context.dpy, attr, &config, 1, &num_config);
        assert(EGL_FALSE != result);

        egl_context.ctx = eglCreateContext(egl_context.dpy, config, EGL_NO_CONTEXT, ctx_attr);
        assert(egl_context.ctx != EGL_NO_CONTEXT);

        if (egl_context.offscreen)
            egl_context.surf = eglCreatePixmapSurface(egl_context.dpy, config, native_context->pixmap(), nullptr);
        else
            egl_context.surf = eglCreateWindowSurface(egl_context.dpy, config, native_context->window(), nullptr);
        assert(egl_context.surf != EGL_NO_SURFACE);

        result = eglMakeCurrent(egl_context.dpy, egl_context.surf, egl_context.surf, egl_context.ctx);
        assert(EGL_FALSE != result);
#if 1
    std::cout << "============================================================================="<< std::endl;
    std::cout << "EGL_VENDOR\t\t\t| "       << eglQueryString(egl_context.dpy, EGL_VENDOR)      << std::endl;
    std::cout << "EGL_VERSION\t\t\t| "      << eglQueryString(egl_context.dpy, EGL_VERSION)     << std::endl;
    std::cout << "EGL_CLIENT_APIS\t\t\t| "  << eglQueryString(egl_context.dpy, EGL_CLIENT_APIS) << std::endl;
    std::cout << "EGL_EXTENSIONS\t\t\t| "   << eglQueryString(egl_context.dpy, EGL_EXTENSIONS)  << std::endl;
    std::cout << "============================================================================="<< std::endl;
#endif
    }
}

void EGLRenderContext::exec() {
    init();
    eglSwapBuffers(egl_context.dpy, egl_context.surf);
    native_context->exec();
}

void EGLRenderContext::stop() {
    native_context->stop();
}

void EGLRenderContext::update() {
    native_context->update();
}

void EGLRenderContext::swap_buffers() {
    eglSwapBuffers(egl_context.dpy, egl_context.surf);
    native_context->swap_buffers();
}

IPulse * EGLRenderContext::get_pulse() {
    return native_context->get_pulse();
}

const Image & EGLRenderContext::snapshot() {
    native_context->take_snapshot(_snapshot);
    return _snapshot;
}

Image * EGLRenderContext::load_image(const Image & img) {
    TRACE << "load_image: " << img;
    if (img.header.memory == Image::Memory::SHM)
        return const_cast<Image *>(&img);
    Image * egl_image = new Image;
    egl_image->header.memory = Image::Memory::EGL;
    egl_image->header.width = img.header.width;
    egl_image->header.height = img.header.height;
    egl_image->data.size = sizeof(EGLImageKHR);
    if (img.header.memory == Image::Memory::TEX) {
        auto texture_id = static_cast<TextureImage*>(img.data.ptr)->id;
        egl_image->data.ptr = eglCreateImageKHR(egl_context.dpy, egl_context.ctx, EGL_GL_TEXTURE_2D_KHR,
                                                (EGLClientBuffer)texture_id, nullptr);
    } else {
        egl_image->data.ptr = native_context->create_egl_image(egl_context.dpy, img);
    }
    return egl_image;
}

void EGLRenderContext::release_image(Image *img) {
    if (!img || img->header.memory == Image::Memory::SHM)
        return;
    if (img->header.memory == Image::Memory::EGL)
        native_context->destroy_egl_image(egl_context.dpy, (EGLImageKHR)img->data.ptr);
    delete img;
}


}}} // namespce vw::gfx::rc
