#ifndef __EGL_NATIVE_CONTEXT_DISPMANX_H__
#define __EGL_NATIVE_CONTEXT_DISPMANX_H__

#include <boost/asio.hpp>
#include <bcm_host.h>
#include "native_context.h"

namespace vw { namespace gfx { namespace rc {

class EGLNativeContextDispmanx : public EGLNativeContextBase {
public:
    explicit EGLNativeContextDispmanx(int);
    virtual ~EGLNativeContextDispmanx();

    EGLNativeDisplayType display() const override;
    EGLNativeWindowType window() const override;
    EGLNativePixmapType pixmap() const override;

    virtual bool ready() const override;

    virtual void init(int w, int h, bool offscr) override;
    virtual void exec() override;
    virtual void stop() override;
    virtual void update() override;
    virtual void swap_buffers() override;

    virtual void take_snapshot(Image & snapshot) override;
    virtual EGLImageKHR create_egl_image(EGLDisplay dpy, const Image & img) override;
    virtual void destroy_egl_image(EGLDisplay dpy, EGLImageKHR img) override;

private:
    DISPMANX_DISPLAY_HANDLE_T dpy = 0;
    EGL_DISPMANX_WINDOW_T win = {};
    EGLint pix[5] = {
        0, 0, 0, 0,
        EGL_PIXEL_FORMAT_ARGB_8888_BRCM |
        EGL_PIXEL_FORMAT_RENDER_GLES2_BRCM |
        EGL_PIXEL_FORMAT_GLES2_TEXTURE_BRCM |
        EGL_PIXEL_FORMAT_RENDER_GLES_BRCM |
        EGL_PIXEL_FORMAT_GLES_TEXTURE_BRCM |
        EGL_PIXEL_FORMAT_RENDER_VG_BRCM |
        EGL_PIXEL_FORMAT_VG_IMAGE_BRCM
    };

    boost::asio::io_service io_service;
};


}}} // namespace vw::gfx::rc

#endif // __EGL_NATIVE_CONTEXT_DISPMANX_H__
