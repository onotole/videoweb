#include "native_context.h"
#include <vector>
#include <log/log.h>
#include <image_util.h>

namespace vw { namespace gfx { namespace rc {
namespace {
EGLImageKHR eglCreateImageKHR(EGLDisplay dpy, EGLContext ctx, EGLenum target,
                              EGLClientBuffer buffer, const EGLint *attrib_list) {
    static const PFNEGLCREATEIMAGEKHRPROC createImageProc =
            (PFNEGLCREATEIMAGEKHRPROC) eglGetProcAddress("eglCreateImageKHR");
    return createImageProc(dpy, ctx, target, buffer, attrib_list);
}
EGLBoolean eglDestroyImageKHR(EGLDisplay dpy, EGLImageKHR image) {
    static const PFNEGLDESTROYIMAGEKHRPROC destroyImageProc =
            (PFNEGLDESTROYIMAGEKHRPROC) eglGetProcAddress("eglDestroyImageKHR");
    return destroyImageProc(dpy, image);
}
}

EGLImageKHR EGLNativeContextBase::create_egl_image(EGLDisplay dpy, const Image & img) {
#ifdef USE_DRM
    if (img.header.memory != Image::Memory::DMA) {
        ERROR << "Unsupported image type";
        return EGL_NO_IMAGE_KHR;
    }
    const DMAImage * image = static_cast<const DMAImage *>(img.data.ptr);

    std::vector<EGLint> attributes = {
        EGL_WIDTH,                      img.header.width,
        EGL_HEIGHT,                     img.header.height,
        EGL_LINUX_DRM_FOURCC_EXT,       (int32_t)img.header.format
    };
    static constexpr EGLint plane_fd[4] = {
        EGL_DMA_BUF_PLANE0_FD_EXT,
        EGL_DMA_BUF_PLANE1_FD_EXT,
        EGL_DMA_BUF_PLANE2_FD_EXT,
        EGL_DMA_BUF_PLANE3_FD_EXT
    };
    static constexpr EGLint plane_offset[4] = {
        EGL_DMA_BUF_PLANE0_OFFSET_EXT,
        EGL_DMA_BUF_PLANE1_OFFSET_EXT,
        EGL_DMA_BUF_PLANE2_OFFSET_EXT,
        EGL_DMA_BUF_PLANE3_OFFSET_EXT
    };
    static constexpr EGLint plane_pitch[4] = {
        EGL_DMA_BUF_PLANE0_PITCH_EXT,
        EGL_DMA_BUF_PLANE1_PITCH_EXT,
        EGL_DMA_BUF_PLANE2_PITCH_EXT,
        EGL_DMA_BUF_PLANE3_PITCH_EXT
    };
    for (uint32_t p = 0; p < image->num_planes; ++p) {
        attributes.push_back(plane_fd[p]); attributes.push_back(image->planes[p].fd);
        attributes.push_back(plane_offset[p]); attributes.push_back(image->planes[p].offset);
        attributes.push_back(plane_pitch[p]); attributes.push_back(image->planes[p].pitch);
    }
    attributes.push_back(EGL_YUV_COLOR_SPACE_HINT_EXT);
    attributes.push_back(EGL_ITU_REC709_EXT);
    attributes.push_back(EGL_SAMPLE_RANGE_HINT_EXT);
    attributes.push_back(EGL_YUV_FULL_RANGE_EXT);
    attributes.push_back(EGL_NONE);
    auto egl_image =  eglCreateImageKHR(dpy, EGL_NO_CONTEXT, EGL_LINUX_DMA_BUF_EXT,
                                        static_cast<EGLClientBuffer>(nullptr), &attributes.front());
    EGL_CHECK_ERROR();
    if (egl_image == EGL_NO_IMAGE_KHR)
        ERROR << "Failed to create egl image: " << img;
    return egl_image;
#else
    return EGL_NO_IMAGE_KHR;
#endif
}

void EGLNativeContextBase::destroy_egl_image(EGLDisplay dpy, EGLImageKHR img) {
    eglDestroyImageKHR(dpy, img);
    EGL_CHECK_ERROR();
}

}}} // namespace vw::gfx::rc
