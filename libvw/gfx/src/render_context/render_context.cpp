#include "render_context/render_context.h"

#ifdef USE_EGL
#include "egl/eglc.h"
typedef vw::gfx::rc::EGLRenderContext RenderContext;
#else
  #ifdef USE_QTGL
  #include "qtgl/qtgl.h"
  typedef vw::gfx::rc::QTGLRenderContext RenderContext;
  #else
    typedef decltype(nullptr) RenderContext;
  #endif
#endif

namespace vw { namespace gfx { namespace rc {

std::unique_ptr<IRenderContext> create_render_context(int fd) {
    return std::unique_ptr<IRenderContext>(new RenderContext(fd));
}

}}} // namespce vw::comp::rc


