#ifndef __VW_GFX_IPC_IMPORT_H__
#define __VW_GFX_IPC_IMPORT_H__

#include <string>
#include <memory>
#include "../image.h"

namespace vw { namespace gfx { namespace ipc {

class ImportBuffer;

class Import {
public:
    Import(const std::string & uuid);
    Import(Import &&);
    ~Import();

    const std::string & uuid() const;
    const Image * load() const;
    void map_fd(int remote, int local);

private:
    std::string _uuid;
    std::unique_ptr<ImportBuffer> _buffer;
};


}}} // namespace vw::gfx::ipc

#endif // __VW_GFX_IPC_IMPORT_H__
