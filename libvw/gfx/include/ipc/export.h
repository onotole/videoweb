#ifndef __VW_GFX_IPC_EXPORT_H__
#define __VW_GFX_IPC_EXPORT_H__

#include <memory>
#include "../image.h"

namespace vw { namespace gfx { namespace ipc {

class ExportPrivate;

class Export {
public:
    struct Packet {
        virtual ~Packet() = default;
        virtual const ImageHandle handle() const = 0;
    };
    Export(const std::string & addr, const std::string & uuid);
    ~Export();

    const std::string & uuid() const;
    void create(const Image::Header & descr);
    void update(std::unique_ptr<Packet> && packet);
    void commit();

private:
    std::unique_ptr<ExportPrivate> _priv;
};


}}} // namespace vw::gfx::ipc

#endif // __VW_GFX_IPC_EXPORT_H__
