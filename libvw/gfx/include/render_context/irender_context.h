#ifndef __GFX_IRENDER_CONTEXT_H__
#define __GFX_IRENDER_CONTEXT_H__

#include <functional>
#include "ipulse.h"
#include "../image.h"

namespace vw { namespace gfx { namespace rc {

struct IRenderContext {
    static constexpr int DEFAULT = -1;
    typedef std::function<void(int w, int h)> resize_callback_t;
    typedef std::function<void()> update_callback_t;

    virtual ~IRenderContext(){}

    virtual void set_resize_callback(resize_callback_t &&) = 0;
    virtual void set_update_callback(update_callback_t &&) = 0;

    virtual void init(int w = DEFAULT, int h = DEFAULT, bool offscreen = false) = 0;
    virtual void exec() = 0;
    virtual void stop() = 0;
    virtual void update() = 0;

    virtual void swap_buffers() = 0;

    virtual const Image & snapshot() = 0;

    virtual Image * load_image(const Image &) = 0;
    virtual void release_image(Image *) = 0;

    // optional support - otherwise nullptr will be returned
    virtual IPulse * get_pulse() = 0;
};

}}} // namespce vw::gfx::rc

#endif // __GFX_IRENDER_CONTEXT_H__
