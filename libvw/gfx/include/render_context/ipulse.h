#ifndef __GFX_IPULSE_H__
#define __GFX_IPULSE_H__

#include <functional>

namespace vw { namespace gfx { namespace rc {

struct IPulse {
    typedef std::function<void()> callback_t;
    virtual ~IPulse() {}
    virtual void set_callback(callback_t &&) = 0;
};

}}} // namespce vw::gfx::rc

#endif // __GFX_IPULSE_H__
