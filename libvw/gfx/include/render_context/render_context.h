#ifndef __GFX_RENDER_CONTEXT_H__
#define __GFX_RENDER_CONTEXT_H__

#include <memory>
#include "irender_context.h"

namespace vw { namespace gfx { namespace rc {

std::unique_ptr<IRenderContext> create_render_context(int fd = 0);

}}} // namespce vw::gfx::rc

#endif // __GFX_RENDER_CONTEXT_H__

