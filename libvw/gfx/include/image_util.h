#ifndef __VW_GFX_IMAGE_UTIL_H__
#define __VW_GFX_IMAGE_UTIL_H__

#include <iostream>
#include "image.h"

namespace vw { namespace gfx {

inline std::ostream & operator << (std::ostream & os, const Image::Memory & mem) {
    static const char* const tags[] = {
        "NONE" ,
        "SHM"  ,
        "EGL"  ,
        "DMA"  ,
        "TEX"
    };
    return os << tags[uint32_t(mem)];
}

inline std::ostream & operator << (std::ostream & os, const Image::Fourcc & fourcc) {
    return os << (std::string)fourcc;
}

inline std::ostream & operator << (std::ostream & os, const Image::Header & hdr) {
    return os << std::dec << "[" << hdr.memory << ":" << hdr.format << "] "
              << hdr.width << "x" << hdr.height;
}

inline std::ostream & operator << (std::ostream & os, const BRCMGlobalImage & img) {
    return os << img.id << ": [0x" << std::hex << img.format << "] "
              << std::dec << img.width << "x" << img.height;
}

inline std::ostream & operator << (std::ostream & os, const TextureImage & img) {
    return os << img.id;
}

inline std::ostream & operator << (std::ostream & os, const DMAImage & img) {
    os << "{";
    std::string prefix;
    for (uint32_t p = 0; p < img.num_planes; ++p) {
        os << prefix << img.planes[p].fd << "@" << img.planes[p].offset << ":"
           << img.planes[p].pitch;
        prefix = ", ";
    }
    return os << "}";
}

inline std::ostream & operator << (std::ostream & os, const Image & img) {
    const auto print_data = [](std::ostream & os, const Image & img) {
        os << img.data.size << ": ";
        switch (img.header.memory) {
            case Image::Memory::TEX:
                os << *static_cast<const TextureImage *>(img.data.ptr); break;
            case Image::Memory::DMA:
                os << *static_cast<const DMAImage *>(img.data.ptr); break;
        }
    };
    os << img.header << " <";
    print_data(os, img);
    return os << ">";
}

}} // namespce vw::gfx

#endif // __VW_GFX_IMAGE_UTIL_H__
