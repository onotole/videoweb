#ifndef __GFX_DRM_SURFACE_H__
#define __GFX_DRM_SURFACE_H__

#include <memory>
#include "bo.h"

struct gbm_surface;

namespace vw { namespace gfx { namespace drm {

class SurfaceImpl;

class Surface {
public:
    Surface(gbm_device * dev, uint32_t w, uint32_t h, bool dumb = false);
    ~Surface();

    uint32_t width() const;
    uint32_t height() const;
    uint32_t format() const;
    gbm_surface * surface();

    Fb * lock(int fd);
    void release();

private:
    std::unique_ptr<SurfaceImpl> _impl;
};

}}}
#endif // __GFX_DRM_SURFACE_H__
