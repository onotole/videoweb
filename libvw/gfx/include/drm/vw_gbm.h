#ifndef __GFX_DRM_GBM_H__
#define __GFX_DRM_GBM_H__

#include <string>
#include "surface.h"

namespace vw { namespace gfx { namespace drm {

class Gbm {
public:
    Gbm(int fd, uint32_t w, uint32_t h);
    ~Gbm();

    uint32_t width() const;
    uint32_t height() const;
    uint32_t format() const;
    int fd() const;

    bool supported(uint32_t format, uint32_t usage) const;

    gbm_device * device() const { return _dev; }
    gbm_surface * surface() const { return _surface; }

    Fb * lock_fb();
    void release(Fb * fb);

    Bo * create(uint32_t w, uint32_t h, uint32_t format);
    void destroy(Bo * bo);

private:
    int _fd                 = -1;
    gbm_device * _dev       = nullptr;
    gbm_surface * _surface  = nullptr;
    uint32_t _format        = -1;
    uint32_t _width         = 0;
    uint32_t _height        = 0;
};

}}}
#endif // __GFX_DRM_GBM_H__
