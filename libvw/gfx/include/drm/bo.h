#ifndef __GFX_DRM_BO_H__
#define __GFX_DRM_BO_H__

#include <cstdint>

struct gbm_bo;
struct gbm_device;

namespace vw { namespace gfx { namespace drm {

class Bo {
public:
    static Bo * create(gbm_device * gbm, int fd, uint32_t w, uint32_t h, uint32_t format);
    static void destroy(gbm_bo *, void * data);

    gbm_bo * bo() { return _bo; }
    int get_prime();

protected:
    Bo(gbm_bo * bo);
    virtual ~Bo();

    gbm_bo * _bo = nullptr;
    int _prime = -1;
};

class Fb : public Bo {
public:
    virtual ~Fb();
    static Fb * get(gbm_bo * bo, int fd);

    int fb_id() const { return _fb_id; }

private:
    Fb(gbm_bo * bo, int fd);
    int _drm_fd = -1;
    uint32_t _fb_id = 0;
};

}}}
#endif // __GFX_DRM_BO_H__
