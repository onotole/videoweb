#ifndef __GFX_DRM_DRM_H__
#define __GFX_DRM_DRM_H__

#include <atomic>
#include <functional>
#include <render_context/ipulse.h>
#include "vw_gbm.h"

namespace vw { namespace gfx { namespace drm {

class Drm {
public:
    typedef std::function<void(int, int)> resize_callback_t;
    typedef std::function<void(void)>     update_callback_t;
    typedef void * mode_info_t;

    explicit Drm(int fd);
    ~Drm();

    uint32_t width() const;
    uint32_t height() const;
    int fd() const;

    void exec(Gbm *);
    void stop();
    void update();
    void page_flip(Gbm *);

    void display(Fb *);

    void set_resize_callback(resize_callback_t &&);
    void set_update_callback(update_callback_t &&);
    void set_vblank_callback(rc::IPulse::callback_t &&);

private:
    int                 _fd = -1;
    mode_info_t       _mode = nullptr;
    uint32_t          _crtc = -1;
    uint32_t     _connector = -1;

    drm::Fb *             _curr = nullptr;
    drm::Fb *             _next = nullptr;

    std::atomic<bool>       _crtc_set;
    std::atomic<bool>       _run;
    std::atomic<bool>       _wait_flip;
    std::atomic<bool>       _update_pending;

    resize_callback_t      _resize_cb;
    update_callback_t      _update_cb;
    rc::IPulse::callback_t _vblank_cb;
};

}}}
#endif // __GFX_DRM_DRM_H__
