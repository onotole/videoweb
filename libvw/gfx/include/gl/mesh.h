#ifndef __GFX_GL_MESH_H__
#define __GFX_GL_MESH_H__

#include <base/types.h>

namespace vw { namespace gfx { namespace gl {

class Mesh {
    struct Vertex {
        float position[3];
        float tex_coor[2];
    };
public:
    Mesh();
    ~Mesh();

    void set_uv_bounds(const base::n_rect_t & r);
    const base::n_rect_t & get_uv_bounds() const;

    void render();

private:
    void create_vertex_buffer();
    void update_vertex_buffer();
    void delete_vertex_buffer();
    void create_index_buffer();
    void delete_index_buffer();

    base::n_rect_t _uv_bounds = { 0.f, 0.f, 1.f, 1.f };
    unsigned int _vertex_buffer = 0U;
    unsigned int _index_buffer = 0U;
    bool _update_pending = false;
};

}}}

#endif // __GFX_GL_MESH_H__
