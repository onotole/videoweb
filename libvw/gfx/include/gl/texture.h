#ifndef __GFX_GL_TEXTURE_H__
#define __GFX_GL_TEXTURE_H__

#include <cstdlib>
#include <cstring>
#include <memory>
#include "../image.h"

namespace vw { namespace gfx { namespace gl {

class Texture {
public:
    Texture();
    Texture(const Texture &) = delete;
    Texture(Texture &&);
    ~Texture();

    uint32_t id() const { return _id; }
    uint32_t width() const { return _descr.width; }
    uint32_t height() const { return _descr.height; }

    void fill(const Image & image);
    void bind();

private:
    void create(const Image::Header & header, const ImageHandle & handle);
    void update(const Image::Header & header, const ImageHandle & handle);
    void release();

    unsigned int _id = -1;
    Image::Header _descr;
};

}}}

#endif // __GFX_GL_TEXTURE_H__
