#ifndef __GFX_GL_SHADER_H__
#define __GFX_GL_SHADER_H__

namespace vw { namespace gfx { namespace gl {

class Shader {
public:
    Shader(const char * vs, const char * fs);
    ~Shader();
    void use() const;

private:
    unsigned int _program = 0;
};

}}}

#endif // __GFX_GL_SHADER_H__
