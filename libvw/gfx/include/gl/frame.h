#ifndef __GFX_GL_FRAME_H__
#define __GFX_GL_FRAME_H__
#include <base/types.h>

namespace vw { namespace gfx { namespace gl {

class Frame {
public:
    void set_viewport(const base::rect_t & vp) { _viewport = vp; }
    const base::rect_t & get_viewport() const { return _viewport; }
    void set();
private:
    base::rect_t _viewport = {0,0,0,0};
};

}}} // namespce vw::gfx::gl

#endif // __GFX_GL_FRAME_H__
