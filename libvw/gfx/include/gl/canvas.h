#ifndef __GFX_GL_CANVAS_H__
#define __GFX_GL_CANVAS_H__
#include <cstdint>

namespace vw { namespace gfx { namespace gl {

class Canvas {
public:
    void set_background(uint32_t argb);
    void set_background(float a, float r, float g, float b);
    void clear();
private:
    struct {
        float a, r, g, b;
    } _background_color;
};

}}} // namespce vw::gfx::gl

#endif // __GFX_GL_CANVAS_H__
