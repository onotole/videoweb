#ifndef __GFX_GL_H__
#define __GFX_GL_H__

namespace vw { namespace gfx { namespace gl {

const char * vendor();
const char * renderer();
const char * version();
const char * sl_version();
const char * extensions();
bool supported(const char * extension);

#define SHADER_POSITION_ATTR_SLOT 0
#define SHADER_TEX_COOR_ATTR_SLOT 1

}}} // namespce vw::gfx::gl

#endif // __GFX_GL_H__
