#ifndef __GFX_GL_SHADER_MANAGER_H__
#define __GFX_GL_SHADER_MANAGER_H__

#include <memory>
#include <map>
#include "shader.h"
#include "../image.h"

namespace vw { namespace gfx { namespace gl {

typedef std::unique_ptr<Shader> shader_ptr_t;

class ShaderManager {
public:
    void use(Image::Memory type) const;
private:
    mutable std::map<Image::Memory, shader_ptr_t> _shaders;
};

}}}

#endif // __GFX_GL_SHADER_MANAGER_H__
