#ifndef __VW_GFX_IMAGE_H__
#define __VW_GFX_IMAGE_H__

#include <cstdlib>
#include <cstdint>
#include <string>

namespace vw { namespace gfx {

typedef void * ImageHandle;

struct Image {
    enum class Memory : uint32_t {
        NONE        = 0U,
        SHM             ,
        EGL             ,
        DMA             ,
        TEX
    };
    class Fourcc {
    public:
        Fourcc(const std::string & fourcc) {
            fmt = uint32_t(fourcc.size() > 0 ? fourcc[0] : ' ')         |
                  uint32_t(fourcc.size() > 1 ? fourcc[1] : ' ') << 8    |
                  uint32_t(fourcc.size() > 2 ? fourcc[2] : ' ') << 16   |
                  uint32_t(fourcc.size() > 3 ? fourcc[3] : ' ') << 24   ;
        }
        Fourcc(const char * fourcc) : Fourcc(std::string(fourcc)) {}
        Fourcc(uint32_t fourcc) : fmt(fourcc) {}
        operator uint32_t() const { return fmt; }
        operator std::string() const {
            const char * c = reinterpret_cast<const char *>(& fmt);
            return {c, c + 4};
        }
    private:
        uint32_t fmt;
    };
    struct Header {
        int32_t width   = 0;
        int32_t height  = 0;
        Memory  memory  = Memory::NONE;
        Fourcc  format  = "NULL";
    } header;
    struct Data {
        uint32_t    size   = 0;
        ImageHandle ptr    = nullptr;
    } data;
};

struct BRCMGlobalImage {
    int32_t id;
    int32_t reserved;
    int32_t width;
    int32_t height;
    int32_t format;
};

struct DMAImage {
    uint32_t num_planes;
    struct {
        int32_t fd;
        int32_t offset;
        int32_t pitch;
    } planes[4] = {};
};

struct TextureImage {
    uint32_t id;
};

}} // namespce vw::gfx

#endif // __VW_GFX_IMAGE_H__
