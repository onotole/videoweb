#ifndef __MEDIA_OMX_UTIL_H__
#define __MEDIA_OMX_UTIL_H__

#include <OMX_Core.h>

namespace vw { namespace media { namespace omx {

const char * event_to_string(OMX_EVENTTYPE event);
const char * command_to_string(OMX_COMMANDTYPE command);
const char * state_to_string(OMX_STATETYPE state);
const char * error_to_string(OMX_ERRORTYPE error);

}}} // namespace vw::video::omx

#endif // __MEDIA_OMX_UTIL_H__
