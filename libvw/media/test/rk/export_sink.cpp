#include <iostream>
#include "export_sink.h"

#define COMPOSITOR_URI "local:///tmp/vw.compositor"
#define SOURCE_UUID "467a9a3b-caab-4301-a68a-562a6f21cd66"

ExportSink::ExportSink()
    : _export(new vw::gfx::ipc::Export(COMPOSITOR_URI, SOURCE_UUID)) {}

ExportSink::~ExportSink() {}

void ExportSink::sink(std::unique_ptr<Frame> && frame) {
    if (!_image_created) {
        _export->create(frame->image().header);
        _image_created = true;
    }
    if (frame->image().data.ptr) {
        _export->update(std::move(frame));
        _export->commit();
    }
}
