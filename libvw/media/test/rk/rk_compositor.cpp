#include <iostream>
#include <list>
#include <thread>
#include <mutex>
#include <gfx/render_context/render_context.h>
#include <io/server.h>
#include <io/stream/server.h>
#include <rpc/dispatcher.h>
#include <rpc/protocol.h>
#include <rpc/json-rpc/protocol.h>
#include "gl_render.h"
#include <gfx/ipc/import.h>
#include <fcntl.h>
#include <unistd.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#define COMPOSITOR_SOCKET "/tmp/vw.compositor"
#define COMPOSITOR_PROTO  "local://"
#define COMPOSITOR_URI    COMPOSITOR_PROTO COMPOSITOR_SOCKET

std::mutex _image_mutex;
const vw::gfx::Image * _render_image = nullptr;

template<typename io_service_t, typename transport_t>
class Compositor {
    typedef vw::io::Server<io_service_t, transport_t> server_t;
    typedef vw::rpc::Proto<vw::rpc::json::Proto> proto_t;
    typedef vw::rpc::Dispatcher<proto_t, typename server_t::socket_t> dispatcher_t;
    typedef std::unique_ptr<dispatcher_t> dispatcher_ptr_t;
    typedef std::unique_ptr<typename server_t::socket_t> socket_ptr_t;
    typedef std::unique_ptr<vw::gfx::ipc::Import> import_ptr_t;
    typedef struct Connection {
        Connection(typename server_t::socket_t && s)
            : socket(new typename server_t::socket_t(std::move(s)))
            , dispatcher(new dispatcher_t(*socket)) {}
        import_ptr_t import;
        socket_ptr_t socket;
        dispatcher_ptr_t dispatcher;
    } connection_t;
public:
    Compositor(io_service_t & io_service, const std::string & address)
        : _server(io_service, address) {
        _server.set_connection_handler([this](typename server_t::socket_t && s) {
            _connections.push_back({std::move(s)});
            auto & conn = _connections.back();
            std::cout << "incomming connection: " << _connections.size() << std::endl;
            conn.dispatcher->register_handler("add_source", [&](const std::string & uuid) {
                std::cout << "add_source: " << uuid << std::endl;
                conn.import.reset(new vw::gfx::ipc::Import(uuid));
                return true;
            });
            conn.dispatcher->register_handler("update", [&](const std::string & uuid) {
                std::lock_guard<std::mutex> lock(_image_mutex);
                _render_image = conn.import->load();
                return true;
            });
            conn.dispatcher->register_handler("fd", [&](int remote, int local) {
                conn.import->map_fd(remote, local);
                return true;
            });
            conn.socket->set_disconnect_handler([&]() {
                std::lock_guard<std::mutex> lock(_image_mutex);
                _render_image = nullptr;
                auto it = std::find_if(_connections.begin(), _connections.end(),
                                       [&conn](const connection_t & c) {
                    return &conn.socket == &c.socket;
                });
                if (it != _connections.end()) {
                    _connections.erase(it);
                }
                std::cout << "connection closed: " << _connections.size() << std::endl;
            });
        });
    }

private:
    server_t _server;
    std::list<connection_t> _connections;
};
typedef boost::asio::io_service io_service_t;
typedef vw::io::stream::LocalServer local_server_t;
typedef Compositor<io_service_t, local_server_t> compositor_t;

int main(int argc, char * argv[]) {
    std::cout << "starting compositor..." << std::endl;
    unlink(COMPOSITOR_SOCKET);
    GlRender render;
    auto context = vw::gfx::rc::create_render_context();
    auto pulse = context->get_pulse();
    if (pulse) {
        pulse->set_callback([&](){ context->update(); });
    }
    context->set_resize_callback([&](int w, int h){
        render.init(w, h);
    });
    context->set_update_callback([&](){
        {
            std::lock_guard<std::mutex> lock(_image_mutex);
            if (_render_image) {
                auto image = context->load_image(*_render_image);
                if (image) render.render(*image);
                context->release_image(image);
            }
        }
        context->swap_buffers();
    });

    io_service_t io_service;
    std::thread ctrl_thread([&](){
        compositor_t compositor(io_service, COMPOSITOR_URI);
        io_service.run();
    });

    context->exec();
    io_service.stop();
    ctrl_thread.join();

    return 0;
}
