#include "render_sink.h"
#include "export_sink.h"

ISink::ptr_t ISink::create() {
    return std::unique_ptr<ISink>(new ExportSink);
}
