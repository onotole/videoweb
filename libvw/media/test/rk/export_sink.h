#ifndef __VW_RK_EXPORT_SINK_H__
#define __VW_RK_EXPORT_SINK_H__

#include <thread>

#include <memory>
#include <gfx/ipc/export.h>

#include "sink.h"

class ExportSink : public ISink {
public:
    ExportSink();
    virtual ~ExportSink();

    virtual void sink(std::unique_ptr<Frame> && frame) override;
private:
    bool _image_created = false;
    std::unique_ptr<vw::gfx::ipc::Export> _export;
};

#endif // __VW_RK_EXPORT_SINK_H__
