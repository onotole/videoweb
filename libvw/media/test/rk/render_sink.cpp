#include "render_sink.h"
#include <gfx/render_context/render_context.h>

RenderSink::RenderSink() {
    _render_thread = std::thread([this](){
        _context = vw::gfx::rc::create_render_context();
        auto pulse = _context->get_pulse();
        if (pulse) {
            pulse->set_callback([&](){ _context->update(); });
        }
        _context->set_resize_callback([&](int w, int h){
            _render.init(w, h);
        });
        _context->set_update_callback([&](){
            std::lock_guard<std::mutex> _(_frame_mutex);
            if (_ren_frame) {
                if(_ren_frame->image().data.size) {
                    auto image = _context->load_image(_ren_frame->image());
                    if (image) _render.render(*image);
                    _context->release_image(image);
                }
            }
            _context->swap_buffers();
            _dec_frame.reset();
        });
        _context->exec();
    });
}

RenderSink::~RenderSink() {
    if (_context)
        _context->stop();
    if (_render_thread.joinable())
        _render_thread.join();
}

void RenderSink::sink(std::unique_ptr<Frame> &&frame) {
    const auto check_ready = [this](){
        std::lock_guard<std::mutex> _(_frame_mutex);
        return _context && !_dec_frame;
    };
    while(!check_ready())
        std::this_thread::yield();
    std::lock_guard<std::mutex> _(_frame_mutex);
    std::swap(_dec_frame, frame);
    std::swap(_dec_frame, _ren_frame);
}
