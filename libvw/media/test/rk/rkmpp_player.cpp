#include <cassert>
#include <fstream>
#include <iostream>
#include <thread>
#include <mutex>
#include <unistd.h>
#include "rkmpp_decoder.h"
#include "sink.h"

void decoder_thread(RkMppDecoder & decoder, ISink::ptr_t & sink) {
    while (true) {
        try {
            auto frame = decoder.get_frame();
            if (frame->image().data.ptr) {
                sink->sink(std::move(frame));
            }
        } catch (...) {
            break;
        }
        std::this_thread::yield();
    }
}

int main(int argc, char * argv[]) {
    if(argc < 2) {
        std::cerr << "Usage: " << argv[0] << " <h264 file>." << std::endl;
        return -1;
    }

    auto sink = ISink::create();
    RkMppDecoder decoder;

    std::thread dec_thread([&](){ decoder_thread(decoder, sink); });

    std::ifstream stream(argv[1], std::ios::binary);
    const size_t buffer_size = 1024*1024; // 1Mb
    char * buffer = new char [buffer_size];
    while(size_t read_size = stream.readsome(buffer, buffer_size)) {
        decoder.write(buffer, read_size);
        std::this_thread::yield();
    }
    stream.close();

    // TODO: wait for eos

    dec_thread.join();
    sink.reset();

    std::cout << "all done." << std::endl;

    return 0;
}
