#ifndef __VW_RK_RENDER_SINK_H__
#define __VW_RK_RENDER_SINK_H__

#include <thread>
#include <mutex>
#include <gfx/render_context/irender_context.h>
#include "sink.h"
#include "gl_render.h"

typedef std::unique_ptr<vw::gfx::rc::IRenderContext> rc_ptr_t;
typedef GlRender render_t;

class RenderSink : public ISink {
public:
    RenderSink();
    virtual ~RenderSink();

    virtual void sink(std::unique_ptr<Frame> &&frame) override;
private:
    std::unique_ptr<Frame> _dec_frame;
    std::unique_ptr<Frame> _ren_frame;
    std::mutex   _frame_mutex;
    std::thread  _render_thread;
    rc_ptr_t     _context;
    render_t     _render;
};

#endif // __VW_RK_RENDER_SINK_H__
