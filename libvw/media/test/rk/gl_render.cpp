#include <cmath>
#include <iostream>
#include <memory>
#include <gfx/gl/gl.h>
#include <gfx/gl/canvas.h>
#include <gfx/gl/shader_manager.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/texture.h>
#include "gl_render.h"

class GlPrivate {
public:
    GlPrivate(int w, int h);
    void render(const vw::gfx::Image &);
private:
    vw::gfx::gl::Canvas         _canvas;
    vw::gfx::gl::ShaderManager  _shader;
    vw::gfx::gl::Frame          _frame;
    vw::gfx::gl::Mesh           _mesh;
    vw::gfx::gl::Texture        _texture;
};

GlPrivate::GlPrivate(int w, int h) {
    std::cout << "=============================================================================" << std::endl;
    std::cout << "SURFACE_SIZE\t\t\t| "            << w << "x" << h                              << std::endl;
    std::cout << "GL_VENDOR\t\t\t| "               << vw::gfx::gl::vendor()                      << std::endl;
    std::cout << "GL_RENDERER\t\t\t| "             << vw::gfx::gl::renderer()                    << std::endl;
    std::cout << "GL_VERSION\t\t\t| "              << vw::gfx::gl::version()                     << std::endl;
    std::cout << "GL_SHADING_LANGUAGE_VERSION\t| " << vw::gfx::gl::sl_version()                  << std::endl;
    std::cout << "GL_EXTENSIONS\t\t\t| "           << vw::gfx::gl::extensions()                  << std::endl;
    std::cout << "=============================================================================" << std::endl;

    _frame.set_viewport({10, 10, w-20, h-20});
}

void GlPrivate::render(const vw::gfx::Image & image) {
    _shader.use(vw::gfx::Image::Memory::DMA);
    _texture.fill(image);
    float r(0.f), g(.2f), b(.8f);
    _canvas.set_background(1.f, r, g, b);
    _canvas.clear();
    _frame.set();
    _mesh.render();
}

GlRender::GlRender() {}

GlRender::~GlRender() {}

void GlRender::init(int w, int h) {
    _gl.reset(new GlPrivate(w, h));
}

void GlRender::render(const vw::gfx::Image & image) {
    if (_gl)
        _gl->render(image);
}
