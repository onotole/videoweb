#ifndef __VW_RK_GL_RENDER_H__
#define __VW_RK_GL_RENDER_H__

#include <gfx/image.h>
#include <memory>

class GlPrivate;

class GlRender {
public:
    typedef void * Image;
    GlRender();
    ~GlRender();

    void init(int w, int h);
    void render(const vw::gfx::Image & image);
private:
    std::unique_ptr<GlPrivate> _gl;
};

#endif // __VW_RK_GL_RENDER_H__
