#ifndef __VW_RK_SINK_H__
#define __VW_RK_SINK_H__

#include <memory>
#include "frame.h"

struct ISink {
    typedef std::unique_ptr<ISink> ptr_t;
    static ptr_t create();

    virtual ~ISink(){}

    virtual void sink(std::unique_ptr<Frame> && frame) = 0;
};

#endif // __VW_RK_GL_RENDER_H__
