#include <cmath>
#include <fstream>
#include <iostream>
#include <chrono>
#include <thread>
#include <memory>
#include <bcm_host.h>
#include <log/log.h>
#include <gfx/render_context/render_context.h>
#include <gfx/gl/canvas.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/shader_manager.h>
#include <gfx/gl/texture.h>
#include <gfx/gl/gl.h>

const char gobal_image_file[] = "/tmp/vw.media.test.image";

class Pulse : public vw::gfx::rc::IPulse {
public:
    Pulse(const bool & run) :
        _run(run), _pulse_thread([this]() {
            usleep(20000);
            while (_run) {
                if (_callback)
                    _callback();
                usleep(1000000/30);
            }
        })
    {}
    virtual ~Pulse() {
        if (_pulse_thread.joinable())
            _pulse_thread.join();
    }
    virtual void set_callback(callback_t && cb) override {
        _callback = cb;
    }
private:
    const bool & _run;
    std::thread  _pulse_thread;
    callback_t   _callback;
};

class GlRender {
public:
    GlRender(int w, int h) {
        std::cout << "=============================================================================" << std::endl;
        std::cout << "SURFACE_SIZE\t\t\t| "            << w << "x" << h                              << std::endl;
        std::cout << "GL_VENDOR\t\t\t| "               << vw::gfx::gl::vendor()                      << std::endl;
        std::cout << "GL_RENDERER\t\t\t| "             << vw::gfx::gl::renderer()                    << std::endl;
        std::cout << "GL_VERSION\t\t\t| "              << vw::gfx::gl::version()                     << std::endl;
        std::cout << "GL_SHADING_LANGUAGE_VERSION\t| " << vw::gfx::gl::sl_version()                  << std::endl;
        std::cout << "GL_EXTENSIONS\t\t\t| "           << vw::gfx::gl::extensions()                  << std::endl;
        std::cout << "=============================================================================" << std::endl;

        _shader.use(vw::gfx::Image::Memory::EGL);
        _frame.set_viewport({10, 10, w-20, h-20});
    }
    void render(const vw::gfx::Image & image) {
        _texture.fill(image);
        float r(.010f * _counter), g(.012f * _counter), b(.015f * _counter++), i;
        _canvas.set_background(1.f, std::modf(r, &i), std::modf(g, &i), std::modf(b, &i));
        _canvas.clear();
        _frame.set();
        _mesh.render();
    }
private:
    size_t _counter = 0;
    vw::gfx::gl::Canvas         _canvas;
    vw::gfx::gl::ShaderManager  _shader;
    vw::gfx::gl::Frame          _frame;
    vw::gfx::gl::Mesh           _mesh;
    vw::gfx::gl::Texture        _texture;
};

int main() {
    std::unique_ptr<GlRender> render;
    auto context = vw::gfx::rc::create_render_context();

    vw::gfx::Image image;
    std::ifstream image_file(gobal_image_file);
    vw::gfx::BRCMGlobalImage * brcm_img = new vw::gfx::BRCMGlobalImage();
    image_file >> brcm_img->id >> brcm_img->reserved >> brcm_img->width >> brcm_img->height >> brcm_img->format;
    image_file.close();
    image.header.memory = vw::gfx::Image::Memory::EGL;
    image.header.format = "RX24";
    image.header.width = brcm_img->width;
    image.header.height = brcm_img->height;
    image.data.size = sizeof(vw::gfx::BRCMGlobalImage);
    image.data.ptr = brcm_img;

    context->set_resize_callback([&](int w, int h){
        render.reset(new GlRender(w, h));
    });
    context->set_update_callback([&](){
        auto egl_image = context->load_image(image);
        if (render && egl_image)
            render->render(*egl_image);
        context->release_image(egl_image);
        context->swap_buffers();
    });

    bool run = true;
    Pulse pulse(run);
    pulse.set_callback([&](){
        context->update();
    });

    context->exec();
    run = false;

    return 0;
}
