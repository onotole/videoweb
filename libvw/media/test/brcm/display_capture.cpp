#undef NDEBUG
#include <cassert>
#include <bcm_host.h>
#include <log/log.h>
#include <omx/component.h>

static const VC_IMAGE_TYPE_T vc_image_types[] = {
	VC_IMAGE_RGB565,
	VC_IMAGE_BGR888
};

static const OMX_COLOR_FORMATTYPE vc_format_types[] = {
	OMX_COLOR_Format16bitRGB565,
	OMX_COLOR_Format24bitBGR888
};

static void print_def(OMX_PARAM_PORTDEFINITIONTYPE def)
{
	printf("Port %u: %s %u/%u %u %u %s,%s,%s %ux%u %ux%u @%u %u\n",
		   def.nPortIndex,
		   def.eDir == OMX_DirInput ? "in" : "out",
		   def.nBufferCountActual,
		   def.nBufferCountMin,
		   def.nBufferSize,
		   def.nBufferAlignment,
		   def.bEnabled ? "enabled" : "disabled",
		   def.bPopulated ? "populated" : "not pop.",
		   def.bBuffersContiguous ? "contig." : "not cont.",
		   def.format.video.nFrameWidth,
		   def.format.video.nFrameHeight,
		   def.format.video.nStride,
		   def.format.video.nSliceHeight,
		   def.format.video.xFramerate,
		   def.format.video.eColorFormat);
}

void try_snapshot(size_t index, int width, int height) {
	DISPMANX_DISPLAY_HANDLE_T display;
	DISPMANX_RESOURCE_HANDLE_T resource;
	VC_IMAGE_TYPE_T type = vc_image_types[index];

	display = vc_dispmanx_display_open(0);
	uint32_t vc_img_ptr;
	resource = vc_dispmanx_resource_create(type, width, height, &vc_img_ptr);

	int result = vc_dispmanx_snapshot(display, resource, (DISPMANX_TRANSFORM_T)0);
	INFO << "snapshot: index[" << index << "]: "
		 << "width = " << width
		 << ", height = " << height
		 << ", result = " << result;
	assert(result == 0);

	result = vc_dispmanx_resource_delete(resource);
	assert(result == 0);
	result = vc_dispmanx_display_close(display);
	assert(result == 0);
}

void try_encode(size_t index, int width, int height) {
	vw::media::omx::Component encoder("OMX.broadcom.video_encode");
	vw::media::omx::OMX_ParamPortDefinition def;

	def.nPortIndex = encoder.input()->index();
	encoder.get_parameter(OMX_IndexParamPortDefinition, &def);

	print_def(def);

	def.format.video.nFrameWidth = width;
	def.format.video.nFrameHeight = height;
	def.format.video.xFramerate = 30 << 16;
	def.format.video.nSliceHeight = def.format.video.nFrameHeight;
	def.format.video.nStride = def.format.video.nFrameWidth;
	def.format.video.eColorFormat = vc_format_types[index];

	print_def(def);

	auto result = encoder.set_parameter(OMX_IndexParamPortDefinition, &def);
	INFO << "encode: index[" << index << "]: "
		 << "width = " << width
		 << ", height = " << height
		 << ", result = " << result;
}

int main() {
	bcm_host_init();
	OMX_Init();

	try_snapshot(0, 640, 360);
	try_encode(0, 640, 360);

	OMX_Deinit();
	return 0;
}
