#include <iostream>
#include <memory>
#include <boost/program_options.hpp>
#include <bcm_host.h>

#include "video_player.h"
#include "egl_player.h"

enum Renderer { DISPMANX, EGL };

std::istream & operator >> (std::istream & in, Renderer & unit) {
	std::string token; in >> token;
	if (token == "egl") unit = EGL;
	else if (token == "dispmanx") unit = DISPMANX;
	else throw boost::program_options::invalid_option_value("Unsuported renderer");
	return in;
}

int main(int argc, char** argv) {
	namespace tp = vw::media::test;
	namespace po = boost::program_options;

	bool no_sync, use_global_image;
	size_t num_buffers;
	Renderer renderer;
	std::string file;

	po::options_description opt("Usage");
	opt.add_options()
			("help,h", "print this help message")
			("no-sync,n", "ignore media clock")
			("renderer,r", po::value<Renderer>(&renderer)->
				default_value(DISPMANX), "set renderer type (dispmanx|egl)")
			("num-buffers,b", po::value<size_t>(&num_buffers)->
				default_value(1), "set number of egl buffers")
			("use-global-image,g", "use global egl images")
			("file", po::value<std::string>(&file)->
				required(), "file to play");
	po::positional_options_description pos;
	pos.add("file", 1);

	po::variables_map vm;
	try {
		po::store(po::command_line_parser(argc, argv).options(opt).positional(pos).run(), vm);
		if (vm.count("help")) {
			std::cout << opt << std::endl;
			return 0;
		}
		po::notify(vm);
	} catch( const std::exception & e) {
		std::cerr << e.what() << std::endl;
		std::cout << opt << std::endl;
		return -1;
	}

	no_sync = vm.count("no-sync");
	use_global_image = vm.count("use-global-image");

    bcm_host_init();

	std::unique_ptr<tp::BasePlayer> player;
	switch (renderer) {
		case DISPMANX:
			player.reset(new tp::VideoPlayer(file, !no_sync));
			break;
		case EGL:
			player.reset(new tp::EglPlayer(file, num_buffers, !no_sync, use_global_image));
			break;
	};
	player->play();

	std::cout << player->stats() << std::endl;

	return 0;
}
