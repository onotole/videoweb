file(GLOB_RECURSE ALL_SOURCES "*.cpp" "*.c")
add_custom_target(show_all_rpi_test_sources SOURCES ${ALL_SOURCES})

find_package(Boost COMPONENTS program_options system thread log)
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})


find_package(Threads)
include_directories(../../omx)

# rpi_player
add_executable(rpi_player rpi_player.cpp base_player.cpp video_player.cpp egl_player.cpp)
target_link_libraries(rpi_player ${OMXIL_LIBRARIES} ${Boost_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT} vw.log vw.media.omx vw.gfx.rc vw.gfx.gl)
install(TARGETS rpi_player DESTINATION ${VW_IMAGE_SHARE}/vw/test)

# rpi_compositor
add_executable(rpi_compositor rpi_compositor.cpp)
target_link_libraries(rpi_compositor ${Boost_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT} vw.log vw.gfx.rc vw.gfx.gl)
install(TARGETS rpi_compositor DESTINATION ${VW_IMAGE_SHARE}/vw/test)

# display_capture
add_executable(display_capture display_capture.cpp)
target_link_libraries(display_capture ${Boost_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT} vw.log vw.media.omx)
install(TARGETS display_capture DESTINATION ${VW_IMAGE_SHARE}/vw/test)

