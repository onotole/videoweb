#ifndef __MEDIA_BRCM_BASE_PLAYER_H__
#define __MEDIA_BRCM_BASE_PLAYER_H__

#include <string>
#include <fstream>
#include <chrono>
#include <omx/component.h>

namespace vw { namespace media { namespace test {

class BasePlayer {
	struct OMXInit {
		OMXInit() { OMX_Init(); }
		~OMXInit() { OMX_Deinit(); }
	};

public:
	struct Stats {
		size_t frames_played;
		size_t frames_skipped;
		size_t frames_discarded;
		size_t play_time;
		float fps;
		friend std::ostream & operator << (std::ostream & os, const Stats & stats) {
			os << "Done " << stats.frames_played << " frames, skipped "
			   << stats.frames_skipped << ", discarded " << stats.frames_discarded
			   << " in " << stats.play_time << " ms, fps = " << stats.fps;
			return os;
		}
	};

	BasePlayer(const std::string & file, bool sync = true);

	void play();
	const Stats & stats() const;
	virtual omx::Component & renderer() = 0;

protected:
	virtual void init_renderer() = 0;
	virtual void update_stats() = 0;

	OMXInit _omx_init;

	omx::Component _decoder;
	omx::Component _scheduler;
	omx::Component _clock;

	Stats _stats = {};
	std::chrono::high_resolution_clock::time_point _start_time;

private:
	void complete_graph();
	void complete_graph_sync();

	bool _sync;
	std::ifstream _file;
};


}}} // namespace vw::media::test

#endif // __MEDIA_BRCM_BASE_PLAYER_H__
