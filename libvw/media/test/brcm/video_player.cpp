#include "video_player.h"

namespace vw { namespace media { namespace test {

VideoPlayer::VideoPlayer(const std::string & file, bool sync)
	: BasePlayer(file, sync), _renderer("OMX.broadcom.video_render") {}

omx::Component & VideoPlayer::renderer() {
	return _renderer;
}

void VideoPlayer::init_renderer() {
	_renderer.set_state(OMX_StateExecuting);
}

void VideoPlayer::update_stats() {
	auto elapsed = std::chrono::high_resolution_clock::now() - _start_time;
	omx::OMX_ConfigBrcmPortStats port_stats;
	port_stats.nPortIndex = _renderer.input()->index();
	_renderer.get_parameter(OMX_IndexConfigBrcmPortStats, &port_stats);
	_stats.frames_played = port_stats.nFrameCount;
	_stats.frames_skipped = port_stats.nFrameSkips;
	_stats.frames_discarded = port_stats.nDiscards;
	_stats.play_time = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
	_stats.fps = (_stats.frames_played * 1000.f) / _stats.play_time;
}

}}} // namespace vw::media::test
