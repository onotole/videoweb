#ifndef __MEDIA_BRCM_EGL_PLAYER_H__
#define __MEDIA_BRCM_EGL_PLAYER_H__

#include <thread>
#include <vector>
#include <gfx/render_context/render_context.h>
#include <gfx/gl/texture.h>
#include "base_player.h"

namespace vw { namespace media { namespace test {

class GlRender;

class EglPlayer : public BasePlayer {
public:
    EglPlayer(const std::string & file, uint32_t num_buffers, bool sync = true,
              bool use_global_image = false);
    ~EglPlayer();

    virtual omx::Component & renderer() override;

protected:
    virtual void init_renderer() override;
    virtual void update_stats() override;

private:
    void add_buffer();

    std::unique_ptr<gfx::rc::IRenderContext> context;
    GlRender * render = nullptr;
    omx::Component _renderer;

    struct Buffer {
        gfx::Image * image;
        gfx::gl::Texture texture;
    };

    std::vector<Buffer> _buffers;
    size_t _current_buffer = -1;

    bool _use_global_image;
    bool _export_image = true;
    std::thread _render_thread;
    bool _run = true;
    uint32_t _frame_counter = 0;
};


}}} // namespace vw::media::test

#endif // __MEDIA_BRCM_EGL_PLAYER_H__
