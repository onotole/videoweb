#ifndef __MEDIA_BRCM_VIDEO_PLAYER_H__
#define __MEDIA_BRCM_VIDEO_PLAYER_H__

#include "base_player.h"

namespace vw { namespace media { namespace test {

class VideoPlayer : public BasePlayer {
public:
	VideoPlayer(const std::string & file, bool sync = true);

	virtual omx::Component & renderer() override;

protected:
	virtual void init_renderer() override;
	virtual void update_stats() override;

private:
	omx::Component _renderer;
};


}}} // namespace vw::media::test

#endif // __MEDIA_BRCM_VIDEO_PLAYER_H__
