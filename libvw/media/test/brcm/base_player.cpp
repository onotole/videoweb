#include <log/log.h>
#include "base_player.h"

namespace vw { namespace media { namespace test {

BasePlayer::BasePlayer(const std::string & file, bool sync)
	: _decoder("OMX.broadcom.video_decode"), _scheduler("OMX.broadcom.video_scheduler")
	, _clock("OMX.broadcom.clock"), _sync(sync)
	, _file(file, std::ifstream::binary | std::ifstream::in) {
	omx::OMX_VideoParamPortFormat format;
	format.eCompressionFormat = OMX_VIDEO_CodingAVC;
	format.nPortIndex = _decoder.input()->index();
	_decoder.set_parameter(OMX_IndexParamVideoPortFormat, &format);
	TRACE << "Enabling vdec input port...";
	_decoder.input()->enable();
	TRACE << "Allocating vdec input buffers...";
	_decoder.input()->alloc_buffers();
	TRACE << "vdec input buffers allocated.";

	if (_sync) {
		omx::OMX_TimeConfigClockState cstate;
		cstate.eState = OMX_TIME_ClockStateWaitingForStartTime;
		cstate.nWaitMask = 1;
		_clock.set_parameter(OMX_IndexConfigTimeClockState, &cstate);
		_clock.output()->connect(_scheduler.input(1));
	}

	_decoder.set_event_handler(OMX_EventPortSettingsChanged, [&](OMX_U32 d1, OMX_U32, OMX_PTR) {
		if (_sync) complete_graph_sync();
		else complete_graph();
		_start_time = std::chrono::high_resolution_clock::now();
		return OMX_ErrorNone;
	});

	_decoder.set_state(OMX_StateExecuting);
	TRACE << "vdec -> executing...";

	if (_sync) {
		_clock.set_state(OMX_StateExecuting);
		TRACE << "clock -> executing...";
	}
}

void BasePlayer::play() {
	bool start_set = false;
	while (!_file.eof()) {
		auto & buffer = _decoder.input()->get_buffer();

		_file.read((char *)buffer.data(), buffer.size());
		buffer.header()->nOffset = 0;
		if (!start_set) {
			start_set = true;
			buffer.header()->nFlags = OMX_BUFFERFLAG_STARTTIME;
		} else {
			buffer.header()->nFlags = OMX_BUFFERFLAG_TIME_UNKNOWN;
		}
		if (_file) {
			buffer.header()->nFilledLen = buffer.size();
		} else {
			buffer.header()->nFilledLen = _file.gcount();
			buffer.header()->nFlags |= OMX_BUFFERFLAG_EOS;
		}
		_decoder.input()->process_buffer(buffer);
	}
	_file.close();

	renderer().wait(OMX_EventBufferFlag);
	update_stats();
}

const BasePlayer::Stats & BasePlayer::stats() const {
	return _stats;
}

void BasePlayer::complete_graph() {
	_decoder.output()->connect(renderer().input());
	TRACE << "decoder -> renderer connected...";
	init_renderer();
}

void BasePlayer::complete_graph_sync() {
	_decoder.output()->connect(_scheduler.input());
	TRACE << "decoder -> scheduler connected...";
	_scheduler.set_state(OMX_StateExecuting);
	TRACE << "scheduler -> executing...";
	_scheduler.output()->connect(renderer().input());
	TRACE << "scheduler -> renderer connected...";
	init_renderer();
}

}}} // namespace vw::media::test
