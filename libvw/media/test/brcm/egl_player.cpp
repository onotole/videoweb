#undef NDEBUG
#include <cassert>
#include <cmath>
#include <chrono>
#include <bcm_host.h>
#include <log/log.h>
#include <gfx/gl/gl.h>
#include <gfx/gl/canvas.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/shader_manager.h>
#include "egl_player.h"

const char gobal_image_file[] = "/tmp/vw.media.test.image";

namespace vw { namespace media { namespace test {

class GlRender {
public:
    GlRender(int w, int h) {
        std::cout << "=============================================================================" << std::endl;
        std::cout << "SURFACE_SIZE\t\t\t| "            << w << "x" << h                              << std::endl;
        std::cout << "GL_VENDOR\t\t\t| "               << vw::gfx::gl::vendor()                      << std::endl;
        std::cout << "GL_RENDERER\t\t\t| "             << vw::gfx::gl::renderer()                    << std::endl;
        std::cout << "GL_VERSION\t\t\t| "              << vw::gfx::gl::version()                     << std::endl;
        std::cout << "GL_SHADING_LANGUAGE_VERSION\t| " << vw::gfx::gl::sl_version()                  << std::endl;
        std::cout << "GL_EXTENSIONS\t\t\t| "           << vw::gfx::gl::extensions()                  << std::endl;
        std::cout << "=============================================================================" << std::endl;

        _shader.use(vw::gfx::Image::Memory::EGL);
        _frame.set_viewport({10, 10, w-20, h-20});
    }
    void render(vw::gfx::gl::Texture & texture) {
        float r(.010f * _counter), g(.012f * _counter), b(.015f * _counter++), i;
        _canvas.set_background(1.f, std::modf(r, &i), std::modf(g, &i), std::modf(b, &i));
        _canvas.clear();
        _frame.set();
        texture.bind();
        _mesh.render();
    }
private:
    size_t _counter = 0;
    vw::gfx::gl::Canvas         _canvas;
    vw::gfx::gl::ShaderManager  _shader;
    vw::gfx::gl::Frame          _frame;
    vw::gfx::gl::Mesh           _mesh;
};

EglPlayer::EglPlayer(const std::string & file, uint32_t num_buffers, bool sync, bool use_global_image)
    : BasePlayer(file, sync), _renderer("OMX.broadcom.egl_render"), _use_global_image(use_global_image) {
    _buffers.reserve(num_buffers);
    _renderer.set_fill_buffer_handler([this](OMX_BUFFERHEADERTYPE * buffer){
        _current_buffer = reinterpret_cast<size_t>(buffer->pAppPrivate);
        TRACE << "frame: " << _frame_counter << ", swaping buffer: " << _current_buffer;
        context->update();
        auto & buf = _renderer.output()->get_buffer();
        _renderer.output()->process_buffer(buf);
        ++_frame_counter;
        return OMX_ErrorNone;
    });
    _render_thread = std::thread([this, num_buffers]() {
        context = gfx::rc::create_render_context();
        context->set_resize_callback([this, num_buffers](int w, int h){
            render = new GlRender(w, h);
            for (size_t i = 0; i < num_buffers; ++i) add_buffer();
        });
        context->set_update_callback([this](){
            if (render)
                render->render(_buffers[_current_buffer].texture);
            context->swap_buffers();
            if (_use_global_image && _export_image) {
                const auto & image = context->snapshot();
                std::ofstream shared_file(gobal_image_file);
                int * data = static_cast<int *>(image.data.ptr);
                for (size_t i = 0; i < 5; ++i)
                    shared_file << data[i] << std::endl;
                shared_file.close();
                _export_image = false;
            }
        });
        context->init(gfx::rc::IRenderContext::DEFAULT,
                      gfx::rc::IRenderContext::DEFAULT,
                      _use_global_image);
        context->exec();
    });
}

EglPlayer::~EglPlayer() {
    if (context)
        context->stop();
    _render_thread.join();
    delete render;
}

omx::Component & EglPlayer::renderer() {
    return _renderer;
}

void EglPlayer::init_renderer() {
    while (_buffers.empty())
        std::this_thread::yield();
    omx::OMX_ConfigPortBoolean config_port;
    config_port.nPortIndex = _renderer.input()->index();
    config_port.bEnabled = OMX_FALSE;
    TRACE << "disabling frame discard...";
    _renderer.set_parameter(OMX_IndexParamBrcmVideoEGLRenderDiscardMode, &config_port);

    TRACE << "allocating egl buffers (" << _buffers.size() << ")...";
    _renderer.output()->alloc_buffers(_buffers.size(), true);

    TRACE << "enabling output port...";
    _renderer.output()->enable();

    TRACE << "using egl images...";
    for (auto & buffer : _buffers)
        _renderer.output()->use_egl_image(buffer.image->data.ptr);

    TRACE << "executing renderer...";
     _renderer.set_state(OMX_StateExecuting);

    TRACE << "filling egl buffers...";
    for (size_t b = 0; b < _buffers.size(); ++b) {
        auto & buffer = _renderer.output()->get_buffer();
        _renderer.output()->process_buffer(buffer);
    }
}

void EglPlayer::update_stats() {
    auto elapsed = std::chrono::high_resolution_clock::now() - _start_time;
    _stats.frames_played = _frame_counter;
    _stats.frames_skipped = 0;
    _stats.frames_discarded = 0;
    _stats.play_time = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    _stats.fps = (_stats.frames_played * 1000.f) / _stats.play_time;
}

void EglPlayer::add_buffer() {
    Buffer buffer;

    gfx::Image image;
    image.header.memory = gfx::Image::Memory::SHM;
    image.header.format = "RX24";
    image.header.width = 1920;
    image.header.height = 1080;
    buffer.texture.fill(image);

    image.header.memory = gfx::Image::Memory::TEX;
    image.data.size = sizeof(gfx::TextureImage *);
    image.data.ptr = new gfx::TextureImage{buffer.texture.id()};
    buffer.image = context->load_image(image);
    assert(buffer.image);

    _buffers.push_back(std::move(buffer));
}

}}} // namespace vw::media::test
