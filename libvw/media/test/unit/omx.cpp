#define BOOST_TEST_MODULE vw.lib.media.omx
#define BOOST_TEST_DYN_LINK

#include <string>
#include <vector>
#include <boost/test/unit_test.hpp>
#ifdef OMX_BRCM
#include <bcm_host.h>
#endif

BOOST_AUTO_TEST_CASE(stub) {
    BOOST_CHECK(true);
}

#ifdef USE_OMX
#include <omx/component.h>

using namespace vw::media::omx;

struct ComponentDef {
    std::string name;
    std::vector<size_t> inputs;
    std::vector<size_t> outputs;
};

ComponentDef components [] = {
    #ifdef OMX_BELLAGIO
    { "OMX.st.video.scheduler", {0, 2}, {1} }
    #endif
    #ifdef OMX_BRCM
    { "OMX.broadcom.video_decode", {130}, {131} },
    { "OMX.broadcom.video_scheduler", {10, 12}, {11} },
    { "OMX.broadcom.video_render", {90}, {} },
    { "OMX.broadcom.clock", {}, {80, 81, 82, 83, 84, 85} }
    #endif
};

struct Config {
    Config() {
#ifdef OMX_BRCM
        bcm_host_init();
#endif
        OMX_Init();
    }
    ~Config() { OMX_Deinit(); }
};

BOOST_GLOBAL_FIXTURE(Config);

BOOST_AUTO_TEST_CASE(component_pins) {
    for (const auto & def : components) {
        Component component(def.name);
        BOOST_CHECK(component.handle());
        BOOST_CHECK_EQUAL(component.name(), def.name);
        for (size_t i = 0; i < def.inputs.size(); ++i) {
            BOOST_CHECK_EQUAL(component.input(i)->index(), def.inputs[i]);
        }
        for (size_t o = 0; o < def.outputs.size(); ++o) {
            BOOST_CHECK_EQUAL(component.output(o)->index(), def.outputs[o]);
        }
    }
}

#ifdef OMX_BRCM

struct Pipeline {
    Component clock        {"OMX.broadcom.clock"};
    Component decoder      {"OMX.broadcom.video_decoder"};
    Component scheduler    {"OMX.broadcom.video_scheduler"};
    Component renderer     {"OMX.broadcom.egl_render"};
};

BOOST_AUTO_TEST_CASE(pipeline_ctor_dtor) {

}
#endif

#endif
