#define BOOST_TEST_MODULE comp.ShmVideoSink
#define BOOST_TEST_DYN_LINK

#include <thread>
#include <memory>
#include <boost/test/unit_test.hpp>
#include <gst/gst.h>
#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <io/server.h>
#include <io/stream/server.h>
#include <rpc/protocol.h>
#include <rpc/dispatcher.h>
#include <rpc/json-rpc/protocol.h>
#include "config.h"

struct Config {
    Config() {
        auto & mts = boost::unit_test::framework::master_test_suite();
        setenv("GST_DEBUG", "*:3", 1);
        setenv("LD_LIBRARY_PATH", LD_LIBRARY_PATH, 1);
        setenv("GST_PLUGIN_PATH", GST_PLUGIN_PATH, 1);
        gst_init(&mts.argc, &mts.argv);
    }
};

BOOST_GLOBAL_FIXTURE(Config);

struct MockCompositor {
    typedef boost::asio::io_service io_service_t;
    typedef vw::io::Server<io_service_t, vw::io::stream::LocalServer> server_t;
    typedef vw::rpc::Proto<vw::rpc::json::Proto> proto_t;
    typedef vw::rpc::Dispatcher<proto_t, server_t::socket_t> dispatcher_t;
    typedef std::unique_ptr<server_t> server_ptr_t;
    typedef std::unique_ptr<dispatcher_t> dispatcher_ptr_t;
    typedef std::unique_ptr<server_t::socket_t> socket_ptr_t;
    static bool request_handler(const std::string &) { return true; }
    MockCompositor() {
        boost::system::error_code ec;
        boost::filesystem::remove("/tmp/boost_interprocess/shmvideosink_test", ec);
        boost::filesystem::remove("/tmp/vw.compositor", ec);
        server.reset(new server_t(io_service, "local:///tmp/vw.compositor"));
        server->set_connection_handler([this](server_t::socket_t && s){
            socket.reset(new server_t::socket_t(std::move(s)));
            dispatcher.reset(new dispatcher_t(*socket));
            dispatcher->register_handler("add_source", request_handler);
            dispatcher->register_handler("rem_source", request_handler);
            dispatcher->register_handler("update", request_handler);
        });
        io_thread = std::thread([this](){io_service.run();});
    }
    ~MockCompositor() {
        io_service.stop();
        if (io_thread.joinable())
            io_thread.join();
    }

    io_service_t io_service;
    server_ptr_t server;
    socket_ptr_t socket;
    dispatcher_ptr_t dispatcher;
    std::thread io_thread;
};

BOOST_FIXTURE_TEST_CASE(create_unref, MockCompositor) {
    GstElement * vsink = gst_element_factory_make("shmvideosink", "vsink");
    BOOST_CHECK(vsink != nullptr);
    BOOST_CHECK_EQUAL(GST_OBJECT_REFCOUNT(vsink), 1);
    gst_object_unref(vsink);
    BOOST_CHECK_EQUAL(GST_OBJECT_REFCOUNT(vsink), 0);
}

BOOST_FIXTURE_TEST_CASE(pipeline, MockCompositor) {
    GstElement * pipeline = gst_parse_launch
            ("videotestsrc ! shmvideosink uuid=shmvideosink_test", nullptr);
    BOOST_CHECK(pipeline);
    gst_element_set_state(pipeline, GST_STATE_PLAYING);
    GstState current, pending;
    gst_element_get_state(pipeline, &current, &pending, GST_CLOCK_TIME_NONE);
    BOOST_CHECK_EQUAL(current, GST_STATE_PLAYING);
    BOOST_CHECK_EQUAL(pending, GST_STATE_VOID_PENDING);
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(pipeline);
}
