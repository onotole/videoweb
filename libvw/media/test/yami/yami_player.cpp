#include <sys/mman.h>
#include <sys/stat.h>
#include <VideoDecoderHost.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#include <va/va_drm.h>
#include <va/va_drmcommon.h>

#include <string>
#include <cstring>
#include <cassert>
#include <thread>
#include <mutex>
#include <deque>

#include <log/log.h>
#include <gfx/image_util.h>
#include <gfx/render_context/render_context.h>
#include <gfx/gl/gl.h>
#include <gfx/gl/canvas.h>
#include <gfx/gl/shader_manager.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/texture.h>

#undef NDEBUG
#include <cassert>

uint32_t frames_decoded(0), frames_rendered(0);

using namespace YamiMediaCodec;

class DecodeInput {
public:
    explicit DecodeInput(const char * fname);
    ~DecodeInput();

    const char * getMimeType() const {
        return YAMI_MIME_H264;
    }

    bool getNextDecodeUnit(VideoDecodeBuffer & buffer);
private:
    int fd;
    uint8_t * start;
    uint32_t offset;
    uint32_t size;
    uint32_t start_code_size;
};

DecodeInput::DecodeInput(const char * fname) {
    INFO << "DI: " << fname;
    fd = open(fname, O_RDONLY);
    assert(fd > 0);
    struct stat st;
    fstat(fd, &st);
    size = st.st_size;
    offset = 0;
    start = (uint8_t*) mmap(nullptr, size, PROT_READ, MAP_PRIVATE, fd, offset);
    assert(start[0] == 0 && start[1] == 0);
    if (start[2] == 1)
        start_code_size = 3;
    else if (start[2] == 0 && start[3] == 1)
        start_code_size = 4;
    else
        assert(0);
}

DecodeInput::~DecodeInput() {
    munmap(start, size);
    close(fd);
}

bool DecodeInput::getNextDecodeUnit(VideoDecodeBuffer & buffer) {
    if (offset == size) return false;
    auto start_code = [&](const uint8_t * data) {
        return data[0] == 0 && data[1] == 0 &&
               (start_code_size == 3 ? data[2] == 2 : (data[2] == 0 && data[3] == 1));
    };
    uint32_t next_nal = offset + start_code_size;
    while(next_nal + start_code_size < size) {
        if (start_code(start + next_nal))
            break;
        next_nal++;
    }
    if (next_nal + start_code_size == size)
        next_nal += start_code_size;
    buffer.data = start + offset;
    buffer.size = next_nal - offset;
    buffer.flag = 0;
    offset = next_nal;
    return true;
}

class Render {
public:
    Render(vw::gfx::rc::IRenderContext & rc, VADisplay va_dpy, int w, int h);
    void put_frame(SharedPtr<VideoFrame> frame);
    void render();
private:
    mutable std::mutex _mutex;
    vw::gfx::rc::IRenderContext & _rc;
    VADisplay _va_dpy;
    std::deque<SharedPtr<VideoFrame>> _frames;
    vw::gfx::gl::Canvas         _canvas;
    vw::gfx::gl::ShaderManager  _shader;
    vw::gfx::gl::Frame          _frame;
    vw::gfx::gl::Mesh           _mesh;
    vw::gfx::gl::Texture        _texture;
};

Render::Render(vw::gfx::rc::IRenderContext & rc, VADisplay va_dpy, int w, int h) : _rc(rc), _va_dpy(va_dpy) {
    std::cout << "=============================================================================" << std::endl;
    std::cout << "SURFACE_SIZE\t\t\t| "            << w << "x" << h                              << std::endl;
    std::cout << "GL_VENDOR\t\t\t| "               << vw::gfx::gl::vendor()                      << std::endl;
    std::cout << "GL_RENDERER\t\t\t| "             << vw::gfx::gl::renderer()                    << std::endl;
    std::cout << "GL_VERSION\t\t\t| "              << vw::gfx::gl::version()                     << std::endl;
    std::cout << "GL_SHADING_LANGUAGE_VERSION\t| " << vw::gfx::gl::sl_version()                  << std::endl;
    std::cout << "GL_EXTENSIONS\t\t\t| "           << vw::gfx::gl::extensions()                  << std::endl;
    std::cout << "=============================================================================" << std::endl;

    _frame.set_viewport({10, 10, w-20, h-20});
}

void Render::put_frame(SharedPtr<VideoFrame> frame) {
    std::lock_guard<std::mutex> lk(_mutex); (void) lk;
    _frames.push_back(frame);
}

void Render::render() {
    SharedPtr<VideoFrame> frame;
    {
        std::lock_guard<std::mutex> lk(_mutex); (void) lk;
        if (_frames.size()) {
            frame = _frames.front();
            _frames.pop_front();
        }
    }
    _shader.use(vw::gfx::Image::Memory::DMA);
    float r(0.f), g(.2f), b(.8f);
    _canvas.set_background(1.f, r, g, b);
    _canvas.clear();
    _frame.set();
    _mesh.render();
    if (frame) {
        VADRMPRIMESurfaceDescriptor descr {};
        auto res = vaExportSurfaceHandle(_va_dpy, (VASurfaceID)frame->surface,
                                         VA_SURFACE_ATTRIB_MEM_TYPE_DRM_PRIME_2,
                                         VA_EXPORT_SURFACE_READ_ONLY |
                                         VA_EXPORT_SURFACE_COMPOSED_LAYERS, &descr);
        if (res == VA_STATUS_SUCCESS) {
            vw::gfx::Image image {};
            image.header.memory = vw::gfx::Image::Memory::DMA;
            image.header.format = descr.fourcc;
            image.header.width = descr.width;
            image.header.height = descr.height;
            vw::gfx::DMAImage dma_image {};
            dma_image.num_planes = descr.layers[0].num_planes;
            for (auto p = 0U; p < dma_image.num_planes; ++p) {
                dma_image.planes[p].fd = descr.objects[0].fd;
                dma_image.planes[p].offset = descr.layers[0].offset[p];
                dma_image.planes[p].pitch = descr.layers[0].pitch[p];
            }
            image.data.size = sizeof(vw::gfx::DMAImage);
            image.data.ptr = &dma_image;
            auto egl_image = _rc.load_image(image);
            _texture.fill(*egl_image);
            close(descr.objects[0].fd);
            ++frames_rendered;
        } else {
            ERROR << "failed to export surface: " << res;
        }
    }
}

class SimplePlayer {
public:
    bool init(int argc, char* argv[]) {
        if (argc != 2) {
            printf("usage: yami_player xxx.264\n");
            return false;
        }
        drm_fd = open("/dev/dri/card0", O_RDWR);
        assert(drm_fd > 0);
        render_thread = std::thread([this]{
            rc = vw::gfx::rc::create_render_context(drm_fd);
            rc->set_resize_callback([this](int w, int h){
                render.reset(new Render(*rc, dpy, w, h));
            });
            rc->set_update_callback([this](){
                if (render) render->render();
                rc->swap_buffers();
            });
            rc->get_pulse()->set_callback([this](){ rc->update(); });
            rc->exec();
        });
        input.reset(new DecodeInput(argv[1]));
        if (!input) {
            fprintf(stderr, "failed to open %s\n", argv[1]);
            return false;
        }
        //init decoder
        decoder.reset(createVideoDecoder(input->getMimeType()), releaseVideoDecoder);
        if (!decoder) {
            fprintf(stderr, "failed create decoder for %s\n", input->getMimeType());
            return false;
        }

        if (!initDisplay()) {
            return false;
        }
        return true;
    }
    bool run() {
        VideoConfigBuffer configBuffer;
        memset(&configBuffer, 0, sizeof(configBuffer));
        configBuffer.profile = VAProfileNone;

        Decode_Status status = decoder->start(&configBuffer);
        assert(status == YAMI_SUCCESS);

        VideoDecodeBuffer inputBuffer;
        while (input->getNextDecodeUnit(inputBuffer)) {
            // whait for render thread
            while((status = decoder->decode(&inputBuffer)) == YAMI_DECODE_NO_SURFACE) {
                std::this_thread::yield();
            }
            if (YAMI_DECODE_FORMAT_CHANGE == status) {
                // drain old buffers
                renderOutputs();
                const VideoFormatInfo *formatInfo = decoder->getFormatInfo();
                INFO << "VI: dims = " << formatInfo->width << "x" << formatInfo->height;
                // resend the buffer
                status = decoder->decode(&inputBuffer);
            }
            if (status == YAMI_SUCCESS) {
                renderOutputs();
            } else if (status > 0) {
                WARN << "decode warn status = " << status;
                std::this_thread::yield();
            } else {
                ERROR << "decode error status = " << status;
                break;
            }
        }
        decoder->stop();
        return true;
    }
    ~SimplePlayer() {
        if (rc)
            rc->stop();
        if (render_thread.joinable())
            render_thread.join();
        close(drm_fd);
    }
private:
    void renderOutputs() {
        do {
            SharedPtr<VideoFrame> frame = decoder->getOutput();
            if (!frame || !render)
                break;
            render->put_frame(frame);
            ++frames_decoded;
        } while (1);
    }
    bool initDisplay() {
        dpy = vaGetDisplayDRM(drm_fd);
        int major, minor;
        VAStatus status;
        status = vaInitialize(dpy, &major, &minor);
        if (status != VA_STATUS_SUCCESS) {
            ERROR << "init va failed status = " << status;
            return false;
        }
        NativeDisplay display{(intptr_t)dpy, NATIVE_DISPLAY_VA};
        decoder->setNativeDisplay(&display);
        return true;
    }
    VADisplay dpy;
    SharedPtr<IVideoDecoder> decoder;
    SharedPtr<DecodeInput> input;

    std::unique_ptr<vw::gfx::rc::IRenderContext> rc;
    int drm_fd;
    std::thread render_thread;
    std::unique_ptr<Render> render;
};

int main(int argc, char* argv[]) {
    SimplePlayer player;
    if (!player.init(argc, argv)) {
        fprintf(stderr, "init player failed with %s\n", argv[1]);
        return -1;
    }
    if (!player.run()){
        fprintf(stderr, "run simple player failed\n");
        return -1;
    }
    printf("play file done\n");
    INFO << "frames decoded: " << frames_decoded << ", frames rendred: " << frames_rendered;
    return  0;

}



