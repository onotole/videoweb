#include <cassert>
#include <chrono>
#include <log/log.h>
#include "vcsnapshotsrc_private.h"

using namespace vw::media;

// GstVcSnapshotSrcPrivate::VCContext
GstVcSnapshotSrcPrivate::VCContext::VCContext() {
	bcm_host_init();
	display = vc_dispmanx_display_open(0);
	OMX_Init();
}

GstVcSnapshotSrcPrivate::VCContext::~VCContext() {
	OMX_Deinit();
	if (resource)
		vc_dispmanx_resource_delete(resource);
	vc_dispmanx_display_close(display);
}

void GstVcSnapshotSrcPrivate::VCContext::create_resource(int width, int height) {
	uint32_t vc_img_ptr;
	resource = vc_dispmanx_resource_create(VC_IMAGE_RGB565, width, height, &vc_img_ptr);
}

void GstVcSnapshotSrcPrivate::VCContext::make_screenshot() {
	auto result = vc_dispmanx_snapshot(display, resource, (DISPMANX_TRANSFORM_T)0);
	assert(result == 0);
}

// GstVcSnapshotSrcPrivate
GstVcSnapshotSrcPrivate::GstVcSnapshotSrcPrivate(int w, int h, double fps)
	:_encoder("OMX.broadcom.video_encode"), _width(w), _height(h), _fps(fps) {
	_input.size = 2 * _width * _height;
	_input.data = new OMX_U8[_input.size];
	_context.create_resource(_width, _height);
	configure_encoder();
}

GstVcSnapshotSrcPrivate::~GstVcSnapshotSrcPrivate() {
	delete [] _meta.data;
	delete [] _input.data;
}

GstVcSnapshotSrcPrivate::Buffer GstVcSnapshotSrcPrivate::get_data_buffer() {
	_output = std::promise<Buffer>();
	std::future<Buffer> f = _output.get_future();

	auto & buffer = _encoder.output()->get_buffer();
	_encoder.output()->process_buffer(buffer);

	make_screenshot();
	feed_encoder();

	return f.get();
}

GstVcSnapshotSrcPrivate::Buffer GstVcSnapshotSrcPrivate::get_meta_buffer() {
	return _meta;
}

void GstVcSnapshotSrcPrivate::configure_encoder() {
	TRACE << __FUNCTION__;
	_encoder.set_fill_buffer_handler([this](OMX_BUFFERHEADERTYPE * buffer){
		static std::chrono::steady_clock::time_point presentation_time =
				std::chrono::steady_clock::now();
		static auto frame_duration_ms = std::chrono::microseconds((long long)(1000000. / _fps));
		TRACE << "nAllocLen = " << buffer->nAllocLen
			  << ", nFilledLen = " << buffer->nFilledLen
			  << ", nOffset = " << buffer->nOffset
			  << ", nFlags = " << buffer->nFlags
			  << ", nTimeStamp = " << (uint64_t(buffer->nTimeStamp.nLowPart)
									| (uint64_t(buffer->nTimeStamp.nHighPart) << 32))
			  << ", nTickCount = " << buffer->nTickCount;
		if(buffer->nFlags & OMX_BUFFERFLAG_CODECCONFIG) {
			add_meta(buffer);
		}
		Buffer result;
		if (buffer->nFlags & OMX_BUFFERFLAG_SYNCFRAME) {
			result.flags = Buffer::SYNCFRAME;
		}
		result.frame_num = _frame_counter++;
		result.size = buffer->nFilledLen;
		result.data = buffer->pBuffer + buffer->nOffset;
		if (buffer->nFilledLen < buffer->nAllocLen) {
			presentation_time += frame_duration_ms;
			std::this_thread::sleep_until(presentation_time);
		}
		_output.set_value(result);
		return OMX_ErrorNone;
	});

	omx::OMX_ParamPortDefinition definition;
	definition.nPortIndex = _encoder.input()->index();
	_encoder.get_parameter(OMX_IndexParamPortDefinition, &definition);
	definition.format.video.nFrameWidth = _width;
	definition.format.video.nFrameHeight = _height;
	definition.format.video.xFramerate = 30 << 16;
	definition.format.video.nSliceHeight = _height;
	definition.format.video.nStride = _width;
	definition.format.video.eColorFormat = OMX_COLOR_Format16bitRGB565;
	_encoder.set_parameter(OMX_IndexParamPortDefinition, &definition);

	omx::OMX_VideoParamPortFormat format;
	format.nPortIndex = _encoder.output()->index();
	format.eCompressionFormat = OMX_VIDEO_CodingAVC;
	_encoder.set_parameter(OMX_IndexParamVideoPortFormat, &format);

	omx::OMX_VideoParamBitrate bitrate;
	bitrate.nPortIndex = _encoder.output()->index();
	bitrate.eControlRate = OMX_Video_ControlRateVariable;
	bitrate.nTargetBitrate = 1000000;
	_encoder.set_parameter(OMX_IndexParamVideoBitrate, &bitrate);

	_encoder.input()->enable();
	_encoder.input()->alloc_buffers();
	_encoder.output()->enable();
	_encoder.output()->alloc_buffers();

	_encoder.set_state(OMX_StateExecuting);
}

void GstVcSnapshotSrcPrivate::make_screenshot() {
	VC_RECT_T rc {0, 0, _width, _height};
	_context.make_screenshot();
	auto result = vc_dispmanx_resource_read_data(_context.resource, &rc,
												 _input.data, 2 * _width);
	if (result != 0) {
		ERROR << "Failed to capture screenshot";
	}
}

void GstVcSnapshotSrcPrivate::feed_encoder() {
	size_t done = 0;
	while (done < _input.size) {
		auto & buffer = _encoder.input()->get_buffer();
		size_t chunk = std::min(_input.size - done, buffer.size());
		buffer.write(_input.data + done, chunk);
		_encoder.input()->process_buffer(buffer);
		done += chunk;
	}
}

void GstVcSnapshotSrcPrivate::add_meta(OMX_BUFFERHEADERTYPE * buffer) {
	OMX_U8 * tmp = new OMX_U8[_meta.size + buffer->nFilledLen];
	std::memcpy(tmp, _meta.data, _meta.size);
	std::memcpy(tmp + _meta.size, buffer->pBuffer, buffer->nFilledLen);
	_meta.size += buffer->nFilledLen;
	delete [] _meta.data;
	_meta.data = tmp;
	TRACE << "encoder config saved (" << _meta.size << " bytes)";
}
