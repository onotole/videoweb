#include <gst/video/video.h>
#include <log/log.h>
#include "es2eglvideosink.h"

static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE
		("sink", GST_PAD_SINK, GST_PAD_ALWAYS, GST_STATIC_CAPS(
			"                           \
				video/x-h264;           \
                video/x-h263            \
			"
		));

static void gst_es2egl_video_sink_init (GstEs2EglVideoSink * sink) {
    INFO << "es2eglvideosink init...";
	sink->priv = new GstEs2EglVideoSinkPrivate;
}

G_DEFINE_TYPE (GstEs2EglVideoSink, gst_es2egl_video_sink, GST_TYPE_BASE_SINK);

static void gst_es2egl_video_sink_finalize (GstEs2EglVideoSink * vsink) {
    INFO << "es2eglvideosink finalize...";
	delete vsink->priv;
	G_OBJECT_CLASS(gst_es2egl_video_sink_parent_class)->finalize(G_OBJECT(vsink));
}

static void gst_es2egl_video_sink_set_property (GObject * object, guint prop_id,
	const GValue * value, GParamSpec * pspec) {
	GstEs2EglVideoSink * vsink = GST_ES2EGL_VIDEO_SINK(object);
	switch (prop_id) {
		case PROP_UUID:
			GST_OBJECT_LOCK(vsink);
			vsink->priv->set_uuid(g_value_get_string(value));
			GST_OBJECT_UNLOCK(vsink);
			break;
		case PROP_WINDOWED:
			GST_OBJECT_LOCK(vsink);
			vsink->priv->set_windowed(g_value_get_boolean(value));
			GST_OBJECT_UNLOCK(vsink);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void gst_es2egl_video_sink_get_property (GObject * object, guint prop_id,
	GValue * value, GParamSpec * pspec) {
	GstEs2EglVideoSink * vsink = GST_ES2EGL_VIDEO_SINK(object);
	switch (prop_id) {
		case PROP_UUID:
			GST_OBJECT_LOCK(vsink);
			g_value_set_string(value, vsink->priv->uuid().c_str());
			GST_OBJECT_UNLOCK(vsink);
			break;
		case PROP_WINDOWED:
			GST_OBJECT_LOCK(vsink);
			g_value_set_boolean(value, vsink->priv->windowed());
			GST_OBJECT_UNLOCK(vsink);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static gboolean gst_es2egl_video_sink_set_caps(GstBaseSink * bsink, GstCaps * caps) {
	GstEs2EglVideoSink * vsink = GST_ES2EGL_VIDEO_SINK_CAST(bsink);
	GstStructure * structure = gst_caps_get_structure(caps, 0);
    std::string encoding = gst_structure_get_name(structure);
    GstMapInfo info {0};
    GstBuffer * buffer = nullptr;
    auto value = gst_structure_get_value (structure, "codec_data");
    if (value) {
        buffer = gst_value_get_buffer(value);
        gst_buffer_map(buffer, &info, GST_MAP_READ);
    }
    TRACE << "media type = " << encoding << ", codec data size = " << info.size;
    vsink->priv->init(encoding, info.data, info.size);
    if (buffer)
        gst_buffer_unmap(buffer, &info);
	return true;
}

static GstFlowReturn gst_es2egl_video_sink_preroll(GstBaseSink * bsink, GstBuffer * buf) {
    GstEs2EglVideoSink * vsink = GST_ES2EGL_VIDEO_SINK_CAST(bsink);
    GstMapInfo info {0};
    gst_buffer_map(buf, &info, GST_MAP_READ);
    TRACE << "preroll: data = " << info.data << ", size = " << info.size;

    vsink->priv->preroll(info.data, info.size);
    gst_buffer_unmap(buf, &info);
    return GST_FLOW_OK;
}

static GstFlowReturn gst_es2egl_video_sink_prepare(GstBaseSink * bsink, GstBuffer * buf) {
	GstEs2EglVideoSink * vsink = GST_ES2EGL_VIDEO_SINK_CAST(bsink);
    GstMapInfo info {0};
    gst_buffer_map(buf, &info, GST_MAP_READ);
    TRACE << "prepare: data = " << info.data << ", size = " << info.size;

	vsink->priv->prepare(info.data, info.size);
	gst_buffer_unmap(buf, &info);
	return GST_FLOW_OK;
}

static GstFlowReturn gst_es2egl_video_sink_render(GstBaseSink *, GstBuffer *) {
	return GST_FLOW_OK;
}

static void gst_es2egl_video_sink_class_init(GstEs2EglVideoSinkClass * klass) {
	GObjectClass * object_class = G_OBJECT_CLASS(klass);
	GstElementClass * element_class = GST_ELEMENT_CLASS(klass);
	GstBaseSinkClass * base_class = GST_BASE_SINK_CLASS(klass);

	object_class->finalize = (GObjectFinalizeFunc) gst_es2egl_video_sink_finalize;
	// TODO: set construct only property
	object_class->set_property = gst_es2egl_video_sink_set_property;
	object_class->get_property = gst_es2egl_video_sink_get_property;

	g_object_class_install_property (object_class, PROP_UUID,
		g_param_spec_string ("uuid", "Unique stream id",
			"Id to be used for stream identification",
			"00000000-0000-0000-0000-000000000000",
			(GParamFlags)(G_PARAM_READWRITE|G_PARAM_STATIC_STRINGS)));
	g_object_class_install_property (object_class, PROP_WINDOWED,
		g_param_spec_boolean ("windowed", "Windowed or attached mode",
			"Windowed(detached) or compositor attached mode of operation",
			false,
			(GParamFlags)(G_PARAM_READWRITE|G_PARAM_STATIC_STRINGS)));

	gst_element_class_add_pad_template (element_class,
		gst_static_pad_template_get (&sinktemplate));

	gst_element_class_set_static_metadata(element_class,
		"Elementary stream to EGL video sink",
		"Sink/Video",
		"Video sink element that writes frames to EGLImage",
		"Anatoliy Klymenko <anatoly.klimenko.ua@gmail.com>");

	// init buffer here...
	base_class->set_caps = GST_DEBUG_FUNCPTR(gst_es2egl_video_sink_set_caps);
    // preroll here...
    base_class->preroll = GST_DEBUG_FUNCPTR(gst_es2egl_video_sink_preroll);
	// copy buffer here...
	base_class->prepare = GST_DEBUG_FUNCPTR(gst_es2egl_video_sink_prepare);
	// trigger update here...
	base_class->render = GST_DEBUG_FUNCPTR(gst_es2egl_video_sink_render);
}
