#include <gst/gst.h>
#include "es2eglvideosink.h"
#include "vcsnapshotsrc.h"

static gboolean plugin_init(GstPlugin * plugin) {
	gboolean result = gst_element_register (plugin, "es2eglvideosink",
											GST_RANK_NONE, GST_TYPE_ES2EGL_VIDEO_SINK);
	return result && gst_element_register (plugin, "vcsnapshotsrc",
										   GST_RANK_NONE, GST_TYPE_VC_SNAPSHOT_SRC);
}

#define STUB " "
#define VERSION STUB
#define PACKAGE STUB

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR, GST_VERSION_MINOR,
				   vwgstrpi, "Collection of RPI platform specific elements",
				   plugin_init, VERSION, GST_LICENSE_UNKNOWN, STUB, STUB);
