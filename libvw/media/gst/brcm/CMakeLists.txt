find_package(Threads)

add_library(vwgstrpi MODULE plugin.cpp
    es2eglvideosink.cpp es2eglvideosink_private.cpp
    vcsnapshotsrc.cpp vcsnapshotsrc_private.cpp)
target_link_libraries(vwgstrpi ${OMXIL_LIBRARIES} ${Boost_LIBRARIES}
    ${GSTREAMER_LIBRARIES} ${GSTREAMER_BASE_LIBRARIES} ${JSONCPP_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT}
    gstvideo-1.0 vw.log vw.gfx.ipc vw.media.omx vw.gfx.rc vw.gfx.gl)

install(TARGETS vwgstrpi LIBRARY DESTINATION ${VW_IMAGE_LIB}/gstreamer-1.0)
