#include <cassert>
#include <log/log.h>
#include "vcsnapshotsrc.h"

#define NS_TO_SEC 1000000000UL

static GstStaticPadTemplate srctemplate = GST_STATIC_PAD_TEMPLATE
		("src", GST_PAD_SRC, GST_PAD_ALWAYS, GST_STATIC_CAPS("video/x-h264"));

static void gst_vc_snapshot_src_init (GstVcSnapshotSrc * src) {
	DEBUG << "vcsnapshotsrc init...";
}

G_DEFINE_TYPE (GstVcSnapshotSrc, gst_vc_snapshot_src, GST_TYPE_PUSH_SRC);

static void gst_vc_snapshot_src_finalize(GstVcSnapshotSrc * src) {
	DEBUG << "vcsnapshotsrc finalize...";
	delete src->priv;
	G_OBJECT_CLASS(gst_vc_snapshot_src_parent_class)->finalize(G_OBJECT(src));
}

static gboolean gst_vc_snapshot_src_set_caps(GstBaseSrc * src, GstCaps * caps) {
	GstVcSnapshotSrc * vc_src = GST_VC_SNAPSHOT_SRC_CAST(src);
	DEBUG << "caps = " << gst_caps_to_string(caps);
	int width(640), height(360), fps_n(10), fps_d(1);
	GstStructure * structure = gst_caps_get_structure(caps, 0);
	gst_structure_get_int(structure, "width", &width);
	gst_structure_get_int(structure, "height", &height);
	// TODO: check default + prevent 0 fps
	gst_structure_get_fraction(structure, "framerate", &fps_n, &fps_d);
	assert(!vc_src->priv);
	double fps = double(fps_n) / double(fps_d);
	vc_src->ts = (uint64_t)(NS_TO_SEC / fps);
	vc_src->priv = new GstVcSnapshotSrcPrivate(width, height, fps);
	return true;
}

static GstFlowReturn gst_vc_snapshot_src_alloc(GstPushSrc *src, GstBuffer **buf) {
	auto log_buffer = [] (GstBuffer * b) {
		TRACE << "flags = " << GST_BUFFER_FLAGS(b)
			  << ", pts = " << GST_BUFFER_PTS(b)
			  << ", dts = " << GST_BUFFER_DTS(b)
			  << ", duration = " << GST_BUFFER_DURATION(b)
			  << ", offset = " << GST_BUFFER_OFFSET(b)
			  << ", offset_end = " << GST_BUFFER_OFFSET_END(b);
	};
	GstVcSnapshotSrc * vc_src = GST_VC_SNAPSHOT_SRC_CAST(src);
	auto buffer = vc_src->priv->get_data_buffer();
	if (buffer.flags & GstVcSnapshotSrcPrivate::Buffer::SYNCFRAME) {
		auto meta_buffer = vc_src->priv->get_meta_buffer();
		if (meta_buffer.size) {
			GstBuffer * meta_buf = gst_buffer_new_wrapped_full(GST_MEMORY_FLAG_PHYSICALLY_CONTIGUOUS,
															   meta_buffer.data, meta_buffer.size, 0,
															   meta_buffer.size, nullptr, nullptr);
			log_buffer(meta_buf);
			GstPad * pad = gst_element_get_static_pad(GST_ELEMENT(vc_src), "src");
			gst_pad_push(pad, meta_buf);
			gst_object_unref(pad);
		}
	}
	*buf = gst_buffer_new_wrapped_full(GST_MEMORY_FLAG_PHYSICALLY_CONTIGUOUS, buffer.data,
									   buffer.size, 0, buffer.size, nullptr, nullptr);

	if (!buffer.flags & GstVcSnapshotSrcPrivate::Buffer::SYNCFRAME)
		GST_BUFFER_FLAGS(*buf) = GST_BUFFER_FLAG_DELTA_UNIT;
	GST_BUFFER_PTS(*buf) = buffer.frame_num * vc_src->ts;
	GST_BUFFER_DTS(*buf) = 0;
	GST_BUFFER_DURATION(*buf) = GST_BUFFER_PTS(*buf);
	GST_BUFFER_OFFSET(*buf) = 0;
	GST_BUFFER_OFFSET_END(*buf) = vc_src->ts;

	log_buffer(*buf);

	return GST_FLOW_OK;
}

static GstFlowReturn gst_vc_snapshot_src_fill(GstPushSrc *, GstBuffer *) {
	return GST_FLOW_OK;
}

void gst_vc_snapshot_src_class_init(GstVcSnapshotSrcClass * klass) {
	GObjectClass * object_class = G_OBJECT_CLASS(klass);
	GstElementClass * element_class = GST_ELEMENT_CLASS(klass);
	GstBaseSrcClass * base_src_class = GST_BASE_SRC_CLASS(klass);
	GstPushSrcClass * push_src_class = GST_PUSH_SRC_CLASS(klass);

	object_class->finalize = (GObjectFinalizeFunc) gst_vc_snapshot_src_finalize;
	gst_element_class_add_pad_template (element_class,
		gst_static_pad_template_get (&srctemplate));

	gst_element_class_set_static_metadata(element_class,
		"VC display snapshot video source",
		"Source/Video",
		"Video source element that produce display snapshot frames",
		"Anatoliy Klymenko <anatoly.klimenko.ua@gmail.com>");

	// init buffer here...
	base_src_class->set_caps = GST_DEBUG_FUNCPTR(gst_vc_snapshot_src_set_caps);
	// map buffer here...
	push_src_class->alloc = GST_DEBUG_FUNCPTR(gst_vc_snapshot_src_alloc);
	// fill buffer here...
	push_src_class->fill = GST_DEBUG_FUNCPTR(gst_vc_snapshot_src_fill);
}
