#include <cmath>
#include <log/log.h>
#include <gfx/render_context/render_context.h>
#include <gfx/gl/gl.h>
#include <gfx/gl/canvas.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/texture.h>
#include <gfx/gl/shader_manager.h>
#include "es2eglvideosink_private.h"

struct Packet : vw::gfx::ipc::Export::Packet {
    Packet(const void * data) : _handle((vw::gfx::ImageHandle)data) {}
    const vw::gfx::ImageHandle handle() const override { return _handle; }
    const vw::gfx::ImageHandle _handle;
};

size_t select_encoding_type(const std::string & encoding) {
    if (encoding == "video/x-h264") {
        return OMX_VIDEO_CodingAVC;
    } else {
        return OMX_VIDEO_CodingAutoDetect;
    }
}

class GlRender {
public:
    GlRender(int w, int h) {
        std::cout << "=============================================================================" << std::endl;
        std::cout << "SURFACE_SIZE\t\t\t| "            << w << "x" << h                              << std::endl;
        std::cout << "GL_VENDOR\t\t\t| "               << vw::gfx::gl::vendor()                      << std::endl;
        std::cout << "GL_RENDERER\t\t\t| "             << vw::gfx::gl::renderer()                    << std::endl;
        std::cout << "GL_VERSION\t\t\t| "              << vw::gfx::gl::version()                     << std::endl;
        std::cout << "GL_SHADING_LANGUAGE_VERSION\t| " << vw::gfx::gl::sl_version()                  << std::endl;
        std::cout << "GL_EXTENSIONS\t\t\t| "           << vw::gfx::gl::extensions()                  << std::endl;
        std::cout << "=============================================================================" << std::endl;

        vw::gfx::Image image;
        image.header.memory = vw::gfx::Image::Memory::SHM;//vw::gfx::Image::Format::RGB32;
        image.header.format = "RX24";
        image.header.width = w;
        image.header.height = h;

        _shader.use(image.header.memory);
        _frame.set_viewport({0, 0, w, h});
        _texture.fill(image);
    }
    vw::gfx::gl::Texture & texture() { return _texture; }
    void render() {
        float r(.010f * _counter), g(.012f * _counter), b(.015f * _counter++), i;
        _canvas.set_background(1.f, std::modf(r, &i), std::modf(g, &i), std::modf(b, &i));
        _canvas.clear();
        _frame.set();
        _texture.bind();
        _mesh.render();
    }
private:
    size_t _counter = 0;
    vw::gfx::gl::Canvas         _canvas;
    vw::gfx::gl::ShaderManager  _shader;
    vw::gfx::gl::Frame          _frame;
    vw::gfx::gl::Mesh           _mesh;
    vw::gfx::gl::Texture        _texture;
};

GstEs2EglVideoSinkPrivate::GstEs2EglVideoSinkPrivate() {
    _render_thread = std::thread(&GstEs2EglVideoSinkPrivate::render_thread, this);
}

GstEs2EglVideoSinkPrivate::~GstEs2EglVideoSinkPrivate() {
    _decoder.set_event_handler(OMX_EventPortSettingsChanged, nullptr);
    _renderer.set_fill_buffer_handler(nullptr);
    _run = false;
    _cond_var.notify_one();
    if (_render_context)
        _render_context->stop();
    _render_thread.join();
}

void GstEs2EglVideoSinkPrivate::preroll(const void * data, size_t size) {
    while (size) {
        auto & buffer = _decoder.input()->get_buffer();
        size_t chunk = std::min(size, buffer.size());
        memcpy((char *)buffer.data(), data, chunk);
        buffer.header()->nOffset = 0;
        buffer.header()->nFlags = OMX_BUFFERFLAG_TIME_UNKNOWN;
        buffer.header()->nFilledLen = chunk;
        data = (char*)data + chunk;
        size -= chunk;
        if (size == 0)
            buffer.header()->nFlags |= OMX_BUFFERFLAG_ENDOFFRAME;
        _decoder.input()->process_buffer(buffer);
    }
    // TODO: wait on renderer executing state
    _state = PREROLL;
}

void GstEs2EglVideoSinkPrivate::prepare(const void * data, size_t size) {
    if (_state < PREROLL) return;
    while (size) {
        auto & buffer = _decoder.input()->get_buffer();
        size_t chunk = std::min(size, buffer.size());
        memcpy((char *)buffer.data(), data, chunk);
        buffer.header()->nOffset = 0;
        if (_state < START) {
            _state = START;
            buffer.header()->nFlags = OMX_BUFFERFLAG_STARTTIME;
        } else {
            buffer.header()->nFlags = OMX_BUFFERFLAG_TIME_UNKNOWN;
        }
        buffer.header()->nFilledLen = chunk;
        data = (char*)data + chunk;
        size -= chunk;
        if (size == 0)
            buffer.header()->nFlags |= OMX_BUFFERFLAG_ENDOFFRAME;
        _decoder.input()->process_buffer(buffer);
    }
}

GstEs2EglVideoSinkPrivate::State GstEs2EglVideoSinkPrivate::state() const {
    return _state;
}

const std::string & GstEs2EglVideoSinkPrivate::uuid() const {
    return _uuid;
}

void GstEs2EglVideoSinkPrivate::set_uuid(const std::string & uuid) {
    _uuid = uuid;
}

bool GstEs2EglVideoSinkPrivate::windowed() const {
    return _windowed;
}
void GstEs2EglVideoSinkPrivate::set_windowed(bool mode) {
    _windowed = mode;
}

void GstEs2EglVideoSinkPrivate::init(const std::string & encoding, const void * codec_data, size_t data_size) {
    if (_state > IDLE)
        return;

    if (!_windowed)
        _export.reset(new vw::gfx::ipc::Export("local:///tmp/vw.compositor", _uuid));

    vw::media::omx::OMX_VideoParamPortFormat format;
    format.eCompressionFormat = (OMX_VIDEO_CODINGTYPE)select_encoding_type(encoding);
    format.nPortIndex = _decoder.input()->index();
    INFO << "init: encoding = " << encoding << ", compression = " << format.eCompressionFormat
         << ", data = " << codec_data << ", size = " << data_size;
    _decoder.set_parameter(OMX_IndexParamVideoPortFormat, &format);
    _decoder.input()->enable();
    _decoder.input()->alloc_buffers();

    vw::media::omx::OMX_TimeConfigClockState cstate;
    cstate.eState = OMX_TIME_ClockStateWaitingForStartTime;
    cstate.nWaitMask = 1;
    _clock.set_parameter(OMX_IndexConfigTimeClockState, &cstate);
    _clock.output()->connect(_scheduler.input(1));

    _decoder.set_event_handler(OMX_EventPortSettingsChanged, [&](OMX_U32, OMX_U32, OMX_PTR) {
        vw::media::omx::OMX_ParamPortDefinition def;
        def.nPortIndex = _decoder.output()->index();
        _decoder.get_parameter(OMX_IndexParamPortDefinition, &def);
        _src_width = def.format.video.nFrameWidth;
        _src_height = def.format.video.nFrameHeight;

        INFO << "decoder settings changed: w = " << _src_width << ", h = " << _src_height;

        _cond_var.notify_one();

        _decoder.output()->connect(_scheduler.input());
        _scheduler.set_state(OMX_StateExecuting);
        _scheduler.output()->connect(_renderer.input());
        vw::media::omx::OMX_ConfigPortBoolean config_port;
        config_port.nPortIndex = _renderer.input()->index();
        config_port.bEnabled = OMX_FALSE;
        _renderer.set_parameter(OMX_IndexParamBrcmVideoEGLRenderDiscardMode, &config_port);

        _renderer.output()->alloc_buffers(1, true);
        _renderer.output()->enable();

        std::unique_lock<std::mutex> l(_lock);
        _cond_var.wait(l, [this]()->bool{ return _image; } );

        _renderer.output()->use_egl_image(_image->data.ptr);

        _renderer.set_state(OMX_StateExecuting);

        auto & buf = _renderer.output()->get_buffer();
        _renderer.output()->process_buffer(buf);

        return OMX_ErrorNone;
    });

    _renderer.set_fill_buffer_handler([this](OMX_BUFFERHEADERTYPE *){
        // FIXME: race condition on destructor. null check is a temp mitigation.
       auto output = _renderer.output();
       if (output) {
            auto & buf = output->get_buffer();
            output->process_buffer(buf);
       }

       _render_context->update();

       return OMX_ErrorNone;
    });

    _decoder.set_state(OMX_StateExecuting);
    _clock.set_state(OMX_StateExecuting);

    if (codec_data && data_size) {
        auto & buffer = _decoder.input()->get_buffer();
        memcpy((char*)buffer.data(), codec_data, data_size);
        buffer.header()->nFilledLen = data_size;
        buffer.header()->nFlags = OMX_BUFFERFLAG_CODECCONFIG | OMX_BUFFERFLAG_ENDOFFRAME;
        _decoder.input()->process_buffer(buffer);
    }

    _state = INIT;
}

void GstEs2EglVideoSinkPrivate::render_thread() {
    {
        std::unique_lock<std::mutex> l(_lock);
        // wait for decoder config
        _cond_var.wait(l, [this]()->bool{ return !_run || _src_width; } );
        // should we continue?
        if (_run)
            init_renderer();
    }

    if (_render_context)
        _render_context->exec();
    close_renderer();
}

void GstEs2EglVideoSinkPrivate::init_renderer() {
    _render_context = vw::gfx::rc::create_render_context();
    _render_context->set_resize_callback([this](int w, int h){
        _dst_width = w;
        _dst_height = h;
        _gl_render.reset(new GlRender(_src_width, _src_height));

        vw::gfx::Image image;
        image.header.memory = vw::gfx::Image::Memory::TEX;
        image.header.width = _src_width;
        image.header.height = _src_height;
        image.data.size = sizeof(vw::gfx::TextureImage *);
        image.data.ptr = new vw::gfx::TextureImage{_gl_render->texture().id()};
        _image = _render_context->load_image(image);

        if (!_windowed) {
            vw::gfx::Image::Header header;
            header.memory = vw::gfx::Image::Memory::EGL;
            header.width = _src_width;
            header.height = _src_height;
            _export->create(header);
            const auto & snapshot = _render_context->snapshot();
            std::unique_ptr<Packet> packet(new Packet(snapshot.data.ptr));
            _export->update(std::move(packet));
        }

        _cond_var.notify_one();
    });
    _render_context->set_update_callback([this](){
        render();
    });

    _render_context->init(_windowed ? vw::gfx::rc::IRenderContext::DEFAULT : _src_width,
                          _windowed ? vw::gfx::rc::IRenderContext::DEFAULT : _src_height, !_windowed);
}

void GstEs2EglVideoSinkPrivate::close_renderer() {
    if (_render_context)
        _render_context->release_image(_image);
    _gl_render.reset();
    _render_context.reset();
}

void GstEs2EglVideoSinkPrivate::render() {
    _gl_render->render();
    _render_context->swap_buffers();
    _export->commit();
}
