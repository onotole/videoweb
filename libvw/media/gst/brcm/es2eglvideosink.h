#ifndef __VW_MEDIA_GST_ES2EGLVIDEOSINK_H__
#define __VW_MEDIA_GST_ES2EGLVIDEOSINK_H__

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>

#include "es2eglvideosink_private.h"

typedef struct _GstEs2EglVideoSink {
	GstBaseSink parent_element;
	GstEs2EglVideoSinkPrivate * priv;
} GstEs2EglVideoSink;

typedef struct _GstEs2EglVideoSinkClass {
	GstBaseSinkClass parent_class;
} GstEs2EglVideoSinkClass;

enum { PROP_UUID = 1,
	   PROP_WINDOWED = 2 };

/* Standard macros for defining types for this element.  */
#define GST_TYPE_ES2EGL_VIDEO_SINK (gst_es2egl_video_sink_get_type())
#define GST_ES2EGL_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_ES2EGL_VIDEO_SINK, GstEs2EglVideoSink))
#define GST_ES2EGL_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_ES2EGL_VIDEO_SINK, GstEs2EglVideoSinkClass))
#define GST_IS_ES2EGL_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_ES2EGL_VIDEO_SINK)
#define GST_IS_ES2EGL_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_ES2EGL_VIDEO_SINK))
#define GST_ES2EGL_VIDEO_SINK_CAST(obj)  ((GstEs2EglVideoSink *) (obj))

GType gst_es2egl_video_sink_get_type(void);

#endif // __VW_MEDIA_GST_ES2EGLVIDEOSINK_H__
