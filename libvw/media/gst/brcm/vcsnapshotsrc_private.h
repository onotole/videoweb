#ifndef __VW_MEDIA_GST_VCSNAPSHOTSRC_PRIVATE_H__
#define __VW_MEDIA_GST_VCSNAPSHOTSRC_PRIVATE_H__

#include <string>
#include <memory>
#include <future>
#include <cstdint>
#include <omx/component.h>
#include <bcm_host.h>


class GstVcSnapshotSrcPrivate {
public:
	struct Buffer {
		enum Flags { SYNCFRAME = 1 };
		OMX_U32 flags = 0;
		OMX_U32 frame_num = 0;
		OMX_U32 size = 0;
		OMX_U8 * data = nullptr;
	};

	GstVcSnapshotSrcPrivate(int w, int h, double fps);
	~GstVcSnapshotSrcPrivate();

	double fps() const;
	Buffer get_data_buffer();
	Buffer get_meta_buffer();

private:
	void configure_encoder();
	void make_screenshot();
	void feed_encoder();
	void add_meta(OMX_BUFFERHEADERTYPE * buffer);

	struct VCContext {
		VCContext();
		~VCContext();
		void create_resource(int width, int height);
		void make_screenshot();

		DISPMANX_DISPLAY_HANDLE_T display = 0;
		DISPMANX_RESOURCE_HANDLE_T resource = 0;
	} _context;

	vw::media::omx::Component _encoder;

	int _width;
	int _height;
	double _fps;
	OMX_U32 _frame_counter = 0;

	Buffer _meta;
	Buffer _input;
	std::promise<Buffer> _output;
};

#endif
