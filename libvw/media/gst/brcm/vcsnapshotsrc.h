#ifndef __VW_MEDIA_GST_VCSNAPSHOTSRC_H__
#define __VW_MEDIA_GST_VCSNAPSHOTSRC_H__

#include <gst/gst.h>
#include <gst/base/gstpushsrc.h>

#include "vcsnapshotsrc_private.h"

typedef struct _GstVcSnapshotSrc {
	GstPushSrc parent_element;
	uint64_t ts;
	GstVcSnapshotSrcPrivate * priv = nullptr;
} GstVcSnapshotSrc;

typedef struct _GstVcSnapshotSrcClass {
	GstPushSrcClass parent_class;
} GstVcSnapshotSrcClass;

/* Standard macros for defining types for this element.  */
#define GST_TYPE_VC_SNAPSHOT_SRC (gst_vc_snapshot_src_get_type())
#define GST_VC_SNAPSHOT_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_VC_SNAPSHOT_SRC, GstVcSnapshotSrc))
#define GST_TYPE_VC_SNAPSHOT_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_VC_SNAPSHOT_SRC, GstVcSnapshotSrcClass))
#define GST_IS_VC_SNAPSHOT_SRC(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_VC_SNAPSHOT_SRC)
#define GST_IS_VC_SNAPSHOT_SRC_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_VC_SNAPSHOT_SRC))
#define GST_VC_SNAPSHOT_SRC_CAST(obj)  ((GstVcSnapshotSrc *) (obj))

GType gst_vc_snapshot_src_get_type(void);

#endif // __VW_MEDIA_GST_VCSNAPSHOTSRC_H__
