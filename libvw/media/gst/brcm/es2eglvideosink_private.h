#ifndef __VW_MEDIA_GST_ES2EGLVIDEOSINK_PRIVATE_H__
#define __VW_MEDIA_GST_ES2EGLVIDEOSINK_PRIVATE_H__

#include <string>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <bcm_host.h>
#include <omx/component.h>
#include <gfx/ipc/export.h>
#include <gfx/render_context/irender_context.h>

class GlRender;

class GstEs2EglVideoSinkPrivate {
public:
    enum State {
        IDLE,
        INIT,
        PREROLL,
        START
    };
    GstEs2EglVideoSinkPrivate();
    ~GstEs2EglVideoSinkPrivate();

    const std::string & uuid() const;
    void set_uuid(const std::string & uuid);
    bool windowed() const;
    void set_windowed(bool mode);

    void init(const std::string & encoding, const void *codec_data = nullptr, size_t data_size = 0);
    void preroll(const void * data, size_t size);
    void prepare(const void * data, size_t size);

    State state() const;

private:
    void render_thread();
    void init_renderer();
    void close_renderer();
    void render();

    struct OMXInit {
        OMXInit() {
            bcm_host_init();
            OMX_Init();
        }
        ~OMXInit() { OMX_Deinit(); }
    } omx_init;

    std::string _uuid = "00000000-0000-0000-0000-000000000000";
    bool _windowed = false;

    std::unique_ptr<vw::gfx::ipc::Export>           _export;
    std::unique_ptr<vw::gfx::rc::IRenderContext>    _render_context;
    std::unique_ptr<GlRender>                       _gl_render;

    vw::media::omx::Component _renderer     {"OMX.broadcom.egl_render"};
    vw::media::omx::Component _scheduler    {"OMX.broadcom.video_scheduler"};
    vw::media::omx::Component _decoder      {"OMX.broadcom.video_decode"};
    vw::media::omx::Component _clock        {"OMX.broadcom.clock"};

    State _state = IDLE;

    vw::gfx::Image * _image = nullptr;

    uint32_t _src_width = 0;
    uint32_t _src_height = 0;
    uint32_t _dst_width = 0;
    uint32_t _dst_height = 0;

    bool _run = true;
    std::thread _render_thread;
    std::mutex _lock;
    std::condition_variable _cond_var;
};

#endif
