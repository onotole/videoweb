#include <cmath>
#include <log/log.h>
#include <gfx/render_context/render_context.h>
#include <gfx/gl/gl.h>
#include <gfx/gl/canvas.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/texture.h>
#include <gfx/gl/shader_manager.h>
#include "es2drmvideosink_private.h"

void GstEs2DrmVideoSinkPrivate::decoder_thread() {
    bool first_image = true;
    while (_run) {
        if (_state >= START) {
            try {
                auto frame = _decoder->get_frame();
                if (frame->image().data.ptr) {
                    if (first_image) {
                        _export->create(frame->image().header);
                        first_image = false;
                    }
                    _export->update(std::move(frame));
                    _export->commit();
                }
            } catch (...) {}
        }
        std::this_thread::yield();
    }
}

GstEs2DrmVideoSinkPrivate::GstEs2DrmVideoSinkPrivate() {
    _decoder_thread = std::thread(&GstEs2DrmVideoSinkPrivate::decoder_thread, this);
}

GstEs2DrmVideoSinkPrivate::~GstEs2DrmVideoSinkPrivate() {
    _state = IDLE;
    _run = false;
    _decoder_thread.join();
}

void GstEs2DrmVideoSinkPrivate::prepare(const void * data, size_t size) {
    if (_state < INIT) return;
    _state = START;
    _decoder->write(data, size);
}

GstEs2DrmVideoSinkPrivate::State GstEs2DrmVideoSinkPrivate::state() const {
    return _state;
}

const std::string & GstEs2DrmVideoSinkPrivate::uuid() const {
    return _uuid;
}

void GstEs2DrmVideoSinkPrivate::set_uuid(const std::string & uuid) {
    _uuid = uuid;
}

void GstEs2DrmVideoSinkPrivate::init(const std::string & encoding, const void * codec_data, size_t data_size) {
    if (_state > IDLE)
        return;
    _export.reset(new vw::gfx::ipc::Export("local:///tmp/vw.compositor", _uuid));
    _decoder.reset(new RkMppDecoder(MPP_VIDEO_CodingAVC, codec_data, data_size));
    _state = INIT;
}
