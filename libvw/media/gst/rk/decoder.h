#ifndef __VW_GST_RK_DECODER_H__
#define __VW_GST_RK_DECODER_H__

#include <memory>
#include <vector>
#include <rockchip/rk_mpi.h>
#include <rockchip/mpp_buffer.h>
#include "frame.h"

class RkMppDecoder {
public:
    class RkMppFrame : public Frame {
    public:
        virtual ~RkMppFrame();
    private:
        friend class RkMppDecoder;
        RkMppFrame(MppFrame);
        MppFrame _frame = nullptr;
    };

    RkMppDecoder(MppCodingType coding, const void * data, size_t size);
    ~RkMppDecoder();

    void write(const void * data, size_t size);
    std::unique_ptr<Frame> get_frame();

private:
    void introspect_frame(MppFrame frame);

    MppCtx          _context    = nullptr;
    MppApi *        _mpi        = nullptr;
    MppBufferGroup  _buffers    = nullptr;

    bool _first_packet  = true;
    bool _eos           = false;
};

#endif // __VW_GST_RK_DECODER_H__
