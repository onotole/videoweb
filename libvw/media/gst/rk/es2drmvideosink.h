#ifndef __VW_MEDIA_GST_ES2EGLVIDEOSINK_H__
#define __VW_MEDIA_GST_ES2EGLVIDEOSINK_H__

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>

struct GstEs2DrmVideoSinkPrivate;

typedef struct _GstEs2DrmVideoSink {
    GstBaseSink parent_element;
    GstEs2DrmVideoSinkPrivate * priv;
} GstEs2DrmVideoSink;

typedef struct _GstEs2DrmVideoSinkClass {
    GstBaseSinkClass parent_class;
} GstEs2DrmVideoSinkClass;

enum { PROP_UUID = 1 };

/* Standard macros for defining types for this element.  */
#define GST_TYPE_ES2DRM_VIDEO_SINK (gst_es2drm_video_sink_get_type())
#define GST_ES2DRM_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_ES2DRM_VIDEO_SINK, GstEs2DrmVideoSink))
#define GST_ES2DRM_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_ES2DRM_VIDEO_SINK, GstEs2DrmVideoSinkClass))
#define GST_IS_ES2DRM_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_ES2DRM_VIDEO_SINK)
#define GST_IS_ES2DRM_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_ES2DRM_VIDEO_SINK))
#define GST_ES2DRM_VIDEO_SINK_CAST(obj)  ((GstEs2DrmVideoSink *) (obj))

GType gst_es2drm_video_sink_get_type(void);

#endif // __VW_MEDIA_GST_ES2EGLVIDEOSINK_H__
