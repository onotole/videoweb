#ifndef __VW_MEDIA_GST_ES2EGLVIDEOSINK_PRIVATE_H__
#define __VW_MEDIA_GST_ES2EGLVIDEOSINK_PRIVATE_H__

#include <string>
#include <memory>
#include <thread>
#include <gfx/ipc/export.h>
#include <gfx/render_context/irender_context.h>
#include "decoder.h"

class GstEs2DrmVideoSinkPrivate {
public:
    enum State {
        IDLE,
        INIT,
        PREROLL,
        START
    };
    GstEs2DrmVideoSinkPrivate();
    ~GstEs2DrmVideoSinkPrivate();

    const std::string & uuid() const;
    void set_uuid(const std::string & uuid);

    void init(const std::string & encoding, const void *codec_data = nullptr, size_t data_size = 0);
    void prepare(const void * data, size_t size);

    State state() const;

private:
    void decoder_thread();

    std::string _uuid = "00000000-0000-0000-0000-000000000000";

    std::unique_ptr<vw::gfx::ipc::Export> _export;
    std::unique_ptr<RkMppDecoder> _decoder;

    State _state = IDLE;

    bool _run = true;
    std::thread _decoder_thread;
};

#endif
