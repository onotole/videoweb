#include <stdexcept>
#include <iostream>
#include <thread>
#include "decoder.h"
#include <drm_fourcc.h>

#define RECEIVE_FRAME_TIMEOUT   100
#define FRAMEGROUP_MAX_FRAMES   16
#define INPUT_MAX_PACKETS       4

RkMppDecoder::~RkMppDecoder() {
       if (_mpi) {
           _mpi->reset(_context);
           mpp_destroy(_context);
       }
       if (_buffers)
           mpp_buffer_group_put(_buffers);
}

RkMppDecoder::RkMppDecoder(MppCodingType coding, const void * data, size_t size) {
    auto error = mpp_create(&_context, &_mpi);
    if (error != MPP_OK)
        throw std::runtime_error("failed to create mpp context");

    RK_S32 param0 = 1;
    error = _mpi->control(_context, MPP_DEC_SET_PARSER_SPLIT_MODE, &param0);
    if (error != MPP_OK)
        throw std::runtime_error("failed to set decoder into split mode");

    error = mpp_init(_context, MPP_CTX_DEC, coding);
    if (error != MPP_OK)
        throw std::runtime_error("failed to init mpp decoder");

    // set decoder calls blocking
    RK_S32 param1 = MPP_POLL_BLOCK;
    error = _mpi->control(_context, MPP_SET_OUTPUT_BLOCK, &param1);
    if (error != MPP_OK)
        throw std::runtime_error("failed to set decoder in blocking mode");

    RK_S64 param2 = RECEIVE_FRAME_TIMEOUT;
    _mpi->control(_context, MPP_SET_OUTPUT_BLOCK_TIMEOUT, &param2);
    if (error != MPP_OK)
        throw std::runtime_error("failed to set decoder block timeout");

    error = mpp_buffer_group_get_internal(&_buffers, MPP_BUFFER_TYPE_DRM);
    if (error != MPP_OK)
        throw std::runtime_error("failed to get buffer group");

    error = _mpi->control(_context, MPP_DEC_SET_EXT_BUF_GROUP, _buffers);
    if (error != MPP_OK)
        throw std::runtime_error("failed to set external buffer group");

    error = mpp_buffer_group_limit_config(_buffers, 0, FRAMEGROUP_MAX_FRAMES);
    if (error != MPP_OK)
        throw std::runtime_error("failed to configure buffer number limit");

    MppPacket packet = nullptr;
    error = mpp_packet_init(&packet, const_cast<void*>(data), size);
    if (error != MPP_OK)
        throw std::runtime_error("failed to init mpp packet");

    error = mpp_packet_set_extra_data(packet);
    if (error != MPP_OK)
        throw std::runtime_error("failed to set extra data");

    error = _mpi->decode_put_packet(_context, packet);
    if (error != MPP_OK)
        throw std::runtime_error("failed to send decoder packet");

    error = mpp_packet_deinit(&packet);
    if (error != MPP_OK)
        throw std::runtime_error("failed to deinit mpp packet");

    std::cout << "sent init data of " << size << " bytes to decoder" << std::endl;
}

void RkMppDecoder::write(const void * data, size_t size) {
    MppPacket packet = nullptr;
    auto error = mpp_packet_init(&packet, const_cast<void*>(data), size);
    if (error != MPP_OK)
        throw std::runtime_error("failed to init mpp packet");
    mpp_packet_set_pos(packet, const_cast<void*>(data));
    mpp_packet_set_length(packet, size);
    while (MPP_OK != _mpi->decode_put_packet(_context, packet))
        std::this_thread::yield();
    error = mpp_packet_deinit(&packet);
}

RkMppDecoder::RkMppFrame::RkMppFrame(MppFrame frame) : _frame(frame) {
    _image.header.memory = vw::gfx::Image::Memory::DMA;
    _image.header.format = DRM_FORMAT_NV12;
    _image.header.width = mpp_frame_get_width(_frame);
    _image.header.height = mpp_frame_get_height(_frame);
    MppBuffer buffer = mpp_frame_get_buffer(_frame);
    if (buffer) {
        _image.data.size = sizeof(vw::gfx::DMAImage);
        vw::gfx::DMAImage * image = new vw::gfx::DMAImage();
        image->num_planes = 2;
        image->planes[0].fd = mpp_buffer_get_fd(buffer);
        image->planes[0].offset = 0;
        image->planes[0].pitch = mpp_frame_get_hor_stride(_frame);
        image->planes[1].fd = mpp_buffer_get_fd(buffer);
        image->planes[1].offset = image->planes[0].pitch * mpp_frame_get_ver_stride(_frame);
        image->planes[1].pitch = image->planes[0].pitch;
        _image.data.ptr = image;
    } else {
        _image.data.size = 0;
        _image.data.ptr = nullptr;
    }
    _flags = NO_FLAGS;
    if (mpp_frame_get_eos(_frame))
        _flags |= FLAG_EOS;
    if (mpp_frame_get_info_change(_frame))
        _flags |= FLAG_CHANGE;
    if (mpp_frame_get_discard(_frame))
        _flags |= FLAG_DISCARD;
}

RkMppDecoder::RkMppFrame::~RkMppFrame() {
    delete static_cast<vw::gfx::DMAImage *>(_image.data.ptr);
    mpp_frame_deinit(&_frame);
}

std::unique_ptr<Frame> RkMppDecoder::get_frame() {
    MppFrame frame = nullptr;
    auto error = _mpi->decode_get_frame(_context, &frame);
    if (error != MPP_OK) {
        std::cerr << "Error getting frame: " << error << std::endl;
        throw std::runtime_error("failed to get decoded frame");
    }
    if (mpp_frame_get_info_change(frame))
        _mpi->control(_context, MPP_DEC_SET_INFO_CHANGE_READY, nullptr);
    return std::unique_ptr<Frame>(new RkMppFrame(frame));
}
