#include <gst/gst.h>
#include "es2drmvideosink.h"

static gboolean plugin_init(GstPlugin * plugin) {
    gboolean result = gst_element_register (plugin, "es2drmvideosink",
                                            GST_RANK_NONE, GST_TYPE_ES2DRM_VIDEO_SINK);
    return result;
}

#define STUB " "
#define VERSION STUB
#define PACKAGE STUB

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR, GST_VERSION_MINOR,
                   vwgstrk, "Collection of Rockchip platform specific elements",
                   plugin_init, VERSION, GST_LICENSE_UNKNOWN, STUB, STUB);
