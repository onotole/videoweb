#ifndef __VW_GST_RK_FRAME_H__
#define __VW_GST_RK_FRAME_H__

#include <memory>
#include <gfx/image.h>
#include <gfx/ipc/export.h>

struct Frame : vw::gfx::ipc::Export::Packet {
    enum Flags {
        NO_FLAGS        = 0x00,
        FLAG_EOS        = 0x01,
        FLAG_CHANGE     = 0x02,
        FLAG_DISCARD    = 0x04,
    };
    virtual ~Frame() = default;
    const vw::gfx::ImageHandle handle() const override {
        return _image.data.ptr;
    }
    const vw::gfx::Image & image() const { return _image; }
    int flags() const { return _flags; }
protected:
    Frame() {}
    vw::gfx::Image _image = {};
    int _flags = 0;
};

#endif // __VW_GST_RK_FRAME_H__
