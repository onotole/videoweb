#ifndef __VW_MEDIA_GST_V4L2M2MVIDEOSINK_H__
#define __VW_MEDIA_GST_V4L2M2MVIDEOSINK_H__

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>

struct GstV4l2m2mVideoSinkPrivate;

typedef struct _V4l2m2mVideoSink {
    GstBaseSink parent_element;
    GstV4l2m2mVideoSinkPrivate * priv;
} GstV4l2m2mVideoSink;

typedef struct _GstV4l2m2mVideoSinkClass {
    GstBaseSinkClass parent_class;
} GstV4l2m2mVideoSinkClass;

enum { PROP_UUID = 1 };

/* Standard macros for defining types for this element.  */
#define GST_TYPE_V4L2M2M_VIDEO_SINK (gst_v4l2m2m_video_sink_get_type())
#define GST_V4L2M2M_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_V4L2M2M_VIDEO_SINK, GstV4l2m2mVideoSink))
#define GST_V4L2M2M_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_V4L2M2M_VIDEO_SINK, GstV4l2m2mVideoSinkClass))
#define GST_IS_V4L2M2M_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_V4L2M2M_VIDEO_SINK)
#define GST_IS_V4L2M2M_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_V4L2M2M_VIDEO_SINK))
#define GST_V4L2M2M_VIDEO_SINK_CAST(obj)  ((GstV4l2m2mVideoSink *) (obj))

GType gst_v4l2m2m_video_sink_get_type(void);

#endif // __VW_MEDIA_GST_V4L2M2MVIDEOSINK_H__
