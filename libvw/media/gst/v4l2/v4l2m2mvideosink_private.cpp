#include <cmath>
#include <log/log.h>
#include <gfx/render_context/render_context.h>
#include <gfx/gl/gl.h>
#include <gfx/gl/canvas.h>
#include <gfx/gl/frame.h>
#include <gfx/gl/mesh.h>
#include <gfx/gl/texture.h>
#include <gfx/gl/shader_manager.h>
#include "v4l2m2mvideosink_private.h"

void GstV4l2m2mVideoSinkPrivate::decoder_thread() {
    bool first_image = true;
    while (_run) {
        if (_state >= START) {
            try {
                auto frame = _decoder->get_frame();
                if (frame && frame->image().data.ptr) {
                    if (first_image) {
                        _export->create(frame->image().header);
                        first_image = false;
                    }
                    _export->update(std::move(frame));
                    _export->commit();
                }
            } catch (...) {}
        }
        std::this_thread::yield();
    }
}

GstV4l2m2mVideoSinkPrivate::GstV4l2m2mVideoSinkPrivate() {
    _decoder_thread = std::thread(&GstV4l2m2mVideoSinkPrivate::decoder_thread, this);
}

GstV4l2m2mVideoSinkPrivate::~GstV4l2m2mVideoSinkPrivate() {
    _state = IDLE;
    _run = false;
    _decoder_thread.join();
}

void GstV4l2m2mVideoSinkPrivate::prepare(const void * data, size_t size, uint64_t pts) {
    if (_state < INIT) return;
    _state = START;
    _decoder->write(data, size, pts);
}

GstV4l2m2mVideoSinkPrivate::State GstV4l2m2mVideoSinkPrivate::state() const {
    return _state;
}

const std::string & GstV4l2m2mVideoSinkPrivate::uuid() const {
    return _uuid;
}

void GstV4l2m2mVideoSinkPrivate::set_uuid(const std::string & uuid) {
    _uuid = uuid;
}

void GstV4l2m2mVideoSinkPrivate::init(const std::string & encoding, uint32_t width, uint32_t height,
                                      const void * codec_data, size_t data_size) {
    (void) encoding; // TODO: map encoding to pixelformat
    if (_state > IDLE)
        return;
    _export.reset(new vw::gfx::ipc::Export("local:///tmp/vw.compositor", _uuid));

    v4l2_format format {};
    format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    format.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_H264;
    format.fmt.pix_mp.width = width;
    format.fmt.pix_mp.height = height;

    _decoder.reset(new V4l2m2mDecoder(format, codec_data, data_size));
    _state = INIT;
}
