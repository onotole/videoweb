#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>

#undef NDEBUG
#include <cassert>
#include <stdexcept>
#include <iostream>
#include <thread>
#include <fstream>
#include <chrono>
#include <log/log.h>

#include <drm_fourcc.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

#include "decoder.h"

namespace {
int xioctl(int fd, int request, void * arg) {
    int result = -1;
    do
        result = ioctl(fd, request, arg);
    while (result < 0 && EINTR == errno);
    if (result < 0 && EAGAIN != errno) {
        //ERROR << "ioctl failed: request = 0x" << std::hex << request << ", error = " << strerror(errno);
    }
    return result;
}

void log_capabilities(const v4l2_capability & caps) {
    INFO << "driver: " << (const char*)caps.driver
         << ", card: " << (const char*)caps.card
         << ", bus: "  << (const char*)caps.bus_info;
    INFO << "device capabilities: 0x" << std::hex << caps.capabilities << std::dec
         << " (" << (caps.capabilities & (V4L2_CAP_VIDEO_M2M_MPLANE |
                                          V4L2_CAP_VIDEO_M2M) ? "m2m" : "")
         << (caps.capabilities & (V4L2_CAP_VIDEO_CAPTURE_MPLANE |
                                  V4L2_CAP_VIDEO_CAPTURE) ? " capture" : "")
         << (caps.capabilities & (V4L2_CAP_VIDEO_OUTPUT_MPLANE |
                                  V4L2_CAP_VIDEO_OUTPUT) ? " output" : "")
         << (caps.capabilities & V4L2_CAP_STREAMING ? " streaming" : "") << ")";
}

constexpr uint64_t invalid_pts = -1ULL;
constexpr uint32_t num_capture_buffers = 16;
constexpr uint32_t num_output_buffers = 16;
constexpr uint64_t usec_to_nsec = 1000ULL;
constexpr uint64_t sec_to_usec = 1000000ULL;
constexpr uint64_t sec_to_nsec = sec_to_usec * usec_to_nsec;
// TODO: std::chrono::high_resolution_clock vs std::chrono::steady_clock
}

V4l2m2mDecoder::buffer_base::buffer_base(int fd, const v4l2_format & fmt)
    : vid_fd(fd), format(fmt) {}

void V4l2m2mDecoder::buffer_base::enqueue() {
    if (xioctl(vid_fd, VIDIOC_QBUF, &buffer) >= 0)
        in_driver = true;
}

uint64_t V4l2m2mDecoder::buffer_base::pts() const {
    return buffer.timestamp.tv_sec * sec_to_usec + buffer.timestamp.tv_usec;
}

V4l2m2mDecoder::output_buffer::output_buffer(int fd, const v4l2_format & fmt, uint32_t idx)
    : buffer_base(fd, fmt) {
    buffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    buffer.memory = V4L2_MEMORY_MMAP;
    buffer.index = idx;
    buffer.length = format.fmt.pix_mp.num_planes;
    buffer.m.planes = planes;
    xioctl(vid_fd, VIDIOC_QUERYBUF, &buffer);
    for (uint32_t i = 0; i < buffer.length; ++i)
        maps[i].ptr = mmap(nullptr, format.fmt.pix_mp.plane_fmt[i].sizeimage,
                           PROT_READ | PROT_WRITE, MAP_SHARED, vid_fd,
                           planes[i].m.mem_offset);
}

V4l2m2mDecoder::output_buffer::~output_buffer() {
    for (auto p = 0; p < format.fmt.pix_mp.num_planes; ++p)
        munmap(maps[p].ptr, format.fmt.pix_mp.plane_fmt[p].sizeimage);
}

void V4l2m2mDecoder::output_buffer::write(const void * data, uint32_t size, uint64_t pts) {
    memcpy(maps[0].ptr, data, size);
    planes[0].bytesused = size;
    if (pts == invalid_pts) {
        buffer.flags |= V4L2_BUF_FLAG_TIMESTAMP_UNKNOWN;
    } else {
        buffer.timestamp.tv_sec = pts / sec_to_nsec;
        buffer.timestamp.tv_usec = pts % sec_to_nsec / usec_to_nsec;
        buffer.flags |= V4L2_BUF_FLAG_TIMESTAMP_COPY;
    }
    enqueue();
}

V4l2m2mDecoder::capture_buffer::capture_buffer(int drmfd, int vidfd, const v4l2_format & fmt, uint32_t idx)
    : buffer_base(vidfd, fmt), drm_fd(drmfd) {
    buffer.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    buffer.memory = V4L2_MEMORY_DMABUF;
    buffer.index = idx;
    buffer.length = format.fmt.pix_mp.num_planes;
    buffer.m.planes = planes;
    xioctl(vid_fd, VIDIOC_QUERYBUF, &buffer);
    for (uint32_t i = 0; i < buffer.length; ++i) {
        drm_mode_create_dumb dmcb{};
        dmcb.bpp = 8;
        dmcb.width = format.fmt.pix_mp.plane_fmt[i].bytesperline;
        dmcb.height = format.fmt.pix_mp.plane_fmt[i].sizeimage /
                format.fmt.pix_mp.plane_fmt[i].bytesperline;
        xioctl(drm_fd, DRM_IOCTL_MODE_CREATE_DUMB, &dmcb);

        drm_prime_handle dmhd {};
        dmhd.handle = dmcb.handle;
        xioctl(drm_fd, DRM_IOCTL_PRIME_HANDLE_TO_FD, &dmhd);

        maps[i].hdl = dmhd.handle;
        planes[i].m.fd = dmhd.fd;
    }
    enqueue();
}

V4l2m2mDecoder::capture_buffer::~capture_buffer() {
    for (auto p = 0; p < format.fmt.pix_mp.num_planes; ++p) {
        drm_mode_destroy_dumb dmdb {};
        dmdb.handle = maps[p].hdl;
        xioctl(drm_fd, DRM_IOCTL_MODE_DESTROY_DUMB, &dmdb);
    }
}

V4l2m2mDecoder::V4l2m2mDecoder(const v4l2_format & format, const void * data, size_t size)
    : _output_format(format) {
    _capture_format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    _capture_format.fmt.pix_mp.pixelformat = V4L2_PIX_FMT_NV12M;
    _capture_format.fmt.pix_mp.width = _output_format.fmt.pix_mp.width;
    _capture_format.fmt.pix_mp.height = _output_format.fmt.pix_mp.height;

    _output_buffers.reserve(num_output_buffers);
    _capture_buffers.reserve(num_capture_buffers);

    configure();
}

V4l2m2mDecoder::~V4l2m2mDecoder() {
    stop();
    close();
}

void V4l2m2mDecoder::write(const void * data, size_t size, uint64_t pts) {
    if (size) {
        dequeue_output().write(data, size, pts);
        start();
    }
}

void V4l2m2mDecoder::configure() {
    _vid_fd = open("/dev/video0", O_RDWR | O_NONBLOCK);
    if (_vid_fd == -1)
        throw std::runtime_error("failed to open codec");
    _drm_fd = open("/dev/dri/card0", O_RDWR);
    if (_drm_fd == -1)
        throw std::runtime_error("failed to open drm");

    v4l2_capability caps {};
    xioctl(_vid_fd, VIDIOC_QUERYCAP, &caps);
    log_capabilities(caps);

    xioctl(_vid_fd, VIDIOC_S_FMT, &_output_format);
    xioctl(_vid_fd, VIDIOC_S_FMT, &_capture_format);

    v4l2_requestbuffers req_buffers {};
    req_buffers.count = num_output_buffers;
    req_buffers.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    req_buffers.memory = V4L2_MEMORY_MMAP;
    xioctl(_vid_fd, VIDIOC_REQBUFS, &req_buffers);
    for (uint32_t i = 0; i < req_buffers.count; ++i)
        _output_buffers.emplace_back(_vid_fd, _output_format, i);
}

void V4l2m2mDecoder::start() {
    if (_capture_buffers.size())
        return;

    v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    xioctl(_vid_fd, VIDIOC_STREAMON, &type);

    // allocate capture buffers
    xioctl(_vid_fd, VIDIOC_G_FMT, &_capture_format);
    v4l2_requestbuffers req_buffers {};
    req_buffers.count = num_capture_buffers;
    req_buffers.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    req_buffers.memory = V4L2_MEMORY_DMABUF;
    xioctl(_vid_fd, VIDIOC_REQBUFS, &req_buffers);
    for (uint32_t i = 0; i < req_buffers.count; ++i)
        _capture_buffers.emplace_back(_drm_fd, _vid_fd, _capture_format, i);

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    xioctl(_vid_fd, VIDIOC_STREAMON, &type);
}

void V4l2m2mDecoder::stop() {
    v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    xioctl(_vid_fd, VIDIOC_STREAMOFF, &type);
    type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    xioctl(_vid_fd, VIDIOC_STREAMOFF, &type);
}

void V4l2m2mDecoder::close() {
    _output_buffers.clear();
    _capture_buffers.clear();

    v4l2_requestbuffers req_buffers {};
    req_buffers.count = 0;
    req_buffers.type = V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE;
    req_buffers.memory = V4L2_MEMORY_MMAP;
    xioctl(_vid_fd, VIDIOC_REQBUFS, &req_buffers);
    req_buffers.type = V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE;
    req_buffers.memory = V4L2_MEMORY_DMABUF;
    xioctl(_vid_fd, VIDIOC_REQBUFS, &req_buffers);
    ::close(_vid_fd);
    ::close(_drm_fd);
}

V4l2m2mDecoder::output_buffer & V4l2m2mDecoder::dequeue_output() {
    // reclaim buffer from driver
    pollfd fds {_vid_fd, POLLOUT};
    if (poll(&fds, 1, -1) > 0 && fds.revents & POLLOUT) {
        v4l2_plane planes[1] {};
        v4l2_buffer buffer {};
        buffer.type = _output_format.type;
        buffer.memory = V4L2_MEMORY_MMAP;
        buffer.length = 1;
        buffer.m.planes = planes;
        xioctl(_vid_fd, VIDIOC_DQBUF, &buffer);
        _output_buffers[buffer.index].in_driver = false;
        return _output_buffers[buffer.index];
    }
    for (auto & out_buffer : _output_buffers)
        if (!out_buffer.in_driver)
            return out_buffer;
}

V4l2m2mDecoder::capture_buffer & V4l2m2mDecoder::dequeue_capture() {
    v4l2_plane planes[2] {};
    v4l2_buffer buffer {};
    buffer.type = _capture_format.type;
    buffer.memory = V4L2_MEMORY_DMABUF;
    buffer.length = 2;
    buffer.m.planes = planes;
    xioctl(_vid_fd, VIDIOC_DQBUF, &buffer);
    auto & cap_buffer = _capture_buffers[buffer.index];
    cap_buffer.buffer.timestamp = buffer.timestamp;
    return cap_buffer;
}

V4l2m2mDecoder::V4l2m2mFrame::V4l2m2mFrame(capture_buffer & buffer)
    : _buffer(buffer) {
    _image.header.memory = vw::gfx::Image::Memory::DMA;
    _image.header.width = buffer.format.fmt.pix_mp.width;
    _image.header.height = buffer.format.fmt.pix_mp.height;
    _image.header.format = DRM_FORMAT_NV12; // TODO: actual format
    _image.data.size = sizeof(vw::gfx::DMAImage);
    vw::gfx::DMAImage * image = new vw::gfx::DMAImage();
    image->num_planes = 2;
    image->planes[0].fd = buffer.buffer.m.planes[0].m.fd;
    image->planes[0].offset = 0;
    image->planes[0].pitch = _image.header.width;
    image->planes[1].fd = buffer.buffer.m.planes[1].m.fd;
    image->planes[1].offset = 0;
    image->planes[1].pitch = _image.header.width / 2;
    _image.data.ptr = image;
    _pts = _buffer.pts();
}

V4l2m2mDecoder::V4l2m2mFrame::~V4l2m2mFrame() {
    delete static_cast<vw::gfx::DMAImage *>(_image.data.ptr);
    _buffer.enqueue();
}

uint64_t V4l2m2mDecoder::now() const {
    using namespace std::chrono;
    if (_stream_time_offset_us == -1ULL)
        return _stream_time_offset_us;
    return duration_cast<microseconds>
            (steady_clock::now().time_since_epoch()).count() - _stream_time_offset_us;
}

std::unique_ptr<Frame> V4l2m2mDecoder::get_frame() {
    using namespace std::chrono;
    std::unique_ptr<Frame> frame;
    while(true) {
        // do we have ready buffer in queue?
        if (_decoded_queue.size()) {
            if (now() >= _decoded_queue.front()->pts()) {
                frame.reset(_decoded_queue.front().release());
                _decoded_queue.pop_front();
                return frame;
            }
        }
        // poll next frame
        pollfd fds {_vid_fd, POLLIN};
        if (poll(&fds, 1, 0) > 0 && fds.revents & POLLIN) {
            auto & buffer = dequeue_capture();
            if (_stream_time_offset_us == -1ULL) {
                _stream_time_offset_us = duration_cast<microseconds>
                        (steady_clock::now().time_since_epoch()).count() + buffer.pts();
            }
            frame = std::unique_ptr<V4l2m2mFrame>(new V4l2m2mFrame(buffer));
            if (now() >= frame->pts())
                return frame;
            else
                _decoded_queue.emplace_back(frame.release());
        }
        std::this_thread::yield();
    }
    return frame;
}
