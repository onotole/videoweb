#ifndef __VW_MEDIA_GST_V4L2M2MVIDEOSINK_PRIVATE_H__
#define __VW_MEDIA_GST_V4L2M2MVIDEOSINK_PRIVATE_H__

#include <cstdint>
#include <string>
#include <memory>
#include <thread>
#include <gfx/ipc/export.h>
#include <gfx/render_context/irender_context.h>
#include "decoder.h"

class GstV4l2m2mVideoSinkPrivate {
public:
    enum State {
        IDLE,
        INIT,
        PREROLL,
        START
    };
    GstV4l2m2mVideoSinkPrivate();
    ~GstV4l2m2mVideoSinkPrivate();

    const std::string & uuid() const;
    void set_uuid(const std::string & uuid);

    void init(const std::string & encoding, uint32_t width, uint32_t height,
              const void *codec_data = nullptr, size_t data_size = 0);
    void prepare(const void * data, size_t size, uint64_t pts);

    State state() const;

private:
    void decoder_thread();

    std::string _uuid = "00000000-0000-0000-0000-000000000000";

    std::unique_ptr<vw::gfx::ipc::Export> _export;
    std::unique_ptr<V4l2m2mDecoder> _decoder;

    State _state = IDLE;

    bool _run = true;
    std::thread _decoder_thread;
};

#endif // __VW_MEDIA_GST_V4L2M2MVIDEOSINK_PRIVATE_H__
