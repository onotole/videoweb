#ifndef __VW_GST_V4L2M2M_DECODER_H__
#define __VW_GST_V4L2M2M_DECODER_H__

#include <memory>
#include <vector>
#include <deque>
#include <linux/videodev2.h>

#include "frame.h"

class V4l2m2mDecoder {
public:
    struct buffer_base {
        static constexpr uint32_t max_planes = 3;
        buffer_base(int fd, const v4l2_format & fmt);
        virtual ~buffer_base() = default;

        void enqueue();
        // pts in usec
        uint64_t pts() const;

        const v4l2_format & format;
        v4l2_buffer buffer = {};
        v4l2_plane planes[max_planes] = {};
        union {
            void * ptr = nullptr;
            uint32_t hdl;
        } maps[max_planes] = {};
        int vid_fd = -1;
        bool in_driver = false;
    };

    struct output_buffer : buffer_base {
        output_buffer(int fd, const v4l2_format & fmt, uint32_t idx);
        void write(const void * data, uint32_t size, uint64_t pts);
        virtual ~output_buffer();
    };

    struct capture_buffer : buffer_base {
        capture_buffer(int drmfd, int vidfd, const v4l2_format & fmt, uint32_t idx);
        virtual ~capture_buffer();
        int drm_fd = -1;
    };

    class V4l2m2mFrame : public Frame {
    public:
        V4l2m2mFrame(capture_buffer &);
        virtual ~V4l2m2mFrame();
    private:
        capture_buffer & _buffer;
    };

    V4l2m2mDecoder(const v4l2_format & format, const void * data, size_t size);
    ~V4l2m2mDecoder();

    void write(const void * data, size_t size, uint64_t pts);
    std::unique_ptr<Frame> get_frame();

private:
    void configure();
    void start();
    void stop();
    void close();
    uint64_t now() const;

    output_buffer & dequeue_output();
    capture_buffer & dequeue_capture();

    int _vid_fd = -1;
    int _drm_fd = -1;
    uint64_t _stream_time_offset_us = -1ULL;
    v4l2_format _output_format;
    v4l2_format _capture_format {};
    std::vector<output_buffer> _output_buffers;
    std::vector<capture_buffer> _capture_buffers;
    std::deque<std::unique_ptr<Frame>> _decoded_queue;
};

#endif // __VW_GST_V4L2M2M_DECODER_H__
