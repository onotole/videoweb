#include <gst/gst.h>
#include "v4l2m2mvideosink.h"

static gboolean plugin_init(GstPlugin * plugin) {
    gboolean result = gst_element_register (plugin, "v4l2m2mvideosink",
                                            GST_RANK_NONE, GST_TYPE_V4L2M2M_VIDEO_SINK);
    return result;
}

#define STUB " "
#define VERSION STUB
#define PACKAGE STUB

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR, GST_VERSION_MINOR,
                   vwgstv4l2, "Collection of V4L2 specific elements",
                   plugin_init, VERSION, GST_LICENSE_UNKNOWN, STUB, STUB);
