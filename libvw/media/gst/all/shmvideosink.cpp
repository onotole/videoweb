#include "shmvideosink.h"
#include "shmvideosink_private.h"
#include <log/log.h>

static GstStaticPadTemplate sinktemplate = GST_STATIC_PAD_TEMPLATE
		("sink", GST_PAD_SINK, GST_PAD_ALWAYS, GST_STATIC_CAPS
			("video/x-raw, format = (string) {RGBx}"));

static void gst_shm_video_sink_init (GstShmVideoSink * sink) {
	TRACE << "shmvideosink init...";
	sink->priv = new GstShmVideoSinkPrivate;
	gst_video_info_init(&sink->video_info);
}

G_DEFINE_TYPE (GstShmVideoSink, gst_shm_video_sink, GST_TYPE_BASE_SINK);

static void gst_shm_video_sink_finalize (GstShmVideoSink * vsink) {
	TRACE << "shmvideosink finalize...";
	delete vsink->priv;
	G_OBJECT_CLASS(gst_shm_video_sink_parent_class)->finalize(G_OBJECT(vsink));
}

static void gst_shm_video_sink_set_property (GObject * object, guint prop_id,
	const GValue * value, GParamSpec * pspec) {
	GstShmVideoSink * vsink = GST_SHM_VIDEO_SINK(object);
	switch (prop_id) {
		case PROP_UUID:
			GST_OBJECT_LOCK(vsink);
			vsink->priv->set_uuid(g_value_get_string(value));
			GST_OBJECT_UNLOCK(vsink);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static void gst_shm_video_sink_get_property (GObject * object, guint prop_id,
	GValue * value, GParamSpec * pspec) {
	GstShmVideoSink * vsink = GST_SHM_VIDEO_SINK(object);
	switch (prop_id) {
		case PROP_UUID:
			GST_OBJECT_LOCK(vsink);
			g_value_set_string(value, vsink->priv->uuid().c_str());
			GST_OBJECT_UNLOCK(vsink);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
			break;
	}
}

static gboolean gst_shm_video_sink_set_caps(GstBaseSink * bsink, GstCaps * caps) {
	GstShmVideoSink * vsink = GST_SHM_VIDEO_SINK_CAST(bsink);
	gst_video_info_from_caps(&vsink->video_info, caps);
	vsink->priv->init(vsink->video_info.width, vsink->video_info.height);
	return true;
}

static GstFlowReturn gst_shm_video_sink_prepare(GstBaseSink * bsink, GstBuffer * buf) {
	GstShmVideoSink * vsink = GST_SHM_VIDEO_SINK_CAST(bsink);
	GstVideoFrame frame;
	gst_video_frame_map(&frame, &vsink->video_info, buf, GST_MAP_READ);
	vsink->priv->prepare(GST_VIDEO_FRAME_PLANE_DATA(&frame, 0));
	gst_video_frame_unmap(&frame);

	return GST_FLOW_OK;
}

static GstFlowReturn gst_shm_video_sink_render(GstBaseSink * bsink, GstBuffer * buf) {
	GstShmVideoSink * vsink = GST_SHM_VIDEO_SINK_CAST(bsink);
	vsink->priv->render();

	return GST_FLOW_OK;
}

static void gst_shm_video_sink_class_init(GstShmVideoSinkClass * klass) {
	GObjectClass * object_class = G_OBJECT_CLASS(klass);
	GstElementClass * element_class = GST_ELEMENT_CLASS(klass);
	GstBaseSinkClass * base_class = GST_BASE_SINK_CLASS(klass);

	object_class->finalize = (GObjectFinalizeFunc) gst_shm_video_sink_finalize;
	// TODO: set construct only property
	object_class->set_property = gst_shm_video_sink_set_property;
	object_class->get_property = gst_shm_video_sink_get_property;

	g_object_class_install_property (object_class, PROP_UUID,
		g_param_spec_string ("uuid", "Unique stream id",
			"Id to be used for stream identification",
			"00000000-0000-0000-0000-000000000000",
			(GParamFlags)(G_PARAM_READWRITE|G_PARAM_STATIC_STRINGS)));

	gst_element_class_add_pad_template (element_class,
		gst_static_pad_template_get (&sinktemplate));

	gst_element_class_set_static_metadata(element_class,
		"Shared memory video sink",
		"Sink/Video",
		"Video sink element that writes frames to shared memory",
		"Anatoliy Klymenko <anatoly.klimenko.ua@gmail.com>");

	// init buffer here...
	base_class->set_caps = GST_DEBUG_FUNCPTR(gst_shm_video_sink_set_caps);
	// copy buffer here...
	base_class->prepare = GST_DEBUG_FUNCPTR(gst_shm_video_sink_prepare);
	// trigger update here...
	base_class->render = GST_DEBUG_FUNCPTR(gst_shm_video_sink_render);
}

