#ifndef __VW_MEDIA_GST_SHMVIDEOSINK_H__
#define __VW_MEDIA_GST_SHMVIDEOSINK_H__

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/video/video.h>

typedef struct GstShmVideoSinkPrivate GstShmVideoSinkPrivate;

typedef struct _GstShmVideoSink {
	GstBaseSink parent_element;
	GstVideoInfo video_info;
	GstShmVideoSinkPrivate * priv;
} GstShmVideoSink;

typedef struct _GstShmVideoSinkClass {
	GstBaseSinkClass parent_class;
} GstShmVideoSinkClass;

enum { PROP_UUID = 1 };

/* Standard macros for defining types for this element.  */
#define GST_TYPE_SHM_VIDEO_SINK (gst_shm_video_sink_get_type())
#define GST_SHM_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_SHM_VIDEO_SINK, GstShmVideoSink))
#define GST_SHM_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_SHM_VIDEO_SINK, GstShmVideoSinkClass))
#define GST_IS_SHM_VIDEO_SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_SHM_VIDEO_SINK)
#define GST_IS_SHM_VIDEO_SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_SHM_VIDEO_SINK))
#define GST_SHM_VIDEO_SINK_CAST(obj)  ((GstShmVideoSink *) (obj))

GType gst_shm_video_sink_get_type(void);

#endif // __VW_MEDIA_GST_SHMVIDEOSINK_H__
