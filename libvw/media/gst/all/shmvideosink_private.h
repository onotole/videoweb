#ifndef __VW_MEDIA_GST_SHMVIDEOSINK_PRIVATE_H__
#define __VW_MEDIA_GST_SHMVIDEOSINK_PRIVATE_H__

#include <string>
#include <memory>
#include <gfx/ipc/export.h>

class GstShmVideoSinkPrivate {
public:
    const std::string & uuid() const;
    void set_uuid(const std::string & uuid);

    void init(size_t width, size_t height);
    void prepare(const void * data);
    void render();

private:
    std::string _uuid;
    std::unique_ptr<vw::gfx::ipc::Export> _export;
};

#endif
