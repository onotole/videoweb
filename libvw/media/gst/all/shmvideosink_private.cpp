#include "shmvideosink_private.h"

struct Packet : vw::gfx::ipc::Export::Packet {
    Packet(const void * data) : _handle((vw::gfx::ImageHandle)data) {}
    const vw::gfx::ImageHandle handle() const override { return _handle; }
    const vw::gfx::ImageHandle _handle;
};

void GstShmVideoSinkPrivate::prepare(const void * data) {
    _export->update(std::unique_ptr<Packet>(new Packet(data)));
}

void GstShmVideoSinkPrivate::render() {
    _export->commit();
}

const std::string & GstShmVideoSinkPrivate::uuid() const {
    return _uuid;
}

void GstShmVideoSinkPrivate::set_uuid(const std::string & uuid) {
    _uuid = uuid;
}

void GstShmVideoSinkPrivate::init(size_t width, size_t height) {
    vw::gfx::Image::Header header;
    header.memory = vw::gfx::Image::Memory::SHM;
    header.format = "RX24";
    header.width = width;
    header.height = height;
    _export.reset(new vw::gfx::ipc::Export("local:///tmp/vw.compositor", _uuid));
    _export->create(header);
}
