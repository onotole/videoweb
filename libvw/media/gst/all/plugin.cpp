#include <gst/gst.h>
#include "shmvideosink.h"

static gboolean plugin_init(GstPlugin * plugin) {
	return gst_element_register (plugin, "shmvideosink",
				GST_RANK_NONE, GST_TYPE_SHM_VIDEO_SINK);
}

#define STUB " "
#define VERSION STUB
#define PACKAGE STUB

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR, GST_VERSION_MINOR,
				   vwgstall, "Shared memory video sink",
				   plugin_init, VERSION, GST_LICENSE_UNKNOWN, STUB, STUB);
