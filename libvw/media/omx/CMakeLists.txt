file(GLOB_RECURSE ALL_SOURCES "*.cpp" "*.c")
add_custom_target(show_all_omx_sources SOURCES ${ALL_SOURCES})

find_package(Threads)
add_library(vw.media.omx ${ALL_SOURCES})
target_link_libraries(vw.media.omx ${OMXIL_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} vw.log)

install(TARGETS vw.media.omx
    LIBRARY DESTINATION ${VW_IMAGE_LIB}
    ARCHIVE DESTINATION ${VW_IMAGE_LIB})
