cmake_minimum_required(VERSION 2.8)
project(vw.lib.io)

set(VW_FRAMEWORK_ROOT ../..)
include(${VW_FRAMEWORK_ROOT}/cmake.config)
project(vw.lib.io)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

add_subdirectory(include)
add_subdirectory(src)
add_subdirectory(test)
