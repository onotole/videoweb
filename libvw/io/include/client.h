#ifndef __IO_CLIENT_H__
#define __IO_CLIENT_H__

#include <chrono>
#include <log/log.h>
#include "socket.h"


namespace vw { namespace io {

template <typename IoService, typename Impl>
class Client : public Socket<IoService, Impl> {
public:
    typedef Socket<IoService, Impl> base;
    Client(IoService & io_service, const std::string & uri, size_t timeout_ms = 500,
           typename base::connect_handler_t && connect_handler = nullptr)
        : Socket<IoService, Impl>(io_service) {
        auto tic = std::chrono::steady_clock::now();
        auto ec = this->_impl->connect(uri, timeout_ms, std::move(connect_handler));
        if (ec) {
            auto toc = std::chrono::steady_clock::now();
            ERROR << "connection failed: " << ec.message() << ". after "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(toc - tic).count() << " ms.";
            throw std::runtime_error(ec.message());
        }
    }
};

}} // namespace vw::io

#endif // __IO_CLIENT_H__
