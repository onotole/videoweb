#ifndef __IO_SERVER_H__
#define __IO_SERVER_H__

#include "socket.h"

namespace vw { namespace io {

template <typename IoService, typename Impl>
class Server {
public:
    typedef Socket<IoService, typename Impl::socket_t> socket_t;
    typedef std::function<void(socket_t &&)> connection_handler_t;

    Server(IoService & service, const std::string & uri)
        : _impl(service, uri) {}
    void set_connection_handler(connection_handler_t && handler) {
        connection_handler = handler;
        _impl.set_connection_handler([this](std::shared_ptr<typename Impl::socket_t> socket){
            connection_handler(socket_t(std::move(*socket)));
        });
    }

private:
    Impl _impl;
    connection_handler_t connection_handler;
};

}} // namespace vw::io

#endif // __IO_SERVER_H__
