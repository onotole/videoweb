#ifndef __IO_SOCKET_H__
#define __IO_SOCKET_H__

#include <iostream>
#include <functional>
#include <memory>
#include <system_error>

#ifndef NDEBUG
#define LOG_TRAFFIC
#endif

#ifdef LOG_TRAFFIC
    #include <sstream>
    #include <algorithm>
    #include <log/log.h>
#endif

namespace vw { namespace io {

template<typename IoService, typename Impl>
class Socket {
public:
    typedef std::function<void(std::istream &)> recv_handler_t;
    typedef std::function<void(const std::error_code & ec)> connect_handler_t;
    typedef std::function<void()> disconnect_handler_t;

    Socket(IoService & io_service) : _impl(new Impl(io_service)) {}
    Socket(Impl && socket) : _impl(new Impl(std::move(socket))) {}
    void send(std::istream & stream) {
#ifdef LOG_TRAFFIC
        stream >> std::noskipws;
        std::stringstream copy;
        std::copy(std::istream_iterator<char>(stream), std::istream_iterator<char>(),
                  std::ostream_iterator<char>(copy));
        std::string to_log = copy.str(); to_log.pop_back();
        TRACE << "tx: <" << to_log << ">";
        _impl->send(copy);
#else
        _impl->send(stream);
#endif
    }
    // Override to send file descriptor. Works only for local sockets
    void send(int fd) {
        _impl->send(fd);
    }
    void set_recv_handler(recv_handler_t && handler) {
#ifdef LOG_TRAFFIC
        _impl->set_recv_handler([handler](std::istream & stream){
            stream >> std::noskipws;
            std::stringstream copy;
            std::copy(std::istream_iterator<char>(stream), std::istream_iterator<char>(),
                      std::ostream_iterator<char>(copy));
            std::stringstream to_log(copy.str());
            std::string record;
            std::getline(to_log, record);
            TRACE << "rx: <" << record << ">";
            handler(copy);
        });
#else
        _impl->set_recv_handler(std::move(handler));
#endif
    }
    void set_disconnect_handler(disconnect_handler_t && handler) {
        _impl->set_disconnect_handler(std::move(handler));
    }

    // TODO: remove this method
    Impl * impl() { return _impl.get(); }
protected:
    std::shared_ptr<Impl> _impl;
};

}} // namespace vw::io

#endif // __IO_SOCKET_H__
