#define BOOST_TEST_MODULE io.TrivialLoop
#define BOOST_TEST_DYN_LINK

#include <memory>
#include <boost/test/unit_test.hpp>
#include <io/server.h>
#include <io/client.h>
#include <trivial_loop.h>
#include <socket.h>
#include <client.h>
#include <server.h>

using namespace vw::io::stdio;

typedef TrivialLoop io_service_t;
typedef vw::io::Socket<io_service_t, Socket<io_service_t>> socket_t;
typedef vw::io::Client<io_service_t, Client<io_service_t>> client_t;
typedef vw::io::Server<io_service_t, Server<io_service_t>> server_t;

BOOST_AUTO_TEST_CASE(general_setup) {

	io_service_t service;
	socket_t socket(service);

	client_t client(service, "stdio://");

	server_t server(service, "stdio://");
	std::shared_ptr<server_t::socket_t> psocket;
	server.set_connection_handler([&psocket](server_t::socket_t && s) {
		psocket.reset(new server_t::socket_t(std::move(s)));
	});

	BOOST_CHECK(psocket);
}
