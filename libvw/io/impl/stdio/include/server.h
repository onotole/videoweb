#ifndef __IO_STDIO_SERVER_H__
#define __IO_STDIO_SERVER_H__

#include <functional>
#include <cassert>
#include "socket.h"

namespace vw { namespace io { namespace stdio {

template <typename IoService>
class Server {
public:
    typedef Socket<IoService> socket_t;
    typedef std::shared_ptr<socket_t> socket_ptr_t;
    typedef std::function<void(socket_ptr_t)> connection_handler_t;

    Server(IoService & s, const std::string & uri)
        : service(s) {
        assert(uri == "stdio://");
    }
    void set_connection_handler(connection_handler_t && handler) {
        connection_handler = handler;
        if (connection_handler) {
            connection_handler(socket_ptr_t(new socket_t(service)));
        }
    }

private:
    IoService & service;
    connection_handler_t connection_handler;
};

}}} // namespace vw::io::stdio

#endif // __IO_STDIO_SERVER_H__
