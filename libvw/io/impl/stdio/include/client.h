#ifndef __IO_STDIO_CLIENT_H__
#define __IO_STDIO_CLIENT_H__

#include "socket.h"

namespace vw { namespace io { namespace stdio {

template<typename IoService>
class Client : public Socket<IoService> {
public:
	typedef std::function<void(const std::error_code & ec)> connect_handler_t;
	Client(IoService & service) : Socket<IoService>(service) {}
	std::error_code connect(const std::string & uri, size_t, connect_handler_t &&) {
		assert(uri == "stdio://");
		return std::error_code();
	}
};

}}}

#endif // __IO_STDIO_CLIENT_H__
