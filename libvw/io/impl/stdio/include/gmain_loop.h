#ifndef __IO_STDIO_GMAIN_LOOP_H__
#define __IO_STDIO_GMAIN_LOOP_H__

#include <functional>
#include <glib.h>

namespace vw { namespace io { namespace stdio {

class GMainIoService {
public:
	typedef std::function<void(const void *, size_t)> read_handler_t;

	GMainIoService();
	~GMainIoService();
	void set_read_handler(read_handler_t && handler);
	void write(const void *, size_t);
	void run();
	void stop();

private:
	static gboolean _io_read_callback(GIOChannel *, GIOCondition, gpointer);
	::GMainLoop		* _loop;
	::GMainContext	* _context;
	::GIOChannel	* _io_channel;
	::GSource		* _io_watch;
	read_handler_t	read_handler;
};

}}} // namespace vw::io::stdio

#endif // __IO_STDIO_GMAIN_LOOP_H__
