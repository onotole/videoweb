#ifndef __IO_STDIO_SOCKET_H__
#define __IO_STDIO_SOCKET_H__

#include <iostream>
#include <sstream>
#include <boost/noncopyable.hpp>

namespace vw { namespace io { namespace stdio {

template <typename IoService>
class Socket : boost::noncopyable {
public:
    typedef std::function<void(std::istream &)> recv_handler_t;
    typedef std::function<void()> disconnect_handler_t;

    Socket(IoService & s) : service(s) {
        service.set_read_handler([this](const void * data, size_t size) {
            if (recv_handler) {
                // TODO: custom "no-copy" stream buffer
                // FIXME: will truncate data on '\0'
                std::stringstream stream(std::string(static_cast<const char *>(data), size));
                recv_handler(stream);
            }
        });
    }

    Socket(Socket && s) : Socket(s.service) {}

    void send(std::istream & stream) {
        std::istreambuf_iterator<char> eos;
        std::string buffer(std::istreambuf_iterator<char>(stream), eos);
        service.write(buffer.c_str(), buffer.length());
    }
    void set_recv_handler(recv_handler_t && handler) {
        recv_handler = handler;
    }
    void set_disconnect_handler(disconnect_handler_t && handler) {
        disconnect_handler = handler;
    }
private:
    IoService & service;
    recv_handler_t recv_handler;
    disconnect_handler_t disconnect_handler;
};

}}} // namespace vw::io::stdio

#endif // __IO_STDIO_SOCKET_H__
