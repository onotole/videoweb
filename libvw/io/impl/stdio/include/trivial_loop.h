#ifndef __IO_STDIO_TRIVIAL_LOOP_H__
#define __IO_STDIO_TRIVIAL_LOOP_H__

#include <functional>

namespace vw { namespace io { namespace stdio {

class TrivialLoop {
public:
	typedef std::function<void(const void *, size_t)> read_handler_t;

	TrivialLoop();
	void set_read_handler(read_handler_t &&);
	void write(const void *, size_t);
	void run();
	void stop();

private:
	read_handler_t _server_handler;
	bool _run;
};

}}} // namespace vw::io::stdio

#endif // __IO_STDIO_LOOP_H__
