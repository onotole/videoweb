#include <iostream>
#include "trivial_loop.h"


namespace vw { namespace io { namespace stdio {

TrivialLoop::TrivialLoop() : _run(false) {}

void TrivialLoop::set_read_handler(read_handler_t && handler) {
		_server_handler = handler;
}

void TrivialLoop::write(const void * data, size_t size) {
	std::cout.write(static_cast<const char *>(data), size);
	std::cout << std::endl;
}

void TrivialLoop::run() {
	_run = true;
	while (_run) {
		if (_server_handler) {
			std::string input;
			getline(std::cin, input);
			_server_handler(input.c_str(), input.length());
		}
	}
}

void TrivialLoop::stop() {
	_run = false;
}

}}} // namespace vw::io::stdio
