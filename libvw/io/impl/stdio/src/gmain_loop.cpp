#include <unistd.h>
#include "gmain_loop.h"
#include <iostream>

namespace vw { namespace io { namespace stdio {

GMainIoService::GMainIoService() {
	_context = g_main_context_new();
	_loop = g_main_loop_new(_context, false);
	_io_channel = g_io_channel_unix_new(STDIN_FILENO);
	_io_watch = g_io_create_watch(_io_channel, G_IO_IN);
	_io_watch = g_source_ref(_io_watch);
	g_source_attach(_io_watch, _context);
	g_source_set_callback(_io_watch, (GSourceFunc)_io_read_callback, this, nullptr);
}

GMainIoService::~GMainIoService() {
	g_main_loop_unref(_loop);
	g_main_context_unref(_context);
	g_source_unref(_io_watch);
	g_io_channel_unref(_io_channel);
}

void GMainIoService::set_read_handler(read_handler_t && handler) {
	read_handler = handler;
}

void GMainIoService::write(const void * data, size_t size) {
	std::cout.write(static_cast<const char *>(data), size);
	std::cout << std::endl;
}


void GMainIoService::run() {
	g_main_loop_run(_loop);
}

void GMainIoService::stop() {
	g_main_loop_quit(_loop);
}

gboolean GMainIoService::_io_read_callback(GIOChannel *, GIOCondition, gpointer context) {
	GMainIoService * self = static_cast<GMainIoService *>(context);
	GError * error = nullptr;
	GString * buffer = g_string_new(nullptr);
	g_io_channel_read_line_string(self->_io_channel, buffer, nullptr, &error);
	if (self->read_handler) {
		self->read_handler(buffer->str, buffer->len - 1);
	}
	if (error) g_error_free(error);
	g_string_free(buffer, true);
	return G_SOURCE_CONTINUE;
}

}}} // namespace vw::io::stdio
