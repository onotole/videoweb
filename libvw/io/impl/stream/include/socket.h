#ifndef __IO_STREAM_SOCKET_H__
#define __IO_STREAM_SOCKET_H__

#include <sys/socket.h>
#include <iostream>
#include <deque>
#include <sstream>
#include <memory>
#include <type_traits>
#include <log/log.h>
#include "transport.h"

namespace vw { namespace io { namespace stream {

template <typename transport>
class Socket : public transport::socket, public std::enable_shared_from_this<Socket<transport>> {
public:
    typedef std::function<void(const boost::system::error_code &, size_t)> read_handler_t;
    typedef std::function<void(std::istream &)> recv_handler_t;
    typedef std::function<void()> disconnect_handler_t;
    typedef typename transport::socket base;
    typedef Socket<transport> this_type;

    Socket(boost::asio::io_service & service) : transport::socket(service) {}
    Socket(Socket && other) : transport::socket(std::move(other)) {}
    Socket & operator = (Socket && other) {
        transport::socket::operator = (std::move(other));
        return *this;
    }

    void send(int fd) {
        static_assert(std::is_same<transport, local_transport>::value,
                      "fd transfer is not supported for this socket");

        struct msghdr msg = msghdr();
        char buf[CMSG_SPACE(sizeof(int))] = {};

        struct iovec io = { .iov_base = static_cast<void*>(&fd),
                            .iov_len = sizeof(int) };

        msg.msg_iov = &io;
        msg.msg_iovlen = 1;
        msg.msg_control = buf;
        msg.msg_controllen = sizeof(buf);

        struct cmsghdr * cmsg = CMSG_FIRSTHDR(&msg);
        cmsg->cmsg_level = SOL_SOCKET;
        cmsg->cmsg_type = SCM_RIGHTS;
        cmsg->cmsg_len = CMSG_LEN(sizeof(int));
        *(int*)CMSG_DATA(cmsg) = fd;

        msg.msg_controllen = cmsg->cmsg_len;

        sendmsg(base::native_handle(), &msg, 0);
    }

    void send(std::istream & stream) {
        auto psstream = std::make_shared<std::stringstream>();
        *psstream << stream.rdbuf() << std::flush;
        auto& context = static_cast<boost::asio::io_context&>(base::get_executor().context());
        context.dispatch([this, psstream]() {
            bool trigger_write = write_buffers.empty();
            write_buffers.emplace_back();
            std::ostream request_stream(&write_buffers.back());
            request_stream << psstream->rdbuf() << std::flush;
            if (trigger_write)
                start_write();
        });
    }

    void set_recv_handler(recv_handler_t && handler) {
        recv_handler = handler;
        start_read();
    }

    void set_disconnect_handler(disconnect_handler_t && handler) {
        disconnect_handler = handler;
    }

protected:
    void start_read() {
        std::weak_ptr<this_type> wptr(this_type::shared_from_this());
        auto read_handler = [wptr](const boost::system::error_code &ec, size_t len) {
            auto sptr = wptr.lock();
            if (sptr)
                sptr->handle_read(ec, len);
        };
        boost::asio::async_read_until(*this, read_buffer, '\n', read_handler);
    }

    void handle_read(const boost::system::error_code &ec, size_t len) {
        if (!ec) {
            TRACE << "socket " << base::native_handle() << ": ready to read " << len << " bytes";
            if (recv_handler) {
                std::istream is(&read_buffer);
                std::string str;
                std::getline(is, str);
                std::stringstream stream(str);
                recv_handler(stream);
            }
            start_read();
        } else {
            WARN << "handle_read: socket " << base::native_handle() << ": "
                 << ec.category().name() << ": [" << ec.value() << "] " << ec.message();
            // cancelled
            if (ec == boost::asio::error::operation_aborted) return;
            if (disconnect_handler)
                return disconnect_handler();
        }
    }

    void start_write() {
        std::weak_ptr<this_type> wptr(this_type::shared_from_this());
        auto write_handler = [wptr](const boost::system::error_code &ec, size_t len) {
            auto sptr = wptr.lock();
            if (sptr)
                sptr->handle_write(ec, len);
        };
        boost::asio::async_write(*this, write_buffers.front(), write_handler);
    }

    void handle_write(const boost::system::error_code & ec, size_t len) {
        if (!ec) {
            // TODO: handle partial transfers
            if (len && !write_buffers.empty()) {
                TRACE << "socket " << base::native_handle() << ": delivered " << len << " bytes";
                write_buffers.pop_front();
                if (!write_buffers.empty())
                    start_write();
            }
        } else {
            WARN << "handle_write: socket " << base::native_handle() << ": "
                 << ec.category().name() << ": [" << ec.value() << "] " << ec.message();
            // cancelled
            if (ec == boost::asio::error::operation_aborted) return;
            if (disconnect_handler)
                return disconnect_handler();
        }
    }

    std::deque<boost::asio::streambuf> write_buffers;
    boost::asio::streambuf read_buffer;
    recv_handler_t recv_handler;
    disconnect_handler_t disconnect_handler;
};

typedef Socket<local_transport> LocalSocket;
typedef Socket<tcp_transport> TcpSocket;

}}} // namespace vw::io::stream

#endif // __IO_STREAM_SOCKET_H__
