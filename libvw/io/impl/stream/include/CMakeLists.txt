file(GLOB_RECURSE HEADERS "*.h")

add_custom_target(show_headers SOURCES ${HEADERS})
install(FILES ${HEADERS} DESTINATION ${VW_IMAGE_INC}/vw/io/stream)
