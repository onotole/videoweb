#ifndef __IO_STREAM_TRANSPORT_H__
#define __IO_STREAM_TRANSPORT_H__

#include <boost/asio.hpp>

namespace vw { namespace io { namespace stream {

struct tcp_transport : boost::asio::ip::tcp {
    static endpoint resolve(boost::asio::io_service &, const std::string &);
    static void unbind(const boost::asio::ip::tcp::endpoint &);
};

struct local_transport : boost::asio::local::stream_protocol {
    static endpoint resolve(boost::asio::io_service &, const std::string &);
    static void unbind(const  boost::asio::local::stream_protocol::endpoint &);
};

}}}

#endif //__IO_STREAM_TRANSPORT_H__
