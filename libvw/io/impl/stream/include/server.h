#ifndef __IO_STREAM_SERVER_H__
#define __IO_STREAM_SERVER_H__

#include <functional>
#include <cassert>
#include <memory>
#include "socket.h"

namespace vw { namespace io { namespace stream {

template<typename transport>
class Server {
public:
    typedef Socket<transport> socket_t;
    typedef std::shared_ptr<socket_t> socket_ptr_t;
    typedef std::function<void(socket_ptr_t)> connection_handler_t;
    typedef std::function<void(const boost::system::error_code &)> accept_handler_t;

    Server(boost::asio::io_service & service, const std::string & uri)
        : io_service(service), acceptor(io_service), socket(new socket_t(io_service)) {
        endpoint = transport::resolve(io_service, uri);
        acceptor.open(endpoint.protocol());
        acceptor.set_option(boost::asio::socket_base::reuse_address(true));
        acceptor.bind(endpoint);
        acceptor.listen();

        accept_handler = [this](const boost::system::error_code & ec) {
            if (!ec && connection_handler) {
                DEBUG << "new connection: " << socket->native_handle();
                connection_handler(socket);
                socket.reset(new socket_t(io_service));
                acceptor.async_accept(*socket, accept_handler);
            }
        };
        acceptor.async_accept(*socket, accept_handler);
    }

    ~Server() {
        transport::unbind(acceptor.local_endpoint());
    }

    void set_connection_handler(connection_handler_t && handler) {
        connection_handler = handler;
    }

private:
    boost::asio::io_service & io_service;
    typename transport::endpoint endpoint;
    typename transport::acceptor acceptor;
    std::shared_ptr<socket_t> socket;
    accept_handler_t accept_handler;
    connection_handler_t connection_handler;
};

typedef Server<local_transport> LocalServer;
typedef Server<tcp_transport> TcpServer;

}}} // namespace vw::io::stream

#endif // __IO_STREAM_SERVER_H__
