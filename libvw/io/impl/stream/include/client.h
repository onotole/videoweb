#ifndef __IO_STREAM_CLIENT_H__
#define __IO_STREAM_CLIENT_H__

#include <chrono>
#include <system_error>
#include <memory>
#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread/future.hpp>
#include "socket.h"

namespace vw { namespace io { namespace stream {

template <typename transport>
class Client : public Socket<transport> {
public:
    typedef Socket<transport> socket_t;
    typedef std::function<void(const std::error_code & ec)> async_handler_t;
    typedef typename socket_t::recv_handler_t recv_handler_t;

    Client(boost::asio::io_service & service) : socket_t(service) {}
    ~Client() {}

    std::error_code connect(const std::string & uri, size_t timeout_ms,
                            async_handler_t && handler) {
        watchdog_timer.reset(new boost::asio::deadline_timer(transport::socket::get_executor(),
                                                             boost::posix_time::milliseconds(timeout_ms)));
        watchdog_timer->async_wait(std::bind(&Client::timeout_expired, this, std::placeholders::_1));
        TRACE << "client::connect : starting watchdog on " << timeout_ms << " ms";
        auto& context = static_cast<boost::asio::io_context&>(socket_t::get_executor().context());
        endpoint = transport::resolve(context, uri);
        if (handler) {
            connect_handler = handler;
            try_connect(boost::system::error_code());
            return std::error_code();
        }
        boost::promise<std::error_code> promise;
        boost::future<std::error_code> error = promise.get_future();
        connect_handler = [&promise](const std::error_code & ec) {
            promise.set_value(ec);
        };
        // TODO: move to io_service thread?
        try_connect(boost::system::error_code());
        auto result = error.get();
        connect_handler = nullptr;
        return result;
    }

    void set_recv_handler(recv_handler_t && handler) {
        socket_t::recv_handler = handler;
    }

private:

    void connect_done(const boost::system::error_code & ec) {
        TRACE << "client::connect_done : " << ec.category().name() << " : " << ec.message();
        if (ec) {
            DEBUG << "connect attempt faled : " << ec.category().name() << " : " << ec.message();
            // cancelled
            if (ec == boost::asio::error::operation_aborted) return;
            // try again
            connect_timer.reset(new boost::asio::deadline_timer(transport::socket::get_executor(),
                                                                boost::posix_time::milliseconds(40)));
            connect_timer->async_wait(std::bind(&Client::try_connect, this, std::placeholders::_1));
        } else {
            connect_timer.reset();
            connect_handler(std::error_code());
            socket_t::start_read();
        }
    }

    void timeout_expired(const boost::system::error_code & ec) {
        TRACE << "client::timeout_expired : " << ec.category().name() << " : " << ec.message();
        if (ec == boost::asio::error::operation_aborted) return;
        connect_timer.reset();
        if (connect_handler)
            connect_handler(std::make_error_code(std::errc::timed_out));
    }

    void try_connect(const boost::system::error_code & ec) {
        if (!ec)
            transport::socket::async_connect(endpoint,
                                             std::bind(&Client::connect_done, this, std::placeholders::_1));
    }

    typename transport::endpoint endpoint;
    std::unique_ptr<boost::asio::deadline_timer> connect_timer;
    std::unique_ptr<boost::asio::deadline_timer> watchdog_timer;
    async_handler_t connect_handler;
};

typedef Client<local_transport> LocalClient;
typedef Client<tcp_transport> TcpClient;

}}} // namespace vw::io::stream

#endif // __IO_STREAM_CLIENT_H__
