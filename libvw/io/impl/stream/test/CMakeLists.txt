file(GLOB_RECURSE HEADERS "*.h")
add_custom_target(show_tests_headers SOURCES ${HEADERS})

include_directories(../include)

find_package(Boost COMPONENTS unit_test_framework thread)
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})

find_package(Threads)

# unit tests
enable_testing()
file(GLOB UNIT_TESTS "unit/*.cpp")
foreach(UNIT_TEST ${UNIT_TESTS})
    get_filename_component(UNIT_TEST_NAME ${UNIT_TEST} NAME_WE)
    set(UNIT_TEST_TARGET test_unit_io_stream_${UNIT_TEST_NAME})
    add_executable(${UNIT_TEST_TARGET} ${UNIT_TEST})
    target_link_libraries(${UNIT_TEST_TARGET} ${Boost_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} vw.io.stream)
    add_test(${UNIT_TEST_TARGET} ${UNIT_TEST_TARGET} ${VW_BOOST_TEST_ARGS} --log_sink=../${UNIT_TEST_TARGET}_log.xml)
    install(TARGETS ${UNIT_TEST_TARGET} DESTINATION ${VW_IMAGE_SHARE}/vw/test)
endforeach()

# general tests
file(GLOB GENERAL_TESTS "*.cpp")
foreach(GENERAL_TEST ${GENERAL_TESTS})
    get_filename_component(GENERAL_TEST_NAME ${GENERAL_TEST} NAME_WE)
    set(GENERAL_TEST_TARGET test_io_stream_${GENERAL_TEST_NAME})
    add_executable(${GENERAL_TEST_TARGET} ${GENERAL_TEST})
    target_link_libraries(${GENERAL_TEST_TARGET} ${CMAKE_THREAD_LIBS_INIT} vw.io.stream)
    install(TARGETS ${GENERAL_TEST_TARGET} DESTINATION ${VW_IMAGE_SHARE}/vw/test)
endforeach()
