#define BOOST_TEST_MODULE io.Stream
#define BOOST_TEST_DYN_LINK

#include <memory>
#include <thread>
#include <algorithm>
#include <vector>
#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>
#include <io/server.h>
#include <io/client.h>
#include <socket.h>
#include <client.h>
#include <server.h>
#include <base/util/debug_helpers.h>

using namespace vw::io::stream;

typedef boost::asio::io_service io_service_t;

#define LOCAL_PROTO     "local://"
#define LOCAL_SOCKET    "/tmp/vw.io.stream.test"

#define TCP_PROTO       "tcp://"
#define TCP_HOST        "localhost"
#define TCP_PORT        ":9999"

#define TCP_BLACKHOLE   "tcp://8.8.8.8:9999"

typedef vw::io::Client<io_service_t, LocalClient> client_t;

struct CleanUp {
	CleanUp() {
		::unlink(LOCAL_SOCKET);
	}
};

template <typename T>
std::string get_uri(bool blackhole = false) {
    vw::base::debug::deduced_type<T>::show;
}

template<>
std::string get_uri<local_transport>(bool) {
	return LOCAL_PROTO LOCAL_SOCKET;
}

template<>
std::string get_uri<tcp_transport>(bool blackhole) {
	return blackhole ? TCP_BLACKHOLE : TCP_PROTO TCP_HOST TCP_PORT;
}

BOOST_GLOBAL_FIXTURE(CleanUp);

static const std::string messages[] = {
	"Hello, World!",
	"Goodbye, World!"
};

static const std::vector<std::string> segassem = {
	"!dlroW ,olleH",
	"!dlroW ,eybdooG"
};

typedef boost::mpl::list<tcp_transport, local_transport> test_types;

BOOST_AUTO_TEST_CASE_TEMPLATE(idle_run, T, test_types) {
    std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
    typedef vw::io::Server<io_service_t, vw::io::stream::Server<T>> server_t;

    io_service_t io_service;
    {
        server_t server(io_service, get_uri<T>());
    }
    BOOST_CHECK_NO_THROW(server_t(io_service, get_uri<T>()));

    std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
    std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
              << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
              << " us" << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_failure, T, test_types) {
    std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
    typedef vw::io::Client<io_service_t, vw::io::stream::Client<T>> client_t;
    io_service_t io_service;
    std::thread run([&io_service](){
        boost::asio::io_service::work _(io_service);
        io_service.run();
    });
    BOOST_CHECK_THROW(client_t(io_service, get_uri<T>()), std::runtime_error);
    io_service.stop();
    run.join();

    std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
    std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
              << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
              << " us" << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_blackhole, T, test_types) {
    std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
    typedef vw::io::Client<io_service_t, vw::io::stream::Client<T>> client_t;
    io_service_t io_service;
    std::thread run([&io_service](){
        boost::asio::io_service::work _(io_service);
        io_service.run();
    });
    BOOST_CHECK_THROW(client_t(io_service, get_uri<T>(true)), std::runtime_error);
    io_service.stop();
    run.join();

    std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
    std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
              << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
              << " us" << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(client_connect, T, test_types) {
    std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
    typedef vw::io::Server<io_service_t, vw::io::stream::Server<T>> server_t;
    typedef vw::io::Client<io_service_t, vw::io::stream::Client<T>> client_t;
    typedef typename server_t::socket_t socket_t;

    io_service_t io_service;

    {
        server_t service(io_service, get_uri<T>());

        size_t connections = 0;
        service.set_connection_handler([&](socket_t &&){
            ++connections;
        });

        std::thread run([&io_service](){
            boost::asio::io_service::work _(io_service);
            io_service.run();
        });

        client_t _0(io_service, get_uri<T>());
        client_t _1(io_service, get_uri<T>());
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        io_service.stop();
        run.join();

        BOOST_CHECK_EQUAL(connections, 2);
    }

    BOOST_CHECK_NO_THROW(server_t(io_service, get_uri<T>()));

    std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
    std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
              << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
              << " us" << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(connect_timeout, T, test_types) {
    std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
    typedef vw::io::Server<io_service_t, vw::io::stream::Server<T>> server_t;
    typedef vw::io::Client<io_service_t, vw::io::stream::Client<T>> client_t;
    typedef typename server_t::socket_t socket_t;

    for (size_t i = 0; i < 2; ++i) {
        io_service_t io_service_server;
        io_service_t io_service_client;

        std::thread server_thread([&](){
            boost::asio::io_service::work _(io_service_server);
            io_service_server.run();
        });

        std::thread client_thread([&](){
            boost::asio::io_service::work _(io_service_client);
            io_service_client.run();
        });

        {
            std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
            BOOST_CHECK_THROW(client_t _0(io_service_client, get_uri<T>(), 400U), std::runtime_error);
            std::chrono::duration<double> elapsed = std::chrono::steady_clock::now() - start;
            BOOST_CHECK_GT(std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count(), 380U);
            BOOST_CHECK_LT(std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count(), 420U);
        }

        server_t server(io_service_server, get_uri<T>());
        size_t connections = 0;
        server.set_connection_handler([&](socket_t &&){
            ++connections;
        });

        client_t _1(io_service_client, get_uri<T>(), 1000U);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));

        io_service_server.stop();
        io_service_client.stop();
        server_thread.join();
        client_thread.join();

        BOOST_CHECK_EQUAL(connections, 1);
    }

    std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
    std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
              << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
              << " us" << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(send_recv, T, test_types) {
    std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
    typedef vw::io::Server<io_service_t, vw::io::stream::Server<T>> server_t;
    typedef vw::io::Client<io_service_t, vw::io::stream::Client<T>> client_t;
    typedef typename server_t::socket_t socket_t;

    io_service_t io_service;
    std::thread run([&io_service](){
        boost::asio::io_service::work _(io_service);
        io_service.run();
    });
    server_t server(io_service, get_uri<T>());

    socket_t socket(io_service);
    server.set_connection_handler([&](socket_t && s) {
        socket = std::move(s);
        socket.set_recv_handler([&](std::istream & stream) {
            std::string msg;
            std::getline(stream, msg);
            if (msg == "term") {
                std::stringstream ss("term\n");
                socket.send(ss);
            } else {
                std::reverse(msg.begin(), msg.end());
                std::stringstream out(msg + "\n");
                socket.send(out);
            }
        });
    });

    std::vector<std::string> log;
    client_t client(io_service, get_uri<T>());
    client.set_recv_handler([&](std::istream & stream) {
        std::string msg;
        std::getline(stream, msg);
        if (msg == "term")
            io_service.stop();
        else
            log.push_back(msg);
    });
    for (const auto & m : messages) {
        std::stringstream s(m + "\n");
        client.send(s);
    }
    std::stringstream kill("term\n");
    client.send(kill);

    run.join();

    BOOST_CHECK_EQUAL_COLLECTIONS(log.begin(), log.end(),
                                  segassem.begin(), segassem.end());

    std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
    std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
              << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
              << " us" << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(interleaved, T, test_types) {
	typedef vw::io::Server<io_service_t, vw::io::stream::Server<T>> server_t;
	typedef vw::io::Client<io_service_t, vw::io::stream::Client<T>> client_t;
	typedef typename server_t::socket_t socket_t;

	std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
	io_service_t io_service;


	server_t server(io_service, get_uri<T>());

	std::vector<std::string> sent, received;

	socket_t socket(io_service);
	server.set_connection_handler([&](socket_t && s) {
		socket = std::move(s);
		socket.set_recv_handler([&](std::istream & s) {
			std::string msg;
			std::getline(s, msg);
			if(msg == "term") {
				io_service.stop();
			} else
				received.push_back(msg);
		});
	});

	std::thread client_thread([&]() {
		client_t client(io_service, get_uri<T>());
		client.set_recv_handler([&](std::istream &) {});
		for (size_t c = 0; c < 1000; ++c) {
			std::stringstream s;
			s << "echo request # " << c << "...";
			sent.push_back(s.str());
			s << std::endl;
			client.send(s);
		}
		std::stringstream kill("term\n");
		client.send(kill);
		while(!io_service.stopped()) {
			std::this_thread::yield();
		}
	});

	io_service.run();
	client_thread.join();

	BOOST_CHECK_EQUAL_COLLECTIONS(sent.begin(), sent.end(),
								  received.begin(), received.end());
	std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
	std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
			  << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
			  << " us" << std::endl;
}

BOOST_AUTO_TEST_CASE_TEMPLATE(server_congestion, T, test_types) {
	typedef vw::io::Server<io_service_t, vw::io::stream::Server<T>> server_t;
	typedef vw::io::Client<io_service_t, vw::io::stream::Client<T>> client_t;
	typedef typename server_t::socket_t socket_t;

	std::chrono::steady_clock::time_point tic = std::chrono::steady_clock::now();
	io_service_t io_service;
	server_t server(io_service, get_uri<T>());

	std::vector<std::string> sent, received;

	socket_t socket(io_service);
	server.set_connection_handler([&](socket_t && s) {
		socket = std::move(s);
		socket.set_recv_handler([&](std::istream & s) {
			std::string msg;
			std::getline(s, msg);
			if(msg == "term") {
				io_service.stop();
			} else {
				received.push_back(msg);
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
			}
		});
	});

	std::thread client_thread([&]() {
		client_t client(io_service, get_uri<T>());
		client.set_recv_handler([&](std::istream &) {});
		for (size_t c = 0; c < 100; ++c) {
			std::stringstream s;
			s << "echo request # " << c << "...";
			sent.push_back(s.str());
			s << std::endl;
			client.send(s);
		}
		std::stringstream kill("term\n");
		client.send(kill);
		while(!io_service.stopped()) {
			std::this_thread::yield();
		}
	});

	io_service.run();
	client_thread.join();

	BOOST_CHECK_EQUAL_COLLECTIONS(sent.begin(), sent.end(),
								  received.begin(), received.end());
	std::chrono::steady_clock::time_point toc = std::chrono::steady_clock::now();
	std::clog << boost::unit_test::framework::current_test_case().p_name << " took "
			  << std::chrono::duration_cast<std::chrono::microseconds>(toc - tic).count()
			  << " us" << std::endl;
}

// TODO: test disconnect handler
