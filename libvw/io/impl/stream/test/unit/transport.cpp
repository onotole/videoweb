#define BOOST_TEST_MODULE io.Stream.Transport
#define BOOST_TEST_DYN_LINK

#include <iostream>
#include <fstream>
#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>
#include "transport.h"

using namespace vw::io::stream;

typedef boost::asio::io_service io_service_t;

BOOST_AUTO_TEST_CASE(tcp_resolve) {
    io_service_t io_service;
    // resolve with ip address
    auto endpoint = tcp_transport::resolve(io_service, "tcp://8.8.4.4:53");
    BOOST_CHECK_EQUAL(endpoint.protocol().type(), boost::asio::ip::tcp::v4().type());
    BOOST_CHECK_EQUAL(endpoint.protocol().family(), boost::asio::ip::tcp::v4().family());
    BOOST_CHECK_EQUAL(endpoint.protocol().protocol(), boost::asio::ip::tcp::v4().protocol());
    BOOST_CHECK_EQUAL(endpoint.address(), boost::asio::ip::address_v4::from_string("8.8.4.4"));
    BOOST_CHECK_EQUAL(endpoint.port(), 53);
    // resolve with host name
    endpoint = tcp_transport::resolve(io_service, "tcp://google.com:80");
    BOOST_CHECK_EQUAL(endpoint.protocol().type(), boost::asio::ip::tcp::v4().type());
    BOOST_CHECK_EQUAL(endpoint.protocol().family(), boost::asio::ip::tcp::v4().family());
    BOOST_CHECK_EQUAL(endpoint.protocol().protocol(), boost::asio::ip::tcp::v4().protocol());
    BOOST_CHECK_EQUAL(endpoint.port(), 80);
    BOOST_CHECK(!endpoint.address().is_unspecified());
    // resolve loopback
    endpoint = tcp_transport::resolve(io_service, "tcp://localhost:8080");
    BOOST_CHECK_EQUAL(endpoint.port(), 8080);
    BOOST_CHECK(endpoint.address().is_loopback());
    // resolve unspecified
    endpoint = tcp_transport::resolve(io_service, "tcp://:22");
    BOOST_CHECK_EQUAL(endpoint.port(), 22);
    BOOST_CHECK(endpoint.address().is_unspecified());
    // no port
    BOOST_CHECK_THROW(tcp_transport::resolve(io_service, "tcp://127.0.0.1"), std::runtime_error);
    // no protocol
    BOOST_CHECK_THROW(tcp_transport::resolve(io_service, "yahoo.com:80"), std::runtime_error);
    // wrong protocol
    BOOST_CHECK_THROW(tcp_transport::resolve(io_service, "local://test:12"), std::runtime_error);
    // empty string
    BOOST_CHECK_THROW(tcp_transport::resolve(io_service, ""), std::runtime_error);
    // junk
    BOOST_CHECK_THROW(tcp_transport::resolve(io_service, "random string"), std::runtime_error);
    // incorrect ip address
    BOOST_CHECK_THROW(tcp_transport::resolve(io_service, "tcp://260.0.0.1:12"), boost::system::system_error);
    // junk hostname
    BOOST_CHECK_THROW(tcp_transport::resolve(io_service, "tcp://*.&:78"), boost::system::system_error);
}

BOOST_AUTO_TEST_CASE(tcp_unbind) {
    io_service_t io_service;
    auto endpoint = tcp_transport::resolve(io_service, "tcp://8.8.4.4:53");
    BOOST_CHECK_NO_THROW(tcp_transport::unbind(endpoint));
}

BOOST_AUTO_TEST_CASE(local_resolve) {
    io_service_t io_service;
    // resolve with full path
    auto endpoint = local_transport::resolve(io_service, "local:///tmp/vw.test");
    BOOST_CHECK_EQUAL(endpoint.protocol().type(), boost::asio::local::stream_protocol().type());
    BOOST_CHECK_EQUAL(endpoint.protocol().protocol(), boost::asio::local::stream_protocol().protocol());
    BOOST_CHECK_EQUAL(endpoint.protocol().family(), boost::asio::local::stream_protocol().family());
    BOOST_CHECK_EQUAL(endpoint.path(), "/tmp/vw.test");
    // resolve with relative path
    endpoint = local_transport::resolve(io_service, "local://vw.test");
    BOOST_CHECK_EQUAL(endpoint.protocol().type(), boost::asio::local::stream_protocol().type());
    BOOST_CHECK_EQUAL(endpoint.protocol().protocol(), boost::asio::local::stream_protocol().protocol());
    BOOST_CHECK_EQUAL(endpoint.protocol().family(), boost::asio::local::stream_protocol().family());
    BOOST_CHECK_EQUAL(endpoint.path(), "vw.test");
    // no protocol
    BOOST_CHECK_THROW(local_transport::resolve(io_service, "/tmp/vw.wrong"), std::runtime_error);
    // empty string
    BOOST_CHECK_THROW(local_transport::resolve(io_service, ""), std::runtime_error);
    // junk
    BOOST_CHECK_THROW(local_transport::resolve(io_service, "random junk"), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(local_unbind) {
    io_service_t io_service;
    std::string path("/tmp/vw.test");
    auto endpoint = local_transport::resolve(io_service, "local://" + path);
    {
        std::ofstream out(path);
        std::ifstream in(path);
        BOOST_CHECK(in.good());
    }
    local_transport::unbind(endpoint);
    {
        std::ifstream in(path);
        BOOST_CHECK(!in.good());
    }
}
