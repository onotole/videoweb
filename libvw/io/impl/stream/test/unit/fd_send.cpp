#define BOOST_TEST_MODULE io.Stream.Fd
#define BOOST_TEST_DYN_LINK

#include <iostream>
#include <fstream>
#include <thread>
#include <boost/test/unit_test.hpp>
#include "server.h"
#include "client.h"
#include <io/server.h>
#include <io/client.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace pt = boost::property_tree;

#define PROTO "local://"
#define SOCKET "/tmp/test.io.local.fd"
#define TEST_FILE "/tmp/fd_test.txt"
#define TEST_CONTENT "Hello, child process!\n"

typedef boost::asio::io_service io_service_t;
typedef vw::io::Server<io_service_t, vw::io::stream::LocalServer> server_t;
typedef vw::io::Client<io_service_t, vw::io::stream::LocalClient> client_t;
typedef vw::io::Socket<io_service_t, vw::io::stream::LocalSocket> socket_t;

typedef std::unique_ptr<socket_t> socket_ptr_t;

void server() {
    unlink(SOCKET);

    io_service_t io_service;
    server_t server(io_service, PROTO SOCKET);
    socket_ptr_t socket;
    bool handler_invoked = false;

    server.set_connection_handler([&](socket_t && s) {
        socket.reset(new socket_t(std::move(s)));
        socket->set_recv_handler([&](std::istream & stream) {
            pt::ptree msg;
            pt::read_json(stream, msg);
            auto method = msg.get<std::string>("method");
            int i = 0, fd[2] = {};
            for (auto & p : msg.get_child("params"))
                fd[i++] = p.second.get_value<int>();
            BOOST_CHECK_EQUAL(method, "fd");
            char content[256] = {};
            auto bytes = read(fd[1], content, sizeof(content));
            BOOST_CHECK_GT(bytes, 0);
            BOOST_CHECK_EQUAL(content, TEST_CONTENT);
            handler_invoked = true;
        });
        socket->set_disconnect_handler([&]() {
            socket.reset();
            io_service.stop();
        });
    });
    io_service.run();
    BOOST_CHECK(handler_invoked);
}


void client() {
    boost::unit_test::unit_test_log.set_stream(std::cout);
    unlink(TEST_FILE);
    io_service_t io_service;
    std::thread run([&io_service](){
        boost::asio::io_service::work _(io_service);
        io_service.run();
    });
    client_t client(io_service, PROTO SOCKET);
    client.set_recv_handler([&](std::istream & stream) {
        std::string msg;
        std::getline(stream, msg);
    });

    auto fd = open(TEST_FILE, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    FILE * f = fdopen(fd, "r+");
    fprintf(f, TEST_CONTENT);
    rewind(f);
    client.send(fd);

    boost::asio::deadline_timer timer(io_service, boost::posix_time::seconds(0));
    timer.async_wait([&io_service](const boost::system::error_code&){
        io_service.stop();
    });

    run.join();
    fclose(f);
}

BOOST_AUTO_TEST_CASE(fd_send) {
    pid_t pid =  fork();
    if (pid)
        server();
    else
        client();
}
