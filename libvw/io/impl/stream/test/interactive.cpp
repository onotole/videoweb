/*
 * USAGE:
 *		1. run this test;
 *		2. open one ore more connections (nc -U /tmp/test.io.local.interactive)
 *		3. use nc interactive console to send some strings
 *		4. issue "exit" or close all connections to exit
 */

#include <unistd.h>
#include <memory>
#include <list>
#include "server.h"
#include <io/server.h>

#define PROTO "local://"
#define SOCKET "/tmp/test.io.local.interactive"

typedef boost::asio::io_service io_service_t;
typedef vw::io::Server<io_service_t, vw::io::stream::LocalServer> server_t;
typedef vw::io::Socket<io_service_t, vw::io::stream::LocalSocket> socket_t;
typedef std::unique_ptr<socket_t> socket_ptr_t;
typedef std::list<socket_ptr_t> storage_t;


int main() {
	unlink(SOCKET);

	io_service_t io_service;
	storage_t connections;
	server_t server(io_service, PROTO SOCKET);

	server.set_connection_handler([&](socket_t && s) {
		connections.push_back(socket_ptr_t(new socket_t(std::move(s))));
		auto & socket = connections.back();
		std::cout << "incomming connection fd("
				  << socket->impl()->native_handle() << ")." << std::endl;
		socket->set_recv_handler([&](std::istream & stream) {
			std::stringstream request;
			request << stream.rdbuf();
			if (request.str() == "exit\n")
				io_service.stop();
			else {
				std::stringstream reply;
				reply << "fd(" << socket->impl()->native_handle() << "): ";
				reply << request.rdbuf();
				socket->send(reply);
			}
		});
		socket->set_disconnect_handler([&]() {
			std::cout << "fd(" << socket->impl()->native_handle() << "): disconnected." << std::endl;
			auto it = std::find(connections.begin(), connections.end(), socket);
			if (it != connections.end())
				connections.erase(it);
			if (connections.empty()) {
				std::cout << "no more connections left -> exiting..." << std::endl;
				io_service.stop();
			}
		});
	});

	io_service.run();

	return 0;
}
