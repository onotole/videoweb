#include <boost/regex.hpp>
#include "transport.h"
#include <iostream>

namespace vw { namespace io { namespace stream {

tcp_transport::endpoint tcp_transport::resolve(boost::asio::io_service & io_service,
                                               const std::string & uri) {
    const boost::regex regex("^(tcp://)(.*):(\\d*)$");
    boost::smatch match;
    if (!boost::regex_match(uri, match, regex) || match.size() != 4)
        throw std::runtime_error("wrong uri format");

    std::string host(match[2].first, match[2].second), port(match[3].first, match[3].second);
    boost::asio::ip::resolver_query_base::flags flags =  boost::asio::ip::resolver_query_base::address_configured;
    if (host.empty())
        flags |= boost::asio::ip::resolver_query_base::passive;
    boost::asio::ip::tcp::resolver resolver(io_service);
    boost::asio::ip::tcp::resolver::query query(host, port, flags);
    return resolver.resolve(query)->endpoint();
}

void tcp_transport::unbind(const  boost::asio::ip::tcp::endpoint &) {}

local_transport::endpoint local_transport::resolve(boost::asio::io_service &,
                                                   const std::string & uri) {
    const boost::regex regex("^(local://)(.*)$");
    boost::smatch match;
    if (!boost::regex_match(uri, match, regex) || match.size() != 3)
        throw std::runtime_error("wrong uri format");

    return endpoint(std::string(match[2].first, match[2].second));
}

void local_transport::unbind(const  boost::asio::local::stream_protocol::endpoint & ep) {
	::unlink(ep.path().c_str());
}

}}}
