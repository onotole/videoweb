#define BOOST_TEST_MODULE io.ClientServer
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <memory>
#include <socket.h>
#include <client.h>
#include <server.h>
#include "fixtures.h"

using namespace vw::io::test;

typedef vw::io::Server<MockIoService, MockServer> server_t;
typedef vw::io::Client<MockIoService, MockClient> client_t;

BOOST_AUTO_TEST_CASE(generic_setup) {

	MockIoService service;
	server_t server(service, "test://");
	std::vector<std::shared_ptr<server_t::socket_t>> server_sockets;
    server.set_connection_handler([&](server_t::socket_t && s){
		server_sockets.push_back(std::shared_ptr<server_t::socket_t>
								 (new server_t::socket_t(std::move(s))));
		service.commit_connect(server_sockets.back().get()->impl());
	});
	client_t client(service, "test://");
	BOOST_CHECK_EQUAL(service.connections.size(), 1);
}

BOOST_AUTO_TEST_CASE(multi_connect) {
	MockIoService service;
	server_t server(service, "test://");
	std::vector<std::shared_ptr<server_t::socket_t>> server_sockets;
	server.set_connection_handler([&](server_t::socket_t && s){
		server_sockets.push_back(std::shared_ptr<server_t::socket_t>
								 (new server_t::socket_t(std::move(s))));
		service.commit_connect(server_sockets.back().get()->impl());
	});
	std::thread worker([&service](){
		service.run();
	});

	bool connect_handler_called = false;

	client_t client0(service, "test://");
	client_t client1(service, "test://");
	client_t client3(service, "test://", -1U, [&](const std::error_code & ec){
		BOOST_CHECK(!ec);
		connect_handler_called = true;
	});

	BOOST_CHECK(connect_handler_called);
	BOOST_CHECK_EQUAL(service.connections.size(), 3);
	BOOST_CHECK_EQUAL(server_sockets.size(), 3);

	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	service.stop();
	worker.join();
}

BOOST_AUTO_TEST_CASE(send_receive) {
	MockIoService service;
	server_t server(service, "test://");
	struct socket_info {
		std::shared_ptr<server_t::socket_t> socket;
		std::vector<std::string> log;
	};
	std::vector<socket_info> server_sockets;
	server.set_connection_handler([&](server_t::socket_t && s){
		auto index = server_sockets.size();
		std::shared_ptr<server_t::socket_t> ptr(new server_t::socket_t(std::move(s)));
		server_sockets.push_back({ptr, {}});
		ptr->set_recv_handler([&, index](std::istream & is){
			std::stringstream ss; ss << is.rdbuf();
			server_sockets[index].log.push_back(ss.str());
			std::string rs(ss.str());
			std::reverse(rs.begin(), rs.end());
			ss.str(rs);
			server_sockets[index].socket->send(ss);
		});
		service.commit_connect(server_sockets.back().socket.get()->impl());

	});
	std::thread worker([&service](){
		service.run();
	});

	std::vector<std::string> test_messages0 =
	{ "Hello, World!", "Goodbye, World!", "I want to sleep..." };
	std::vector<std::string> test_messages1 =
	{ "Hello, Babe!", "You Stink!", "beep...beep...", "Ola!" };

	client_t client0(service, "test://");
	client_t client1(service, "test://");

	BOOST_CHECK_EQUAL(service.connections.size(), 2);
	BOOST_CHECK_EQUAL(server_sockets.size(), 2);

	client0.set_recv_handler([&](std::istream & is) {
		static size_t counter = 0;
		std::stringstream ss; ss << is.rdbuf();
		std::string rs(ss.str()); std::reverse(rs.begin(), rs.end());
		BOOST_CHECK_EQUAL(rs, test_messages0[counter++]);
	});
	client1.set_recv_handler([&](std::istream & is) {
		static size_t counter = 0;
		std::stringstream ss; ss << is.rdbuf();
		std::string rs(ss.str()); std::reverse(rs.begin(), rs.end());
		BOOST_CHECK_EQUAL(rs, test_messages1[counter++]);
	});

	for (const auto & msg : test_messages0) {
		std::stringstream ss(msg);
		client0.send(ss);
	}
	for (const auto & msg : test_messages1) {
		std::stringstream ss(msg);
		client1.send(ss);
	}

	std::this_thread::sleep_for(std::chrono::milliseconds(100));
	service.stop();
	worker.join();

	BOOST_CHECK_EQUAL_COLLECTIONS(server_sockets[0].log.begin(), server_sockets[0].log.end(),
			test_messages0.begin(), test_messages0.end());
	BOOST_CHECK_EQUAL_COLLECTIONS(server_sockets[1].log.begin(), server_sockets[1].log.end(),
			test_messages1.begin(), test_messages1.end());
}

// TODO: test disconnect handling
// FIXME: hangs someimes...
