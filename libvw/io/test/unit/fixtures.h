#ifndef __IO_TEST_FIXTURES_H__
#define __IO_TEST_FIXTURES_H__

#include <iostream>
#include <vector>
#include <list>
#include <sstream>
#include <mutex>
#include <memory>
#include <functional>
#include <thread>
#include <boost/noncopyable.hpp>
#include <socket.h>

namespace vw { namespace io { namespace test {

struct MockSocket;
struct MockClient;
struct MockServer;

typedef std::function<void(std::istream &)> recv_handler_t;
typedef std::function<void(const std::error_code & ec)> connect_handler_t;
typedef std::function<void()> disconnect_handler_t;
typedef std::function<void(std::shared_ptr<MockSocket>)> connection_handler_t;

struct MockPipe {
	MockPipe(MockSocket * l, MockSocket * r)
		: left(l), right(r), lock(new std::recursive_mutex) {}

	std::string left_to_right;
	std::string right_to_left;
	MockSocket * left;
	MockSocket * right;
	std::recursive_mutex * lock;
};

struct MockIoService : boost::noncopyable {
	MockIoService();
	void run();
	void stop();

	void prepare_connect(MockSocket * socket);
	void commit_connect(MockSocket * socket);

	std::mutex lock;
	std::list<MockPipe> connections;
	bool _run;
	MockServer * server;
};

struct MockSocket {
	MockSocket(MockIoService & s);
	void send(std::istream & is);
	void recv(std::istream & is);
	void set_recv_handler(recv_handler_t && h);
	void set_disconnect_handler(disconnect_handler_t && h);

	MockIoService & service;
	recv_handler_t recv_handler;
	disconnect_handler_t disconnect_handler;
};

struct MockServer {
	typedef MockSocket socket_t;

	MockServer(MockIoService & s, const std::string & uri);
	void set_connection_handler(connection_handler_t && h);

	void connect(MockClient * client);

	MockIoService & service;
	connection_handler_t connection_handler;
};


inline MockSocket::MockSocket(MockIoService & s) : service(s) {}

inline void MockSocket::send(std::istream & is) {
	for (auto & pipe : service.connections) {
		if (this == pipe.left) {
			while(!pipe.left_to_right.empty()) { std::this_thread::yield(); }
			std::lock_guard<std::recursive_mutex> _(*pipe.lock);
			std::getline(is, pipe.left_to_right);
			break;
		} else if (this == pipe.right) {
			std::lock_guard<std::recursive_mutex> _(*pipe.lock);
			std::getline(is, pipe.right_to_left);
			break;
		}
	}
}

inline void MockSocket::recv(std::istream &is) {
	recv_handler(is);
}

void MockSocket::set_recv_handler(recv_handler_t && h) {
	recv_handler = h;
}

void MockSocket::set_disconnect_handler(disconnect_handler_t && h) {
	disconnect_handler = h;
}

struct MockClient : MockSocket {
	MockClient(MockIoService & s);
	std::error_code connect(const std::string & uri, size_t timeout,
							connect_handler_t && connect_handler);
};

inline MockClient::MockClient(MockIoService & s)
	: MockSocket(s) {}

inline std::error_code MockClient::connect(const std::string &, size_t,
										   connect_handler_t && connect_handler) {
	service.prepare_connect(this);
	service.server->connect(this);
	if (connect_handler) connect_handler(std::error_code());
	return std::error_code();
}

inline MockServer::MockServer(MockIoService & s, const std::string & uri)
	: service(s) {
	service.server = this;
}

inline void MockServer::set_connection_handler(connection_handler_t && h) {
	connection_handler = h;
}

inline void MockServer::connect(MockClient * client) {
    connection_handler(std::shared_ptr<MockSocket>(new MockSocket(service)));
}

inline MockIoService::MockIoService() : _run(false) {}

inline void MockIoService::run() {
	if (!_run) {
		_run = true;
		while(_run) {
			std::lock_guard<std::mutex> _(lock);
			for (auto & pipe : connections) {
				std::lock_guard<std::recursive_mutex> __(*pipe.lock);
				if (!pipe.left_to_right.empty()) {
					std::stringstream ss(pipe.left_to_right);
					pipe.right->recv(ss);
					pipe.left_to_right.clear();
				}
				if (!pipe.right_to_left.empty()) {
					std::stringstream ss(pipe.right_to_left);
					pipe.left->recv(ss);
					pipe.right_to_left.clear();
				}
			}
		}
	}
}

inline void MockIoService::stop() {
	_run = false;
}

inline void MockIoService::prepare_connect(MockSocket * socket) {
	std::lock_guard<std::mutex> _(lock);
	connections.push_back({socket, nullptr});
}

inline void MockIoService::commit_connect(MockSocket * socket) {
	std::lock_guard<std::mutex> _(lock);
	connections.back().right = socket;
}


}}} // namespace vw::io::test

#endif // __IO_TEST_FIXTURES_H__
