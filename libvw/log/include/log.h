#ifndef __LOG_LOG_H__
#define __LOG_LOG_H__

#include <iostream>

#define BOOST_LOG_DYN_LINK

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#endif

#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/record_ostream.hpp>

#ifdef __clang__
#pragma clang diagnistic pop
#endif

namespace vw { namespace log {

enum class severity_level {
	trace = 0,
	debug,
	info,
	warning,
	error,
	fatal
};

typedef boost::log::sources::severity_logger_mt<severity_level> logger_t;

BOOST_LOG_GLOBAL_LOGGER(g_log, logger_t)

void init_logging(std::ostream & err = std::cerr, std::ostream & log = std::clog);

}} // namespace vw::log

#define VW_LOG(severity) BOOST_LOG_SEV(vw::log::g_log::get(), (severity))

#ifndef TRACE
	#define TRACE	VW_LOG(::vw::log::severity_level::trace)
#endif
#ifndef DEBUG
	#define DEBUG	VW_LOG(::vw::log::severity_level::debug)
#endif
#ifndef INFO
	#define INFO	VW_LOG(::vw::log::severity_level::info)
#endif
#ifndef WARN
	#define WARN	VW_LOG(::vw::log::severity_level::warning)
#endif
#ifndef ERROR
	#define ERROR	VW_LOG(::vw::log::severity_level::error)
#endif
#ifndef FATAL
	#define FATAL	VW_LOG(::vw::log::severity_level::fatal)
#endif

#endif
