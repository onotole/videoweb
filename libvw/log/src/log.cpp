#include "log.h"

#include <boost/log/core.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/algorithm/string.hpp>

#include <unistd.h>
#include <sys/syscall.h>

namespace vw { namespace log {

namespace detail {
#include <boost/version.hpp>
#if(((BOOST_VERSION / 100) % 1000) > 56)
#include <boost/core/null_deleter.hpp>
typedef boost::null_deleter null_deleter;
#else
#include <boost/log/utility/empty_deleter.hpp>
typedef boost::log::empty_deleter null_deleter;
#endif
}

typedef boost::log::sinks::synchronous_sink
			< boost::log::sinks::syslog_backend > syslog_sink_t;
typedef boost::log::sinks::synchronous_sink
			< boost::log::sinks::text_ostream_backend > stream_sink_t;

namespace {
	boost::shared_ptr<syslog_sink_t> syslog_sink;
	boost::shared_ptr<stream_sink_t> err_stream_sink;
	boost::shared_ptr<stream_sink_t> log_stream_sink;
}

BOOST_LOG_GLOBAL_LOGGER_INIT(g_log, logger_t) {
	init_logging();
	return logger_t();
}

int gettid() {
    static __thread int thread_id = -1;
#if defined(__MACH__)
    static const int syscall_get_thread_id = SYS_thread_selfid;
#elif defined(__linux__)
    static const int syscall_get_thread_id = SYS_gettid;
#endif
    if (thread_id < 0)
        thread_id = syscall(syscall_get_thread_id);
    return thread_id;
}

std::ostream & operator<< (std::ostream& strm, severity_level level) {
	static const char* severity_str[] = {"TRACE", "DEBUG", "INFO ", "WARN ", "ERROR", "FATAL"};
	return strm << severity_str[size_t(level) % (sizeof(severity_str) / sizeof(*severity_str))];
}

void reset_attributes() {
	auto core = boost::log::core::get();
	core->set_global_attributes({});
}

void init_attributes() {
	reset_attributes();
	auto core = boost::log::core::get();

	core->add_global_attribute("TimeStamp", boost::log::attributes::local_clock());
	core->add_global_attribute("Process", boost::log::attributes::current_process_name());
	core->add_global_attribute("ProcessID", boost::log::attributes::constant<pid_t>(getpid()));
	core->add_global_attribute("ThreadID", boost::log::attributes::make_function(&gettid));
}

void reset_sinks() {
	auto core = boost::log::core::get();
	core->remove_all_sinks();

	if (syslog_sink) {
		syslog_sink->flush();
		syslog_sink.reset();
	}

	if (err_stream_sink) {
		err_stream_sink->flush();
		err_stream_sink.reset();
	}

	if (log_stream_sink) {
		log_stream_sink->flush();
		log_stream_sink.reset();
	}
}

BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", severity_level)
BOOST_LOG_ATTRIBUTE_KEYWORD(process, "Process", std::string)
BOOST_LOG_ATTRIBUTE_KEYWORD(process_id, "ProcessID", pid_t)
BOOST_LOG_ATTRIBUTE_KEYWORD(thread_id, "ThreadID", int)
BOOST_LOG_ATTRIBUTE_KEYWORD(scope, "Scope", boost::log::attributes::named_scope_list)

void init_sinks(std::ostream & err, std::ostream & log) {
	reset_sinks();
	auto core = boost::log::core::get();

	boost::log::formatter formatter = boost::log::expressions::stream
			<< boost::log::expressions::format_date_time<boost::posix_time::ptime>	/* TIMESTAMP */
					("TimeStamp", "%Y-%m-%d %T.%f") << " "
			<< process << "[" << process_id << ":" << thread_id << "] "				/* PROCESS */
			<< severity << " "														/* SEVERITY */
			<< boost::log::expressions::message;									/* MESSAGE */

	auto syslog_backend = boost::make_shared< boost::log::sinks::syslog_backend >();
	syslog_sink = boost::make_shared< syslog_sink_t >(syslog_backend);

	auto err_stream_backend = boost::make_shared< boost::log::sinks::text_ostream_backend >();
	err_stream_backend->add_stream( boost::shared_ptr< std::ostream >(&err, detail::null_deleter()) );
	err_stream_sink = boost::make_shared<stream_sink_t>(err_stream_backend);
	err_stream_sink->set_filter
			(boost::log::expressions::attr<severity_level>("Severity") >= severity_level::info);
	err_stream_sink->set_formatter(formatter);

	auto log_stream_backend = boost::make_shared< boost::log::sinks::text_ostream_backend >();
	log_stream_backend->add_stream( boost::shared_ptr< std::ostream >(&log, detail::null_deleter()) );
	log_stream_sink = boost::make_shared<stream_sink_t>(log_stream_backend);
	log_stream_sink->set_filter
			(boost::log::expressions::attr<severity_level>("Severity") < severity_level::info);
	log_stream_sink->set_formatter(formatter);

	core->add_sink(syslog_sink);
	core->add_sink(err_stream_sink);
	core->add_sink(log_stream_sink);
}

void set_log_level() {
	severity_level log_level = severity_level::info;
	auto value = getenv("VW_LOG_LEVEL");
	if (value) {
		std::string level = value;
		if (boost::iequals(level, "fatal"))
			log_level = severity_level::fatal;
		else if (boost::iequals(level, "error"))
			log_level = severity_level::error;
		else if (boost::iequals(level, "warning"))
			log_level = severity_level::warning;
		else if (boost::iequals(level, "info"))
			log_level = severity_level::info;
		else if (boost::iequals(level, "debug"))
			log_level = severity_level::debug;
		else if (boost::iequals(level, "trace"))
			log_level = severity_level::trace;
	}
	boost::log::core::get()->set_filter
			(boost::log::expressions::attr<severity_level>("Severity") >= log_level);
}

void init_logging(std::ostream & err, std::ostream & log) {
	init_attributes();
	init_sinks(err, log);
	set_log_level();
}

}} // namespace vw::log
