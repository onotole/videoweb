#define BOOST_TEST_MODULE log.Logging
#define BOOST_TEST_DYN_LINK

#include <sstream>
#include <thread>
#include <vector>
#include <boost/test/unit_test.hpp>

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-register"
#endif

#include <boost/regex.hpp>

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include <boost/filesystem.hpp>

#include "log.h"

namespace vw { namespace log { namespace test {

namespace tf = boost::unit_test::framework;
namespace fs = boost::filesystem;

const vw::log::severity_level LEVELS[] = {
	vw::log::severity_level::debug,
	vw::log::severity_level::info,
	vw::log::severity_level::warning,
	vw::log::severity_level::error,
	vw::log::severity_level::fatal
};

const std::string  PATTERN_DATE = "^[0-9]{4}-[0-9]{2}-[0-9]{2}\\s+";
const std::string  PATTERN_TIME = "[0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{6}\\s+";
const std::string  PATTERN_SVRT = "(TRACE|DEBUG|INFO |WARN |ERROR|FATAL)\\s+";
const std::string  PATTERN_MESG = ".*";
static std::string PATTERN_PROC;

struct LogConfig {
	LogConfig() {
		setenv("VW_LOG_LEVEL", "debug", 1);
		INFO << "init logging...";
		std::string proc_name =
				fs::path(tf::master_test_suite().argv[0]).filename().c_str();
		PATTERN_PROC = proc_name + "\\[[0-9]+:[0-9]+\\]\\s+";
	}
};

BOOST_GLOBAL_FIXTURE(LogConfig);

struct LogFixture {
	LogFixture() {
		init_logging(err, log);

		std::string pattern_str = PATTERN_DATE;
		pattern_str += PATTERN_TIME;
		pattern_str += PATTERN_PROC;
		pattern_str += PATTERN_SVRT;
		pattern_str += PATTERN_MESG;
		log_line_pattern = pattern_str;
	}

	std::stringstream err;
	std::stringstream log;
	boost::regex log_line_pattern;

	std::vector<std::string> collect(std::istream & is) {
		std::vector<std::string> result;
		std::string line;
		while(std::getline(is, line)) {
			result.push_back(line);
		}
		return result;
	}
	std::vector<std::string> collect_err() {
		return collect(err);
	}
	std::vector<std::string> collect_log() {
		return collect(log);
	}
	std::vector<std::string> collect_all() {
		std::vector<std::string> result = collect_err();
		std::vector<std::string> log = collect_log();
		result.insert(result.end(), log.begin(), log.end());
		return result;
	}
};

}}} // namespace vw::log::test

void log_random_severity_message() {
	using namespace vw::log::test;
	vw::log::severity_level severity = LEVELS[rand() % (sizeof(LEVELS) / sizeof(*LEVELS))];
	VW_LOG(severity) << "random message from thread: (" << std::this_thread::get_id() << ")";
}

BOOST_FIXTURE_TEST_CASE(null_log, vw::log::test::LogFixture) {
	INFO << "This should be logged";
	DEBUG << "This one should";
	FATAL << "This should be logged";
	TRACE << "This should be skipped";
	ERROR << "Should be logged";
	auto log = collect_log();
	auto err = collect_err();
	BOOST_CHECK_EQUAL(log.size(), 1);
	BOOST_CHECK_EQUAL(err.size(), 3);
}

BOOST_FIXTURE_TEST_CASE(threaded_log, vw::log::test::LogFixture) {

	static const size_t n_threads = 40;
	static const size_t n_messages = 100;

	std::vector<std::thread> worker_pool;
	for (size_t t = 0; t < n_threads; ++t) {
		worker_pool.push_back(std::thread([](){
			for (size_t m = 0; m < n_messages; ++m)
				log_random_severity_message();
		}));
	}
	for (size_t t = 0; t < n_threads; ++t)
		worker_pool[t].join();

	auto logs = collect_all();
	BOOST_CHECK_EQUAL(logs.size(), n_threads * n_messages);
	for (const auto & line : logs) {
		BOOST_CHECK(boost::regex_match(line, log_line_pattern));
	}
}

