#define BOOST_TEST_MODULE rpc.Dispatcher
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <memory>
#include <functional>
#include <vector>
#include <dispatcher.h>
#include <protocol.h>
#include "fixtures.h"

using namespace vw::rpc;

namespace {

struct MockConnector {
	typedef std::function<void(std::istream &)> handler_t;
	void set_recv_handler(handler_t && handler) {
		_handler = handler;
	}
	void send(std::istream & msg) {
		std::string reply; msg >> reply; msg >> reply;
		replies.push_back(reply);
	}
	void process_message(std::istream & msg) {
		_handler(msg);
	}
	void process_message(const char * msg) {
		std::stringstream stream(msg);
		process_message(stream);
	}
	handler_t _handler;
	std::vector<std::string> replies;
};

size_t add_count = 0;
double add (double a, double b) {
	++add_count;
	return a + b;
}

struct Accumulator {
	Accumulator() : sum(0) {}
	int operator () (int a) {
		return sum += a;
	}
	int sum;
};

class MockPipeline {
public:
	MockPipeline() : _playing(false) {}
	bool load(const std::string & uri) {
		_uri = uri;
		return true;
	}
	bool unload() {
		_uri.clear();
		return true;
	}
	bool play() {
		return _playing = true;
	}
	bool pause() {
		return !(_playing = false);
	}

	bool _playing;
	std::string _uri;
};

typedef Dispatcher<test::Proto, MockConnector> dispatcher_t;

} // anon namespace

BOOST_AUTO_TEST_CASE(func_dispatch) {

	MockConnector connector;
	dispatcher_t dispatcher(connector);
	std::string sentence;
	Accumulator accum;

	dispatcher.register_handler("add", add);
	dispatcher.register_handler("accum", &accum);
	dispatcher.register_handler("concat", [&](const std::string & word){
		if (sentence.empty()) { sentence += word; }
		else { sentence.pop_back(); sentence += " " + word; }
		sentence += ".";
		return true;
	});

	connector.process_message("1 add 1.2 3.7");
	connector.process_message("2 accum 23");
	BOOST_CHECK_EQUAL(accum.sum, 23);
	connector.process_message("3 accum 14");
	BOOST_CHECK_EQUAL(accum.sum, 37);
	connector.process_message("4 concat Hello,");
	connector.process_message("5 concat World");
	BOOST_CHECK_EQUAL(sentence, "Hello, World.");
	dispatcher.notify("dimensions", 720, 576);
	std::vector<std::string> expected_replies =
	{ "4.9", "23", "37", "true", "true", "dimensions" };
	BOOST_CHECK_EQUAL_COLLECTIONS(connector.replies.begin(), connector.replies.end(),
								  expected_replies.begin(), expected_replies.end());
}

BOOST_AUTO_TEST_CASE(class_dispatch) {
	MockConnector connector;
	dispatcher_t dispatcher(connector);
	MockPipeline pipeline;

	dispatcher.register_handler("load",		&pipeline, &MockPipeline::load);
	dispatcher.register_handler("unload",	&pipeline, &MockPipeline::unload);
	dispatcher.register_handler("play",		&pipeline, &MockPipeline::play);
	dispatcher.register_handler("pause",	&pipeline, &MockPipeline::pause);

	connector.process_message("1 load http://lge.com/intro.mov");
	BOOST_CHECK_EQUAL(pipeline._uri, "http://lge.com/intro.mov");
	BOOST_CHECK_EQUAL(pipeline._playing, false);
	connector.process_message("2 play");
	BOOST_CHECK_EQUAL(pipeline._playing, true);
	connector.process_message("3 pause");
	BOOST_CHECK_EQUAL(pipeline._playing, false);
	connector.process_message("4 unload");
	BOOST_CHECK_EQUAL(pipeline._uri, "");
}

BOOST_AUTO_TEST_CASE(async_callback) {
	MockConnector connector;
	dispatcher_t dispatcher(connector);

	std::string load_uri;
	bool load_loop = false;
	size_t load_id;

	dispatcher.register_async_handler("load", [&](size_t call_id, const std::string & uri, bool loop) {
		load_uri = uri;
		load_id = call_id;
		load_loop = loop;
	});
	connector.process_message("7 load http://apple.com/iTunes.mov true");
	BOOST_CHECK_EQUAL(load_uri, "http://apple.com/iTunes.mov");
	BOOST_CHECK_EQUAL(load_id, 7);
	BOOST_CHECK_EQUAL(load_loop, false);
	dispatcher.reply(load_id, true);
}

