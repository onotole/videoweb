#define BOOST_TEST_MODULE rpc.CallManager
#define BOOST_TEST_DYN_LINK

#include <queue>
#include <thread>
#include <memory>
#include <sstream>
#include <algorithm>
#include <boost/test/unit_test.hpp>
#include <call_manager.h>
#include "fixtures.h"

using namespace vw::rpc;

struct MockClient {
	typedef Proto<test::Proto> proto_t;
	MockClient() {
		task_dispatcher = std::thread([this](){
			while(true) {
				std::lock_guard<std::mutex> _(queue_lock);
				if (!task_queue.empty()) {
					if (task_queue.front()) {
						task_queue.front()();
						task_queue.pop();
					} else {
						// treat empty task as exit signal
						break;
					}
				}
			}
		});
	}
	~MockClient() {
		post(task_t());
		task_dispatcher.join();
	}

	void set_recv_handler(std::function<void(std::istream &)> && handler) {
		recv_handler = handler;
	}
	void send(std::istream & request) {
		proto_t::request::parser_t parser(request);
		std::stringstream sstr;
		proto_t::response::serializer_t serializer(sstr, parser.id());
		if (parser.call() == "sum") {
			int first; float second;
			parser.get_arg(first);
			parser.get_arg(second);
			serializer.set_result(double(first) + double(second));
		} else if (parser.call() == "char_count") {
			std::string first, second;
			parser.get_arg(first);
			parser.get_arg(second);
			serializer.set_result(first.length() + second.length());
		} else if (parser.call() == "reverse") {
			std::string str;
			parser.get_arg(str);
			std::reverse(str.begin(), str.end());
			serializer.set_result(str);
		}
		std::string reply = sstr.str();
		post([reply,this](){ std::stringstream r(reply); recv_handler(r); });
	}
	void notify_dimensions(size_t w, size_t h) {
		std::shared_ptr<std::stringstream> event(new std::stringstream);
		proto_t::event::serializer_t serializer(*event, "dimensions");
		serializer.put_arg(w);
		serializer.put_arg(h);
		serializer.commit();
		post([event,this](){ auto e = event; recv_handler(*e); });
	}
	void notify_state(const std::string & state) {
		std::shared_ptr<std::stringstream> event(new std::stringstream);
		proto_t::event::serializer_t serializer(*event, "state");
		serializer.put_arg(state);
		serializer.commit();
		post([event,this](){ auto e = event; recv_handler(*e); });
	}

private:
	std::function<void(std::istream &)> recv_handler;

	typedef std::function<void()> task_t;
	void post(task_t && task) {
		std::lock_guard<std::mutex> _(queue_lock);
		task_queue.push(task);
	}

	std::queue<task_t> task_queue;
	std::thread task_dispatcher;
	std::mutex queue_lock;
};

typedef Proto<test::Proto> proto_t;
typedef CallManager<proto_t, MockClient> call_manager_t;

BOOST_AUTO_TEST_CASE(call_manager_async) {
	size_t num_calls = 0;

	struct Functor {
		Functor(size_t e, size_t & c) : expected(e), counter(c) {}
		void operator()(const size_t & count) const {
			BOOST_CHECK_EQUAL(count, expected);
			++counter;
		}
		size_t expected;
		size_t & counter;
	} functor(6, num_calls);

	class Klass {
	public:
		Klass(const std::string & e, size_t & c) : expected(e), counter(c) {}
		void check(const std::string & str) {
			BOOST_CHECK_EQUAL(str, expected);
			++counter;
		}
	private:
		std::string expected;
		size_t & counter;
	} klass("olleh", num_calls);

	std::unique_ptr<call_manager_t> call_manager;

	{
		MockClient client;
		call_manager.reset(new call_manager_t(client));

		call_manager->call_async("sum", [&num_calls](const double & sum){
			BOOST_CHECK_EQUAL(sum, 9.34);
			++num_calls;
		}, 7, 2.34f);
		call_manager->call_async("reverse", &klass, &Klass::check, "hello");
		call_manager->call_async("char_count", functor, "one", "two");
	}

	BOOST_CHECK_EQUAL(num_calls, 3);
}

BOOST_AUTO_TEST_CASE(call_manager_sync) {
	MockClient client;
	call_manager_t call_manager(client);

	const size_t max_count = 1000;

	for (size_t count = 0; count <= max_count; ++count) {
		double sum = call_manager.call<double>("sum", 12, 3.4f);
		BOOST_CHECK_EQUAL(sum, 15.4);
		size_t char_count = call_manager.call<size_t>("char_count", "Hello", "World");
		BOOST_CHECK_EQUAL(char_count, 10);
		std::string rev = call_manager.call<std::string>("reverse", "string");
		BOOST_CHECK_EQUAL(rev, "gnirts");
	}
}

BOOST_AUTO_TEST_CASE(call_manager_event) {
	struct Klass {
		void on_dims(const size_t & w, const size_t & h) {
			width = w;
			height = h;
		}
		size_t width;
		size_t height;
	} klass;

	std::unique_ptr<call_manager_t> call_manager;
	std::string state;

	{
		MockClient client;
		call_manager.reset(new call_manager_t(client));

		call_manager->register_event_handler("dimensions", &klass, &Klass::on_dims);
		auto lambda = [&state](const std::string & s){ state = s; };
		call_manager->register_event_handler("state", lambda);
		client.notify_dimensions(720, 540);
		client.notify_state("paused");
	}
	BOOST_CHECK_EQUAL(klass.width, 720);
	BOOST_CHECK_EQUAL(klass.height, 540);
	BOOST_CHECK_EQUAL(state, "paused");
}

