#define BOOST_TEST_MODULE rpc.FunctionTraits
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <adapter/function_traits.h>
#include <type_traits>

using namespace vw::rpc;

float do_some(int i, double d, unsigned short c);

BOOST_AUTO_TEST_CASE(function) {

	typedef function_traits<decltype(do_some)> ft;
	static_assert(ft::arity == 3, "should accept 3 arguments");
	static_assert(std::is_same<ft::return_type, float>::value, "should return float");
	static_assert(std::is_same<ft::arg<0>::type, int>::value, "should accept int as first arg");
	static_assert(std::is_same<ft::arg<1>::type, double>::value, "should accept double as second arg");
	static_assert(std::is_same<ft::arg<2>::type, unsigned short>::value, "should accept ushort as third arg");
}

BOOST_AUTO_TEST_CASE(functor) {
	std::function<void(char, const std::string &)> func;
	typedef function_traits<decltype(func)> ft;
	static_assert(ft::arity == 2, "should accept 2 arguments");
	static_assert(std::is_same<ft::return_type, void>::value, "should return void");
	static_assert(std::is_same<ft::arg<0>::type, char>::value, "should accept char as first arg");
	static_assert(std::is_same<ft::arg<1>::type, const std::string &>::value, "should accept strng as second arg");
}

BOOST_AUTO_TEST_CASE(method) {
	struct Class {
		bool eval(void) const;
	};
	typedef function_traits<decltype(&Class::eval)> ft;
	static_assert(ft::arity == 1, "should accept 1 argument");
	static_assert(std::is_same<ft::return_type, bool>::value, "should return bool");
	static_assert(std::is_same<ft::arg<0>::type, Class&>::value, "should accept Class& as first arg");
}
