#define BOOST_TEST_MODULE rpc.Invoker
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <adapter/invoker.h>
#include "fixtures.h"

// TODO: cases with void(...) functions

using namespace vw::rpc;

template<const std::size_t N> struct invoker_t {
	typedef Invoker<test::RequestParser, test::TypeTaggedSink, N> type;
};

float test_sum(int i, double d, unsigned short c) {
	return float(i + d + c);
}

BOOST_FIXTURE_TEST_CASE(function, test::InvokerFixture) {
	init("32 test_sum 10 3.5 2", 15.5f);
	invoker_t<3>::type invoker;
	invoker.apply(test_sum, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

BOOST_FIXTURE_TEST_CASE(functor_ptr, test::InvokerFixture) {
	struct Concat {
		Concat(const std::string & d) : delim(d) {}
		std::string operator() (const std::string & head, const std::string & tail) {
			return head + delim + tail;
		}
		std::string delim;
	} test_concat("_");
	init<std::string>("12 test_concat Hello, World!", "Hello,_World!");
	invoker_t<2>::type invoker;
	invoker.apply(&test_concat, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

BOOST_FIXTURE_TEST_CASE(functor, test::InvokerFixture) {
	struct OddEven {
		bool operator() (int odd_or_even) {
			return odd_or_even % 2;
		}
		std::string delim;
	} is_odd;
	init<bool>("3 test_odd_even 7", true);
	invoker_t<1>::type invoker;
	invoker.apply(is_odd, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

BOOST_FIXTURE_TEST_CASE(class_method, test::InvokerFixture) {
	struct Reverse {
		Reverse(const std::string & l, const std::string & r)
			: left_bracket(l), right_bracket(r) {}
		std::string reverse(const std::string & s) {
			std::string sc(s);
			std::reverse(std::begin(sc), std::end(sc));
			std::stringstream ss;
			ss << left_bracket << sc << right_bracket;
			return ss.str();
		}
		std::string left_bracket;
		std::string right_bracket;
	} test_reverse("<", ">");
	init<std::string>("1 test_reverse DEAD_BEEF", "<FEEB_DAED>");
	invoker_t<1>::type invoker;
	invoker.apply(&Reverse::reverse, &test_reverse, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

BOOST_FIXTURE_TEST_CASE(lambda, test::InvokerFixture) {
	double d = 0.5;
	auto wdiff = [&](float f, float g)->float { return float(d * (f - g)); };
	init("58 test_wdiff 3.7 5.5", -0.9f);
	invoker_t<2>::type invoker;
	invoker.apply(wdiff, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
	auto signal = [&](){ return d = 10.; };
	init("signal", 10.);
	invoker_t<0>::type void_invoker;
	void_invoker.apply(signal, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
	BOOST_CHECK_EQUAL(d, 10.);
}

BOOST_FIXTURE_TEST_CASE(std_function, test::InvokerFixture) {
	std::function<bool(int, int)> le = [](int a, int b)->bool { return a <= b; };
	init("41 test_le 412 87", false);
	invoker_t<2>::type invoker;
	invoker.apply(le, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

BOOST_FIXTURE_TEST_CASE(bind_result, test::InvokerFixture) {
	auto diff = [](double a, double b) { return a - b; };
	// unfortunately bind's operator () is templated - so we can't use it to deduce signature
	// as the result we have to wrap it by std::function
	std::function<double(double)> fdiff = std::bind(diff, std::placeholders::_1, 7.4);
	init("111 test_diff 8.7", 1.3);
	invoker_t<1>::type invoker;
	invoker.apply(fdiff, *parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

