#define BOOST_TEST_MODULE rpc.ArgsPackager
#define BOOST_TEST_DYN_LINK

#include <sstream>
#include <boost/test/unit_test.hpp>
#include <adapter/args_packager.h>
#include "fixtures.h"

using namespace vw::rpc;

typedef test::RequestSerializer serializer_t;
typedef ArgsPackager<serializer_t> packager_t;

BOOST_AUTO_TEST_CASE(pack_many) {

	std::stringstream _stream;
	serializer_t serializer(_stream, "concat", 0);
	packager_t::pack(serializer, "some_string", 3, true, -5);
	BOOST_CHECK_EQUAL(_stream.str(), "0 concat some_string 3 true -5");
}

BOOST_AUTO_TEST_CASE(pack_one) {
	std::stringstream _stream;
	serializer_t serializer(_stream, "print", 1);
	packager_t::pack(serializer, 14);
	BOOST_CHECK_EQUAL(_stream.str(), "1 print 14");
}

BOOST_AUTO_TEST_CASE(pack_zero) {
	std::stringstream _stream;
	serializer_t serializer(_stream, "void", 2);
	packager_t::pack(serializer);
	BOOST_CHECK_EQUAL(_stream.str(), "2 void");
}
