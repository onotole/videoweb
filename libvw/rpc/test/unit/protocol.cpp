#define BOOST_TEST_MODULE rpc.Protocol
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include "fixtures.h"

using namespace vw::rpc;

BOOST_AUTO_TEST_CASE(request_parser) {
typedef Proto<test::Proto>::request::parser_t parser_t;
	std::stringstream request("1 add 10 12.5");
	parser_t parser(request);
	BOOST_CHECK_EQUAL(parser.call(), "add");
	int a; parser.get_arg(a);
	BOOST_CHECK_EQUAL(a, 10);
	float b; parser.get_arg(b);
	BOOST_CHECK_EQUAL(b, 12.5);
	BOOST_CHECK_EQUAL(parser.id(), 1);
}

BOOST_AUTO_TEST_CASE(request_serializer) {
typedef Proto<test::Proto>::request::serializer_t serializer_t;
	std::stringstream stream;
	serializer_t serializer(stream, "divide", 0);
	serializer.put_arg(10.0);
	serializer.put_arg(2.5);
	BOOST_CHECK_EQUAL(stream.str(), "0 divide 10 2.5");
}

BOOST_AUTO_TEST_CASE(response_parser) {
typedef Proto<test::Proto>::response::parser_t parser_t;
	std::stringstream stream("2 3.14");
	parser_t parser(stream);
	double pi; parser.get_result(pi);
	BOOST_CHECK_EQUAL(pi, 3.14);
	BOOST_CHECK_EQUAL(parser.id(), 2);
}

BOOST_AUTO_TEST_CASE(response_serializer) {
typedef Proto<test::Proto>::response::serializer_t serializer_t;
	std::stringstream stream;
	serializer_t serializer(stream, 10);
	serializer.set_result("Hello!");
	BOOST_CHECK_EQUAL(stream.str(), "10 Hello!");
}

BOOST_AUTO_TEST_CASE(error_parser) {
typedef Proto<test::Proto>::error::parser_t parser_t;
	std::stringstream stream("error 404 Page not found");
	parser_t parser(stream);
	BOOST_CHECK_EQUAL(parser.code(), 404);
	BOOST_CHECK_EQUAL(parser.what(), "Page not found");
	BOOST_CHECK_EQUAL(parser.id(), 0);
}

BOOST_AUTO_TEST_CASE(error_serializer) {
typedef Proto<test::Proto>::error::serializer_t serializer_t;
	std::stringstream stream;
	serializer_t serializer(stream, "Memory access violation", 10);
	BOOST_CHECK_EQUAL(stream.str(), "error 10 Memory access violation");
	std::error_code code;
	auto exception = std::system_error(std::error_code(200, std::generic_category()), "Out of memory");
	stream.str(std::string());
	serializer_t exception_serializer(stream, exception);
    std::string expected_error("error 200 "); expected_error += exception.what();
    BOOST_CHECK_EQUAL(stream.str(), expected_error);
}

BOOST_AUTO_TEST_CASE(event_parser) {
typedef Proto<test::Proto>::event::parser_t parser_t;
	std::stringstream event("event size 720 576");
	parser_t parser(event);
	BOOST_CHECK_EQUAL(parser.event(), "size");
	size_t width; parser.get_arg(width);
	BOOST_CHECK_EQUAL(width, 720);
	size_t height; parser.get_arg(height);
	BOOST_CHECK_EQUAL(height, 576);
}

BOOST_AUTO_TEST_CASE(event_serializer) {
typedef Proto<test::Proto>::event::serializer_t serializer_t;
	std::stringstream stream;
	serializer_t serializer(stream, "position");
	serializer.put_arg(12.3);
	BOOST_CHECK_EQUAL(stream.str(), "event position 12.3");
}

BOOST_AUTO_TEST_CASE(message_parser) {
	std::stringstream event("event size 720 576");
	Proto<test::Proto>::message::parser_t parser_evt(event);
	BOOST_CHECK(parser_evt.type() == MessageType::Event);
	Proto<test::Proto>::event::parser_t evt_parser = parser_evt.get_event_parser();
	size_t w, h; evt_parser.get_arg(w); evt_parser.get_arg(h);
	BOOST_CHECK_EQUAL(w, 720);
	BOOST_CHECK_EQUAL(h, 576);
	std::stringstream error("error wrong_arg");
	Proto<test::Proto>::message::parser_t parser_err(error);
	BOOST_CHECK(parser_err.type() == MessageType::Error);
	std::stringstream response("true");
	Proto<test::Proto>::message::parser_t parser_res(response);
	BOOST_CHECK(parser_res.type() == MessageType::Response);
}
