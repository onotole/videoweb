#define BOOST_TEST_MODULE rpc.CallAdapter
#define BOOST_TEST_DYN_LINK

#include <boost/test/unit_test.hpp>
#include <adapter/call_adapter.h>
#include "fixtures.h"

using namespace vw::rpc;

typedef test::RequestParser parser_t;
typedef test::TypeTaggedSink sink_t;
typedef CallAdapter<parser_t, sink_t> adapter_t;


std::string concat(const std::string & s, size_t d) {
	std::stringstream ss;
	ss << s << d;
	return ss.str();
}

BOOST_FIXTURE_TEST_CASE(function, test::InvokerFixture) {

	auto f = adapter_t::wrap_callback(concat);
	init<std::string>("10 concat variable= 23", "variable=23");
	f(*parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

BOOST_FIXTURE_TEST_CASE(lambda, test::InvokerFixture) {
	int z = 1;
	auto f = adapter_t::wrap_callback([&](size_t a, int b){ return (a + z) < b; });
	init("12 less 3 14", true);
	f(*parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
	init("signal", 3);
	auto g = adapter_t::wrap_callback([&](){ return z = 3;});
	g(*parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
	BOOST_CHECK_EQUAL(z, 3);
}

BOOST_FIXTURE_TEST_CASE(functor_ptr, test::InvokerFixture) {
	struct fnctr {
		fnctr(const std::string & head) : _head(head) {}
		std::string operator () (const std::string & tail) {
			return (_head += " " + tail);
		}
		std::string _head;
	} fn("head");
	auto f = adapter_t::wrap_callback(&fn);
	init("43 head tail", std::string("head tail"));
	f(*parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}

BOOST_FIXTURE_TEST_CASE(method, test::InvokerFixture) {
	struct Class {
		Class(float d) : denom(d) {}
		double divide(double num) {
			return num / denom;
		}
		float denom;
	} klass(3.5f);
	auto f = adapter_t::wrap_callback(&klass, &Class::divide);
	init("11 div 10.5", 3.);
	f(*parser, sink);
	BOOST_CHECK_EQUAL(out.str(), expected.str());
}
