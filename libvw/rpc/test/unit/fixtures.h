#ifndef __TEST_ADAPTER_FIXTURES_H__
#define __TEST_ADAPTER_FIXTURES_H__

#include <iostream>
#include <iterator>
#include <sstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <base/util/debug_helpers.h>
#include <protocol.h>

namespace vw { namespace rpc { namespace test {

struct RequestParser {
    RequestParser(std::istream & stream) : _istream(stream) {
        _istream >> _id;
        _istream >> _call;
    }
    const std::string & call() const {
        return _call;
    }
    template<typename Arg>
    void get_arg(Arg & arg) {
        _istream >> arg;
    }
    size_t id() const {
        return _id;
    }

    std::string _call;
    std::istream & _istream;
    size_t _id;
};

struct RequestSerializer {
    RequestSerializer(std::ostream & stream, const std::string & call, size_t id)
        : _ostream(stream) {
        _ostream << id << " ";
        _ostream << call;
    }
    template<typename Arg>
    void put_arg(const Arg & arg) {
        _ostream << std::boolalpha << " " << arg;
    }
    void commit() {}
    std::ostream & _ostream;
};

struct ResponseParser {
    ResponseParser(std::istream & stream) : _istream(stream) {
        _istream >> _id;
    }
    template<typename Res>
    void get_result(Res & result) {
        _istream >> result;
    }
    size_t id() const { return _id; }

    std::istream & _istream;
    size_t _id;
};

struct ResponseSerializer {
    ResponseSerializer(std::ostream & stream, size_t id)
        : _ostream(stream), _id(id) {
        _ostream << _id << " ";
    }
    template <typename Res>
    void set_result(const Res & result) {
        _ostream << std::boolalpha << result;
    }

    std::ostream & _ostream;
    size_t _id;
};

struct EventParser {
    EventParser(std::istream & stream) : _istream(stream) {
        std::string event;
        _istream >> event;
        if (event != "event")
            throw std::runtime_error("bad message format");
        _istream >> _event;
    }
    const std::string & event() const {
        return _event;
    }
    template<typename Arg>
    void get_arg(Arg & arg) {
        _istream >> arg;
    }

    std::istream & _istream;
    std::string _event;
};

struct EventSerializer {
    EventSerializer(std::ostream & stream, const std::string & event)
        : _ostream(stream) {
        _ostream << "event " << event;
    }
    template<typename Arg>
    void put_arg(const Arg & arg) {
        _ostream << std::boolalpha << " " << arg;
    }
    void commit() {}
    std::ostream & _ostream;
};

struct ErrorParser {
    ErrorParser(std::istream & stream) : _id(0) {
        std::string error;
        stream >> error;
        if (error != "error")
            throw std::runtime_error("bad message format");
        try {
            stream >> _code;
        } catch (const std::exception &) {
            _code = -1;
        }
        while(stream) {
            std::string word;
            stream >> word;
            _message += " " + word;
        }
        auto trim = [](std::string & str)->std::string & {
            str.erase(str.begin(), std::find_if(str.begin(), str.end(),
                                                [](char & c)->bool { return !isspace(c); }));
            str.erase(std::find_if(str.rbegin(), str.rend(),
                                   [](char & c)->bool { return !isspace(c); }).base(), str.end());
            return str;
        };
        trim(_message);
    }
    const std::string & what() const {
        return _message;
    }
    size_t code() const {
        return _code;
    }
    size_t id() const {
        return _id;
    }

    std::string _message;
    size_t _code;
    size_t _id;
};

struct ErrorSerializer {
    ErrorSerializer(std::ostream & stream, const std::string & msg, size_t code, size_t id) {
        stream << "error ";
        if (code != -1 )
            stream << code << " ";
        stream << msg;
    }
};

struct MessageParser {
    MessageParser(std::istream & stream) {
        stream >> std::noskipws;
        std::copy(std::istream_iterator<char>(stream), std::istream_iterator<char>(),
                  std::ostream_iterator<char>(_stream));
        if (_stream.str().find("event") == 0)
            _type = MessageType::Event;
        else if (_stream.str().find("error") == 0)
            _type = MessageType::Error;
        else
            _type = MessageType::Response;
    }
    const MessageType & type() const {
        return _type;
    }
    RequestParser get_request_parser() {
        return RequestParser(_stream);
    }
    ResponseParser get_response_parser() {
        return ResponseParser(_stream);
    }
    EventParser get_event_parser() {
        return EventParser(_stream);
    }
    ErrorParser get_error_parser() {
        return ErrorParser(_stream);
    }

    MessageType _type;
    std::stringstream _stream;
};

struct Proto {
    struct message {
        typedef MessageParser parser_t;
    };
    struct request {
        typedef RequestParser parser_t;
        typedef RequestSerializer serializer_t;
    };
    struct response {
        typedef ResponseParser parser_t;
        typedef ResponseSerializer serializer_t;
    };
    struct event {
        typedef EventParser parser_t;
        typedef EventSerializer serializer_t;
    };
    struct error {
        typedef ErrorParser parser_t;
        typedef ErrorSerializer serializer_t;
    };
};

struct TypeTaggedSink {

    TypeTaggedSink(std::ostream & os) : output(os) {}

    template<typename Res>
    typename std::enable_if< ! std::is_void<Res>::value >::type
    set_result(const Res & result) {
        base::debug::print_value(output, result);
    }

    void set_result(void) {
        base::debug::print_value(output);
    }

    std::ostream & output;
};

struct InvokerFixture {
    InvokerFixture() : sink(out) {}
protected:
    template <typename Res>
    void init(const std::string & call, const Res & expected_result) {
        in << call;
        parser.reset(new test::RequestParser(in));
        base::debug::print_value(expected, expected_result);
    }

    std::stringstream in;
    std::stringstream out;
    std::stringstream expected;

    test::TypeTaggedSink sink;
    std::unique_ptr<test::RequestParser> parser;
};

}}} // namespace vw::rpc::test

#endif
