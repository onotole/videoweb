#include "detail.h"

namespace vw { namespace rpc { namespace json { namespace detail {

template <>
void parse_primitive<bool>(const Json::Value & v, bool & p) {
	p = v.asBool();
}

template <>
void parse_primitive<int>(const Json::Value & v, int & p) {
	p = v.asInt();
}

template <>
void parse_primitive<size_t>(const Json::Value & v, size_t & p) {
	p = v.asUInt();
}

template <>
void parse_primitive<float>(const Json::Value & v, float & p) {
	p = v.asFloat();
}

template <>
void parse_primitive<double>(const Json::Value & v, double & p) {
	p = v.asDouble();
}

template <>
void parse_primitive<std::string>(const Json::Value & v, std::string & p) {
	p = v.asString();
}

template <>
void store_primitive<size_t>(Json::Value & v, const size_t & p) {
#if defined(__x86_64__)
	v = Json::UInt64(p);
#else
	v = Json::UInt(p);
#endif
}

}}}}
