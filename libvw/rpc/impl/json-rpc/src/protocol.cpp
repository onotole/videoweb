#include <protocol.h>
#include <cassert>

namespace vw { namespace rpc { namespace json {

// Request

RequestParser::RequestParser(std::istream & stream) : _param(0) {
	stream >> _root;
	detail::parse_primitive(_root["method"], _call);
	detail::parse_primitive(_root["id"], _id);
}

RequestParser::RequestParser(Json::Value && root) : _root(root), _param(0) {
	detail::parse_primitive(_root["method"], _call);
	detail::parse_primitive(_root["id"], _id);
}

const std::string & RequestParser::call() const {
	return _call;
}
size_t RequestParser::id() const {
	return _id;
}

RequestSerializer::RequestSerializer(std::ostream & stream, const std::string & call, size_t id)
	: _ostream(stream) {
	detail::store_primitive(_root["method"], call);
	detail::store_primitive(_root["id"], id);
	_root["params"] = Json::arrayValue;
}

void RequestSerializer::commit() {
	_ostream << Json::FastWriter().write(_root);
}

// Response

ResponseParser::ResponseParser(std::istream & stream) {
	stream >> _root;
	detail::parse_primitive(_root["id"], _id);
}

ResponseParser::ResponseParser(Json::Value && root) : _root(root) {
	detail::parse_primitive(_root["id"], _id);
}

size_t ResponseParser::id() const {
	return _id;
}

ResponseSerializer::ResponseSerializer(std::ostream & stream, size_t id)
	: _ostream(stream) {
	detail::store_primitive(_root["id"], id);
}

// Event

EventParser::EventParser(std::istream & stream) : _param(0) {
	stream >> _root;
	detail::parse_primitive(_root["event"], _event);
}

EventParser::EventParser(Json::Value && root) : _root(root), _param(0) {
	detail::parse_primitive(_root["event"], _event);
}

const std::string & EventParser::event() const {
	return _event;
}

EventSerializer::EventSerializer(std::ostream & stream, const std::string & event)
	: _ostream(stream) {
	detail::store_primitive(_root["event"], event);
	_root["params"] = Json::arrayValue;
}

void EventSerializer::commit() {
	_ostream << Json::FastWriter().write(_root);
}

// Error

ErrorParser::ErrorParser(std::istream & stream) {
	Json::Value root;
	stream >> root;
	detail::parse_primitive(root["error"]["message"], _message);
	detail::parse_primitive(root["error"]["code"], _code);
	detail::parse_primitive(root["id"], _id);
}

ErrorParser::ErrorParser(Json::Value && root) {
	detail::parse_primitive(root["error"]["message"], _message);
	detail::parse_primitive(root["error"]["code"], _code);
	detail::parse_primitive(root["id"], _id);
}

const std::string & ErrorParser::what() const {
	return _message;
}

int ErrorParser::code() const {
	return _code;
}

size_t ErrorParser::id() const {
	return _id;
}

ErrorSerializer::ErrorSerializer(std::ostream & stream, const std::string & message,
								 int error_code, size_t id) {
	Json::Value root;
	detail::store_primitive(root["error"]["message"], message);
	detail::store_primitive(root["error"]["code"], error_code);
	if (id)
		detail::store_primitive(root["id"], id);
	stream << Json::FastWriter().write(root);
}

MessageParser::MessageParser(std::istream & stream) {
	stream >> _root;
	if (_root.isMember("event")) {
		_type = MessageType::Event;
		assert(_root.isMember("params"));
	}
	else if (_root.isMember("method"))
		_type = MessageType::Request;
	else if (_root.isMember("result"))
		_type = MessageType::Response;
	else if (_root.isMember("error"))
		_type = MessageType::Error;
	else
		throw std::runtime_error("bad message format");
}

const MessageType & MessageParser::type() const {
	return _type;
}

RequestParser MessageParser::get_request_parser() {
	return RequestParser(std::move(_root));
}

ResponseParser MessageParser::get_response_parser() {
	return ResponseParser(std::move(_root));
}

EventParser MessageParser::get_event_parser() {
	return EventParser(std::move(_root));
}

ErrorParser MessageParser::get_error_parser() {
	return ErrorParser(std::move(_root));
}

}}} // namespace vw::rpc::wss
