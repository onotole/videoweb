file(GLOB_RECURSE HEADERS "*.h")
file(GLOB_RECURSE SOURCES "*.cpp")

include_directories(../include)
add_library(vw.rpc.json-rpc ${HEADERS} ${SOURCES})
target_link_libraries(vw.rpc.json-rpc ${JSONCPP_LIBRARIES})

install(TARGETS vw.rpc.json-rpc
        LIBRARY DESTINATION ${VW_IMAGE_LIB}
        ARCHIVE DESTINATION ${VW_IMAGE_LIB})
