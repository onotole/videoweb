#ifndef __RPC_JSON_PROTOCOL_H__
#define __RPC_JSON_PROTOCOL_H__

#include <iostream>
#include <boost/fusion/algorithm.hpp>
#include <boost/mpl/range_c.hpp>
#include <rpc/protocol.h>
#include "detail.h"

namespace vw { namespace rpc { namespace json {

namespace bf = boost::fusion;

class MessageParser;

class RequestParser {
public:
	RequestParser(std::istream &);
	size_t id() const;
	const std::string & call() const;
	template<typename Arg, typename std::enable_if
			 <traits::is_sequence<Arg>::value>::type * = nullptr>
	void get_arg(Arg & arg) {
		detail::parse_visitor<Arg> visitor(_root["params"][_param++], arg);
		boost::mpl::range_c<size_t, 0, boost::fusion::result_of::size<Arg>::value> indices;
		boost::fusion::for_each(indices, visitor);
	}
	template<typename Arg, typename std::enable_if
			 <traits::is_primitive<Arg>::value>::type * = nullptr>
	void get_arg(Arg & arg) {
		return detail::parse_primitive(_root["params"][_param++], arg);
	}

private:
	friend class MessageParser;
	RequestParser(Json::Value &&);

	Json::Value _root;
	std::string _call;
	// TODO: switch to const_iterator. in current jsoncpp version they are broken
	int _param;
	size_t _id;
};

class RequestSerializer {
public:
	RequestSerializer(std::ostream &, const std::string &, size_t);
	template<typename Arg, typename std::enable_if
			 <traits::is_sequence<Arg>::value>::type * = nullptr>
	void put_arg(const Arg & arg) {
		Json::Value v(Json::objectValue);
		detail::serialize_visitor<Arg> visitor(v, arg);
		boost::mpl::range_c<size_t, 0, boost::fusion::result_of::size<Arg>::value> indices;
		boost::fusion::for_each(indices, visitor);
		_root["params"].append(v);
	}
	template<typename Arg, typename std::enable_if
			 <traits::is_primitive<Arg>::value>::type * = nullptr>
	void put_arg(const Arg & arg) {
		Json::Value v;
		detail::store_primitive(v, arg);
		_root["params"].append(v);
	}
	void commit();
private:
	Json::Value _root;
	std::ostream & _ostream;
};

class ResponseParser {
public:
	ResponseParser(std::istream &);
	template<typename Res, typename std::enable_if
			 <traits::is_sequence<Res>::value>::type * = nullptr>
	void get_result(Res & result) {
		detail::parse_visitor<Res> visitor(_root["result"], result);
		boost::mpl::range_c<size_t, 0, boost::fusion::result_of::size<Res>::value> indices;
		boost::fusion::for_each(indices, visitor);
	}
	template<typename Res, typename std::enable_if
			 <traits::is_primitive<Res>::value>::type * = nullptr>
	void get_result(Res & result) {
		return detail::parse_primitive(_root["result"], result);
	}
	size_t id() const;

private:
	friend class MessageParser;
	ResponseParser(Json::Value &&);

	Json::Value _root;
	size_t _id;
};

class ResponseSerializer {
public:
	ResponseSerializer(std::ostream &, size_t);
	template<typename Res, typename std::enable_if
			 <traits::is_sequence<Res>::value>::type * = nullptr>
	void set_result(const Res & result) {
		detail::serialize_visitor<Res> visitor(_root["result"], result);
		boost::mpl::range_c<size_t, 0, boost::fusion::result_of::size<Res>::value> indices;
		boost::fusion::for_each(indices, visitor);
		_ostream << Json::FastWriter().write(_root);;
	}
	template<typename Res, typename std::enable_if
			 <traits::is_primitive<Res>::value>::type * = nullptr>
	void set_result(const Res & result) {
		detail::store_primitive(_root["result"], result);
		_ostream << Json::FastWriter().write(_root);;
	}

private:
	Json::Value _root;
	std::ostream & _ostream;
};

class EventParser {
public:
	EventParser(std::istream &);
	const std::string & event() const;
	template<typename Arg, typename std::enable_if
			 <traits::is_sequence<Arg>::value>::type * = nullptr>
	void get_arg(Arg & arg) {
		detail::parse_visitor<Arg> visitor(_root["params"][_param++], arg);
		boost::mpl::range_c<size_t, 0, boost::fusion::result_of::size<Arg>::value> indices;
		boost::fusion::for_each(indices, visitor);
	}
	template<typename Arg, typename std::enable_if
			 <traits::is_primitive<Arg>::value>::type * = nullptr>
	void get_arg(Arg & arg) {
		return detail::parse_primitive(_root["params"][_param++], arg);
	}

private:
	friend class MessageParser;
	EventParser(Json::Value &&);

	Json::Value _root;
	std::string _event;
	int _param;
	size_t _id;
};

class EventSerializer {
public:
	EventSerializer(std::ostream &, const std::string &);
	template<typename Arg, typename std::enable_if
			 <traits::is_sequence<Arg>::value>::type * = nullptr>
	void put_arg(const Arg & arg) {
		Json::Value v(Json::objectValue);
		detail::serialize_visitor<Arg> visitor(v, arg);
		boost::mpl::range_c<size_t, 0, boost::fusion::result_of::size<Arg>::value> indices;
		boost::fusion::for_each(indices, visitor);
		_root["params"].append(v);
	}
	template<typename Arg, typename std::enable_if
			 <traits::is_primitive<Arg>::value>::type * = nullptr>
	void put_arg(const Arg & arg) {
		_root["params"].append(arg);
	}
	void commit();
private:
	Json::Value _root;
	std::ostream & _ostream;
};

class ErrorParser {
public:
	ErrorParser(std::istream &);
	const std::string & what() const;
	int code() const;
	size_t id() const;

private:
	friend class MessageParser;
	ErrorParser(Json::Value &&);

	std::string _message;
	int _code;
	size_t _id;
};

class ErrorSerializer {
public:
	ErrorSerializer(std::ostream &, const std::string &, int, size_t = 0);
};

class MessageParser {
public:
	MessageParser(std::istream &);
	const MessageType & type() const;

	RequestParser  get_request_parser();
	ResponseParser get_response_parser();
	EventParser    get_event_parser();
	ErrorParser    get_error_parser();

private:
	MessageType _type;
	Json::Value _root;
};

struct Proto {
	struct message {
		typedef MessageParser parser_t;
	};
	struct request {
		typedef RequestParser parser_t;
		typedef RequestSerializer serializer_t;
	};
	struct response {
		typedef ResponseParser parser_t;
		typedef ResponseSerializer serializer_t;
	};
	struct event {
		typedef EventParser parser_t;
		typedef EventSerializer serializer_t;
	};
	struct error {
		typedef ErrorParser parser_t;
		typedef ErrorSerializer serializer_t;
	};
};

}}} // namespace vw::rpc::wss

#endif // __RPC_JSON_PROTOCOL_H__
