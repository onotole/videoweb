#ifndef __RPC_JSON_TRAITS_H__
#define __RPC_JSON_TRAITS_H__

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wheader-guard"
#endif

#include <boost/fusion/sequence.hpp>

#ifdef __clang__
#pragma clang diagnistic pop
#endif

namespace vw { namespace rpc { namespace json { namespace traits {

template<typename>
struct is_string : std::false_type {};

template<>
struct is_string<std::string> : std::true_type {};

template<>
struct is_string<const char *> : std::true_type {};

template<size_t N>
struct is_string<char[N]> : std::true_type {};

template<typename T>
struct is_primitive : std::integral_constant
		<bool, std::is_arithmetic<T>::value || is_string<T>::value> {};

template<typename T>
struct is_sequence : boost::fusion::traits::is_sequence<T> {};

template<typename T, size_t N>
struct is_sequence<T[N]> : std::false_type {};

}}}}


#endif // __RPC_JSON_TRAITS_H__
