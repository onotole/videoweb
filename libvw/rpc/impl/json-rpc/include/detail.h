#ifndef __RPC_JSON_DETAIL_H__
#define __RPC_JSON_DETAIL_H__

#include <json/json.h>
#include <boost/fusion/adapted.hpp>
#include "traits.h"

namespace vw { namespace rpc { namespace json { namespace detail {

template<typename P, typename std::enable_if
		 <traits::is_primitive<P>::value>::type * = nullptr>
void parse_primitive(const Json::Value &, P &) {
	throw std::runtime_error("unsupported argument type");
}

template <>
void parse_primitive<bool>(const Json::Value & v, bool & p);
template <>
void parse_primitive<int>(const Json::Value & v, int & p);
template <>
void parse_primitive<size_t>(const Json::Value & v, size_t & p);
template <>
void parse_primitive<float>(const Json::Value & v, float & p);
template <>
void parse_primitive<double>(const Json::Value & v, double & p);
template <>
void parse_primitive<std::string>(const Json::Value & v, std::string & p);

template<typename T>
struct parse_visitor {
	parse_visitor(const Json::Value & v, T & t) : value(v), instance(t) {}
	template<typename I>
	void operator() (const I &) const {
		using namespace boost::fusion;
		const auto & v = value[extension::struct_member_name<T, I::value>::call()];
		parse_primitive(v, at_c<I::value>(instance));
	}
	const Json::Value & value;
	T & instance;
};

template<typename P, typename std::enable_if
		 <traits::is_primitive<P>::value>::type * = nullptr>
void store_primitive(Json::Value & v, const P & p) {
	v = p;
}

template <>
void store_primitive<size_t>(Json::Value & v, const size_t & p);

template<typename T>
struct serialize_visitor {
	serialize_visitor(Json::Value & v, const T & t) : value(v), instance(t) {}
	template<typename I>
	void operator() (const I &) const {
		using namespace boost::fusion;
		store_primitive(value[extension::struct_member_name<T, I::value>::call()],
				at_c<I::value>(instance));
	}
	Json::Value & value;
	const T & instance;
};

}}}}


#endif // __RPC_JSON_DETAIL_H__

