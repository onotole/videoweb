#define BOOST_TEST_MODULE rpc.json.Protocol
#define BOOST_TEST_DYN_LINK
#include <boost/fusion/adapted.hpp>
#include <boost/test/unit_test.hpp>
#include <rpc/protocol.h>
#include "protocol.h"

using namespace vw::rpc::json;

typedef vw::rpc::Proto<Proto>	proto_t;

namespace {
template<typename T, typename std::enable_if
		 <traits::is_string<T>::value>::type * = nullptr>
bool is_string(const T &) {
	return true;
}
template<typename T, typename std::enable_if
		 <!traits::is_string<T>::value>::type * = nullptr>
bool is_string(const T &) {
	return false;
}
template<typename T, typename std::enable_if
		 <traits::is_primitive<T>::value>::type * = nullptr>
bool is_primitive(const T &) {
	return true;
}
template<typename T, typename std::enable_if
		 <!traits::is_primitive<T>::value>::type * = nullptr>
bool is_primitive(const T &) {
	return false;
}
template<typename T, typename std::enable_if
		 <traits::is_sequence<T>::value>::type * = nullptr>
bool is_sequence(const T &) {
	return true;
}
template<typename T, typename std::enable_if
		 <!traits::is_sequence<T>::value>::type * = nullptr>
bool is_sequence(const T &) {
	return false;
}
}

namespace test0 {
namespace keys {
	struct name;
	struct grade;
	struct rate;
}
struct Student {
	std::string name;
	size_t grade;
	double rate;
};
}

BOOST_FUSION_ADAPT_ASSOC_STRUCT(test0::Student,
		(std::string, name, test0::keys::name)
		(size_t, grade, test0::keys::grade)
		(double, rate, test0::keys::rate))

BOOST_AUTO_TEST_CASE(traits_string) {
	std::string test_std_str;
	size_t number;
	BOOST_CHECK(is_string(test_std_str));
	BOOST_CHECK(is_string(test_std_str.c_str()));
	BOOST_CHECK(is_string("hello"));
	BOOST_CHECK(!is_string(10));
	BOOST_CHECK(!is_string(23.5));
	BOOST_CHECK(!is_string(true));
	BOOST_CHECK(!is_string(number));
	BOOST_CHECK(!is_string(&number));
	BOOST_CHECK(!is_string(test0::Student()));
}

BOOST_AUTO_TEST_CASE(traits_primitive) {
	std::string test_std_str;
	size_t number;
	BOOST_CHECK(is_primitive(test_std_str));
	BOOST_CHECK(is_primitive(test_std_str.c_str()));
	BOOST_CHECK(is_primitive("hello"));
	BOOST_CHECK(is_primitive(10));
	BOOST_CHECK(is_primitive(23.5));
	BOOST_CHECK(is_primitive(true));
	BOOST_CHECK(is_primitive(number));
	BOOST_CHECK(!is_primitive(&number));
	BOOST_CHECK(!is_primitive(test0::Student()));
}

BOOST_AUTO_TEST_CASE(traits_sequence) {
	std::string test_std_str;
	size_t number;
	BOOST_CHECK(!is_sequence(test_std_str));
	BOOST_CHECK(!is_sequence(test_std_str.c_str()));
	BOOST_CHECK(!is_sequence("hello"));
	BOOST_CHECK(!is_sequence(10));
	BOOST_CHECK(!is_sequence(23.5));
	BOOST_CHECK(!is_sequence(true));
	BOOST_CHECK(!is_sequence(number));
	BOOST_CHECK(!is_sequence(&number));
	BOOST_CHECK(is_sequence(test0::Student()));
}

BOOST_AUTO_TEST_CASE(request_parser) {
typedef proto_t::request::parser_t parser_t;
	{
		std::stringstream stream(R"__(
			{"jsonrpc": "2.0", "method": "subtract", "params": [42, 23], "id": 1}
		)__");
		parser_t parser(stream);
		BOOST_CHECK_EQUAL(parser.call(), "subtract");
		int a; parser.get_arg(a);
		BOOST_CHECK_EQUAL(a, 42);
		float b; parser.get_arg(b);
		BOOST_CHECK_EQUAL(b, 23.0);
		BOOST_CHECK_EQUAL(parser.id(), 1);
	}

	{
		std::stringstream stream(R"__(
			{"jsonrpc": "2.0", "method": "update", "params": [1,2,3,4,5]}
		)__");
		parser_t parser(stream);
		BOOST_CHECK_EQUAL(parser.call(), "update");
		for (size_t i = 1; i < 6; ++i) {
			size_t a; parser.get_arg(a);
			BOOST_CHECK_EQUAL(a, i);
		}
		BOOST_CHECK_EQUAL(parser.id(), 0);
	}
}

BOOST_AUTO_TEST_CASE(request_serializer) {
typedef proto_t::request::serializer_t serializer_t;
typedef proto_t::request::parser_t parser_t;

	std::stringstream stream;
	serializer_t serializer(stream, "divide", 12);
	serializer.put_arg(10);
	serializer.put_arg(2.5);
	serializer.commit();
	parser_t parser(stream);
	BOOST_CHECK_EQUAL(parser.call(), "divide");
	BOOST_CHECK_EQUAL(parser.id(), 12);
	int a; parser.get_arg(a);
	BOOST_CHECK_EQUAL(a, 10);
	float b; parser.get_arg(b);
	BOOST_CHECK_EQUAL(b, 2.5);
}

BOOST_AUTO_TEST_CASE(response_parser) {
typedef proto_t::response::parser_t parser_t;

	std::stringstream stream(R"__(
		{"jsonrpc": "2.0", "result": -19, "id": 47}
	)__");
	parser_t parser(stream);
	int a;
	parser.get_result(a);
	BOOST_CHECK_EQUAL(a, -19);
	BOOST_CHECK_EQUAL(parser.id(), 47);
}

BOOST_AUTO_TEST_CASE(response_serializer) {
typedef proto_t::response::serializer_t serializer_t;
typedef proto_t::response::parser_t parser_t;
	std::stringstream stream;
	serializer_t serializer(stream, 10);
	serializer.set_result("Hello!");
	parser_t parser(stream);
	std::string r;
	parser.get_result(r);
	BOOST_CHECK_EQUAL(r, "Hello!");
	BOOST_CHECK_EQUAL(parser.id(), 10);
}

BOOST_AUTO_TEST_CASE(event_parser) {
typedef proto_t::event::parser_t parser_t;

	std::stringstream stream(R"__(
		{"jsonrpc": "2.0", "event": "size", "params": [720, 576]}
	)__");
	parser_t parser(stream);
	int width; parser.get_arg(width);
	int height; parser.get_arg(height);
	BOOST_CHECK_EQUAL(width, 720);
	BOOST_CHECK_EQUAL(height, 576);
}

BOOST_AUTO_TEST_CASE(event_serializer) {
typedef proto_t::event::serializer_t serializer_t;
typedef proto_t::event::parser_t parser_t;

	std::stringstream stream;
	serializer_t serializer(stream, "size");
	serializer.put_arg(720);
	serializer.put_arg(576);
	serializer.commit();
	parser_t parser(stream);
	size_t w; parser.get_arg(w);
	size_t h; parser.get_arg(h);
	BOOST_CHECK_EQUAL(w, 720);
	BOOST_CHECK_EQUAL(h, 576);
}

BOOST_AUTO_TEST_CASE(error_parser) {
typedef proto_t::error::parser_t parser_t;

	std::stringstream stream(R"__(
		{"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found"}, "id": 1}
	)__");
	parser_t parser(stream);
	BOOST_CHECK_EQUAL(parser.what(), "Method not found");
	BOOST_CHECK_EQUAL(parser.code(), -32601);
	BOOST_CHECK_EQUAL(parser.id(), 1);
}

BOOST_AUTO_TEST_CASE(error_serializer) {
typedef proto_t::error::serializer_t serializer_t;
typedef proto_t::error::parser_t parser_t;

	std::stringstream stream;
	serializer_t serializer(stream, "Parse error", -32700);
	parser_t parser(stream);
	BOOST_CHECK_EQUAL(parser.what(), "Parse error");
	BOOST_CHECK_EQUAL(parser.code(), -32700);
	BOOST_CHECK_EQUAL(parser.id(), 0);
}

BOOST_AUTO_TEST_CASE(message_parser) {
	std::stringstream event(R"__(
		{"jsonrpc": "2.0", "event": "size", "params": [720, 576]}
	)__");
	proto_t::message::parser_t parser_evt(event);
	BOOST_CHECK(parser_evt.type() == vw::rpc::MessageType::Event);
	proto_t::event::parser_t evt_parser = parser_evt.get_event_parser();
	BOOST_CHECK_EQUAL(evt_parser.event(), "size");
	size_t w, h; evt_parser.get_arg(w); evt_parser.get_arg(h);
	BOOST_CHECK_EQUAL(w, 720);
	BOOST_CHECK_EQUAL(h, 576);
	std::stringstream error(R"__(
		{"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found"}, "id": 1}
	)__");
	proto_t::message::parser_t parser_err(error);
	BOOST_CHECK(parser_err.type() == vw::rpc::MessageType::Error);
	std::stringstream response(R"__(
		{"jsonrpc": "2.0", "result": -19, "id": 47}
	)__");
	proto_t::message::parser_t parser_res(response);
	BOOST_CHECK(parser_res.type() == vw::rpc::MessageType::Response);
}

BOOST_AUTO_TEST_CASE(adapted_struct_request) {
	std::stringstream request0(R"__(
		{"method": "get", "params": [{"name":"Petro Shchur", "grade":3, "rate":0.7}], "id": 1}
	)__");
	proto_t::request::parser_t parser(request0);
	test0::Student student;
	parser.get_arg(student);
	BOOST_CHECK_EQUAL(student.name, "Petro Shchur");
	BOOST_CHECK_EQUAL(student.grade, 3);
	BOOST_CHECK_EQUAL(student.rate, 0.7);
	std::stringstream request1;
	proto_t::request::serializer_t serializer(request1, "set", 2);
	serializer.put_arg(student);
	serializer.commit();
	Json::Value parsed;
	request1 >> parsed;
	BOOST_CHECK(parsed["params"].isArray());
	BOOST_CHECK_EQUAL(parsed["params"][0]["grade"], 3);
	BOOST_CHECK_EQUAL(parsed["params"][0]["name"], "Petro Shchur");
	BOOST_CHECK_EQUAL(parsed["params"][0]["rate"], 0.7);
}

namespace test1 {
namespace keys {
	struct key;
	struct val;
}
template <typename T>
struct Pair {
	std::string key;
	T val;
};
typedef Pair<int> int_pair_t;
typedef Pair<float> float_pair_t;
typedef Pair<std::string> str_pair_t;
}

BOOST_FUSION_ADAPT_ASSOC_STRUCT(test1::int_pair_t,
		(std::string, key, test1::keys::key)
		(int, val, test1::keys::val))

BOOST_FUSION_ADAPT_ASSOC_STRUCT(test1::str_pair_t,
		(std::string, key, test1::keys::key)
		(std::string, val, test1::keys::val))

BOOST_AUTO_TEST_CASE(adapted_struct_response) {
	std::stringstream response0(R"__(
		{"result": {"key":"one","val":1}, "id": 10}
	)__");
	proto_t::response::parser_t parser(response0);
	test1::int_pair_t pair;
	parser.get_result(pair);
	BOOST_CHECK_EQUAL(pair.key, "one");
	BOOST_CHECK_EQUAL(pair.val, 1);
	std::stringstream response1;
	proto_t::response::serializer_t serializer(response1, 10);
	serializer.set_result(pair);
	Json::Value parsed;
	response1 >> parsed;
	BOOST_CHECK(parsed["result"].isObject());
	BOOST_CHECK_EQUAL(parsed["result"]["key"], "one");
	BOOST_CHECK_EQUAL(parsed["result"]["val"], 1);
}

BOOST_AUTO_TEST_CASE(adapted_struct_event) {
	std::stringstream event0(R"__(
		{"event":"done","params":[{"key":"one","val":1}, {"key":"two","val":"three"}]}
	)__");
	proto_t::event::parser_t parser(event0);
	test1::int_pair_t pair0;
	test1::str_pair_t pair1;
	parser.get_arg(pair0);
	parser.get_arg(pair1);
	BOOST_CHECK_EQUAL(pair0.key, "one");
	BOOST_CHECK_EQUAL(pair0.val, 1);
	BOOST_CHECK_EQUAL(pair1.key, "two");
	BOOST_CHECK_EQUAL(pair1.val, "three");
	std::stringstream event1;
	proto_t::event::serializer_t serializer(event1, "ready");
	serializer.put_arg(pair0);
	serializer.put_arg(pair1);
	serializer.commit();
	Json::Value parsed;
	event1 >> parsed;
	BOOST_CHECK_EQUAL(parsed["event"], "ready");
	BOOST_CHECK(parsed["params"].isArray());
	BOOST_CHECK_EQUAL(parsed["params"][0]["key"], "one");
	BOOST_CHECK_EQUAL(parsed["params"][0]["val"], 1);
	BOOST_CHECK_EQUAL(parsed["params"][1]["key"], "two");
	BOOST_CHECK_EQUAL(parsed["params"][1]["val"], "three");
}
