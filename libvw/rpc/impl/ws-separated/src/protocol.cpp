#include <protocol.h>
#include <stdexcept>
#include <algorithm>

namespace vw { namespace rpc { namespace wss {

RequestParser::RequestParser(std::istream & stream)
	: _istream(stream),  _id(0) {
	_istream >> _id;
	if (_istream.rdstate() & std::istream::failbit) {
		_istream.clear();
		_istream.seekg(0, _istream.beg);
		_id = 0;
	}
	_istream >> _call;
}
const std::string & RequestParser::call() const {
	return _call;
}
size_t RequestParser::id() const {
	return _id;
}

RequestSerializer::RequestSerializer(std::ostream & stream, const std::string & call, size_t id)
	: _ostream(stream) {
		if (id != 0) _ostream << id << " ";
		_ostream << call;
}

ResponseParser::ResponseParser(std::istream & stream)
	: _istream(stream), _id(0) {
	_istream >> _id;
	if (_istream.rdstate() & std::istream::failbit) {
		_istream.clear();
		_istream.seekg(0, _istream.beg);
		_id = 0;
	}
}
size_t ResponseParser::id() const {
	return _id;
}

ResponseSerializer::ResponseSerializer(std::ostream & stream, size_t id)
	: _ostream(stream) {
	if (id != 0) _ostream << id;
}

ErrorParser::ErrorParser(const std::string & msg) : _id(0) {
	std::stringstream stream;
	stream << msg;
	std::string error;
	stream >> error;
	if (error != "error")
		throw std::runtime_error("bad message format");
	try {
		stream >> _code;
	} catch (const std::exception &) {
		_code = -1;
	}
	while(stream) {
		std::string word;
		stream >> word;
		_message += " " + word;
	}
	auto trim = [](std::string & str)->std::string & {
		str.erase(str.begin(), std::find_if(str.begin(), str.end(),
											[](char & c)->bool { return !isspace(c); }));
		str.erase(std::find_if(str.rbegin(), str.rend(),
							   [](char & c)->bool { return !isspace(c); }).base(), str.end());
		return str;
	};
	trim(_message);
}

const std::string & ErrorParser::what() const {
	return _message;
}

size_t ErrorParser::code() const {
	return _code;
}

size_t ErrorParser::id() const {
	return _id;
}

ErrorSerializer::ErrorSerializer(const std::string & msg, size_t code, size_t id) {
	std::stringstream stream;
	stream << "error ";
	if (code != -1 )
		stream << code << " ";
	stream << msg;
	_message = stream.str();
}

const std::string & ErrorSerializer::message() const {
	return _message;
}

}}} // namespace vw::rpc::wss
