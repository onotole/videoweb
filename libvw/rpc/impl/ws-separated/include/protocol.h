#ifndef __RPC_WS_SEPARATED_PROTOCOL_H__
#define __RPC_WS_SEPARATED_PROTOCOL_H__

#include <string>
#include <sstream>

namespace vw { namespace rpc { namespace wss {

class RequestParser {
public:
	RequestParser(std::istream & stream);
	size_t id() const;
	const std::string & call() const;
	template<typename Arg>
	void get_arg(Arg & arg) {
		_istream >> arg;
	}

private:
	std::string _call;
	std::istream & _istream;
	size_t _id;
};

class RequestSerializer {
public:
	RequestSerializer(std::ostream & stream, const std::string &, size_t);
	template<typename Arg>
	void put_arg(const Arg & arg) {
		_ostream << std::boolalpha << " " << arg;
	}

private:
	std::ostream & _ostream;
};

class ResponseParser {
public:
	ResponseParser(std::istream &);
	template<typename Res>
	void get_result(Res & result) {
		_istream >> result;
	}
	size_t id() const;

private:
	std::istream & _istream;
	size_t _id;
};

class ResponseSerializer {
public:
	ResponseSerializer(std::ostream &, size_t);
	template <typename Res>
	void set_result(const Res & result) {
		_ostream << std::boolalpha << " " << result;
	}

private:
	std::ostream & _ostream;
};


// TODO: error parser / seriallizer

struct ErrorParser {
	ErrorParser(const std::string & msg);
	const std::string & what() const;
	size_t code() const;
	size_t id() const;

	std::string _message;
	size_t _code;
	size_t _id;
};

struct ErrorSerializer {
	ErrorSerializer(const std::string & msg, size_t code, size_t id);
	const std::string & message() const;

	std::string _message;
};

struct Proto {
	struct request {
		typedef RequestParser parser_t;
		typedef RequestSerializer serializer_t;
	};
	struct response {
		typedef ResponseParser parser_t;
		typedef ResponseSerializer serializer_t;
	};
	struct error {
		typedef ErrorParser parser_t;
		typedef ErrorSerializer serializer_t;
	};
};

}}} // namespace vw::rpc::wss

#endif // __RPC_WS_SEPARATED_PROTOCOL_H__
