#define BOOST_TEST_MODULE rpc.wss.Protocol
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <rpc/protocol.h>
#include "protocol.h"

using namespace vw::rpc::wss;

typedef vw::rpc::Proto<Proto>	proto_t;

BOOST_AUTO_TEST_CASE(request_parser) {

typedef proto_t::request::parser_t parser_t;

	std::stringstream _stream("add 10 12.5");
	parser_t parser(_stream);
	BOOST_CHECK_EQUAL(parser.call(), "add");
	int a; parser.get_arg(a);
	BOOST_CHECK_EQUAL(a, 10);
	float b; parser.get_arg(b);
	BOOST_CHECK_EQUAL(b, 12.5);
	BOOST_CHECK_EQUAL(parser.id(), 0);
}

BOOST_AUTO_TEST_CASE(request_serializer) {
typedef proto_t::request::serializer_t serializer_t;

	std::stringstream _stream;
	serializer_t serializer(_stream, "divide", 12);
	serializer.put_arg(10.0);
	serializer.put_arg(2.5);
	BOOST_CHECK_EQUAL(_stream.str(), "12 divide 10 2.5");
}

BOOST_AUTO_TEST_CASE(response_parser) {
typedef proto_t::response::parser_t parser_t;

	std::stringstream _stream("10 3.14");
	parser_t parser(_stream);
	double pi; parser.get_result(pi);
	BOOST_CHECK_EQUAL(pi, 3.14);
	BOOST_CHECK_EQUAL(parser.id(), 10);
}

BOOST_AUTO_TEST_CASE(response_serializer) {
typedef proto_t::response::serializer_t serializer_t;

	std::stringstream _stream;
	serializer_t serializer(_stream, 10);
	serializer.set_result("Hello!");
	BOOST_CHECK_EQUAL(_stream.str(), "10 Hello!");
}


