#ifndef __RPC_DISPATCHER_H__
#define __RPC_DISPATCHER_H__

#include <sstream>
#include <functional>
#include <map>
#include <log/log.h>
#include "adapter/call_adapter.h"
#include "adapter/args_packager.h"

namespace vw { namespace rpc {

// TODO: 1. Handle methods
//       2. Handle void return type
//       3. Handle errors
//       4. Handle temp variables

namespace {
static struct NullSink {
    template <typename T> void set_result(const T&) {}
    void set_result() {}
} null_sink;

template<typename Parser, typename Arg>
class IdAccessor {
public:
    IdAccessor(Parser & p, bool & e) : parser(p), extracted(e) {}
    Arg & get_arg(Arg & arg) {
        if (!extracted)
            throw std::runtime_error("call id is ignored");
        parser.get_arg(arg);
        return arg;
    }
private:
    Parser & parser;
    bool & extracted;
};

template<typename Parser>
class IdAccessor<Parser, size_t> {
public:
    IdAccessor(Parser & p, bool & e) : parser(p), extracted(e) {}
    size_t & get_arg(size_t & arg) {
        if (extracted) {
            parser.get_arg(arg);
            return arg;
        }
        extracted = true;
        return arg = parser.id();
    }
private:
    Parser & parser;
    bool & extracted;
};

template<typename Parser>
struct IdExtractParser {
    IdExtractParser(Parser & p)
        : parser(p), id_extracted(false) {}
    template<typename Arg>
    Arg & get_arg(Arg & arg) {
        IdAccessor<Parser, Arg> accessor(parser, id_extracted);
        return accessor.get_arg(arg);
    }
private:
    Parser & parser;
    bool id_extracted;
};
}

template <typename Proto, typename Connector>
class Dispatcher {
public:
    Dispatcher(Connector & connector) : _connector(connector) {
        _connector.set_recv_handler
                ([this] (std::istream & msg) {
            try {
                typename Proto::request::parser_t parser(msg);
                auto async_call_it = _async_callmap.find(parser.call());
                if (async_call_it != _async_callmap.end()) {
                    IdExtractParser<typename Proto::request::parser_t> id_parser(parser);
                    async_call_it->second(id_parser, null_sink);
                } else {
                    auto call_it = _callmap.find(parser.call());
                    if (call_it != _callmap.end()) {
                        std::stringstream out;
                        typename Proto::response::serializer_t serializer(out, parser.id());
                        call_it->second(parser, serializer);
                        _connector.send(out);
                    }
                }
            } catch (const std::exception & e) {
                ERROR << "error parsing message: " << e.what();
            }
        });
    }
    template <typename Func>
    void register_handler(const std::string & call, Func && handler) {
        _callmap[call] = adapter_t::wrap_callback(std::move(handler));
    }
    template <typename Class, typename Method>
    void register_handler(const std::string & call, Class * klass, Method method) {
        _callmap[call] = adapter_t::wrap_callback(klass, method);
    }
    template <typename Func>
    void register_async_handler(const std::string & call, Func && handler) {
        typedef function_traits<Func> ft;
        static_assert(std::is_same<typename ft::return_type, void>::value,
                      "async handler should return void");
        static_assert(std::is_same<typename ft::template arg<0>::type, size_t>::value,
                      "async handler should accept call tag as a first argument");
        _async_callmap[call] = async_adapter_t::wrap_callback(std::move(handler));
    }
    template <typename Class, typename Method>
    void register_async_handler(const std::string & call, Class * klass, Method method) {
        typedef function_traits<Method> ft;
        static_assert(std::is_same<typename ft::return_type, void>::value,
                      "async handler should return void");
        static_assert(std::is_same<typename ft::template arg<0>::type, size_t>::value,
                      "async handler should accept call tag as a first argument");
        _async_callmap[call] = adapter_t::wrap_callback(klass, method);
    }
    template <typename ... Args>
    void notify(const std::string & event, const Args & ... args) {
        typedef typename Proto::event::serializer_t serializer_t;
        typedef ArgsPackager<serializer_t> packager_t;
        std::stringstream stream;
        serializer_t serializer(stream, event);
        packager_t::pack(serializer, args...);
        _connector.send(stream);
    }
    template <typename R>
    void reply(size_t call_id, const R & result) {
        std::stringstream out;
        typename Proto::response::serializer_t serializer(out, call_id);
        _connector.send(out);
    }

private:
    typedef CallAdapter<typename Proto::request::parser_t,
    typename Proto::response::serializer_t> adapter_t;
    typedef CallAdapter<IdExtractParser<typename Proto::request::parser_t>,
    NullSink> async_adapter_t;

    std::map<std::string, typename adapter_t::func_t> _callmap;
    std::map<std::string, typename async_adapter_t::func_t> _async_callmap;
    Connector & _connector;
};

}} // namespace vw::rpc

#endif // __RPC_DISPATCHER_H__
