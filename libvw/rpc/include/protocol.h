#ifndef __RPC_PROTOCOL_H__
#define __RPC_PROTOCOL_H__

#include <string>
#include <iostream>
#include <stdexcept>
#include <system_error>

namespace vw { namespace rpc {

enum class MessageType { Request, Response, Event, Error };

template <typename Proto>
class RequestParser {
public:
	RequestParser(std::istream & stream) : _impl(stream) {}
	RequestParser(typename Proto::request::parser_t && impl) : _impl(impl) {}
	const std::string & call() const {
		return _impl.call();
	}
	template <typename Arg>
	void get_arg(Arg & arg) {
		return _impl.get_arg(arg);
	}
	size_t id() const {
		return _impl.id();
	}
private:
	typename Proto::request::parser_t _impl;
};

template <typename Proto>
class RequestSerializer {
public:
	RequestSerializer(std::ostream & stream, const std::string & call, size_t id = 0)
		:_impl(stream, call, id) {}
	template <typename Arg>
	void put_arg(const Arg & arg) {
		_impl.template put_arg<Arg>(arg);
	}
	void commit() {
		_impl.commit();
	}

private:
	typename Proto::request::serializer_t _impl;
};

template <typename Proto>
class ResponseParser {
public:
	ResponseParser(std::istream & stream) : _impl(stream) {}
	ResponseParser(typename Proto::response::parser_t && impl) : _impl(impl) {}
	template <typename Res>
	void get_result(Res & result) {
		return _impl.template get_result<Res>(result);
	}
	size_t id() const {
		return _impl.id();
	}

private:
	typename Proto::response::parser_t _impl;
};

template <typename Proto>
class ResponseSerializer {
public:
	ResponseSerializer(std::ostream & stream, size_t id) : _impl(stream, id) {}
	template <typename Res>
	void set_result(const Res & result) {
		_impl.template set_result<Res>(result);
	}

private:
	typename Proto::response::serializer_t _impl;
};

template <typename Proto>
class EventParser {
public:
	EventParser(std::istream & stream) : _impl(stream) {}
	EventParser(typename Proto::event::parser_t && impl) : _impl(impl) {}
	const std::string & event() const {
		return _impl.event();
	}
	template <typename Arg>
	void get_arg(Arg & arg) {
		return _impl.get_arg(arg);
	}
private:
	typename Proto::event::parser_t _impl;
};

template <typename Proto>
class EventSerializer {
public:
	EventSerializer(std::ostream & stream, const std::string & event)
		:_impl(stream, event) {}
	template <typename Arg>
	void put_arg(const Arg & arg) {
		_impl.template put_arg<Arg>(arg);
	}
	void commit() {
		_impl.commit();
	}

private:
	typename Proto::event::serializer_t _impl;
};

template <typename Proto>
class ErrorParser {
public:
	ErrorParser(std::istream & stream) : _impl(stream) {}
	ErrorParser(typename Proto::error::parser_t && impl) : _impl(impl) {}
	size_t id() const {
		return _impl.id();
	}
	size_t code() const {
		return _impl.code();
	}
	const std::string & what() const {
		return _impl.what();
	}
private:
	typename Proto::error::parser_t _impl;
};

template <typename Proto>
class ErrorSerializer {
public:
	ErrorSerializer(std::ostream & stream, const std::string & msg, int code, size_t id = size_t(0))
		: _impl(stream, msg, code, id) {}
	ErrorSerializer(std::ostream & stream, const char * msg, int code, size_t id = 0)
		: ErrorSerializer(stream, std::string(msg), code, id) {}
	ErrorSerializer(std::ostream & stream, const std::exception & e, int code, size_t id = 0)
		: ErrorSerializer(stream, e.what(), code, id) {}
	ErrorSerializer(std::ostream & stream, const std::system_error & se, size_t id = 0)
		: ErrorSerializer(stream, se, se.code().value(), id) {}
private:
	typename Proto::error::serializer_t _impl;
};

template <typename Impl>
struct Proto {
	struct request {
		typedef RequestParser<Impl> parser_t;
		typedef RequestSerializer<Impl> serializer_t;
	};
	struct response {
		typedef ResponseParser<Impl> parser_t;
		typedef ResponseSerializer<Impl> serializer_t;
	};
	struct event {
		typedef EventParser<Impl> parser_t;
		typedef EventSerializer<Impl> serializer_t;
	};
	struct error {
		typedef ErrorParser<Impl> parser_t;
		typedef ErrorSerializer<Impl> serializer_t;
	};
	struct message {
		class parser_t {
		public:
			parser_t(std::istream & stream) : _impl(stream) {}
			const MessageType & type() const {
				return _impl.type();
			}
			typename request::parser_t get_request_parser() {
				if (type() != MessageType::Request)
					throw std::runtime_error("wrong parser");
				return typename request::parser_t(_impl.get_request_parser());
			}
			typename response::parser_t get_response_parser() {
				if (type() != MessageType::Response)
					throw std::runtime_error("wrong parser");
				return typename response::parser_t(_impl.get_response_parser());
			}
			typename event::parser_t get_event_parser() {
				if (type() != MessageType::Event)
					throw std::runtime_error("wrong parser");
				return typename event::parser_t(_impl.get_event_parser());
			}
			typename error::parser_t get_error_parser() {
				if (type() != MessageType::Error)
					throw std::runtime_error("wrong parser");
				return typename error::parser_t(_impl.get_error_parser());
			}
		private:
			typename Impl::message::parser_t _impl;
		};
	};
};

}} // namespace vw::rpc

#endif // __RPC_PROTOCOL_H__
