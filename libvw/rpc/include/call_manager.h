#ifndef __RPC_CALL_MANAGER_H__
#define __RPC_CALL_MANAGER_H__

#include <string>
#include <map>
#include <sstream>
#include <mutex>
#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread/future.hpp>
#include "adapter/function_traits.h"
#include "adapter/call_adapter.h"
#include "adapter/args_packager.h"
#include "protocol.h"

namespace vw { namespace rpc {

static size_t next_token = 1;

template <typename Proto, typename Client>
class CallManager {
    typedef std::function<void(typename Proto::response::parser_t &)> completion_callback_t;
    typedef std::function<void(typename Proto::event::parser_t &)> event_callback_t;
    // TODO: error handler
    struct NullSink { void set_result() {} } null_sink;
    typedef CallAdapter<typename Proto::event::parser_t, NullSink> event_adapter_t;

public:
    CallManager(Client & client) : _client(client) {
        _client.set_recv_handler
                ([this] (std::istream & msg) {
            typename Proto::message::parser_t parser(msg);
            if (parser.type() == MessageType::Event) {
                auto event_parser = parser.get_event_parser();
                auto event_it = _eventmap.find(event_parser.event());
                if (event_it != _eventmap.end()) {
                    event_it->second(event_parser);
                }
            } else if (parser.type() == MessageType::Response) {
                auto response_parser = parser.get_response_parser();
                completion_callback_t callback;
                {
                    std::lock_guard<std::mutex> _(_callmap_guard);
                    auto call_it = _callmap.find(response_parser.id());
                    if (call_it != _callmap.end()) {
                        callback = std::move(call_it->second);
                        _callmap.erase(call_it);
                    }
                }
                if (callback) callback(response_parser);
            }
        });
    }

    template<typename Res, typename ... Args>
    Res call(const std::string & call, const Args & ... args) {
        boost::promise<Res> promise;
        boost::future<Res> future = promise.get_future();
        call_async(call, [&promise] (const Res & result) {
            promise.set_value(result);
        }, args...);
        return future.get();
    }

    template<typename Class, typename Method, typename ... Args>
    void call_async(const std::string & call, Class * klass, Method method, const Args & ... args) {
        typedef function_traits<decltype(method)> ft;
        auto cb = [klass, method](const typename ft::template arg<1>::type & result) {
            (klass->*method)(result);
        };
        call_async(call, cb, args...);
    }

    template<typename Callback, typename ... Args>
    void call_async(const std::string & call, Callback && cb, const Args & ... args) {
        typedef typename Proto::request::serializer_t serializer_t;
        typedef typename Proto::response::parser_t parser_t;
        typedef ArgsPackager<serializer_t> packager_t;
        std::stringstream stream;
        size_t call_token = next_token++;
        serializer_t serializer(stream, call, call_token);
        packager_t::pack(serializer, args...);
        auto wrapped_cb = [cb](parser_t & parser) {
            typedef function_traits<decltype(cb)> ft;
            typename std::decay<typename ft::template arg<0>::type>::type result;
            parser.get_result(result);
            cb(result);
        };
        {
            std::lock_guard<std::mutex> _(_callmap_guard);
            _callmap.emplace(call_token, wrapped_cb);
        }
        _client.send(stream);
    }

    void send_fd(int fd) {
        return _client.send(fd);
    }

    template<typename Func>
    void register_event_handler(const std::string & event, Func && handler) {
        _eventmap[event] = std::bind(event_adapter_t::wrap_callback(std::move(handler)),
                                     std::placeholders::_1, null_sink);
    }
    template <typename Class, typename Method>
    void register_event_handler(const std::string & event, Class * klass, Method method) {
        _eventmap[event] = std::bind(event_adapter_t::wrap_callback(klass, method),
                                     std::placeholders::_1, null_sink);
    }

private:
    Client & _client;
    std::mutex _callmap_guard;
    std::map<size_t, completion_callback_t> _callmap;
    std::map<std::string, event_callback_t> _eventmap;
};

}}

#endif //__RPC_CALL_MANAGER_H__
