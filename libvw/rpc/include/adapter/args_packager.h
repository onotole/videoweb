#ifndef __RPC_ARGS_PACKAGER_H__
#define __RPC_ARGS_PACKAGER_H__

namespace vw { namespace rpc {

template<typename Serializer>
struct ArgsPackager {
	static void pack(Serializer & serializer) {
		serializer.commit();
	}
	template<typename Arg, typename ... Args>
	static void pack(Serializer & serializer, const Arg & head, const Args & ... tail) {
		serializer.put_arg(head);
		pack(serializer, tail...);
	}
};

}}

#endif //__RPC_ARGS_PACKAGER_H__
