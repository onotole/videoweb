#ifndef __RPC_FUNCTION_TRAITS_H__
#define __RPC_FUNCTION_TRAITS_H__

#include <tuple>

namespace vw { namespace rpc {

template <typename F> struct function_traits;

// function
template <typename R, typename ... Args>
struct function_traits<R(Args...)> {
    typedef R return_type;
    static constexpr std::size_t arity = sizeof...(Args);
	template<const std::size_t N>
    struct arg {
        static_assert(N < arity, "error: invalid parameter index.");
        typedef typename std::tuple_element<N, std::tuple<Args...>>::type type;
    };
};

// function pointer
template <typename R, typename ... Args>
struct function_traits<R(*)(Args...)> : function_traits<R(Args...)> {};

// member function pointer
template <typename C, typename R, typename ... Args>
struct function_traits<R(C::*)(Args...)> : function_traits<R(C&,Args...)> {};

// const member function pointer
template <typename C, typename R, typename ... Args>
struct function_traits<R(C::*)(Args...) const> : function_traits<R(C&,Args...)> {};

// functor
template <typename F>
struct function_traits {
    typedef function_traits<decltype(&F::operator())> call_type;
    typedef typename call_type::return_type return_type;
    static constexpr std::size_t arity = call_type::arity - 1;
	template <const std::size_t N>
    struct arg {
        static_assert(N < arity, "error: invalid parameter index.");
        typedef typename call_type::template arg<N + 1>::type type;
    };
};

template <typename F> struct function_traits<F&> : function_traits<F> {};
template <typename F> struct function_traits<F&&> : function_traits<F> {};

/* TODO: add test case */
template <typename Function>
struct is_function_ptr : std::integral_constant<bool, std::is_pointer<Function>::value
			&& std::is_function<typename std::remove_pointer<Function>::type>::value> {};

}} // namespace vw::rpc

#endif
