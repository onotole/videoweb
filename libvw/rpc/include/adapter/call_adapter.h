#ifndef __RPC_CALL_ADAPTER_H__
#define __RPC_CALL_ADAPTER_H__

#include <functional>

#include "function_traits.h"
#include "invoker.h"

namespace vw { namespace rpc {

template<typename CallParser, typename ResultSink>
class CallAdapter {
public:
	typedef std::function<void(CallParser &, ResultSink &)> func_t;

	template<typename Function>
	static inline typename std::enable_if<is_function_ptr<Function>::value, func_t>::type
	wrap_callback(Function func) {
		typedef function_traits<Function> ft;
		return std::bind( & Invoker<CallParser, ResultSink, ft::arity>::template
					apply<Function>, func, std::placeholders::_1, std::placeholders::_2 );
	}

	template<typename Functor>
	static inline typename std::enable_if<std::is_class<Functor>::value, func_t>::type
	wrap_callback(Functor && functor) {
		typedef function_traits<Functor> ft;
		return std::bind( & Invoker<CallParser, ResultSink, ft::arity>::template
					apply<Functor>, functor, std::placeholders::_1, std::placeholders::_2 );
	}

	template<typename Functor>
	static inline typename std::enable_if<std::is_class<Functor>::value, func_t>::type
	wrap_callback(Functor * functor) {
		typedef function_traits<Functor> ft;
		return std::bind( & Invoker<CallParser, ResultSink, ft::arity>::template
					apply<decltype(functor)>, functor, std::placeholders::_1, std::placeholders::_2 );
	}

	template<typename Class, typename Method>
	static inline func_t
	wrap_callback(Class * klass, Method method) {
		typedef function_traits<Method> ft;
		return std::bind( & Invoker<CallParser, ResultSink, ft::arity-1>::template
				 apply<Method, Class>, method, klass,
						  std::placeholders::_1, std::placeholders::_2 );
	}
};

}} // namespace vw::rpc

#endif
