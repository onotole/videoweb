#ifndef __RPC_INVOKER_H__
#define __RPC_INVOKER_H__

#include <functional>
#include "function_traits.h"

namespace vw { namespace rpc {

template<typename CallParser, typename ResultSink, const std::size_t N>
struct Invoker {

	// function pointer
	template <typename Function, typename ... Args>
	static inline typename std::enable_if<is_function_ptr<Function>::value>::type
	apply(Function func, CallParser & parser, ResultSink & sink, Args ... args) {
		typedef function_traits<Function> ft;
		typedef typename ft::template arg<ft::arity - N>::type arg_t;
		typename std::decay<arg_t>::type arg;
		parser.get_arg(arg);
		Invoker<CallParser, ResultSink, N-1>::template apply<Function>
				( func, parser, sink, args..., arg );
	}

	// functor ptr
	template <typename FunctorPtr, typename ... Args>
	static inline typename std::enable_if<std::is_pointer<FunctorPtr>::value
		&& std::is_class<typename std::remove_pointer<FunctorPtr>::type>::value>::type
	apply(FunctorPtr func, CallParser & parser, ResultSink & sink, Args ... args) {
		typedef typename std::remove_pointer<FunctorPtr>::type Class;
		Invoker<CallParser, ResultSink, N>::template apply<decltype(&Class::operator()), Class>
				( &Class::operator(), func, parser, sink, args... );
	}

	// functor
	template <typename Functor, typename ... Args>
	static inline typename std::enable_if <std::is_class<Functor>::value>::type
	apply(Functor & func, CallParser & parser, ResultSink & sink, Args ... args) {
		Invoker<CallParser, ResultSink, N>::template apply<decltype(&Functor::operator()), Functor>
				( &Functor::operator(), &func, parser, sink, args...);
	}

	// pointer to member function
	template <typename Method, typename Class, typename ... Args>
	static inline typename std::enable_if
		<std::is_member_function_pointer<Method>::value>::type
	apply(Method method, Class * instance,
		  CallParser & parser, ResultSink & sink, Args ... args) {
		typedef function_traits<Method> ft;
		typedef typename ft::template arg<ft::arity - N>::type arg_t;
		typename std::decay<arg_t>::type arg;
		parser.get_arg(arg);
		Invoker<CallParser, ResultSink, N-1>::template apply<Method, Class>
				( method, instance, parser, sink, args..., arg );
	}

};

template<typename CallParser, typename ResultSink>
struct Invoker<CallParser, ResultSink, 0> {

	// TODO: void return case; function pointer
	template <typename Function, typename ... Args>
	static inline typename std::enable_if<is_function_ptr<Function>::value>::type
	apply(Function func, CallParser &, ResultSink & sink, Args ... args) {
		sink.set_result(func(args...));
    }

	// functor ptr
	template <typename FunctorPtr, typename ... Args>
	static inline typename std::enable_if<std::is_pointer<FunctorPtr>::value
		&& std::is_class<typename std::remove_pointer<FunctorPtr>::type>::value>::type
	apply(FunctorPtr func, CallParser & parser, ResultSink & sink, Args ... args) {
		typedef typename std::remove_pointer<FunctorPtr>::type Class;
		Invoker<CallParser, ResultSink, 0>::template apply<decltype(&Class::operator()), Class>
				( &Class::operator(), func, parser, sink, args... );
	}

	// functor
	template <typename Functor, typename ... Args>
	static inline typename std::enable_if <std::is_class<Functor>::value>::type
	apply(Functor & func, CallParser & parser, ResultSink & sink, Args ... args) {
		Invoker<CallParser, ResultSink, 0>::template apply<decltype(&Functor::operator()), Functor>
				( &Functor::operator(), &func, parser, sink, args...);
	}

	// pointer to member function
	template <typename Method, typename Class, typename ... Args>
	static inline typename std::enable_if
		<!std::is_void<typename function_traits<Method>::return_type>::value>::type
	/* TODO: typename std::enable_if
		<std::is_member_function_pointer<Method>::value>::type*/
	apply(Method method, Class * instance, CallParser &, ResultSink & sink, Args ... args) {
		sink.set_result((instance->*method)(args...));
	}

	template <typename Method, typename Class, typename ... Args>
	static inline typename std::enable_if
		<std::is_void<typename function_traits<Method>::return_type>::value>::type
	apply(Method method, Class * instance, CallParser &, ResultSink & sink, Args ... args) {
		(instance->*method)(args...);
		sink.set_result();
	}

};

}} // namespace ums::rpc

#endif
