#ifndef __VW_CLIENT_GALLERY_PAGE_VIEW_MODEL_H__
#define __VW_CLIENT_GALLERY_PAGE_VIEW_MODEL_H__

#include <QAbstractListModel>
#include <QStringList>
#include "interface/imedia_gallery.h"

namespace vw { namespace client {

class GalleryPageViewModel : public QAbstractListModel {
	Q_OBJECT	
	Q_PROPERTY(QString selected MEMBER _selected NOTIFY selectedChanged)

public:
	GalleryPageViewModel(IMediaGallery & gallery, const std::string & category);

	int rowCount(const QModelIndex & parent = QModelIndex()) const;
	QVariant data(const QModelIndex & index, int role) const;

signals:
	void selectedChanged(const QString &);

private:
	IMediaGallery & _gallery;
	QStringList _elements;
	QString _selected;
};


}}

#endif //__VW_CLIENT_GALLERY_PAGE_VIEW_MODEL_H__
