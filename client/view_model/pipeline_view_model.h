#ifndef __VW_CLIENT_PIPELINE_VIEW_MODEL_H__
#define __VW_CLIENT_PIPELINE_VIEW_MODEL_H__

#include <QObject>
#include "interface/ipipeline.h"

namespace vw { namespace client {

class PipelineViewModel : public QObject {
	Q_OBJECT

	Q_PROPERTY(double duration MEMBER _duration NOTIFY durationChanged)
	Q_PROPERTY(double position MEMBER _position NOTIFY positionChanged)
	Q_PROPERTY(QString uri READ uri WRITE setUri NOTIFY uriChanged)
public:
	explicit PipelineViewModel(IPipeline & pipeline);

	Q_INVOKABLE void play();
	Q_INVOKABLE void pause();
	Q_INVOKABLE void seek(double pos_ms);

	const QString & uri() const;
	void setUri(const QString &);

signals:
	void positionChanged(double);
	void durationChanged(double);
	void uriChanged(const QString &);

private:
	IPipeline & _pipeline;
	double _duration;
	double _position;
	QString _uri;
};


}}

#endif //__VW_CLIENT_PIPELINE_VIEW_MODEL_H__
