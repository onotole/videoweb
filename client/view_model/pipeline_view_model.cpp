#include "pipeline_view_model.h"

namespace vw { namespace client {

PipelineViewModel::PipelineViewModel(IPipeline & pipeline)
	: _pipeline(pipeline), _duration(-1.), _position(0.) {
	_pipeline.set_duration_handler([this](double d){
		emit durationChanged(_duration = d);
	});
	_pipeline.set_position_handler([this](double p){
		emit positionChanged(_position = p);
	});
}

const QString & PipelineViewModel::uri() const {
	return _uri;
}

void PipelineViewModel::setUri(const QString & uri) {
	_pipeline.unload();
	if (!uri.isNull()) {
		_pipeline.load(uri.toStdString());
	}
	emit uriChanged(_uri);
}

void PipelineViewModel::play() {
	_pipeline.play();
}

void PipelineViewModel::pause() {
	_pipeline.pause();
}

void PipelineViewModel::seek(double pos_ms) {
	_pipeline.seek(pos_ms);
}

}}
