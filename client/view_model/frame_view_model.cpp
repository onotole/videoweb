#include "frame_view_model.h"

namespace vw { namespace client {

FrameViewModel::FrameViewModel(IFrame & frame) : _frame(frame), _out_rect(0,0,800,600) {}

const QRect & FrameViewModel::getOutRect() const {
	return _out_rect;
}

void FrameViewModel::setOutRect(const QRect & r) {
	if (_out_rect != r) {
		_out_rect = r;
		_frame.set_out_rect({r.x(), r.y(), r.width(), r.height()});
		emit outRectChanged(_out_rect);
	}
}

const QRectF & FrameViewModel::getOutNRect() const {
	return _out_n_rect;
}

void FrameViewModel::setOutNRect(const QRectF & nr) {
	if (_out_n_rect != nr) {
		_out_n_rect = nr;
		_frame.set_out_rect_n({float(nr.x()), float(nr.y()), float(nr.width()), float(nr.height())});
		emit outNRectChanged(_out_n_rect);
	}
}

const QRect & FrameViewModel::getCropRect() const {
	return _crop_rect;
}

void FrameViewModel::setCropRect(const QRect & r) {
	if (_crop_rect != r) {
		_crop_rect = r;
		_frame.set_crop_rect({r.x(), r.y(), r.width(), r.height()});
		emit cropRectChanged(_crop_rect);
	}
}

const QRectF & FrameViewModel::getCropNRect() const {
	return _crop_n_rect;
}

void FrameViewModel::setCropNRect(const QRectF & nr) {
	if (_crop_n_rect != nr) {
		_crop_n_rect = nr;
		_frame.set_crop_rect_n({float(nr.x()), float(nr.y()), float(nr.width()), float(nr.height())});
		emit cropNRectChanged(_crop_n_rect);
	}
}

}}
