#ifndef __VW_CLIENT_FRAME_VIEW_MODEL_H__
#define __VW_CLIENT_FRAME_VIEW_MODEL_H__

#include <QObject>
#include <QRect>
#include <QRectF>
#include "interface/iframe.h"

namespace vw { namespace client {

class FrameViewModel : public QObject {
	Q_OBJECT

	Q_PROPERTY(QRect  outRect   READ getOutRect   WRITE setOutRect   NOTIFY outRectChanged)
	Q_PROPERTY(QRectF outNRect  READ getOutNRect  WRITE setOutNRect  NOTIFY outNRectChanged)
	Q_PROPERTY(QRect  cropRect  READ getCropRect  WRITE setCropRect  NOTIFY cropRectChanged)
	Q_PROPERTY(QRectF cropNRect READ getCropNRect WRITE setCropNRect NOTIFY cropNRectChanged)

public:
	explicit FrameViewModel(IFrame & frame);

	const QRect & getOutRect() const;
	void setOutRect(const QRect &);
	const QRectF & getOutNRect() const;
	void setOutNRect(const QRectF &);
	const QRect & getCropRect() const;
	void setCropRect(const QRect &);
	const QRectF & getCropNRect() const;
	void setCropNRect(const QRectF &);

signals:
	void outRectChanged(const QRect &);
	void outNRectChanged(const QRectF &);
	void cropRectChanged(const QRect &);
	void cropNRectChanged(const QRectF &);

private:
	IFrame & _frame;
	QRect  _out_rect;
	QRectF _out_n_rect;
	QRect  _crop_rect;
	QRectF _crop_n_rect;
};


}}

#endif //__VW_CLIENT_FRAME_VIEW_MODEL_H__
