#include "gallery_page_view_model.h"

namespace vw { namespace client {

GalleryPageViewModel::GalleryPageViewModel(IMediaGallery & gallery, const std::string & category)
	: _gallery(gallery) {
	const auto & elements = _gallery.get_elements(category);
	for (const auto & element : elements) {
		_elements.push_back(element.c_str());
	}
	_gallery.set_category_observer(category, [this](const std::list<std::string> & elements) {
		// TODO: implement collection update
	});
}

int GalleryPageViewModel::rowCount(const QModelIndex &) const {
	return _elements.size();
}

QVariant GalleryPageViewModel::data(const QModelIndex & index, int) const {
	return _elements.at(index.row());
}

}}
