import QtQuick 2.3

Rectangle {
    id: textBox

    property alias text: input.text

    width: 120
    height: 24
    color: "#4c4c4c"
    radius: 4
    border.color: "#b3b3b3"
    border.width: 2

    anchors.verticalCenter: parent.verticalCenter

    TextInput {
        id: input
        anchors.fill: parent
        anchors.margins: 5
        color: "#b3b3b3"
        font.bold: true
        clip: true
    }
}
