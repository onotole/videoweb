import QtQuick 2.3

FocusScope {
    id: button

    property alias text: label.text
    property alias icon: icon.source
    property alias enabled: mouseArea.enabled
    signal clicked

    width: 64
    height: 24

    anchors.verticalCenter: parent.verticalCenter

    Rectangle {
        id: background
        color: "#4c4c4c"
        anchors.fill: parent
        radius: 4
        border.color: button.activeFocus ? "#ff9600" : "#b3b3b3"
        border.width: 2
        Image {
            id: icon
            fillMode: Image.PreserveAspectFit
            anchors.margins: 6
            anchors.fill: parent
            antialiasing: true
            smooth: true
            opacity: .5
        }
        Text {
            id: label
            color: "#b3b3b3"
            font.bold: true
            anchors.centerIn: parent
        }
        states: [
            State {
                name: "disbled"; when: !mouseArea.enabled
            },
            State {
                name: "pressed"; when: mouseArea.pressed
                PropertyChanges { target: background; color: "#ff9600" }
                PropertyChanges { target: icon; opacity: 1.0 }
                PropertyChanges { target: label; color: "#ffffff" }
            },
            State {
                name: "hover"; when: mouseArea.containsMouse
                PropertyChanges { target: background; color: "#d99226" }
                PropertyChanges { target: icon; opacity: 1.0 }
                PropertyChanges { target: label; color: "#ffffff" }
            }
        ]
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: button.clicked()
        onPressed: button.focus = true
    }

    Keys.onSpacePressed:  button.clicked()
    Keys.onEnterPressed:  button.clicked()
    Keys.onReturnPressed: button.clicked()
}
