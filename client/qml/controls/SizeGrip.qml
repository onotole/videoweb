import QtQuick 2.3

Item {
    id: grip

    property alias drag: mouseArea.drag

    width: 12
    height: 12

    Rectangle {
        id: dot
        width: 6
        height: 6
        color: "#b3b3b3"
        anchors { horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter }
        states: [
            State {
                name: "disbled"; when: !mouseArea.enabled
            },
            State {
                name: "pressed"; when: mouseArea.pressed
                PropertyChanges { target: dot; color: "#ff9600" }
            },
            State {
                name: "hover"; when: mouseArea.containsMouse
                PropertyChanges { target: dot; color: "#d99226" }
            }
        ]
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        drag.target: parent
        drag.threshold: 0
    }
}
