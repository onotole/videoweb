import QtQuick 2.3

Item {
    property var model: null
    property string uri: ""

    height: row.height
    width: row.width

    Row {
        id: row
        spacing: 5

        ToggleButton {
            width: 24
            meta: [
                { name: "play", icon: "../img/play.svg" },
                { name: "pause", icon: "../img/pause.svg" }
            ]
            onToggled: {
                if (command === "play") model.play();
                else if (command === "pause") model.pause();
            }
        }

        TrackBar {
            duration:  model.duration
            position:  model.position
            onClicked: model.seek(position)
        }
    }

    onUriChanged: {
        if (model) {
            model.uri = uri
            model.play()
        }
    }
}
