import QtQuick 2.3

FocusScope {
    id: trackBar

    property alias enabled: mouseArea.enabled
    property double position: 0.
    property double duration: 100.

    signal clicked(double position)

    width: 256
    height: 12

    anchors.verticalCenter: parent.verticalCenter

    Rectangle {
        id: progress
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        color: "#b3b3b3"
        radius: 4
        width: parent.width * (parent.position / parent.duration)
        states: [
            State {
                name: "disbled"; when: !mouseArea.enabled
            },
            State {
                name: "pressed"; when: mouseArea.pressed
                PropertyChanges { target: progress; color: "#ff9600" }
            },
            State {
                name: "hover"; when: mouseArea.containsMouse
                PropertyChanges { target: progress; color: "#d99226" }
            }
        ]

    }

    Rectangle {
        id: background
        color: "transparent"
        anchors.fill: parent
        radius: 4
        border.color: parent.activeFocus ? "#ff9600" : "#b3b3b3"
        border.width: 2
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onPressed: {
            parent.focus = true
            parent.clicked(parent.duration * mouseX / parent.width)
        }
    }
}
