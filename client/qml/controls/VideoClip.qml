import QtQuick 2.3
import QtMultimedia 5.0
import QtGraphicalEffects 1.0

Item {
    id: videoClip

    property alias source: player.source
    property bool selected: false

    width: 192
    height: 108

    MediaPlayer {
        id: player
        loops: MediaPlayer.Infinite
        volume: 0
    }

    VideoOutput {
        id: output
        anchors.fill: parent
        source: player
        fillMode: VideoOutput.PreserveAspectCrop
        visible: false
    }

    Timer {
        id: timer
        interval: 50
        running: true
        onTriggered: {
            player.seek(player.duration/2)
            player.pause()
        }
    }

    OpacityMask {
        id: opacityMask
        anchors.fill: mask
        source: output
        maskSource: mask
    }

    Desaturate {
        id: desaturation
        anchors.fill: opacityMask
        source: opacityMask
        desaturation: 0.7
    }

    Rectangle {
        id: frame
        anchors.fill: parent
        radius: 8
        color: "#00000000"
        border.color: videoClip.selected ? "#ff9600" : "#b3b3b3"
        border.width: 3
        clip: true
    }

    Rectangle {
        id: mask
        anchors.fill: parent
        radius: 8
        visible: false
        clip: true
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }

    states: [
        State {
            name: "selected"; when: videoClip.selected
            PropertyChanges { target: desaturation; desaturation: 0. }
            StateChangeScript { script: { player.seek(0); player.play() } }
        },
        State {
            name: "pressed"; when: mouseArea.pressed
            PropertyChanges { target: desaturation; desaturation: 0. }
        },
        State {
            name: "hover"; when: !videoClip.selected && mouseArea.containsMouse
            StateChangeScript { script: player.play() }
        },
        State {
            name: "idle"; when: !videoClip.selected && !mouseArea.containsMouse
            StateChangeScript { script: { player.pause(); player.seek(player.duration / 2)  } }
        }
    ]

}
