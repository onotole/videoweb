import QtQuick 2.3

Button {
    id: toggleButton

    property alias enabled: toggleButton.enabled
    property var meta: [
        { name: "one", text: "", icon: "" },
        { name: "two", text: "", icon: "" }
    ]
    signal toggled(string command)

    state: meta[0].name
    states: [
        State {
            name: meta[0].name
            PropertyChanges { target: toggleButton; text: meta[0].text }
            PropertyChanges { target: toggleButton; icon: meta[0].icon }
        },
        State {
            name: meta[1].name
            PropertyChanges { target: toggleButton; text: meta[1].text }
            PropertyChanges { target: toggleButton; icon: meta[1].icon }
        }
    ]

    onClicked: {
        toggled(state)
        state = (state === meta[0].name ? meta[1].name : meta[0].name)
    }
}
