import QtQuick 2.3

Item {
    id: rc
    property real scale: 0.2
    property rect rect: Qt.rect(0, 0, 720, 576)

    Rectangle {
        id: frame
        color: "transparent"
        border.color: "white"

        MouseArea {
            id: frameArea
            anchors.fill: parent
            drag.threshold: 0
            drag.target: tl
        }
    }

    SizeGrip { id: top; y: tl.y; x: tl.x + 6; width: frame.width; drag.axis: Drag.YAxis }
    SizeGrip { id: left; x: tl.x; y: tl.y + 6; height: frame.height; drag.axis: Drag.XAxis }
    SizeGrip { id: bottom; x: tl.x + 6; y: br.y; width: frame.width; drag.axis: Drag.YAxis }
    SizeGrip { id: right; x: br.x; y: tl.y + 6; height: frame.height; drag.axis: Drag.XAxis }
    SizeGrip { id: tl }
    SizeGrip { id: br }
    SizeGrip { id: tr; x: br.x; y: tl.y }
    SizeGrip { id: bl; x: tl.x; y: br.y }

    Component.onCompleted: {
        tl.y = -6
        tl.x = -6
        br.y = rc.scale * rc.rect.height - 6
        br.x = rc.scale * rc.rect.width - 6
        rc.rect = Qt.binding(function() {
            return Qt.rect(frame.x / rc.scale, frame.y / rc.scale,
                           frame.width / rc.scale, frame.height / rc.scale);
        })
    }

    Binding { target: tl; property: "x"; value: left.x }
    Binding { target: tl; property: "y"; value: top.y }
    Binding { target: br; property: "x"; value: right.x }
    Binding { target: br; property: "y"; value: bottom.y }

    Binding { target: tl; property: "x"; value: bl.x }
    Binding { target: tl; property: "y"; value: tr.y }
    Binding { target: br; property: "x"; value: tr.x }
    Binding { target: br; property: "y"; value: bl.y }

    Binding { target: frame; property: "x"; value: tl.x + 6 }
    Binding { target: frame; property: "y"; value: tl.y + 6 }
    Binding { target: frame; property: "width"; value: br.x - tl.x; when: !frameArea.drag.active }
    Binding { target: frame; property: "height"; value: br.y - tl.y; when: !frameArea.drag.active }

    Binding { target: br; property: "x"; value: tl.x + frame.width; when: frameArea.drag.active }
    Binding { target: br; property: "y"; value: tl.y + frame.height; when: frameArea.drag.active }
}
