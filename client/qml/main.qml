import QtQuick 2.3
import QtQuick.Controls 1.3
import "qrc:/controls"
import "qrc:/view"

ApplicationWindow {
    visible: true
    width: 800
    height: 600
    color: "#333333"

    menuBar: MenuBar {
        Menu {
            title: "File"

            MenuItem {
                text: "New..."
                shortcut: "Ctrl+N"
            }
            MenuItem {
                text: "Open..."
                shortcut: "Ctrl+O"
            }
            MenuItem {
                text: "Save"
                shortcut: "Ctrl+S"
            }
        }
    }

    Column {
        anchors.centerIn: parent
        spacing: 5

        GalleryPage {
            id: gal
            width: 600
            height: 300
            model: gallery
        }

        PlayerControl {
            model: player
            uri: gal.model.selected
        }

        Text {
            text: gal.model.selected
            color: "orange"
            font.pixelSize: 20
        }

        Rectangle {
            id: display
            width: 320
            height: 180
            border.color: "yellow"
            color: "transparent"
            Repeater {
                model: frames
                delegate: Item {
                    RectLayout {
                        id: rc
                        rect: model.modelData.outRect
                    }
                    Binding { target: model.modelData; property: "outRect"; value: rc.rect }
                }
            }
        }

//        Repeater {
//            model: pipelines
//            delegate: Row {
//                spacing: 5

//                TextBox {
//                    id: uri
//                }

//                ToggleButton {
//                    meta: [
//                        { name: "load", text: "Load" },
//                        { name: "unload", text: "Unload" }
//                    ]
//                    onToggled: {
//                        if (command === "load") model.modelData.load(uri.text);
//                        else if (command === "unload") model.modelData.unload();
//                    }
//                }

//                ToggleButton {
//                    width: 24
//                    meta: [
//                        { name: "play", icon: "../img/play.svg" },
//                        { name: "pause", icon: "../img/pause.svg" }
//                    ]
//                    onToggled: {
//                        if (command === "play") model.modelData.play();
//                        else if (command === "pause") model.modelData.pause();
//                    }
//                }

//                TrackBar {
//                    duration:  model.modelData.duration
//                    position:  model.modelData.position
//                    onClicked: model.modelData.seek(position)
//                }
//            }
//        }

    }
}

