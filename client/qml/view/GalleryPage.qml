import QtQuick 2.3
import "qrc:/controls"

GridView {
    id: grid

    cellWidth: 200
    cellHeight: 116

    currentIndex: -1

    delegate: VideoClip {
        source: display
        selected: GridView.isCurrentItem
    }

    MouseArea {
        anchors.fill: parent
        onPressed: parent.focus = true
        onClicked: {
            var index = grid.indexAt(mouse.x, mouse.y)
            grid.currentIndex = grid.currentIndex === index ? -1 : index
        }
    }

    onCurrentItemChanged: {
        model.selected = currentItem ? currentItem.source : ""
    }
    Keys.onEscapePressed: currentIndex = -1
}
