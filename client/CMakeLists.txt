cmake_minimum_required(VERSION 2.8)
set(VW_FRAMEWORK_ROOT ..)
include(${VW_FRAMEWORK_ROOT}/cmake.config)

project(vw.client)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_AUTOMOC ON)

find_package(Qt5 COMPONENTS Quick Core)
qt5_add_resources(RESOURCE_SCRIPT res/resources.qrc)

find_package(Boost COMPONENTS system thread log program_options)
include_directories(${Boost_INCLUDE_DIRS})
link_directories(${Boost_LIBRARY_DIRS})

find_package(PkgConfig REQUIRED)
pkg_check_modules(JSONCPP jsoncpp)
include_directories(${JSONCPP_INCLUDE_DIRS})

include_directories(.)
include_directories(vw)

file(GLOB_RECURSE HEADERS "*.h")
file(GLOB_RECURSE RESOURCES "*.qml" "*.svg")
add_custom_target(show_extras SOURCES ${HEADERS} ${RESOURCES})

file(GLOB VM_SOURCES "view_model/*.cpp")
file(GLOB M_SOURCES "model/*.cpp")
set(SOURCES ${VM_SOURCES} ${M_SOURCES} main.cpp)

find_package(Threads)

add_executable(client ${SOURCES} ${RESOURCE_SCRIPT})
qt5_use_modules(client Quick Core)
target_link_libraries(client ${JSONCPP_LIBRARIES} ${Boost_LIBRARIES}
    ${CMAKE_THREAD_LIBS_INIT} vw.io.stream vw.rpc.json-rpc vw.log)

install(TARGETS client RUNTIME DESTINATION ${VW_IMAGE_BIN})

