#include "compositor.h"

namespace vw { namespace client {

Compositor::Compositor(const std::string & ip) {
	message_thread = std::thread([this](){
		boost::asio::io_service::work _(io_service);
		io_service.run();
	});
	client.reset(new client_t(io_service, PROTO_NAME + ip + COMPOSITOR_PORT));
	call_manager.reset(new call_manager_t(*client));
}

Compositor::~Compositor() {
	io_service.stop();
	message_thread.join();
}

void Compositor::add_frame(const std::string & id)
{
	frames.emplace(id, Frame{id, call_manager});
}

void Compositor::rem_frame(const std::string & id)
{
	frames.erase(id);
}

Frame & Compositor::frame(const std::string & id) {
	auto fit = frames.find(id);
	if (fit == frames.end())
		throw std::runtime_error("frame does not exist");
	return fit->second;
}

}}
