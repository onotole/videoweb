#include <unistd.h>
#include "pipeline.h"

namespace vw { namespace client {

Pipeline::Pipeline(const std::string & ip) {
	message_thread = std::thread([this](){
		boost::asio::io_service::work _(io_service);
		io_service.run();
	});
	client.reset(new client_t(io_service, PROTO_NAME + ip + MEDIASERVER_PORT));
	call_manager.reset(new call_manager_t(*client));
	call_manager->register_event_handler("duration", [this](const double & duration) {
		if (duration_handler) duration_handler(duration);
	});
	call_manager->register_event_handler("position", [this](const double & position) {
		if (position_handler) position_handler(position);
	});
	call_manager->register_event_handler("id", [this](const std::string & id) {
		_id = id;
		if (id_hahdler) id_hahdler(_id);
	});
}

Pipeline::~Pipeline() {
	call_manager->call_async("exit", [](bool){});
	io_service.stop();
	message_thread.join();
}

const std::string & Pipeline::id() const {
	return _id;
}

void Pipeline::load(const std::string & uri) {
	call_manager->call<bool>("load", uri);
}

void Pipeline::unload() {
	call_manager->call<bool>("unload");
}

void Pipeline::play() {
	call_manager->call<bool>("play");
}

void Pipeline::pause() {
	call_manager->call<bool>("pause");
}

void Pipeline::seek(double pos_ms) {
	call_manager->call<bool>("seek", pos_ms);
}

void Pipeline::set_duration_handler(std::function<void (double)> && handler) {
	duration_handler = handler;
}

void Pipeline::set_position_handler(std::function<void (double)> && handler) {
	position_handler = handler;
}

void Pipeline::set_id_handler(std::function<void (const std::string &)> && handler) {
	id_hahdler = handler;
}

}}


