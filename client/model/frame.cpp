#include "frame.h"

namespace vw { namespace client {

Frame::Frame(const std::string & _id, std::shared_ptr<call_manager_t> _call_manager)
	: id(_id), call_manager(_call_manager) {}

void Frame::set_z_order(size_t z)
{
	call_manager->call<bool>("set_z_order", id, z);
}

void Frame::set_crop_rect(const vw::base::rect_t & r)
{
	call_manager->call<bool>("set_crop_rect", id, r);
}

void Frame::set_crop_rect_n(const vw::base::n_rect_t & nr)
{
	call_manager->call<bool>("set_crop_rect_n", id, nr);
}

void Frame::set_out_rect(const vw::base::rect_t & r)
{
	call_manager->call<bool>("set_out_rect", id, r);
}

void Frame::set_out_rect_n(const vw::base::n_rect_t & nr)
{
	call_manager->call<bool>("set_out_rect_n", id, nr);
}

}}
