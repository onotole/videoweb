#ifndef __VW_CLIENT_FRAME_H__
#define __VW_CLIENT_FRAME_H__

#include <string>
#include <memory>
#include <thread>

#include "common.h"
#include "../interface/iframe.h"

namespace vw { namespace client {

class Frame : public IFrame {
public:
    Frame(const std::string & _id, std::shared_ptr<call_manager_t> _call_manager);

    virtual void set_z_order(size_t z);
    virtual void set_crop_rect(const base::rect_t & r);
    virtual void set_crop_rect_n(const base::n_rect_t & nr);
    virtual void set_out_rect(const base::rect_t & r);
    virtual void set_out_rect_n(const base::n_rect_t & nr);

private:
    std::string id;
    std::shared_ptr<call_manager_t> call_manager;
};

}}


#endif //__VW_CLIENT_FRAME_H__
