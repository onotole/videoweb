#ifndef __VW_CLIENT_PIPELINE_H__
#define __VW_CLIENT_PIPELINE_H__

#include <memory>
#include <thread>

#include "common.h"
#include "../interface/ipipeline.h"

namespace vw { namespace client {

class Pipeline : public IPipeline {
public:
	Pipeline(const std::string & ip);
	virtual ~Pipeline();

	const std::string & id() const;

	virtual void load(const std::string & uri);
	virtual void unload();
	virtual void play();
	virtual void pause();
	virtual void seek(double pos_ms);

	virtual void set_duration_handler(std::function<void(double)> &&);
	virtual void set_position_handler(std::function<void(double)> &&);
	virtual void set_id_handler(std::function<void (const std::string &)> &&);

private:
	std::string _id;
	io_service_t io_service;
	std::thread message_thread;
	std::unique_ptr<client_t> client;
	std::unique_ptr<call_manager_t> call_manager;
	std::function<void (double)> position_handler;
	std::function<void (double)> duration_handler;
	std::function<void(const std::string &)> id_hahdler;
};

}}


#endif //__VW_CLIENT_PIPELINE_H__
