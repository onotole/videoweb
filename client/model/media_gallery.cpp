#include "media_gallery.h"

namespace vw { namespace client {

void MediaGallery::add_category(const std::string & category) {
	rem_category(category);
	categories.emplace(category, Category{});
}

void MediaGallery::rem_category(const std::string & category) {
	auto cit = categories.find(category);
	if (cit != categories.end()) {
		if (cit->second.observer) {
			cit->second.elements.clear();
			notify(cit);
		}
		categories.erase(cit);
	}
}

std::list<std::string> MediaGallery::get_categories() const {
	std::list<std::string> category_list;
	for (const auto & c : categories) {
		category_list.push_back(c.first);
	}
	return category_list;
}

void MediaGallery::add_element(const std::string & category, const std::string & element) {
	auto cit = categories.find(category);
	if (cit != categories.end()) {
		cit->second.elements.push_back(element);
		notify(cit);
	}
}

void MediaGallery::rem_element(const std::string & category, const std::string & element) {
	auto cit = categories.find(category);
	if (cit != categories.end()) {
		cit->second.elements.remove(element);
		notify(cit);
	}
}

const std::list<std::string> & MediaGallery::get_elements(const std::string & category) const {
	auto cit = categories.find(category);
	if (cit != categories.end()) {
		return cit->second.elements;
	}
	throw std::runtime_error("no such category");
}

void MediaGallery::set_category_observer(const std::string & category,
										 std::function<void (const std::list<std::string> &)> && func) {
	auto cit = categories.find(category);
	if (cit != categories.end()) {
		cit->second.observer = func;
		notify(cit);
	}
}

void MediaGallery::notify(std::map<std::string, Category>::iterator cit) {
	if (cit->second.observer) {
		cit->second.observer(cit->second.elements);
	}
}

}}
