#ifndef __VW_CLIENT_COMMON_H__
#define __VW_CLIENT_COMMON_H__

#include <io/client.h>
#include <rpc/call_manager.h>
#include <rpc/protocol.h>

#include <io/stream/client.h>
#include <rpc/json-rpc/protocol.h>

namespace vw { namespace client {

#define PROTO_NAME "tcp://"
#define COMPOSITOR_PORT ":7211"
#define MEDIASERVER_PORT ":7210"

typedef rpc::Proto<rpc::json::Proto> proto_t;
typedef boost::asio::io_service io_service_t;
typedef io::Client<io_service_t, io::stream::TcpClient> client_t;
typedef rpc::CallManager<proto_t, client_t> call_manager_t;

}}

#endif
