#ifndef __VW_CLIENT_COMPOSITOR_H__
#define __VW_CLIENT_COMPOSITOR_H__

#include <thread>
#include <map>

#include "frame.h"

namespace vw { namespace client {

class Compositor {
public:
	Compositor(const std::string & ip);
	~Compositor();

	void add_frame(const std::string & id);
	void rem_frame(const std::string & id);
	Frame & frame(const std::string & id);

private:
	io_service_t io_service;
	std::thread message_thread;
	std::unique_ptr<client_t> client;
	std::shared_ptr<call_manager_t> call_manager;
	std::map<std::string, Frame> frames;
};

}}


#endif //__VW_CLIENT_COMPOSITOR_H__
