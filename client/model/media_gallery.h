#ifndef __VW_CLIENT_MEDIA_GALLERY_H__
#define __VW_CLIENT_MEDIA_GALLERY_H__

#include <map>
#include "../interface/imedia_gallery.h"

namespace vw { namespace client {

class MediaGallery : public IMediaGallery
{
	struct Category {
		typedef std::function<void(const std::list<std::string> &)> observer_t;
		std::list<std::string> elements;
		observer_t observer;
	};

public:
	virtual void add_category(const std::string & category);
	virtual void rem_category(const std::string & category);
	virtual std::list<std::string> get_categories() const;
	virtual void add_element(const std::string & category, const std::string & element);
	virtual void rem_element(const std::string & category, const std::string & element);
	virtual const std::list<std::string> & get_elements(const std::string & category) const;
	virtual void set_category_observer(const std::string & category,
									   std::function<void(const std::list<std::string> &)> &&);

private:
	void notify(std::map<std::string, Category>::iterator cit);
	std::map<std::string, Category> categories;
};

}}


#endif // __VW_CLIENT_MEDIA_GALLERY_H__
