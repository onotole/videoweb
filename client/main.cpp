#include <boost/program_options.hpp>

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "view_model/pipeline_view_model.h"
#include "view_model/frame_view_model.h"
#include "view_model/gallery_page_view_model.h"
#include "model/pipeline.h"
#include "model/compositor.h"
#include "model/media_gallery.h"

int main(int argc, char *argv[])
{
	using namespace vw::client;
	namespace po = boost::program_options;

	std::string node_ip;
	po::options_description opt("Usage");
	opt.add_options()
			("help,h", "print this help message")
			("node-ip,n", po::value<std::string>(&node_ip)->
				default_value("127.0.0.1"), "set remote node ip address");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, opt), vm);
	if (vm.count("help")) {
		std::cout << opt << std::endl;
		return 0;
	}
	try {
		po::notify(vm);
	} catch( const std::exception & e) {
		std::cerr << e.what() << std::endl;
		std::cout << opt << std::endl;
		return -1;
	}

	QGuiApplication app(argc, argv);
	QQmlApplicationEngine engine;

	QList<QObject *> frames_list;

	Compositor compositor(node_ip);
	Pipeline pipelines[2] {{node_ip}, {node_ip}};
	pipelines[0].set_id_handler([&](const std::string & id) {
		QObject tmp;
		QObject::connect(&tmp, &QObject::destroyed, &engine, [&](){
			compositor.add_frame(id);
			frames_list.push_back(new FrameViewModel(compositor.frame(id)));
			engine.rootContext()->setContextProperty("frames", QVariant::fromValue(frames_list));
		}, Qt::QueuedConnection);
	});
	pipelines[1].set_id_handler([&](const std::string & id) {
		QObject tmp;
		QObject::connect(&tmp, &QObject::destroyed, &engine, [&](){
			compositor.add_frame(id);
			frames_list.push_back(new FrameViewModel(compositor.frame(id)));
			engine.rootContext()->setContextProperty("frames", QVariant::fromValue(frames_list));
		}, Qt::QueuedConnection);
	});

//	QList<QObject *> pipeline_list = { new PipelineViewModel(pipelines[0]) ,
//									   new PipelineViewModel(pipelines[1]) };


	PipelineViewModel pvm(pipelines[0]);

	MediaGallery gallery;
	gallery.add_category("media");
	gallery.add_element("media", "test://");
	gallery.add_element("media", "file:///tmp/fear.mp4");
	gallery.add_element("media", "file:///tmp/disgust.mp4");
	gallery.add_element("media", "file:///tmp/disgust.mp4");
	gallery.add_element("media", "file:///tmp/fun.mp4");
	GalleryPageViewModel gpvm(gallery, "media");

	engine.rootContext()->setContextProperty("gallery", &gpvm);
	engine.rootContext()->setContextProperty("player", &pvm);
	// engine.rootContext()->setContextProperty("pipelines", QVariant::fromValue(pipeline_list));
	engine.load(QUrl(QStringLiteral("qrc:main.qml")));

	return app.exec();
}
