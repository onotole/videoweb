#ifndef __VW_CLIENT_IMEDIA_GALLERY_H__
#define __VW_CLIENT_IMEDIA_GALLERY_H__

#include <string>
#include <list>
#include <functional>

namespace vw { namespace client {

struct IMediaGallery {
	virtual void add_category(const std::string & category) = 0;
	virtual void rem_category(const std::string & category) = 0;
	virtual std::list<std::string> get_categories() const = 0;

	virtual void add_element(const std::string & category, const std::string & element) = 0;
	virtual void rem_element(const std::string & category, const std::string & element) = 0;
	virtual const std::list<std::string> & get_elements(const std::string & category) const = 0;

	virtual void set_category_observer(const std::string & category,
									   std::function<void(const std::list<std::string>&)> &&) = 0;
};

}}

#endif // __VW_CLIENT_IMEDIA_GALLERY_H__
