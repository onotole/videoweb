#ifndef __VW_CLIENT_IFRAME_H__
#define __VW_CLIENT_IFRAME_H__

#include <string>
#include <base/types.h>

namespace vw { namespace client {

struct IFrame {
    virtual void set_z_order(size_t z)                      = 0;
    virtual void set_crop_rect(const base::rect_t & r)      = 0;
    virtual void set_crop_rect_n(const base::n_rect_t & nr) = 0;
    virtual void set_out_rect(const base::rect_t & r)       = 0;
    virtual void set_out_rect_n(const base::n_rect_t & nr)  = 0;
};

}}

#endif //__VW_CLIENT_IFRAME_H__
