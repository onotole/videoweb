#ifndef __VW_CLIENT_IPIPELINE_H__
#define __VW_CLIENT_IPIPELINE_H__

#include <string>
#include <functional>

namespace vw { namespace client {

struct IPipeline {
	virtual void load(const std::string & uri) = 0;
	virtual void unload()                      = 0;
	virtual void play()                        = 0;
	virtual void pause()                       = 0;
	virtual void seek(double pos_ms)           = 0;

	virtual void set_duration_handler(std::function<void(double)> &&)        = 0;
	virtual void set_position_handler(std::function<void(double)> &&)        = 0;
	virtual void set_id_handler(std::function<void(const std::string &)> &&) = 0;
};

}}

#endif //__VW_CLIENT_IPIPELINE_H__
