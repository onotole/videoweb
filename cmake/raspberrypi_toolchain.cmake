# expects $ENV{OE_TARGET_SYSROOT} and $ENV{OE_NATIVE_SYSROOT}

message(STATUS "target sysroot = $ENV{OE_TARGET_SYSROOT}")
message(STATUS "native sysroot = $ENV{OE_NATIVE_SYSROOT}")

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(TC_PREFIX arm-poky-linux-gnueabi)

set(ENV{PATH} $ENV{OE_NATIVE_SYSROOT}/usr/bin:$ENV{OE_NATIVE_SYSROOT}/usr/bin/${TC_PREFIX}:$ENV{PATH})

set(TC_FLAGS "-march=armv6 -mfloat-abi=hard -mtune=arm1176jzf-s -mfpu=vfp -O2 -g -DNDEBUG")
set(TC_SYSROOT "--sysroot=$ENV{OE_TARGET_SYSROOT}")

set(ENV{CC}      "${TC_PREFIX}-gcc ${TC_FLAGS} ${TC_SYSROOT}")
set(ENV{CXX}     "${TC_PREFIX}-g++ ${TC_FLAGS} ${TC_SYSROOT}")
set(ENV{CPP}     "${TC_PREFIX}-gcc -E ${TC_FLAGS} ${TC_SYSROOT}")
set(ENV{AS}      "${TC_PREFIX}-as")
set(ENV{LD}      "${TC_PREFIX}-ld ${TC_SYSROOT}")
set(ENV{GDB}     "${TC_PREFIX}-gdb")
set(ENV{STRIP}   "${TC_PREFIX}-strip")
set(ENV{RANLIB}  "${TC_PREFIX}-ranlib")
set(ENV{OBJCOPY} "${TC_PREFIX}-objcopy")
set(ENV{OBJDUMP} "${TC_PREFIX}-objdump")
set(ENV{AR}      "${TC_PREFIX}-ar")
set(ENV{NM}      "${TC_PREFIX}-nm")

set(CMAKE_FIND_ROOT_PATH $ENV{OE_TARGET_SYSROOT})
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

set(ENV{PKG_CONFIG_DIR} "")
set(ENV{PKG_CONFIG_SYSROOT_DIR} $ENV{OE_TARGET_SYSROOT})
set(ENV{PKG_CONFIG_LIBDIR} "$ENV{OE_TARGET_SYSROOT}/usr/lib/pkgconfig:$ENV{OE_TARGET_SYSROOT}/usr/share/pkgconfig")
set(MACHINE "raspberrypi")
