#include <sys/wait.h>
#include <fstream>
#include <iostream>

#include <boost/program_options.hpp>

#include <log/log.h>
#include <io/stream/server.h>

#include "server.h"

typedef vw::ms::MediaServer<vw::io::stream::LocalServer> ms_local_t;
typedef vw::ms::MediaServer<vw::io::stream::TcpServer> ms_tcp_t;

namespace po = boost::program_options;

int load_config(int argc, char *argv[], po::variables_map & vm) {
	po::options_description generic("Generic Options");
	generic.add_options()
			("help,h", "print out help message");

    po::options_description config("Configuration");
    config.add_options()
            ("local,l", po::value<std::string>()->default_value("local:///tmp/vw.mediaserver"),
             "local socket address")
            ("tcp,t", po::value<std::string>()->default_value("tcp://:7210"),
             "tcp socket address")
            ("connection-timeout,o", po::value<size_t>()->default_value(3000),
             "set pipeline connection timeout in milliseconds");

	po::options_description config_file_options;
	config_file_options.add(config);

	po::options_description cmd_line_options;
	cmd_line_options.add(generic).add(config);

	if (argc && argv) {
		try {
			po::store(po::parse_command_line(argc, argv, cmd_line_options), vm);
		} catch( const std::exception & e) {
			std::cerr << e.what() << std::endl;
			std::cout << cmd_line_options << std::endl;
			return -1;
		}

		if (vm.count("help")) {
			std::cout << cmd_line_options << std::endl;
			exit(0);
		}
	}

	std::ifstream cfg_file(MEDIASERVER_CONFIG_FILE);
	if (cfg_file.good()) {
		try {
			po::store(po::parse_config_file(cfg_file, config_file_options), vm);
		} catch( const std::exception & e) {
			std::cerr << e.what() << std::endl;
			std::cout << config_file_options << std::endl;
			return -1;
		}
	}

	try {
		po::notify(vm);
	} catch( const std::exception & e) {
		std::cerr << e.what() << std::endl;
		std::cout << cmd_line_options << std::endl;
		return -1;
	}

	return 0;
}

int main(int argc, char *argv[]) {
	po::variables_map vm;
	int error = load_config(argc, argv, vm);
	if (error)
		return error;

	// TODO: set externally, via config?
	setenv("GST_PLUGIN_PATH", GST_PLUGIN_PATH, 1);

	vw::ms::io_service_t io_service;
	auto exit_handler = [&io_service](const boost::system::error_code & ec, int sig){
		INFO << "exiting by signal: " << sig << ", error: " << ec;
		io_service.stop();
	};
	boost::asio::signal_set exit_signals(io_service, SIGINT, SIGTERM);
	exit_signals.async_wait(exit_handler);
	struct ChildHandler {
		ChildHandler(vw::ms::io_service_t & io_service) : child_signals(io_service, SIGCHLD) {
			namespace ph = std::placeholders;
			child_signals.async_wait
					(std::bind(&ChildHandler::wait, this, ph::_1, ph::_2));
		}
		void wait(const boost::system::error_code &, int) {
			namespace ph = std::placeholders;
			int status = 0;
			pid_t pid = 0;
			do {
				pid = waitpid(-1, &status, WNOHANG);
				if (pid > 0)
					INFO << "child process " << pid << " exited: " << status;
			} while(pid > 0);
			child_signals.async_wait(std::bind(&ChildHandler::wait, this, ph::_1, ph::_2));
		}
		boost::asio::signal_set child_signals;
	}_(io_service);

    std::unique_ptr<ms_local_t> local_server(new ms_local_t(io_service, vm["local"].as<std::string>(),
                                             vm["connection-timeout"].as<size_t>()));
    std::unique_ptr<ms_tcp_t> tcp_server(new ms_tcp_t(io_service, vm["tcp"].as<std::string>(),
                                         vm["connection-timeout"].as<size_t>()));

	io_service.run();

	return 0;
}
