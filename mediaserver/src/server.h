#ifndef __VW_SERVER_H__
#define __VW_SERVER_H__

#include <sstream>
#include <map>
#include <memory>

#include <boost/asio.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <rpc/protocol.h>
#include <rpc/json-rpc/protocol.h>
#include <io/server.h>

#include "pipeline_controller.h"

typedef vw::rpc::Proto<vw::rpc::json::Proto> proto_t;

namespace vw { namespace ms {

template<typename Transport>
class MediaServer {
public:
	typedef typename io::Server<io_service_t, Transport> server_t;
	typedef PipelineController<typename server_t::socket_t> pipeline_t;
	MediaServer(io_service_t & io_service, const std::string & address, size_t timeout = 3000U)
		: _server(io_service, address) {
		_server.set_connection_handler([&io_service, this, timeout](typename server_t::socket_t && s){
			boost::uuids::uuid uuid = boost::uuids::random_generator()();
			std::string id = boost::uuids::to_string(uuid);
			INFO << "new client " << id << " connected";

			auto it = _controllers.emplace(id, std::unique_ptr<pipeline_t>
						(new pipeline_t(id, io_service, std::move(s), timeout)));
			auto & socket = it.first->second->socket();
			socket.set_disconnect_handler([id, this]() {
				INFO << "client " << id << " disconnected";
				_controllers.erase(id);
			});
			std::stringstream os;
			proto_t::event::serializer_t serializer(os, "id");
			serializer.put_arg(id); serializer.commit();
			socket.send(os);
		});
	}

private:
	server_t _server;
	std::map<std::string, std::unique_ptr<pipeline_t>> _controllers;
};

}} // namespace vw::ms


#endif // __VW_SERVER_H__
