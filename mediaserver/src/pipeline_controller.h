#ifndef __PIPELINE_CONTROLLER_H__
#define __PIPELINE_CONTROLLER_H__

#include <map>
#include <list>
#include <memory>
#include <algorithm>

#include <boost/filesystem.hpp>

#include <io/socket.h>
#include <io/client.h>
#include <io/stream/socket.h>
#include <io/stream/client.h>

#include <log/log.h>

#include "config.h"

namespace vw { namespace ms {

typedef boost::asio::io_service io_service_t;
typedef io::Socket<io_service_t, io::stream::LocalSocket> socket_t;
typedef io::Client<io_service_t, io::stream::LocalClient> client_t;

template<typename Socket>
class PipelineController {
public:
	PipelineController(const std::string & id, io_service_t & ios, Socket && s, size_t timeout = 3000U)
		:	_id(id), _child(exec()), _socket(std::move(s))
		, _client(ios, "local:///tmp/vw.pipeline." + id, timeout,
				  [this](const std::error_code & ec) { connected(ec); }) {
		_socket.set_recv_handler([this](std::istream & is) {
			is >> std::noskipws;
			std::stringstream ss;
			std::copy(std::istream_iterator<char>(is), std::istream_iterator<char>(),
					  std::ostream_iterator<char>(ss));
			ss << std::endl;
			send(ss);
		});
		_client.set_recv_handler([this](std::istream & is) {
			is >> std::noskipws;
			std::stringstream ss;
			std::copy(std::istream_iterator<char>(is), std::istream_iterator<char>(),
					  std::ostream_iterator<char>(ss));
			ss << std::endl;
			_socket.send(ss);
		});
	}
    ~PipelineController() {
        INFO << "sending sigkill to: " << _child << " (" << _id << ")";
        // FIXME: we should send sigterm but it somtimes "bounces back" to parent process
        boost::filesystem::remove("/tmp/vw.pipeline." + _id);
        kill(_child, SIGKILL);
    }

	Socket & socket() {
		return _socket;
	}

private:
	pid_t exec() {
		pid_t pid = fork();
		if (pid == 0) {
			std::string id_arg = "--id=" + _id;
            int ec = execl(PIPELINE_PROC, PIPELINE_PROC, id_arg.c_str(), nullptr);
			if (ec)
				ERROR << "Failed to exec pipeline: " << PIPELINE_PROC << ", error = " << errno;
			exit(0);
		}
		INFO << "started new child process: " << pid << " (" << _id << ")";
		return pid;
	}

	void send(std::stringstream & ss) {
		if (_connected)
			_client.send(ss);
		else
			_message_queue.push_back(ss.str());
	}

	void connected(const std::error_code & ec) {
		if (!ec) {
			_connected = true;
			for (const auto & msg : _message_queue) {
				std::stringstream ss(msg);
				_client.send(ss);
			}
		} else {
			// TODO: report an error to server
			ERROR << "pipeline " << _id << " connection failed: " << ec.message();
		}
	}

	std::string _id;
	pid_t _child;
	Socket _socket;
	client_t _client;
	bool _connected = false;
	std::list<std::string> _message_queue;
};

}} // namespace vw::ms

#endif //__PIPELINE_CONTROLLER_H__
