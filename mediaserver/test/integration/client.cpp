#define BOOST_TEST_MODULE mediaserver.Client
#define BOOST_TEST_DYN_LINK
#include <unistd.h>
#include <iostream>
#include <boost/test/unit_test.hpp>
#include <boost/program_options.hpp>
#include <thread>

#include <io/client.h>
#include <rpc/call_manager.h>
#include <rpc/protocol.h>

#include <io/stream/client.h>
#include <rpc/json-rpc/protocol.h>

typedef boost::asio::io_service io_service_t;
typedef vw::rpc::Proto<vw::rpc::json::Proto> proto_t;
typedef vw::io::Client<io_service_t, vw::io::stream::TcpClient> client_t;
typedef vw::rpc::CallManager<proto_t, client_t> call_manager_t;

struct Config {
    static std::string tcp_address;
    static std::string uri;
    static size_t num_iterations;
    static size_t play_time;
    Config() {
        auto & mts = boost::unit_test::framework::master_test_suite();
        parse_command_line(mts.argc, mts.argv);
    }
    void parse_command_line(int argc, char ** argv) {
        namespace po = boost::program_options;
        po::options_description opt("Usage");
        opt.add_options()
                ("info", "print this help message and exit")
                ("tcp_address", po::value<std::string>(&tcp_address)->
                 default_value("127.0.0.1"), "set media server tcp address")
                ("uri", po::value<std::string>(&uri)->
                 default_value("test://"), "set media uri")
                ("iterations", po::value<size_t>(&num_iterations)->
                 default_value(1), "set number of iterations")
                ("play_time", po::value<size_t>(&play_time)->
                 default_value(0), "set playback time in ms");;


        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, opt), vm);
        if (vm.count("info")) {
            std::cout << opt << std::endl;
            exit(0);
        }
        try {
            po::notify(vm);
        } catch( const std::exception & e) {
            std::cerr << e.what() << std::endl;
            std::cout << opt << std::endl;
            exit(-1);
        }
    }
};

std::string Config::tcp_address;
std::string Config::uri;
size_t Config::num_iterations;
size_t Config::play_time;

BOOST_GLOBAL_FIXTURE(Config);

struct Client {
    Client() : comm_thread([this](){ boost::asio::io_service::work _(io_service); io_service.run(); }),
        client(io_service, "tcp://" + Config::tcp_address + ":7210", 3000), call_manager(client) {}
    ~Client() {
        io_service.stop();
        comm_thread.join();
    }

    void load() {
        call_manager.call<bool>("load", Config::uri);
    }

    void play() {
        call_manager.call<bool>("play");
    }

    io_service_t io_service;
    std::thread comm_thread;
    client_t client;
    call_manager_t call_manager;
};

BOOST_AUTO_TEST_CASE(connect_disconnect) {
    std::cerr << "connect_disconnect: ";
    size_t iteration = 0;
    while (iteration++ < Config::num_iterations) {
        if (iteration % 10) std::cerr << ".";
        else std::cerr << iteration;
        Client client;
    }
    std::cerr << std::endl;
}

BOOST_AUTO_TEST_CASE(load_play) {
    std::cerr << "load_play: ";
    size_t iteration = 0;
    while (iteration++ < Config::num_iterations) {
        if (iteration % 10) std::cerr << ".";
        else std::cerr << iteration;
        Client client;
        client.load();
        client.play();
        usleep(1000 * Config::play_time);
    }
    std::cerr << std::endl;
}
