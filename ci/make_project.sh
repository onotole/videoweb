#!/bin/sh

PROJECT=$1
SRC=$2

DST=BUILD/${PROJECT}-build

mkdir -p ${DST}
cd ${DST}

# Phase 1: release build
cmake -DCMAKE_BUILD_TYPE=relwithdebinfo -DVW_IMAGE_ROOT=../../shippable/buildoutput/release ../../${SRC}
make -j `nproc` all install

# Phase 2: debug build + test + coverage
rm -rf ./*
cmake -DVW_ENABLE_COVERAGE=true -DVW_IMAGE_ROOT=../../shippable/buildoutput/debug ../../${SRC}
make -j `nproc` all install
ctest -T test --output-on-failure --test-output-size-passed 1024 \
              --test-output-size-failed 1024 --timeout 5 || true
XSLT=../../ci/boosttest-1.0-to-junit-1.0.xsl.xml
CTEST_XSLT=../../ci/ctest-to-junit-2.xsl.xml
OUT=../../shippable/testresults
ORIG_OUT=../../shippable/buildoutput/debug/tmp
[ -d ${ORIG_OUT} ] || mkdir ${ORIG_OUT}
CTEST_REPORT=`find . -name "Test\.xml"`
[ -f ${CTEST_REPORT} ] && xsltproc ${CTEST_XSLT} ${CTEST_REPORT} > ${OUT}/${PROJECT}.xml || true
[ -f ${CTEST_REPORT} ] && cp ${CTEST_REPORT} ${ORIG_OUT}/${PROJECT}.xml
for REPORT in *_log.xml; do
    echo "procssing test report: ${REPORT}"
    [ -f ${REPORT} ] && cp ${REPORT} ${OUT}/${PROJECT}-${REPORT} || true
    [ -f ${REPORT} ] && cp ${REPORT} ${ORIG_OUT}/${PROJECT}-${REPORT} || true
done
