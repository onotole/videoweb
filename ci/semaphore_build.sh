#!/bin/sh

# Patch boost asio
sudo patch -N -s /usr/include/boost/asio/detail/impl/socket_ops.ipp < ci/boost_asio_enable_recv_fd.patch || true

mkdir -p shippable/testresults
mkdir -p shippable/codecoverage
uname -a >> shippable/build-info.txt
echo "repo: ${SEMAPHORE_REPO_SLUG}" >> shippable/build-info.txt
echo "build: ${SEMAPHORE_BUILD_NUMBER}" >> shippable/build-info.txt
sh -e ci/make_project.sh vwlib-base libvw/base
sh -e ci/make_project.sh vwlib-log libvw/log
sh -e ci/make_project.sh vwlib-rpc libvw/rpc
sh -e ci/make_project.sh vwlib-rpc-ws-separated libvw/rpc/impl/ws-separated
sh -e ci/make_project.sh vwlib-rpc-json-rpc libvw/rpc/impl/json-rpc
sh -e ci/make_project.sh vwlib-io libvw/io
sh -e ci/make_project.sh vwlib-io-stdio libvw/io/impl/stdio
sh -e ci/make_project.sh vwlib-io-stream libvw/io/impl/stream
sh -e ci/make_project.sh vwlib-gfx libvw/gfx
sh -e ci/make_project.sh vwlib-media libvw/media
sh -e ci/make_project.sh pipeline pipeline
sh -e ci/make_project.sh mediaserver mediaserver
sh -e ci/make_project.sh client client
sh -e ci/make_project.sh compositor compositor
sh ci/gen_coverage_report.sh
