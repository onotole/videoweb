#!/bin/sh
sudo apt-get -qq -y update
sudo apt-get -qq -y install cmake
sudo apt-get -qq -y install xsltproc
sudo apt-get -qq -y install pkg-config
sudo apt-get -qq -y install libboost-dev
sudo apt-get -qq -y install libboost-thread-dev
sudo apt-get -qq -y install libboost-system-dev
sudo apt-get -qq -y install libboost-filesystem-dev
sudo apt-get -qq -y install libboost-log-dev
sudo apt-get -qq -y install libboost-regex-dev
sudo apt-get -qq -y install libboost-test-dev
sudo apt-get -qq -y install libboost-program-options-dev
sudo apt-get -qq -y install libboost-thread-dev
sudo apt-get -qq -y install libboost-regex-dev
sudo apt-get -qq -y install libjsoncpp-dev
sudo apt-get -qq -y install libglib2.0-dev
sudo apt-get -qq -y install libgstreamer1.0-dev
sudo apt-get -qq -y install gstreamer1.0-plugins-base
sudo apt-get -qq -y install libgstreamer-plugins-base1.0-dev
sudo apt-get -qq -y install gcovr
sudo apt-get -qq -y install qtbase5-dev
sudo apt-get -qq -y install qtdeclarative5-dev
sudo apt-get -qq -y install libqt5opengl5-dev
sudo apt-get -qq -y install libgles2-mesa-dev
sudo apt-get -qq -y install libgbm-dev
sudo apt-get -qq -y install libva-dev
sudo apt-get -qq -y install libyami-dev
