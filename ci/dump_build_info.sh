#!/bin/sh
uname -a >> $1
echo "repo: ${REPOSITORY_URL}" >> $1
echo "branch: ${BRANCH}" >> $1
echo "commit: ${COMMIT}" >> $1
echo "build: ${BUILD_NUMBER}" >> $1
