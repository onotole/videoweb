#!/bin/sh
mkdir -p shippable/codecoverage
gcovr -x -k -r . -f 'libvw/.*' -e '.*/test/.*' -o shippable/codecoverage/report.xml
gcovr -k -r . -f 'libvw/.*' -e '.*/test/.*' --html --html-details -o shippable/codecoverage/report.html
cd shippable/codecoverage
tar czf ../cc.tar.gz *
